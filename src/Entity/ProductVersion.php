<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\Table(name="products_version_props")
 *
 * Defines the properties of the Product entity .
 */
class ProductVersion
{

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $productId;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private $keywords;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @Groups({"elastica", "search"})
     * @var float|null
     * @ORM\Column(type="decimal")
     */
    private $priceRet;

    /**
     * @Groups({"elastica", "search"})
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $count;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private $preview;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $versionId;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="productVersions")
     * orphanRemoval=true,
     * cascade={"persist"}
     *
     */
    private $product;

    /**
     * @var SiteVersion
     *
     * @ORM\OneToOne(targetEntity="SiteVersion")
     * @ORM\JoinColumn(name="version_id", referencedColumnName="id")
     */
    private $siteVersion;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     */
    public function setProductId(int $productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return null|string
     */
    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    /**
     * @param null|string $keywords
     */
    public function setKeywords(?string $keywords): void
    {
        $this->keywords = $keywords;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }
    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        if(isset($this->priceRet))
            return $this->priceRet;
        else
            return 0;
    }
    /**
     * @return float|null
     */
    public function getOldPrice(): ?float
    {
        if(isset($this->priceRet))
            return $this->priceRet + $this->priceRet*0.1;
        else
            return 0;
    }

    /**
     * @return float|null
     */
    public function getBonus(): ?float
    {
        if(isset($this->priceRet))
            return round($this->priceRet/100, 0, PHP_ROUND_HALF_DOWN);
        else
            return 0;
    }
    /**
     * @return float|null
     */
    public function getPriceRet(): ?float
    {
        return $this->priceRet;
    }

    /**
     * @param float|null $priceRet
     */
    public function setPriceRet(?float $priceRet): void
    {
        $this->priceRet = $priceRet;
    }

    /**
     * @return int|null
     */
    public function getCount(): ?int
    {
        if(isset($this->count))
            return $this->count;
        else
            return 0;
    }

    /**
     * @param int|null $count
     */
    public function setCount(?int $count): void
    {
        $this->count = $count;
    }

    /**
     * @return null|string
     */
    public function getPreview(): ?string
    {
        return $this->preview;
    }

    /**
     * @param null|string $preview
     */
    public function setPreview(?string $preview): void
    {
        $this->preview = $preview;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return int
     */
    public function getVersionId(): int
    {
        return $this->versionId;
    }

    /**
     * @param int $versionId
     */
    public function setVersionId(int $versionId): void
    {
        $this->versionId = $versionId;
    }

    /**
     * @return SiteVersion
     */
    public function getSiteVersion(): SiteVersion
    {
        return $this->siteVersion;
    }

    /**
     * @param SiteVersion $siteVersion
     */
    public function setSiteVersion(SiteVersion $siteVersion): void
    {
        $this->siteVersion = $siteVersion;
    }


}
