<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\Table(name="products")
 *
 * @Vich\Uploadable
 *
 * Defines the properties of the Product entity .
 */
class Product implements  MetaInterface
{
    /**
     * Use constants to define configuration options that rarely change instead
     * of specifying them in app/config/config.yml.
     *
     */
    const NUM_ITEMS = 15;

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"json"})
     */
    private $id;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="product_instruction", fileNameProperty="instruction")
     *
     * @var File
     */
    private $pdf;
    /**
     *
     * @ORM\Column(type="integer")
     */
    private $parent;

    /**
     * @Groups({"search"})
     * @var int
     * @ORM\Column(type="integer")
     */
    private $catalogId;

    /**
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $brandId;

    /**
     * @Groups({"elastica","search"})
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $article;

    /**
     *
     * @Groups({"elastica","search","json"})
     * @var string|null
     * @ORM\Column(type="text")
     *
     */
    private $name;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     *
     */
    //private $packageType;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     *
     */
    private $preview;

    /**
     * @var string|null
     * @ORM\Column(type="string", name="full_name_1c")
     */
    private $fullName1c;

    /**
     * @var string|null
     * @ORM\Column(type="string", name="name_1c")
     */
    private $name1c;

    /**
     * @Groups({"elastica", "search"})
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $url;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $text;

    /**
     * @Groups({"elastica","search"})
     * @var string|null
     * @ORM\Column(type="string",name="tags")
     */
    private $searchTags;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $guarantee;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $video;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $event;

    /**
     * @Groups({"elastica", "search"})
     * @var bool|null
     * @ORM\Column(type="smallint")
     */
    private $active;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $syncId;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $syncCode;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $timeCreate;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $timeUpdate;

    /**
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $sort;

    /**
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $valueAddedTax;

    /**
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $marketId;
    /**
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $priceRet;
    /**
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $priceRetail;
    /**
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $priceOpt;
    /**
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $priceFranchize;
    /**
     * @var int|null
     * @ORM\Column(type="integer")
     * @Groups({"json"})
     */
    private $countSpb;
    /**
     * @var int|null
     * @ORM\Column(type="integer")
     * @Groups({"json"})
     */
    private $countMsk;
    /**
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $countKirov;
    /**
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $countUfa;
    /**
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $countYaroslavl;
    /**
     * @var int|null
     * @ORM\Column(type="integer",)
     */
    private $countVolgograd;
    /**
     * @var int|null
     * @ORM\Column(type="integer",)
     */
    private $countUlyanovsk;
    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime")
     */
    private $lastParseTime;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private $composition;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $divType;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $divValue;

    /**
     * @Groups({"elastica", "search"})
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private $instruction;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $sameProductsIds;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $packageType;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private $videoUrl;

    /**
     * @var Catalog
     *
     * @ORM\ManyToOne(targetEntity="Catalog", inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     */
    private $catalog;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ProductVersion", mappedBy="product")
     */

    //private $productVersions;

    /**
     * @var integer|null
     * @ORM\Column(type="integer")
     */
    private $viewsCount;

    /**
     * @var ProductBrand
     *
     * @ORM\ManyToOne(targetEntity="ProductBrand", inversedBy="products")
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id", nullable=true)
     */
    private $productBrand;

    /**
     * @var ProductReview[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="ProductReview",
     *      mappedBy="product",
     *      orphanRemoval=true,
     *      cascade={"persist"}
     * )
     */
    private $productReviews;

    /**
     * @var ProductImage[]
     *
     * @ORM\OneToMany(
     *      targetEntity="ProductImage",
     *      mappedBy="product",
     *      orphanRemoval=true,
     *      cascade={"persist"}
     * )
     */
    private $productImages;

    /**
     *
     * @var ProductProp[]
     * @ORM\OneToMany(
     *      targetEntity="ProductProp",
     *      mappedBy="product",
     *      orphanRemoval=true,
     *      cascade={"persist"}
     * )
     */
    private $productProps;

    /**
     * Many catalogs have many tags.
     * @ORM\ManyToMany(targetEntity="Tag")
     * @ORM\JoinTable(name="products_to_tags",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     *      )
     */
    private $tags;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $article3m;

    /**
     * Product constructor.
     * @param $versionId
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->productImages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return ProductProp[]
     */
    public function getProductProps()
    {
        return $this->productProps;
    }

    /**
     * @param ProductProp[] $productProps
     */
    public function setProductProps(array $productProps): void
    {
        $this->productProps = $productProps;
    }

    /**
     * @return ProductImage[]
     */
    public function getProductImages()
    {
        return $this->productImages;
    }

    /**
     * @param ProductImage[] $productImages
     */
    public function setProductImages(array $productImages): void
    {
        $this->productImages = $productImages;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return int
     */
    public function getCatalogId(): int
    {
        return $this->catalogId;
    }

    /**
     * @param int $catalogId
     */
    public function setCatalogId(int $catalogId): void
    {
        $this->catalogId = $catalogId;
    }

    /**
     * @return int|null
     */
    public function getBrandId(): ?int
    {
        return $this->brandId;
    }

    /**
     * @param int|null $brandId
     */
    public function setBrandId(?int $brandId): void
    {
        $this->brandId = $brandId;
    }

    /**
     * @return null|string
     */
    public function getArticle(): ?string
    {
        return $this->article;
    }

    /**
     * @param null|string $article
     */
    public function setArticle(?string $article): void
    {
        $this->article = $article;
    }

    /**
     * @return null|string
     */
    public function getName(): string
    {
        return str_replace('"','',$this->name);
    }

    /**
     * @param null|string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getFullName1c(): string
    {
        return $this->fullName1c;
    }

    /**
     * @param null|string $name
     */
    public function setFullName1c(string $fullName1c): void
    {
        $this->fullName1c = $fullName1c;
    }

    /**
     * @return null|string
     */
    public function getName1c(): string
    {
        return $this->name1c;
    }

    /**
     * @param null|string $name
     */
    public function setName1c(string $name1c): void
    {
        $this->name1c = $name1c;
    }

    /**
     * @return null|string
    */
    public function getPackageType(): string
    {
        return $this->packageType;
    }

    /**
     * @param null|string $name
    */
    public function setPackageType(string $packageType): void
    {
        $this->packageType = $packageType;
    }

    /**
     * @return null|string
     */
    public function getUrl(): ?string
    {
        return $this->url.'/';
    }

    /**
     * @param null|string $url
     */
    public function setUrl(?string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return null|string
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param null|string $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return null|string
     */
    public function getSearchTags(): ?string
    {
        return $this->searchTags;
    }

    /**
     * @param null|string $searchTags
     */
    public function setSearchTags(?string $searchTags): void
    {
        $this->searchTags = $searchTags;
    }

    /**
     * @return null|string
     */
    public function getGuarantee(): ?string
    {
        return $this->guarantee;
    }

    /**
     * @param null|string $guarantee
     */
    public function setGuarantee(?string $guarantee): void
    {
        $this->guarantee = $guarantee;
    }

    /**
     * @return null|string
     */
    public function getVideo(): ?string
    {
        return $this->video;
    }

    /**
     * @param null|string $video
     */
    public function setVideo(?string $video): void
    {
        $this->video = $video;
    }

    /**
     * @return null|string
     */
    public function getEvent(): ?string
    {
        return $this->event;
    }

    /**
     * @param null|string $event
     */
    public function setEvent(?string $event): void
    {
        $this->event = $event;
    }

    /**
     * @return null|string
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param null|bool $active
     */
    public function setActive(?bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return null|string
     */
    public function getSyncId(): ?string
    {
        return $this->syncId;
    }

    /**
     * @param null|string $syncId
     */
    public function setSyncId(?string $syncId): void
    {
        $this->syncId = $syncId;
    }

    /**
     * @return null|string
     */
    public function getSyncCode(): ?string
    {
        return $this->syncCode;
    }

    /**
     * @param null|string $syncCode
     */
    public function setSyncCode(?string $syncCode): void
    {
        $this->syncCode = $syncCode;
    }

    /**
     * @return \DateTime
     */
    public function getTimeCreate(): \DateTime
    {
        return $this->timeCreate;
    }

    /**
     * @param \DateTime $timeCreate
     */
    public function setTimeCreate(\DateTime $timeCreate): void
    {
        $this->timeCreate = $timeCreate;
    }

    /**
     * @return \DateTime
     */
    public function getTimeUpdate(): \DateTime
    {
        return $this->timeUpdate;
    }

    /**
     * @param \DateTime $timeUpdate
     */
    public function setTimeUpdate(\DateTime $timeUpdate): void
    {
        $this->timeUpdate = $timeUpdate;
    }

    /**
     * @return int|null
     */
    public function getSort(): ?int
    {
        return $this->sort;
    }

    /**
     * @param int|null $sort
     */
    public function setSort(?int $sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @return int|null
     */
    public function getValueAddedTax(): ?int
    {
        return $this->valueAddedTax;
    }

    /**
     * @param int|null $sort
     */
    public function setValueAddedTax(?int $valueAddedTax): void
    {
        $this->sort = $valueAddedTax;
    }

    /**
     * @return int|null
     */
    public function getMarketId(): ?int
    {
        return $this->marketId;
    }

    /**
     * @param int|null $marketId
     */
    public function setMarketId(?int $marketId): void
    {
        $this->marketId = $marketId;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastParseTime(): ?\DateTime
    {
        return $this->lastParseTime;
    }

    /**
     * @param \DateTime|null $lastParseTime
     */
    public function setLastParseTime(?\DateTime $lastParseTime): void
    {
        $this->lastParseTime = $lastParseTime;
    }

    /**
     * @return null|string
     */
    public function getComposition(): ?string
    {
        return $this->composition;
    }

    /**
     * @param null|string $composition
     */
    public function setComposition(?string $composition): void
    {
        $this->composition = $composition;
    }

    /**
     * @return null|string
     */
    public function getDivType(): ?string
    {
        return $this->divType;
    }

    /**
     * @param null|string $divType
     */
    public function setDivType(?string $divType): void
    {
        $this->divType = $divType;
    }

    /**
     * @return null|string
     */
    public function getDivValue(): ?string
    {
        return $this->divValue;
    }

    /**
     * @param null|string $divValue
     */
    public function setDivValue(?string $divValue): void
    {
        $this->divValue = $divValue;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int|null $status
     */
    public function setStatus(?int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return null|string
     */
    public function getInstruction(): ?string
    {
        return $this->instruction;
    }

    /**
     * @param null|string $instruction
     */
    public function setInstruction(?string $instruction): void
    {
        $this->instruction = $instruction;
    }

    /**
     * @return null|string
     */
    public function getSameProductsIds(): ?string
    {
        return $this->sameProductsIds;
    }

    /**
     * @param null|string $sameProductsIds
     */
    public function setSameProductsIds(?string $sameProductsIds): void
    {
        $this->sameProductsIds = $sameProductsIds;
    }

    /**
     * @return null|string
     */
    public function getVideoUrl(): ?string
    {
        return $this->videoUrl;
    }

    /**
     * @param null|string $videoUrl
     */
    public function setVideoUrl(?string $videoUrl): void
    {
        $this->videoUrl = $videoUrl;
    }

    /**
     * @return Catalog
     */
    public function getCatalog(): Catalog
    {
        return $this->catalog;
    }

    /**
     * @param Catalog $catalog
     */
    public function setCatalog(Catalog $catalog): void
    {
        $this->catalog = $catalog;
    }

    /**
     * @return ArrayCollection|NULL

    public function getProductVersions()
    {
        return $this->productVersions;
    }
    /**
     * @return ProductVersion

    public function getProductVersion($versionId = 1)
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('versionId', $versionId));
        if($this->productVersions != NULL){
            return $this->productVersions->matching($criteria)[0];
        } else{
            return NULL;
        }
    }

    /**
     * @param ProductVersion $productVersion

    public function setProductVersion(ProductVersion $productVersion): void
    {
        $this->productVersions = $productVersion;
    }
*/
    /**
     * @return ProductBrand
     */
    public function getProductBrand(): ProductBrand
    {
        return $this->productBrand;
    }

    /**
     * @param ProductBrand $productBrand
     */
    public function setProductBrand(ProductBrand $productBrand): void
    {
        $this->productBrand = $productBrand;
    }

    /**
     * @return ProductReview[]|ArrayCollection
     */
    public function getModeratedProductReviews()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('status', 1));
        return $this->productReviews->matching($criteria);
    }
    /**
     * @return ProductReview[]|ArrayCollection
     */
    public function getProductReviews()
    {
        return $this->productReviews;
    }
    /**
     * @param ProductReview[]|ArrayCollection $productReviews
     */
    public function setProductReviews($productReviews): void
    {
        $this->productReviews = $productReviews;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags): void
    {
        $this->tags = $tags;
    }

    /**
     * @return string
     */
    public function getImage(string $size) :string
    {
        $images = $this->getProductImages();
        if($images[0] != null)
            return $images[0]->getImage($size);
        else
            return '/files/thumbs/products/s/default.png';

    }

    public function getOriginalImage()
    {
        $images = $this->getProductImages();
        $productImage = $images[0];
        if($productImage){
            return '/'.ProductImage::PATH_DIR. $productImage->getFilename();
        } else {
            return uniqid();
        }
    }

    public function getPrice(SiteVersion $siteVersion = NULL): int
    {
        if($siteVersion){
            if ($siteVersion->getIsOptPrice()){
                return (int) $this->priceOpt;
            } else {
                return (int) $this->priceRet;
            }
        }
         else {
            return (int) $this->priceRet;
         }

        /*if($this->priceRet){
            return (int) $this->priceRet;
        } else {
            return 0;
        }*/

    }

    public function getOldPrice(int $versionId = 1): int
    {
        if($this->event == 'sale'){
            return (int) $this->priceRet * 1.1;
        } else {
            return 0;
        }

    }

    public function getDiscount(int $versionId = 1): int
    {
        if($this->event == 'sale')
            return (int) $this->getOldPrice() - $this->getPrice();
        else
            return 0;
        /*if($this->event == 'sale')
            return (int) $this->getProductVersion($versionId)->getOldPrice() - $this->getProductVersion($versionId)->getPrice();
        else
            return 0;
        */
    }

    public function getCount(int $versionId = 1): int
    {
        $yaroslavIds = [ 5, 6, 7, 8, 9];
        $spbFilialIds = [ 1, 10, 11, 12, 13, 14, 15, 16 ,18 ];
        if($versionId == 2){
            $return = $this->countMsk;
        } elseif($versionId == 17) {
            $return = $this->countVolgograd;
        } elseif($versionId == 18) {
            $return = $this->countUlyanovsk;
        } elseif($versionId == 4) {
            $return = $this->countKirov;
        } elseif (in_array($versionId, $yaroslavIds)){
            $return = $this->countYaroslavl;
        } else {
            $return = $this->countSpb;
        }
        return (int) $return;
    }

    public function getBonus(int $versionId = 1): int
    {
        return (int) $this->priceRet*0.01;
        //return $this->getProductVersion($versionId)->getBonus();
    }

    public function getMetaDescription(SiteVersion $siteVersion): ?string
    {
        $productMetaTemplate = $siteVersion->getProductMetaTemplate();
        $metaDescription = str_replace(['{PRODUCT_NAME}','{BRAND_NAME}'],[$this->getName(),$this->getProductBrand()->getName()],$productMetaTemplate->getMetaDescription());
        return $metaDescription;
    }
    public function getMetaKeywords(SiteVersion $siteVersion): ?string
    {
        $productMetaTemplate = $siteVersion->getProductMetaTemplate();

        $metaKeywords = str_replace(['{PRODUCT_NAME}'],[$this->getName()],$productMetaTemplate->getMetaKeywords());
        return $metaKeywords;
    }
    public function getMetaTitle(SiteVersion $siteVersion): ?string
    {
        $productMetaTemplate = $siteVersion->getProductMetaTemplate();
        $metaTitle = str_replace('{PRODUCT_NAME}',$this->getName(),$productMetaTemplate->getMetaTitle());
        return $metaTitle;
    }

    /**
     * @param ProductImage $productImage
     */
    public function addImage(ProductImage $productImage)
    {
        if (!$this->productImages->contains($productImage)) {
            $this->productImages->add($productImage);
        }
    }

    /**
     * @return File
     */
    public function getPdf(): ?File
    {
        return $this->pdf;
    }

    /**
     * @param File $pdf
     */
    public function setPdf(?File $pdf): void
    {
        $this->pdf = $pdf;
    }

    /**
     * @return int|null
     */
    public function getPriceRet(): ?int
    {
        return (int) $this->priceRet;
    }

    /**
     * @param int|null $priceRet
     */
    public function setPriceRet(?int $priceRet): void
    {
        if($priceRet==NULL) {$priceRet = 0;}
        $this->priceRet = (int) $priceRet;
    }

    /**
     * @return int|null
     */
    public function getPriceOpt(): ?int
    {
        return (int) $this->priceOpt;
    }

    /**
     * @param int|null $priceOpt
     */
    public function setPriceOpt(?int $priceOpt): void
    {
        if($priceOpt==NULL){$priceOpt = 0;}
        $this->priceOpt = (int) $priceOpt;
    }

    /**
     * @return int|null
     */
    public function getCountSpb(): ?int
    {
        return $this->countSpb;
    }

    /**
     * @param int|null $countSpb
     */
    public function setCountSpb(?int $countSpb): void
    {
        $this->countSpb = $countSpb;
    }

    /**
     * @return int|null
     */
    public function getCountMsk(): ?int
    {
        return $this->countMsk;
    }

    /**
     * @param int|null $countMsk
     */
    public function setCountMsk(?int $countMsk): void
    {
        $this->countMsk = $countMsk;
    }

    /**
     * @return int|null
     */
    public function getCountKirov(): ?int
    {
        return $this->countKirov;
    }

    /**
     * @param int|null $countKirov
     */
    public function setCountKirov(?int $countKirov): void
    {
        $this->countKirov = $countKirov;
    }

    /**
     * @return int|null
     */
    public function getCountYaroslavl(): ?int
    {
        return $this->countYaroslavl;
    }

    /**
     * @param int|null $countYaroslavl
     */
    public function setCountYaroslavl(?int $countYaroslavl): void
    {
        $this->countYaroslavl = $countYaroslavl;
    }

    /**
     * @return int|null
     */
    public function getCountVolgograd(): ?int
    {
        return $this->countVolgograd;
    }

    /**
     * @param int|null $countVolgograd
     */
    public function setCountVolgograd(?int $countVolgograd): void
    {
        $this->countVolgograd = $countVolgograd;
    }

    /**
     * @return null|string
     */
    public function getPreview(int $versionId = 1 ): ?string
    {
        return $this->preview;
    }

    /**
     * @param null|string $preview
     */
    public function setPreview(?string $preview): void
    {
        $this->preview = $preview;
    }


    public function setCount(SiteVersion $siteVersion, int $count): void
    {
        /*$yaroslavlIds = [ 5, 6, 7, 8, 9];
        if(in_array($siteVersion->getId(), $yaroslavlIds)){
            $this->setCountYaroslavl($count);
        }
        if($siteVersion->getId() == 17){
            $this->setCountVolgograd($count);
        }*/
        $field = self::getCountField($siteVersion->getId());
        $set = 'set'.ucfirst($field);
        $this->$set($count);
    }
    public static function getCountField(int $versionId){
        $yaroslavlIds = [5, 6, 7, 8, 9];
        if(in_array($versionId, $yaroslavlIds)){
            return 'countYaroslavl';
        } elseif ($versionId == 1){
            return 'countSpb';
        } elseif ($versionId == 2){
            return 'countMsk';
        } elseif ($versionId == 17){
            return 'countVolgograd';
        } elseif ($versionId == 4){
            return 'countKirov';
        } elseif ($versionId == 19){
            return 'countUfa';
        } else {
            return 'countSpb';
        }
    }

    /**
     * @return int|null
     */
    public function getViewsCount(): ?int
    {
        return $this->viewsCount;
    }

    /**
     * @param int|null $viewsCount
     */
    public function setViewsCount(?int $viewsCount): void
    {
        $this->viewsCount = $viewsCount;
    }

    /**
     * @return int|null
     */
    public function getPriceFranchize(): ?int
    {
        return (int) $this->priceFranchize;
    }

    /**
     * @param int|null $priceFranchize
     */
    public function setPriceFranchize(?int $priceFranchize): void
    {
        $this->priceFranchize = $priceFranchize;
    }

    /**
     * @return int|null
     */
    public function getPriceRetail(): ?int
    {
        return $this->priceRetail;
    }

    /**
     * @param int|null $priceRetail
     */
    public function setPriceRetail(?int $priceRetail): void
    {
        $this->priceRetail = $priceRetail;
    }

    public function getArticle3m(): ?string
    {
        return $this->article3m;
    }

    public function setArticle3m(?string $article3m): self
    {
        $this->article3m = $article3m;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCountUlyanovsk(): ?int
    {
        return $this->countUlyanovsk;
    }

    /**
     * @param int|null $countUlyanovsk
     */
    public function setCountUlyanovsk(?int $countUlyanovsk): void
    {
        $this->countUlyanovsk = $countUlyanovsk;
    }

    /**
     * @return int|null
     */
    public function getCountUfa(): ?int
    {
        return $this->countUfa;
    }

    /**
     * @param int|null $countUfa
     */
    public function setCountUfa(?int $countUfa): void
    {
        $this->countUfa = $countUfa;
    }


}
