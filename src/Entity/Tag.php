<?php

namespace App\Entity;

use Denismitr\Translit\Translit;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="tags")
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 */
class Tag implements  MetaInterface
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"elastica", "search"})
     * @var string|NULL
     *
     * @ORM\Column(type="string", unique=true)
     */
    private $name;

    /**
     * @Groups({"elastica", "search"})
     * @var string|NULL
     *
     * @ORM\Column(type="string", unique=true)
     */
    private $url;

    /**
     * @var string|NULL
     *
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var string|NULL
     *
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @Groups({"elastica", "search"})
     * @var bool|NULL
     * @ORM\Column(type="smallint")
     */
    private $active;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $timeCreate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $timeUpdate;

    /**
     * Many tags have many products.
     * @ORM\ManyToMany(targetEntity="Product")
     * @ORM\JoinTable(name="products_to_tags")
     */
    private $products;

    /**
     * Many catalogs have many tags.
     * @ORM\ManyToMany(targetEntity="Catalog")
     * @ORM\JoinTable(name="tags_to_catalog")
     */
    private $catalogs;

    public function __construct() {

        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
        $this->catalogs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return NULL|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param NULL|string $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return NULL|string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param NULL|string $url
     */
    public function setUrl(?string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return NULL|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param NULL|string $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return NULL|string
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param NULL|string $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return bool|NULL
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool|NULL $active
     */
    public function setActive(?bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return \DateTime
     */
    public function getTimeCreate(): \DateTime
    {
        return $this->timeCreate;
    }

    /**
     * @param \DateTime $timeCreate
     */
    public function setTimeCreate(\DateTime $timeCreate): void
    {
        $this->timeCreate = $timeCreate;
    }

    /**
     * @return \DateTime
     */
    public function getTimeUpdate(): \DateTime
    {
        return $this->timeUpdate;
    }

    /**
     * @param \DateTime $timeUpdate
     */
    public function setTimeUpdate(\DateTime $timeUpdate): void
    {
        $this->timeUpdate = $timeUpdate;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products): void
    {
        $this->products = $products;
    }

    /**
     * @return mixed
     */
    public function getCatalogs()
    {
        return $this->catalogs;
    }

    /**
     * @param mixed $catalogs
     */
    public function setCatalogs($catalogs): void
    {
        $this->catalogs = $catalogs;
    }
    public function getMetaDescription(SiteVersion $siteVersion): ?string
    {
        $tagMetaTemplate = $siteVersion->getTagMetaTemplate();
        $metaDescription = str_replace(['{TAG_NAME}'],[$this->getName()],$tagMetaTemplate->getMetaDescription());
        return $metaDescription;
    }
    public function getMetaKeywords(SiteVersion $siteVersion): ?string
    {
        $tagMetaTemplate = $siteVersion->getTagMetaTemplate();
        $metaKeywords = str_replace(['{TAG_NAME}'],[$this->getName()],$tagMetaTemplate->getMetaKeywords());
        return $metaKeywords;
    }
    public function getMetaTitle(SiteVersion $siteVersion): ?string
    {
        $tagMetaTemplate = $siteVersion->getTagMetaTemplate();
        $metaTitle = str_replace('{TAG_NAME}',$this->getName(),$tagMetaTemplate->getMetaTitle());
        return $metaTitle;
    }
}
