<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 23.03.18
 * Time: 13:29
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SiteVersionRepository")
 * @ORM\Table(name="site_versions")
 */

class SiteVersion
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;


    /**
     * @var bool|null
     *
     * @ORM\Column(type="smallint")
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $key;
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $storeName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $mobileName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $mobileUrl;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $sxgeoName;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text")
     */
    private $metrika;

    /**
     * @var string|NULL
     *
     * @ORM\Column(type="text")
     */
    private $footer;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text")
     */
    private $head;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $mobilePhone;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $workTime;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $address;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $mapAddress;
    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $fb;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $vk;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text")
     */
    private $oferta;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text")
     */
    private $confidential;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text")
     */
    private $delivery;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text")
     */
    private $payment;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text")
     */
    private $about;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $footerLine1;


    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $latitude;


    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $longitude;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $footerLine2;

    /**
     * @var ProductMetaTemplate
     *
     * One SiteVersion has One ProductMetaTemplate.
     * @ORM\OneToOne(targetEntity="App\Entity\ProductMetaTemplate", mappedBy="siteVersion")
     */
    private $productMetaTemplate;

    /**
     * @var CatalogMetaTemplate
     *
     * One SiteVersion has One CatalogMetaTemplate.
     * @ORM\OneToOne(targetEntity="App\Entity\CatalogMetaTemplate", mappedBy="siteVersion")
     */
    private $catalogMetaTemplate;

    /**
     * @var BrandMetaTemplate
     *
     * One SiteVersion has One BrandMetaTemplate.
     * @ORM\OneToOne(targetEntity="App\Entity\BrandMetaTemplate", mappedBy="siteVersion")
     */
    private $brandMetaTemplate;

    /**
     * @var TagMetaTemplate
     *
     * One SiteVersion has One BrandMetaTemplate.
     * @ORM\OneToOne(targetEntity="App\Entity\TagMetaTemplate", mappedBy="siteVersion")
     */
    private $tagMetaTemplate;

    /**
     * @var ArrayCollection|PersistentCollection
     *
     * One SiteVersion has many DeliveryType.
     * @ORM\OneToMany(targetEntity="App\Entity\DeliveryType", mappedBy="siteVersion")
     */
    private $deliveryTypes;

    /**
     * @var boolean|null
     *
     * @ORM\Column(type="boolean")
     */
    private $isOptPrice;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getStoreName(): string
    {
        return $this->storeName;
    }

    /**
     * @param string $storeName
     */
    public function setStoreName(string $storeName): void
    {
        $this->storeName = $storeName;
    }

    /**
     * @return string
     */
    public function getMobileName(): string
    {
        return $this->mobileName;
    }

    /**
     * @param string $mobileName
     */
    public function setMobileName(string $mobileName): void
    {
        $this->mobileName = $mobileName;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getMobileUrl(): string
    {
        return $this->mobileUrl;
    }

    /**
     * @param string $mobileUrl
     */
    public function setMobileUrl(string $mobileUrl): void
    {
        $this->mobileUrl = $mobileUrl;
    }

    /**
     * @return string
     */
    public function getSxgeoName(): string
    {
        return $this->sxgeoName;
    }

    /**
     * @param string $sxgeoName
     */
    public function setSxgeoName(string $sxgeoName): void
    {
        $this->sxgeoName = $sxgeoName;
    }

    /**
     * @return null|string
     */
    public function getMetrika(): ?string
    {
        return $this->metrika;
    }

    /**
     * @param null|string $metrika
     */
    public function setMetrika(?string $metrika): void
    {
        $this->metrika = $metrika;
    }

    /**
     * @return NULL|string
     */
    public function getFooter(): ?string
    {
        return $this->footer;
    }

    /**
     * @param NULL|string $footer
     */
    public function setFooter(?string $footer): void
    {
        $this->footer = $footer;
    }

    /**
     * @return null|string
     */
    public function getHead(): ?string
    {
        return $this->head;
    }

    /**
     * @param null|string $head
     */
    public function setHead(?string $head): void
    {
        $this->head = $head;
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return null|string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param null|string $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return null|string
     */
    public function getMobilePhone(): ?string
    {
        return $this->mobilePhone;
    }

    /**
     * @param null|string $mobilePhone
     */
    public function setMobilePhone(?string $mobilePhone): void
    {
        $this->mobilePhone = $mobilePhone;
    }

    /**
     * @return null|string
     */
    public function getWorkTime(): ?string
    {
        return $this->workTime;
    }

    /**
     * @param null|string $workTime
     */
    public function setWorkTime(?string $workTime): void
    {
        $this->workTime = $workTime;
    }

    /**
     * @return null|string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param null|string $address
     */
    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return null|string
     */
    public function getMapAddress(): ?string
    {
        return $this->mapAddress;
    }

    /**
     * @param null|string $mapAddress
     */
    public function setMapAddress(?string $mapAddress): void
    {
        $this->mapAddress = $mapAddress;
    }

    /**
     * @return null|string
     */
    public function getFb(): ?string
    {
        return $this->fb;
    }

    /**
     * @param null|string $fb
     */
    public function setFb(?string $fb): void
    {
        $this->fb = $fb;
    }

    /**
     * @return null|string
     */
    public function getVk(): ?string
    {
        return $this->vk;
    }

    /**
     * @param null|string $vk
     */
    public function setVk(?string $vk): void
    {
        $this->vk = $vk;
    }

    /**
     * @return null|string
     */
    public function getOferta(): ?string
    {
        return $this->oferta;
    }

    /**
     * @param null|string $oferta
     */
    public function setOferta(?string $oferta): void
    {
        $this->oferta = $oferta;
    }

    /**
     * @return null|string
     */
    public function getConfidential(): ?string
    {
        return $this->confidential;
    }

    /**
     * @param null|string $confidential
     */
    public function setConfidential(?string $confidential): void
    {
        $this->confidential = $confidential;
    }

    /**
     * @return null|string
     */
    public function getDelivery(): ?string
    {
        return $this->delivery;
    }

    /**
     * @param null|string $delivery
     */
    public function setDelivery(?string $delivery): void
    {
        $this->delivery = $delivery;
    }

    /**
     * @return null|string
     */
    public function getPayment(): ?string
    {
        return $this->payment;
    }

    /**
     * @param null|string $payment
     */
    public function setPayment(?string $payment): void
    {
        $this->payment = $payment;
    }

    /**
     * @return null|string
     */
    public function getAbout(): ?string
    {
        return $this->about;
    }

    /**
     * @param null|string $about
     */
    public function setAbout(?string $about): void
    {
        $this->about = $about;
    }

    /**
     * @return null|string
     */
    public function getFooterLine1(): ?string
    {
        return $this->footerLine1;
    }

    /**
     * @param null|string $footerLine1
     */
    public function setFooterLine1(?string $footerLine1): void
    {
        $this->footerLine1 = $footerLine1;
    }

    /**
     * @return null|string
     */
    public function getFooterLine2(): ?string
    {
        return $this->footerLine2;
    }

    /**
     * @param null|string $footerLine2
     */
    public function setFooterLine2(?string $footerLine2): void
    {
        $this->footerLine2 = $footerLine2;
    }

    /**
     * @return ProductMetaTemplate
     */
    public function getProductMetaTemplate(): ProductMetaTemplate
    {
        return $this->productMetaTemplate;
    }

    /**
     * @param ProductMetaTemplate $productMetaTemplate
     */
    public function setProductMetaTemplate(ProductMetaTemplate $productMetaTemplate): void
    {
        $this->productMetaTemplate = $productMetaTemplate;
    }

    /**
     * @return CatalogMetaTemplate
     */
    public function getCatalogMetaTemplate(): CatalogMetaTemplate
    {
        return $this->catalogMetaTemplate;
    }

    /**
     * @param CatalogMetaTemplate $catalogMetaTemplate
     */
    public function setCatalogMetaTemplate(CatalogMetaTemplate $catalogMetaTemplate): void
    {
        $this->catalogMetaTemplate = $catalogMetaTemplate;
    }

    /**
     * @return BrandMetaTemplate
     */
    public function getBrandMetaTemplate(): BrandMetaTemplate
    {
        return $this->brandMetaTemplate;
    }

    /**
     * @param BrandMetaTemplate $brandMetaTemplate
     */
    public function setBrandMetaTemplate(BrandMetaTemplate $brandMetaTemplate): void
    {
        $this->brandMetaTemplate = $brandMetaTemplate;
    }

    /**
     * @return TagMetaTemplate
     */
    public function getTagMetaTemplate(): TagMetaTemplate
    {
        return $this->tagMetaTemplate;
    }

    /**
     * @param TagMetaTemplate $tagMetaTemplate
     */
    public function setTagMetaTemplate(TagMetaTemplate $tagMetaTemplate): void
    {
        $this->tagMetaTemplate = $tagMetaTemplate;
    }

    /**
     * @return ArrayCollection|PersistentCollection
     */
    public function getDeliveryTypes()
    {
        $criteria = Criteria::create()->andWhere(Criteria::expr()->eq('active', true));
        return $this->deliveryTypes->matching($criteria);
    }

    /**
     * @param ArrayCollection|PersistentCollection $deliveryTypes
     */
    public function setDeliveryTypes($deliveryTypes): void
    {
        $this->deliveryTypes = $deliveryTypes;
    }

    /**
     * @return null|string
     */
    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    /**
     * @param null|string $latitude
     */
    public function setLatitude(?string $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return null|string
     */
    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    /**
     * @param null|string $longitude
     */
    public function setLongitude(?string $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool|null $active
     */
    public function setActive(?bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return bool|null
     */
    public function getIsOptPrice(): ?bool
    {
        return $this->isOptPrice;
    }

    /**
     * @param bool|null $isOptPrice
     */
    public function setIsOptPrice(?bool $isOptPrice): void
    {
        $this->isOptPrice = $isOptPrice;
    }

}