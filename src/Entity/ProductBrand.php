<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductBrandRepository")
 * @ORM\Table(name="products_brands")
 *
 * Defines the properties of the Product Brand.
 */

class ProductBrand implements MetaInterface
{
    /**
     * Use constants to define configuration options that rarely change instead
     * of specifying them in app/config/config.yml.
     *
     */
    const NUM_ITEMS = 15;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"elastica", "search"})
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @Groups({"elastica", "search"})
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $url;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @var int|null
     * @ORM\Column(type="smallint")
     */
    private $isSale;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $syncId;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $syncCode;

    /**
     * @Groups({"elastica", "search"})
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $active;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $imageFile;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="Product",
     *      mappedBy="productBrand",
     *      orphanRemoval=true,
     *      cascade={"persist"}
     * )
     * @ORM\OrderBy({"timeUpdate": "DESC"})
     */

    private $products;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getUrl(): ?string
    {
        return $this->url.'/';
    }

    /**
     * @param null|string $url
     */
    public function setUrl(?string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return null|string
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param null|string $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return int|null
     */
    public function getIsSale(): ?bool
    {
        return $this->isSale;
    }

    /**
     * @param int|null $isSale
     */
    public function setIsSale(?int $isSale): void
    {
        $this->isSale = $isSale;
    }

    /**
     * @return string
     */
    public function getSyncId(): string
    {
        return $this->syncId;
    }

    /**
     * @param string $syncId
     */
    public function setSyncId(string $syncId): void
    {
        $this->syncId = $syncId;
    }

    /**
     * @return string
     */
    public function getSyncCode(): string
    {
        return $this->syncCode;
    }

    /**
     * @param string $syncCode
     */
    public function setSyncCode(string $syncCode): void
    {
        $this->syncCode = $syncCode;
    }

    /**
     * @return int|null
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param int|null $active
     */
    public function setActive(?int $active): void
    {
        $this->active = $active;
    }

    /**
     * @return ArrayCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param ArrayCollection $products
     */
    public function setProducts(ArrayCollection $products): void
    {
        $this->products = $products;
    }

    /**
     * @return null|string
     */
    public function getImageFile(): ?string
    {
        return $this->imageFile;
    }

    /**
     * @param null|string $imageFile
     */
    public function setImageFile(?string $imageFile): void
    {
        $this->imageFile = $imageFile;
    }

    public function getImage(string $size): ?string
    {
        if(isset($this->imageFile)){
            return '/files/thumbs/brands/'.$size.'/'.$this->imageFile;
        } else {
            return '/files/thumbs/brands/'.$size.'/'.'default.png';
        }
    }
    public function getMetaDescription(SiteVersion $siteVersion): ?string
    {
        $brandMetaTemplate = $siteVersion->getBrandMetaTemplate();
        $metaDescription = str_replace(['{BRAND_NAME}'],[$this->getName()],$brandMetaTemplate->getMetaDescription());
        return $metaDescription;
    }
    public function getMetaKeywords(SiteVersion $siteVersion): ?string
    {
        $brandMetaTemplate = $siteVersion->getBrandMetaTemplate();
        $metaKeywords = str_replace(['{BRAND_NAME}'],[$this->getName()],$brandMetaTemplate->getMetaKeywords());
        return $metaKeywords;
    }
    public function getMetaTitle(SiteVersion $siteVersion): ?string
    {
        $brandMetaTemplate = $siteVersion->getBrandMetaTemplate();
        $metaTitle = str_replace('{BRAND_NAME}',$this->getName(),$brandMetaTemplate->getMetaTitle());
        return $metaTitle;
    }
}
