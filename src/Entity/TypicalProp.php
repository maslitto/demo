<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 16.03.18
 * Time: 17:33
 */

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Hashids\Hashids;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReviewRepository")
 * @ORM\Table(name="typical_props")
 */

class TypicalProp
{

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\TypicalPropValue", mappedBy="typicalProp",cascade={"persist", "remove"})
     */
    private $typicalPropValues;

    /**
     * TypicalProp constructor.
     */
    public function __construct()
    {
        $this->typicalPropValues = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getTypicalPropValues()
    {
        return $this->typicalPropValues;
    }

    /**
     * @param ArrayCollection $typicalPropValues
     */
    public function setTypicalPropValues( $typicalPropValues): void
    {
        $this->typicalPropValues = $typicalPropValues;
    }


    public function addTypicalPropValues(TypicalPropValue $typicalPropValue): void
    {
        $this->typicalPropValues->add($typicalPropValue);
    }

    public function removeTypicalPropValues(TypicalPropValue $typicalPropValue): void
    {
        $this->typicalPropValues->remove($typicalPropValue);
    }
}