<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 16.03.18
 * Time: 17:33
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PageRepository")
 * @ORM\Table(name="pages")
 */

class Page implements MetaInterface
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $url;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $title;
    /**
     * @var string|NULL
     * @ORM\Column(type="string")
     */
    private $keywords;

    /**
     * @var string|NULL
     * @ORM\Column(type="string")
     */
    private $description;

    /**
     * @var string|NULL
     * @ORM\Column(type="text")
     */
    private $text;
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $timeCreate;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $timeUpdate;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $versionId;

    public function __construct()
    {
        $this->timeCreate = new \DateTime();
        $this->timeUpdate = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return NULL|string
     */
    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    /**
     * @param NULL|string $keywords
     */
    public function setKeywords(?string $keywords): void
    {
        $this->keywords = $keywords;
    }

    /**
     * @return NULL|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param NULL|string $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }


    /**
     * @return \DateTime
     */
    public function getTimeCreate(): \DateTime
    {
        return $this->timeCreate;
    }

    /**
     * @param \DateTime $timeCreate
     */
    public function setTimeCreate(\DateTime $timeCreate): void
    {
        $this->timeCreate = $timeCreate;
    }

    /**
     * @return \DateTime
     */
    public function getTimeUpdate(): \DateTime
    {
        return $this->timeUpdate;
    }

    /**
     * @param \DateTime $timeUpdate
     */
    public function setTimeUpdate(\DateTime $timeUpdate): void
    {
        $this->timeUpdate = $timeUpdate;
    }

    /**
     * @return int
     */
    public function getVersionId(): int
    {
        return $this->versionId;
    }

    /**
     * @param int $versionId
     */
    public function setVersionId(int $versionId): void
    {
        $this->versionId = $versionId;
    }

    /**
     * @return NULL|string
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param NULL|string $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    public function getMetaTitle(SiteVersion $siteVersion): ?string
    {
        return $this->name;
    }
    public function getMetaKeywords(SiteVersion $siteVersion): ?string
    {
        return $this->keywords;
    }

    public function getMetaDescription(SiteVersion $siteVersion): ?string
    {
        return $this->description;
    }
}