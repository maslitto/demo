<?php


namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 * @ORM\Table(name="orders")
 * @ORM\HasLifecycleCallbacks
 */

class Order
{
    const STATUS_NEW        = 1;
    const STATUS_PROCESSING = 2;
    const STATUS_COMPLETE   = 3;
    const STATUS_CANCELED   = 4;

    const STATUS_PAID       = 1;
    const STATUS_UNPAID     = 0;

    const DELIVERY_TYPE_NOT_SET       = 0;
    const DELIVERY_TYPE_RUS           = 4;
    const DELIVERY_TYPE_SPB           = 2;
    const DELIVERY_TYPE_SPB_PICKUP    = 11;
    const DELIVERY_TYPE_LEN_REG       = 3;
    const DELIVERY_TYPE_MSK           = 5;
    const DELIVERY_TYPE_MSK_REG       = 6;
    const DELIVERY_TYPE_MSK_PICKUP    = 10;
    const DELIVERY_TYPE_ULJANOVSK     = 7;
    const DELIVERY_TYPE_ULJANOVSK_REG = 8;
    const DELIVERY_TYPE_DIMITROVGRAD  = 9;

    const DELIVERY_TYPE_TATARSTAN    = 41;
    const DELIVERY_TYPE_KOMI         = 42;
    const DELIVERY_TYPE_KIROV        = 43;
    const DELIVERY_TYPE_KIROV_PICKUP = 44;

    const DELIVERY_TYPE_YAROSLAVL = 45;
    const DELIVERY_TYPE_YAROSLAVL_PICKUP = 46;
    const DELIVERY_TYPE_TVER = 48;
    const DELIVERY_TYPE_VOLOGDA = 49;
    const DELIVERY_TYPE_IVANOVO = 50;
    const DELIVERY_TYPE_KOSTROMA = 54;

    const PAYMENT_TYPE_NOT_SET  = 0;
    const PAYMENT_TYPE_CASH     = 1;
    const PAYMENT_TYPE_CASHLESS = 2;
    const PAYMENT_TYPE_CASHLESS_OFFLINE = 10;
    const PAYMENT_TYPE_BILL     = 3;
    const PAYMENT_TYPE_BONUS    = 8;

    const PAYMENT_TYPE_CASH_TITLE     = 'наличными при получении';
    const PAYMENT_TYPE_CASHLESS_TITLE = 2;
    const PAYMENT_TYPE_BILL_TITLE     = 3;

    const STORE_SPB = 1;
    const STORE_MSK = 2;

    const SOURCE_DESKTOP = 0;
    const SOURCE_MOBILE  = 1;
    //
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $encryptedId;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @var boolean|null
     *
     * @ORM\Column(type="smallint")
     */
    private $sourceType;
    /**
     * @var float|null
     *
     * @ORM\Column(type="decimal")
     */
    private $price;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $sync;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $timeCreate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $timeUpdate;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $username;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $phone;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $bonus;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isRef;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $sort;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $managerId;


    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $agreement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $agreementTime;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text")
     */
    private $address;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $zipCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $city;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $deliveryType;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $versionId;
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $crmId;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $bonusHasApplied;

    /**
     * @var Collection|OrderProduct[]
     * @ORM\OneToMany(targetEntity="OrderProduct", mappedBy="order")
     */
    private $orderProducts;

    /**
     * @var Collection|OrderPayment[]
     * @ORM\OneToMany(targetEntity="OrderPayment", mappedBy="order")
     */
    private $payments;

    /**
     * @var OrderStatus|NULL
     * @ORM\OneToOne(targetEntity="App\Entity\OrderStatus")
     * @ORM\JoinColumn(name="status", referencedColumnName="id")
     */
    private $orderStatus;

    /**
     * @var Manager|NULL
     * @ORM\OneToOne(targetEntity="App\Entity\Manager")
     * @ORM\JoinColumn(name="manager_id", referencedColumnName="id")
     */
    private $manager;

    /**
     * @var SiteVersion
     * @ORM\OneToOne(targetEntity="App\Entity\SiteVersion")
     * @ORM\JoinColumn(name="version_id", referencedColumnName="id")
     */
    private $siteVersion;

    /**
     * @var DeliveryType
     * @ORM\OneToOne(targetEntity="App\Entity\DeliveryType")
     * @ORM\JoinColumn(name="delivery_type", referencedColumnName="cms_code")
     */
    private $delivery;

    /**
     * @var ArrayCollection|PersistentCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="App\Entity\OrderSms",
     *      mappedBy="order",
     *      orphanRemoval=true,
     *      cascade={"persist"}
     * )
     */
    private $orderSmses;

    /**
     * @var int
     */
    protected $orderProductsTotal = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $promoCode;

    /**
     * @return Collection|OrderProduct[]
     */

    public function __construct()
    {
        $this->orderProducts = new ArrayCollection();
        $this->status = 0;
        $this->payments = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEncryptedId(): ?string
    {
        return $this->encryptedId;
    }

    /**
     * @param string $encryptedId
     */
    public function setEncryptedId(string $encryptedId): void
    {
        $this->encryptedId = $encryptedId;
    }

    /**
     * @return int
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(?int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getPrice(): ?int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(?int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isSync(): bool
    {
        if(isset($this->sync)) return $this->sync;
        else return false;
    }

    /**
     * @param bool $sync
     */
    public function setSync(bool $sync): void
    {
        $this->sync = $sync;
    }

    /**
     * @return \DateTime
     */
    public function getTimeCreate(): \DateTime
    {
        return $this->timeCreate;
    }

    /**
     * @param \DateTime $timeCreate
     */
    public function setTimeCreate(\DateTime $timeCreate): void
    {
        $this->timeCreate = $timeCreate;
    }

    /**
     * @return \DateTime
     */
    public function getTimeUpdate(): \DateTime
    {
        return $this->timeUpdate;
    }

    /**
     * @param \DateTime $timeUpdate
     */
    public function setTimeUpdate(\DateTime $timeUpdate): void
    {
        $this->timeUpdate = $timeUpdate;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(?string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return bool
     */
    public function isRef(): bool
    {
        if(isset($this->isRef))return $this->isRef;
        else return false;
    }

    /**
     * @param bool $isRef
     */
    public function setIsRef(bool $isRef): void
    {
        $this->isRef = $isRef;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getSort(): ?int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort(int $sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @return int
     */
    public function getManagerId(): ?int
    {
        return $this->managerId;
    }

    /**
     * @param int $managerId
     */
    public function setManagerId(?int $managerId): void
    {
        $this->managerId = $managerId;
    }

    /**
     * @return int
     */
    public function getAgreement(): ?int
    {
        return $this->agreement;
    }

    /**
     * @param int $agreement
     */
    public function setAgreement(int $agreement): void
    {
        $this->agreement = $agreement;
    }

    /**
     * @return \DateTime
     */
    public function getAgreementTime(): ?\DateTime
    {
        return $this->agreementTime;
    }

    /**
     * @param \DateTime $agreementTime
     */
    public function setAgreementTime(\DateTime $agreementTime): void
    {
        $this->agreementTime = $agreementTime;
    }

    /**
     * @return null|string
     */
    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    /**
     * @param null|string $zipCode
     */
    public function setZipCode(?string $zipCode): void
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return null|string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param null|string $address
     */
    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return int
     */
    public function getDeliveryType(): ?int
    {
        return $this->deliveryType;
    }

    /**
     * @param int $deliveryType
     */
    public function setDeliveryType(?int $deliveryType): void
    {
        $this->deliveryType = $deliveryType;
    }

    /**
     * @return int
     */
    public function getCrmId(): int
    {
        return $this->crmId;
    }

    /**
     * @param int $crmId
     */
    public function setCrmId(int $crmId): void
    {
        $this->crmId = $crmId;
    }

    /**
     * @return int
     */
    public function getVersionId(): ?int
    {
        return $this->versionId;
    }

    /**
     * @param int $versionId
     */
    public function setVersionId(int $versionId): void
    {
        $this->versionId = $versionId;
    }

    /**
     * @return int
     */
    public function getBonusHasApplied(): int
    {
        return $this->bonusHasApplied;
    }

    /**
     * @param int $bonusHasApplied
     */
    public function setBonusHasApplied(int $bonusHasApplied): void
    {
        $this->bonusHasApplied = $bonusHasApplied;
    }

    /**
     * @return int
     */
    public function getBonus(): ?int
    {
        return $this->bonus;
    }

    /**
     * @param int $bonus
     */
    public function setBonus(int $bonus): void
    {
        $this->bonus = $bonus;
    }

    /**
     * @ORM\PostPersist()
     */
    public function encryptOrderId()
    {
        if(empty($this->encryptedId)){
            $number = $this->id;
            $ahphaEncodeTable = [
                0  => 'N', 1  => 'E', 2  => 'D', 3  => 'O',
                4  => 'K', 5  => 'Z', 6  => 'M', 7  => 'R',
                8  => 'L', 9  => 'A'
            ];

            $numEncodeTable = [
                0 => 3, 1 => 6,
                2 => 9, 3 => 0,
                4 => 4, 5 => 8,
                6 => 1, 7 => 7,
                8 => 5, 9 => 2
            ];

            $numbersArr        = array_map('intval', str_split($number));
            $numbersFirstIndex = key($numbersArr);
            $numbersArrKeys    = array_keys($numbersArr);
            $numbersLastIndex  = end($numbersArrKeys);

            $resultHash = '';

            foreach ($numbersArr as $index => $number) {
                if($numbersFirstIndex === $index) {
                    $resultHash .= $ahphaEncodeTable[$number];

                    if($numbersFirstIndex === $numbersLastIndex) {
                        $resultHash .= mt_rand(0, 9);
                    }
                }
                elseif($numbersLastIndex === $index) {
                    if($numbersFirstIndex !== $numbersLastIndex) {
                        //insert after first letter in string
                        $resultHash = substr_replace($resultHash, $ahphaEncodeTable[$number], 1, 0);
                    }

                    $resultHash .= mt_rand(0, 9);
                }
                else {
                    $resultHash .= $numEncodeTable[$number];
                }
            }

            $this->encryptedId = $resultHash;
        }
    }
    /**
     * {@inheritdoc}
     */
    public function getOrderProducts(): Collection
    {
        return $this->orderProducts;
    }

    /**
     * @param OrderProduct[]|Collection $orderProducts
     */
    public function setOrderProducts($orderProducts): void
    {
        $this->orderProducts = $orderProducts;
    }

    /**
     * @return OrderStatus
     */
    public function getOrderStatus(): ?OrderStatus
    {
        return $this->orderStatus;
    }

    /**
     * @param OrderStatus $orderStatus
     */
    public function setOrderStatus(?OrderStatus $orderStatus): void
    {
        $this->orderStatus = $orderStatus;
    }

    /**
     * @param int $orderProductsTotal
     */
    public function setOrderProductsTotal(int $orderProductsTotal): void
    {
        $this->orderProductsTotal = $orderProductsTotal;
    }

    /**
     * {@inheritdoc}
     */
    public function clearOrderProducts(): void
    {
        $this->orderProducts->clear();
    }
    /**
     * {@inheritdoc}
     */
    public function countOrderProducts(): int
    {
        return $this->orderProducts->count();
    }
    /**
     * {@inheritdoc}
     */
    public function addOrderProduct(OrderProduct $orderProduct): void
    {
        if ($this->hasOrderProduct($orderProduct)) {
            return;
        }
        $this->orderProductsTotal += $orderProduct->getCount();
        $this->orderProducts->add($orderProduct);
        $orderProduct->setOrder($this);
    }

    /**
     * {@inheritdoc}
     */
    public function removeOrderProduct(OrderProduct $orderProduct): void
    {
        if ($this->hasOrderProduct($orderProduct)) {
            $this->orderProducts->removeElement($orderProduct);
            $this->orderProductsTotal -= $orderProduct->getCount();
            $orderProduct->setOrder(null);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasOrderProduct(OrderProduct $orderProduct): bool
    {
        return $this->orderProducts->contains($orderProduct);
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty(): bool
    {
        return $this->orderProducts->isEmpty();
    }

    /**
     * @return string
     */
    public function getFullAddress(): string
    {
        return $this->zipCode.', '.$this->city.', '.$this->address;
    }

    /**
     * @return OrderPayment[]|Collection
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    /**
     * @param OrderPayment[]|Collection $payments
     */
    public function setPayments($payments): void
    {
        $this->payments = $payments;
    }

    /**
     * @return OrderPayment|NULL
     */

    public function getOriginPayment(): OrderPayment
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('isOrigin', true));
        return $this->payments->matching($criteria)[0] ? $this->payments->matching($criteria)[0] : NULL;
    }
    /**
     * return bool
     */
    public function hasPayment(OrderPayment $payment): bool
    {
        return $this->payments->contains($payment);
    }

    /**
     * @param OrderPayment $payment
     */
    public function addPayment(OrderPayment $payment): void
    {
        if ($this->hasPayment($payment)) {
            return;
        }
        $this->payments->add($payment);
        $payment->setOrder($this);
    }

    /**
     * @param OrderPayment $payment
     */
    public function removePayment(OrderPayment $payment): void
    {
        if ($this->hasPayment($payment)) {
            $this->payments->removeElement($payment);
            $payment->setOrder(null);
        }
    }
    public function getPaymentTitle() : string
    {
        $originPayment = $this->getOriginPayment()->getType()->getId();
        switch ($originPayment){
            case self::PAYMENT_TYPE_BILL:
                $return = 'Договор с покупателем';
                break;
            case self::PAYMENT_TYPE_CASHLESS:
                $return = 'Договор с покупателем (БК)';
                break;
            case self::PAYMENT_TYPE_CASHLESS_OFFLINE:
                $return = 'Договор с покупателем (БК)';
                break;
            default:
                $return = 'Договор за наличный расчет по заказам';
                break;
        }
        return $return;

    }
    public function isSelfPickup() : bool
    {
        if(in_array($this->deliveryType,[
            self::DELIVERY_TYPE_MSK_PICKUP,
            self::DELIVERY_TYPE_SPB_PICKUP,
            self::DELIVERY_TYPE_KIROV_PICKUP
        ]))
            return true;
        else
            return false;
    }

    /**
     * @return bool|null
     */
    public function getSourceType(): ?bool
    {
        return $this->sourceType;
    }

    /**
     * @param bool|null $sourceType
     */
    public function setSourceType(?bool $sourceType): void
    {
        $this->sourceType = $sourceType;
    }

    /**
     * @return Manager
     */
    public function getManager(): ?Manager
    {
        return $this->manager;
    }

    /**
     * @param Manager $manager
     */
    public function setManager(?Manager $manager): void
    {
        $this->manager = $manager;
    }

    /**
     * @return SiteVersion
     */
    public function getSiteVersion(): SiteVersion
    {
        return $this->siteVersion;
    }

    /**
     * @param SiteVersion $siteVersion
     */
    public function setSiteVersion(SiteVersion $siteVersion): void
    {
        $this->siteVersion = $siteVersion;
    }

    /**
     * @return DeliveryType
     */
    public function getDelivery(): DeliveryType
    {
        return $this->delivery;
    }

    /**
     * @param DeliveryType $delivery
     */
    public function setDelivery(DeliveryType $delivery): void
    {
        $this->delivery = $delivery;
    }

    /**
     * @return ArrayCollection|PersistentCollection
     */
    public function getOrderSmses()
    {
        return $this->orderSmses;
    }

    /**
     * @param ArrayCollection|PersistentCollection $orderSmses
     */
    public function setOrderSmses($orderSmses): void
    {
        $this->orderSmses = $orderSmses;
    }

    public function getPromoCode(): ?string
    {
        return $this->promoCode;
    }

    public function setPromoCode(?string $promoCode): self
    {
        $this->promoCode = $promoCode;

        return $this;
    }




}
