<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserFeedRepository")
 * @ORM\Table(name="users_feed")
 *
 */
class UserFeed
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer")
     */
    private $news;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer")
     */
    private $events;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer")
     */
    private $sales;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer")
     */
    private $newProducts;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $userId;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User",inversedBy="userFeed")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getNews(): ?int
    {
        return $this->news;
    }

    /**
     * @param int|null $news
     */
    public function setNews(?int $news): void
    {
        $this->news = $news;
    }

    /**
     * @return int|null
     */
    public function getEvents(): ?int
    {
        return $this->events;
    }

    /**
     * @param int|null $events
     */
    public function setEvents(?int $events): void
    {
        $this->events = $events;
    }

    /**
     * @return int|null
     */
    public function getSales(): ?int
    {
        return $this->sales;
    }

    /**
     * @param int|null $sales
     */
    public function setSales(?int $sales): void
    {
        $this->sales = $sales;
    }

    /**
     * @return int|null
     */
    public function getNewProducts(): ?int
    {
        return $this->newProducts;
    }

    /**
     * @param int|null $newProducts
     */
    public function setNewProducts(?int $newProducts): void
    {
        $this->newProducts = $newProducts;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

}