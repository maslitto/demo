<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="slider")

 */
class Slider
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $url;

    /**
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $sort;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $header;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $btnText;

    /**
     * @var boolean|null
     * @ORM\Column(type="smallint")
     */
    private $active;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $backgroundColor;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private $backgroundStyle;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private $btnInlineJs;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private $btnInlineCss;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $image;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $mobileImage;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $buttonClass;

    /**
     * @var array|null
     * @ORM\Column(type="json")
     */
    private $versions;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param null|string $url
     */
    public function setUrl(?string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return int|null
     */
    public function getSort(): ?int
    {
        return $this->sort;
    }

    /**
     * @param int|null $sort
     */
    public function setSort(?int $sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @return null|string
     */
    public function getHeader(): ?string
    {
        return $this->header;
    }

    /**
     * @param null|string $header
     */
    public function setHeader(?string $header): void
    {
        $this->header = $header;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return null|string
     */
    public function getBtnText(): ?string
    {
        return $this->btnText;
    }

    /**
     * @param null|string $btnText
     */
    public function setBtnText(?string $btnText): void
    {
        $this->btnText = $btnText;
    }

    /**
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool|null $active
     */
    public function setActive(?bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return null|string
     */
    public function getBackgroundColor(): ?string
    {
        return $this->backgroundColor;
    }

    /**
     * @param null|string $backgroundColor
     */
    public function setBackgroundColor(?string $backgroundColor): void
    {
        $this->backgroundColor = $backgroundColor;
    }

    /**
     * @return null|string
     */
    public function getBackgroundStyle(): ?string
    {
        return $this->backgroundStyle;
    }

    /**
     * @param null|string $backgroundStyle
     */
    public function setBackgroundStyle(?string $backgroundStyle): void
    {
        $this->backgroundStyle = $backgroundStyle;
    }

    /**
     * @return null|string
     */
    public function getBtnInlineJs(): ?string
    {
        return $this->btnInlineJs;
    }

    /**
     * @param null|string $btnInlineJs
     */
    public function setBtnInlineJs(?string $btnInlineJs): void
    {
        $this->btnInlineJs = $btnInlineJs;
    }

    /**
     * @return null|string
     */
    public function getBtnInlineCss(): ?string
    {
        return $this->btnInlineCss
;
    }

    /**
     * @param null|string $btnInlineCss
     */
    public function setBtnInlineCss(?string $btnInlineCss): void
    {
        $this->btnInlineCss = $btnInlineCss;
    }

    /**
     * @return null|string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param null|string $image
     */
    public function setImage(?string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return null|string
     */
    public function getButtonClass(): ?string
    {
        return $this->buttonClass;
    }

    /**
     * @param null|string $buttonClass
     */
    public function setButtonClass(?string $buttonClass): void
    {
        $this->buttonClass = $buttonClass;
    }

    /**
     * @param string|NULL $size
     * @return string
     */
    public function getPicture(string $size = NULL):string
    {
        if(strpos($this->image, 'ckfinder')){
            return $this->image;
        }
        if($size){
            return '/files/thumbs/slider/'.$size.'/'.$this->image;
        } else{
            return '/files/thumbs/slider/'.$this->image;
        }
    }

    public function getBg(string $size = NULL):string
    {
        if($size){
            return '/files/thumbs/slider/'.$size.'/'.$this->image;
        } else{
            return '/files/thumbs/slider/'.$this->image;
        }
    }

    /**
     * @return null|string
     */
    public function getMobileImage(): ?string
    {
        return $this->mobileImage;
    }

    /**
     * @param null|string $mobileImage
     */
    public function setMobileImage(?string $mobileImage): void
    {
        $this->mobileImage = $mobileImage;
    }

    /**
     * @return array|null
     */
    public function getVersions(): ?array
    {
        return $this->versions;
    }

    /**
     * @param array|null $versions
     */
    public function setVersions(?array $versions): void
    {
        $this->versions = $versions;
    }

}
