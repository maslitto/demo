<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 15.03.18
 * Time: 17:28
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductViewRepository")
 * @ORM\Table(name="products_views")
 *
 * Defines product views.
 */
class ProductView
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    //private $versionId;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $productId;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $viewsCount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $timeCreate;
    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="productViews")
     * orphanRemoval=true,
     * cascade={"persist"}
     *
     */
    private $product;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getVersionId(): int
    {
        return $this->versionId;
    }

    /**
     * @param int $versionId
     */
    public function setVersionId(int $versionId): void
    {
        $this->versionId = $versionId;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     */
    public function setProductId(int $productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return int
     */
    public function getViewsCount(): int
    {
        return $this->viewsCount;
    }

    /**
     * @param int $viewsCount
     */
    public function setViewsCount(int $viewsCount): void
    {
        $this->viewsCount = $viewsCount;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return \DateTime
     */
    public function getTimeCreate(): \DateTime
    {
        return $this->timeCreate;
    }

    /**
     * @param \DateTime $timeCreate
     */
    public function setTimeCreate(\DateTime $timeCreate): void
    {
        $this->timeCreate = $timeCreate;
    }

}