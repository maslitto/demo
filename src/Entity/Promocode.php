<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 16.03.18
 * Time: 17:33
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PromocodeRepository")
 * @ORM\Table(name="promocodes")
 */

class Promocode

{

    const TYPE_PERCENTS = 'percents';
    const TYPE_ROUBLES = 'roubles';
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $code;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer")
     */
    private $discount;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @var ArrayCollection|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Cart", mappedBy="promocode")
     */
    private $carts;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ProductBrand")
     */
    private $brands;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product")
     */
    private $products;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expiry;

    /**
     * @var bool
     * @ORM\Column(type="integer", nullable=true)
     */
    private $disposable;

    /**
     * @var bool
     * @ORM\Column(type="integer", nullable=true)
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $groupName;

    public function __construct()
    {
        $this->brands = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->expiry = new \DateTime();
    }

    public function isExpired()
    {
        if(new \DateTime() > $this->expiry) return true;
        else return false;
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param null|string $code
     */
    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return int|null
     */
    public function getDiscount(): ?int
    {
        return $this->discount;
    }

    /**
     * @param int|null $discount
     */
    public function setDiscount(?int $discount): void
    {
        $this->discount = $discount;
    }

    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param null|string $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return ArrayCollection|PersistentCollection
     */
    public function getCarts()
    {
        return $this->carts;
    }

    /**
     * @param ArrayCollection|PersistentCollection $carts
     */
    public function setCarts($carts): void
    {
        $this->carts = $carts;
    }

    /**
     * @return Collection|ProductBrand[]
     */
    public function getBrands(): Collection
    {
        return $this->brands;
    }

    public function addBrand(ProductBrand $brand): self
    {
        if (!$this->brands->contains($brand)) {
            $this->brands[] = $brand;
        }

        return $this;
    }

    public function removeBrand(ProductBrand $brand): self
    {
        if ($this->brands->contains($brand)) {
            $this->brands->removeElement($brand);
        }

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }
        return $this;
    }

    public function getExpiry(): ?\DateTimeInterface
    {
        return $this->expiry;
    }

    public function setExpiry(?\DateTimeInterface $expiry): self
    {
        $this->expiry = $expiry;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisposable(): bool
    {
        return (bool) $this->disposable;
    }

    /**
     * @param bool $disposable
     */
    public function setDisposable(bool $disposable): void
    {
        $this->disposable = $disposable;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return (bool) $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function getGroupName(): ?string
    {
        return $this->groupName;
    }

    public function setGroupName(?string $groupName): self
    {
        $this->groupName = $groupName;

        return $this;
    }

}
