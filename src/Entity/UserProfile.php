<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserProfileRepository")
 * @ORM\Table(name="users_data")
 *
 */
class UserProfile
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $surname;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $patronymic;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $country;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $city;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer")
     */
    protected $bonus;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer")
     */
    protected $priceType;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer")
     */
    protected $personType;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $skype;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $comName;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $comInn;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $comKpp;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $comOgrn;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $comOkved;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $comUridAddress;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $comFactAddress;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $comPhone;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $comRc;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $comKc;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $comBik;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $userId;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User",inversedBy="userProfile")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param null|string $surname
     */
    public function setSurname(?string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return null|string
     */
    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    /**
     * @param null|string $patronymic
     */
    public function setPatronymic(?string $patronymic): void
    {
        $this->patronymic = $patronymic;
    }

    /**
     * @return null|string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param null|string $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return null|string
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param null|string $country
     */
    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return null|string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param null|string $city
     */
    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return int|null
     */
    public function getBonus(): ?int
    {
        return $this->bonus;
    }

    /**
     * @param int|null $bonus
     */
    public function setBonus(?int $bonus): void
    {
        $this->bonus = $bonus;
    }

    /**
     * @return int|null
     */
    public function getPriceType(): ?int
    {
        return $this->priceType;
    }

    /**
     * @param int|null $priceType
     */
    public function setPriceType(?int $priceType): void
    {
        $this->priceType = $priceType;
    }

    /**
     * @return null|string
     */
    public function getSkype(): ?string
    {
        return $this->skype;
    }

    /**
     * @param null|string $skype
     */
    public function setSkype(?string $skype): void
    {
        $this->skype = $skype;
    }

    /**
     * @return int|null
     */
    public function getPersonType(): ?int
    {
        return $this->personType;
    }

    /**
     * @param int|null $personType
     */
    public function setPersonType(?int $personType): void
    {
        $this->personType = $personType;
    }

    /**
     * @return null|string
     */
    public function getComName(): ?string
    {
        return $this->comName;
    }

    /**
     * @param null|string $comName
     */
    public function setComName(?string $comName): void
    {
        $this->comName = $comName;
    }

    /**
     * @return null|string
     */
    public function getComInn(): ?string
    {
        return $this->comInn;
    }

    /**
     * @param null|string $comInn
     */
    public function setComInn(?string $comInn): void
    {
        $this->comInn = $comInn;
    }

    /**
     * @return null|string
     */
    public function getComKpp(): ?string
    {
        return $this->comKpp;
    }

    /**
     * @param null|string $comKpp
     */
    public function setComKpp(?string $comKpp): void
    {
        $this->comKpp = $comKpp;
    }

    /**
     * @return null|string
     */
    public function getComOgrn(): ?string
    {
        return $this->comOgrn;
    }

    /**
     * @param null|string $comOgrn
     */
    public function setComOgrn(?string $comOgrn): void
    {
        $this->comOgrn = $comOgrn;
    }

    /**
     * @return null|string
     */
    public function getComOkved(): ?string
    {
        return $this->comOkved;
    }

    /**
     * @param null|string $comOkved
     */
    public function setComOkved(?string $comOkved): void
    {
        $this->comOkved = $comOkved;
    }

    /**
     * @return null|string
     */
    public function getComUridAddress(): ?string
    {
        return $this->comUridAddress;
    }

    /**
     * @param null|string $comUridAddress
     */
    public function setComUridAddress(?string $comUridAddress): void
    {
        $this->comUridAddress = $comUridAddress;
    }

    /**
     * @return null|string
     */
    public function getComFactAddress(): ?string
    {
        return $this->comFactAddress;
    }

    /**
     * @param null|string $comFactAddress
     */
    public function setComFactAddress(?string $comFactAddress): void
    {
        $this->comFactAddress = $comFactAddress;
    }

    /**
     * @return null|string
     */
    public function getComPhone(): ?string
    {
        return $this->comPhone;
    }

    /**
     * @param null|string $comPhone
     */
    public function setComPhone(?string $comPhone): void
    {
        $this->comPhone = $comPhone;
    }

    /**
     * @return null|string
     */
    public function getComRc(): ?string
    {
        return $this->comRc;
    }

    /**
     * @param null|string $comRc
     */
    public function setComRc(?string $comRc): void
    {
        $this->comRc = $comRc;
    }

    /**
     * @return null|string
     */
    public function getComKc(): ?string
    {
        return $this->comKc;
    }

    /**
     * @param null|string $comKc
     */
    public function setComKc(?string $comKc): void
    {
        $this->comKc = $comKc;
    }

    /**
     * @return null|string
     */
    public function getComBik(): ?string
    {
        return $this->comBik;
    }

    /**
     * @param null|string $comBik
     */
    public function setComBik(?string $comBik): void
    {
        $this->comBik = $comBik;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }


}