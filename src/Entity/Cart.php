<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 16.03.18
 * Time: 17:33
 */

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Hashids\Hashids;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartRepository")
 * @ORM\Table(name="carts")
 * @ORM\HasLifecycleCallbacks
 */

class Cart
{

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string",name="hash_id")
     */
    private $hashId;

    /**
     * @var string|NULL
     * @ORM\Column(type="string")
     */
    private $userAgent;
    /**
     * @var string|NULL
     * @ORM\Column(type="string")
     */
    private $ip;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $finished;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $timeCreate;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $timeUpdate;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $versionId;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $siteDeviceVersion;

    /**
     * @var bool
     * @ORM\Column(type="smallint")
     */
    private $mailSended;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="CartProduct", mappedBy="cart", cascade={"persist"})
     */
    private $cartProducts;

    /**
     * @var SiteVersion|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\SiteVersion")
     * @ORM\JoinColumn(name="version_id", referencedColumnName="id")
     */
    private $siteVersion;
    /**
     * @var Promocode|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Promocode", inversedBy="cart")
     * @ORM\JoinColumn(name="promocode_id", referencedColumnName="id", nullable=true)
     */
    private $promocode;

    public function __construct()
    {
        $this->cartProducts = new ArrayCollection();
        $this->timeCreate = new \DateTime();
        $this->timeUpdate = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getHashId(): string
    {
        return $this->hashId;
    }

    public function setHashId($hash = NULL): void
    {
        $this->hashId = $hash;
    }

    /**
     * @return string
     */
    public function getUserAgent(): ?string
    {
        return $this->userAgent;
    }

    /**
     * @param string $userAgent
     */
    public function setUserAgent(?string $userAgent): void
    {
        $this->userAgent = $userAgent;
    }


    /**
     * @return NULL|string
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @param NULL|string $ip
     */
    public function setIp(?string $ip): void
    {
        $this->ip = $ip;
    }

    /**
     * @return int
     */
    public function getFinished(): int
    {
        return $this->finished;
    }

    /**
     * @param int $finished
     */
    public function setFinished(int $finished): void
    {
        $this->finished = $finished;
    }

    /**
     * @return \DateTime
     */
    public function getTimeCreate(): \DateTime
    {
        return $this->timeCreate;
    }

    /**
     * @param \DateTime $timeCreate
     */
    public function setTimeCreate(\DateTime $timeCreate): void
    {
        $this->timeCreate = $timeCreate;
    }

    /**
     * @return \DateTime
     */
    public function getTimeUpdate(): \DateTime
    {
        return $this->timeUpdate;
    }

    /**
     * @param \DateTime $timeUpdate
     */
    public function setTimeUpdate(\DateTime $timeUpdate): void
    {
        $this->timeUpdate = $timeUpdate;
    }

    /**
     * @return int
     */
    public function getVersionId(): int
    {
        return $this->versionId;
    }

    /**
     * @param int $versionId
     */
    public function setVersionId(int $versionId): void
    {
        $this->versionId = $versionId;
    }

    /**
     * @return int
     */
    public function getSiteDeviceVersion(): int
    {
        return $this->siteDeviceVersion;
    }

    /**
     * @param int $siteDeviceVersion
     */
    public function setSiteDeviceVersion(int $siteDeviceVersion): void
    {
        $this->siteDeviceVersion = $siteDeviceVersion;
    }

    /**
     * @return bool
     */
    public function isMailSended(): bool
    {
        return $this->mailSended;
    }

    /**
     * @param bool $mailSended
     */
    public function setMailSended(bool $mailSended): void
    {
        $this->mailSended = $mailSended;
    }

    /**
     * @return Collection
     */
    public function getCartProducts(): Collection
    {
        return $this->cartProducts;
    }

    /**
     * @param ArrayCollection $cartProducts
     */
    public function setCartProducts(ArrayCollection $cartProducts): void
    {
        $this->cartProducts = $cartProducts;
    }

    public function addCartProduct(CartProduct $cartProduct): void
    {
        if ($this->hasCartProduct($cartProduct)) {
            return;
        }
        $this->cartProducts->add($cartProduct);
        $cartProduct->setCart($this);
    }

    /**
     * {@inheritdoc}
     */
    public function removeCartProduct(CartProduct $cartProduct): void
    {
        if ($this->hasCartProduct($cartProduct)) {
            $this->cartProducts->removeElement($cartProduct);
            $cartProduct->setCart(null);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasCartProduct(CartProduct $cartProduct): bool
    {
        return $this->cartProducts->contains($cartProduct);
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty(): bool
    {
        return $this->cartProducts->isEmpty();
    }

    /**
     * @return SiteVersion|null
     */
    public function getSiteVersion(): ?SiteVersion
    {
        return $this->siteVersion;
    }

    /**
     * @param SiteVersion|null $siteVersion
     */
    public function setSiteVersion(?SiteVersion $siteVersion): void
    {
        $this->siteVersion = $siteVersion;
    }

    /**
     * @return float|int
     */
    public function getPrice()
    {
        $price = 0;
        foreach ($this->cartProducts as $cartProduct){
            $price += $cartProduct->getPrice() * $cartProduct->getCount();
        }
        return $price;
    }

    /**
     * @return Promocode|null
     */
    public function getPromocode(): ?Promocode
    {
        return $this->promocode;
    }

    /**
     * @param Promocode|null $promocode
     */
    public function setPromocode(?Promocode $promocode): void
    {
        $this->promocode = $promocode;
    }

    /**
     * @return bool
     */
    public function hasPromocode()
    {
        if (!$this->getPromocode()){
            return false;
        } else {
            return true;
        }
    }
}