<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 16.03.18
 * Time: 17:33
 */

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Hashids\Hashids;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReviewRepository")
 * @ORM\Table(name="typical_props_vals")
 */

class TypicalPropValue
{

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string",name="k")
     */
    private $key;

    /**
     * @var string
     *
     * @ORM\Column(type="string",name="v")
     */
    private $value;

    /**
     * @var TypicalProp
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TypicalProp",inversedBy="typicalPropValues",cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="typical_prop_id", referencedColumnName="id")
     */
    private $typicalProp;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getKey(): ?string
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    /**
     * @return TypicalProp
     */
    public function getTypicalProp(): TypicalProp
    {
        return $this->typicalProp;
    }

    /**
     * @param TypicalProp $typicalProp
     */
    public function setTypicalProp(TypicalProp $typicalProp): void
    {
        $this->typicalProp = $typicalProp;
    }


}