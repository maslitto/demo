<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 16.03.18
 * Time: 17:33
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DeliveryTypeRepository")
 * @ORM\Table(name="delivery_types")
 */

class DeliveryType
{
    /**
     * @var int
     *
     *
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Id
     * @var int
     * @ORM\Column(type="integer")
     */
    private $cmsCode;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $crmCode;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @var string
     * @ORM\Column(type="string",name="create_time")
     */
    private $timeCreate;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $price;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $title;
    /**
     * @var bool|null
     * @ORM\Column(type="integer")
     */
    private $isDelivery;
    /**
     * @var SiteVersion
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\SiteVersion", inversedBy="deliveryTypes")
     * @ORM\JoinColumn(name="version_id", referencedColumnName="id", nullable=true)
     */
    private $siteVersion;
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCmsCode(): string
    {
        return $this->cmsCode;
    }

    /**
     * @param string $cmsCode
     */
    public function setCmsCode(string $cmsCode): void
    {
        $this->cmsCode = $cmsCode;
    }

    /**
     * @return string
     */
    public function getCrmCode(): string
    {
        return $this->crmCode;
    }

    /**
     * @param string $crmCode
     */
    public function setCrmCode(string $crmCode): void
    {
        $this->crmCode = $crmCode;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getTimeCreate(): string
    {
        return $this->timeCreate;
    }

    /**
     * @param string $timeCreate
     */
    public function setTimeCreate(string $timeCreate): void
    {
        $this->timeCreate = $timeCreate;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return SiteVersion
     */
    public function getSiteVersion(): SiteVersion
    {
        return $this->siteVersion;
    }

    /**
     * @param SiteVersion $siteVersion
     */
    public function setSiteVersion(SiteVersion $siteVersion): void
    {
        $this->siteVersion = $siteVersion;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice(string $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return bool|null
     */
    public function getisDelivery(): ?bool
    {
        return $this->isDelivery;
    }

    /**
     * @param bool|null $isDelivery
     */
    public function setIsDelivery(?bool $isDelivery): void
    {
        $this->isDelivery = $isDelivery;
    }


}