<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 14.03.18
 * Time: 17:08
 */

namespace App\Entity;


interface MetaInterface
{
    /**
     * @return null|string
     */
    public function getMetaTitle(SiteVersion $siteVersion): ?string ;

    /**
     * @return null|string
     */
    public function getMetaDescription(SiteVersion $siteVersion): ?string ;

    /**
     * @return null|string
     */
    public function getMetaKeywords(SiteVersion $siteVersion): ?string ;


}