<?php

namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(name="catalog")
 * use repository for handy tree functions
 * @ORM\Entity(repositoryClass="App\Repository\CatalogRepository")
 */
class Catalog implements MetaInterface
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;
    /**
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $parentId;
    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="Catalog")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Catalog", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Catalog", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;


    /**
     * @Groups({"elastica", "search"})
     * @ORM\Column(length=255)
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @Groups({"elastica", "search"})
     * @ORM\Column(length=255)
     * @ORM\Column(type="string")
     */
    private $url;

    /**
     * @Groups({"elastica", "search"})
     * @ORM\Column(length=255)
     * @ORM\Column(type="string")
     */
    private $urlPath;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @Groups({"elastica", "search"})
     * @var boolean
     * @ORM\Column(type="smallint")
     */
    private $active;

    /**
     * @ORM\Column(type="string")
     */
    private $syncId;

    /**
     * @ORM\Column(type="string")
     */
    private $syncCode;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timeCreate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timeUpdate;

    /**
     * @ORM\Column(type="smallint")
     */
    private $showBrand;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="Product",
     *      mappedBy="catalog",
     *      orphanRemoval=true,
     *      cascade={"persist"}
     * )
     * @ORM\OrderBy({"timeUpdate": "DESC"})
     */
    private $products;

    /**
     * @Groups({"search"})
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CatalogVersion", mappedBy="catalog")
     */

    private $catalogVersions;

    /**
     * @var ArrayCollection
     * Many catalogs have many tags.
     * @ORM\ManyToMany(targetEntity="Tag")
     * @ORM\JoinTable(name="tags_to_catalog",
     *      joinColumns={@ORM\JoinColumn(name="catalog_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     *      )
     */
    private $tags;


    /**
     * Catalog constructor.
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * @param mixed $lft
     */
    public function setLft($lft): void
    {
        $this->lft = $lft;
    }

    /**
     * @return mixed
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * @param mixed $lvl
     */
    public function setLvl($lvl): void
    {
        $this->lvl = $lvl;
    }

    /**
     * @return mixed
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * @param mixed $rgt
     */
    public function setRgt($rgt): void
    {
        $this->rgt = $rgt;
    }

    /**
     * @return mixed
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @param mixed $root
     */
    public function setRoot($root): void
    {
        $this->root = $root;
    }

    /**
     * @return null|Catalog
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children): void
    {
        $this->children = $children;
    }

    /**
     * @return mixed
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getUrlPath()
    {
        return $this->urlPath.'/';
    }

    /**
     * @param mixed $urlPath
     */
    public function setUrlPath($urlPath): void
    {
        $this->urlPath = $urlPath;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text): void
    {
        $this->text = $text;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return (bool)$this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active): void
    {
        $this->active = (bool) $active;
    }


    /**
     * @return mixed
     */
    public function getSyncId()
    {
        return $this->syncId;
    }

    /**
     * @param mixed $syncId
     */
    public function setSyncId($syncId): void
    {
        $this->syncId = $syncId;
    }

    /**
     * @return mixed
     */
    public function getSyncCode()
    {
        return $this->syncCode;
    }

    /**
     * @param mixed $syncCode
     */
    public function setSyncCode($syncCode): void
    {
        $this->syncCode = $syncCode;
    }

    /**
     * @return mixed
     */
    public function getTimeCreate()
    {
        return $this->timeCreate;
    }

    /**
     * @param mixed $timeCreate
     */
    public function setTimeCreate($timeCreate): void
    {
        $this->timeCreate = $timeCreate;
    }

    /**
     * @return mixed
     */
    public function getTimeUpdate()
    {
        return $this->timeUpdate;
    }

    /**
     * @param mixed $timeUpdate
     */
    public function setTimeUpdate($timeUpdate): void
    {
        $this->timeUpdate = $timeUpdate;
    }

    /**
     * @return mixed
     */
    public function getShowBrand()
    {
        return $this->showBrand;
    }

    /**
     * @param mixed $showBrand
     */
    public function setShowBrand($showBrand): void
    {
        $this->showBrand = $showBrand;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products): void
    {
        $this->products = $products;
    }


    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    /**
     * @param int|null $parentId
     */
    public function setParentId(?int $parentId): void
    {
        $this->parentId = $parentId;
    }

    /**
     * @return CatalogVersion
     */
    public function getCatalogVersion(int $versionId = 1) : CatalogVersion
    {
        // TODO убрать заглушку
        $versionId = 1;
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('versionId', $versionId));
        return $this->catalogVersions->matching($criteria)[0];
    }

    /**
     * @return ArrayCollection
     */
    public function getCatalogVersions()
    {
        return $this->catalogVersions;
    }

    /**
     * @param ArrayCollection $catalogVersions
     */
    public function setCatalogVersions(ArrayCollection $catalogVersions): void
    {
        $this->catalogVersions = $catalogVersions;
    }


    /**
     * @return ArrayCollection
     */
    public function getActiveChildren($lvl = NULL)
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('active', true));
        if(isset($lvl)){
            $criteria->andWhere(Criteria::expr()->eq('lvl', $lvl));
        }
        return $this->children->matching($criteria);
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @return bool
     */
    public function hasChildren(): bool
    {
        if(count($this->getActiveChildren()) > 0 ){
            return true;
        } else{
            return false;
        }
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags): void
    {
        $this->tags = $tags;
    }
    public function getMetaDescription(SiteVersion $siteVersion): ?string
    {
        $productMetaTemplate = $siteVersion->getCatalogMetaTemplate();
        /*$productVersion = $this->getCatalogVersion($siteVersion->getId());
        if($productVersion->getDescription() && $siteVersion->getKey()=='spb'){
            return $productVersion->getDescription();
        } else {
            */
        $metaDescription = str_replace(['{CATALOG_NAME}'],[$this->getName()],$productMetaTemplate->getMetaDescription());
        return $metaDescription;
        //}
    }
    public function getMetaKeywords(SiteVersion $siteVersion): ?string
    {
        $productMetaTemplate = $siteVersion->getCatalogMetaTemplate();
        /*$productVersion = $this->getCatalogVersion($siteVersion->getId());
        if($productVersion->getKeywords() && $siteVersion->getKey()=='spb'){
            return $productVersion->getKeywords();
        } else {*/
        $metaKeywords = str_replace(['{CATALOG_NAME}'],[$this->getName()],$productMetaTemplate->getMetaKeywords());
        return $metaKeywords;
        //}
    }
    public function getMetaTitle(SiteVersion $siteVersion): ?string
    {
        $productMetaTemplate = $siteVersion->getCatalogMetaTemplate();
        /*$productVersion = $this->getCatalogVersion($siteVersion->getId());
        if($productVersion->getTitle() && $siteVersion->getKey()=='spb'){
            return $productVersion->getTitle();
        } else {*/
        $metaTitle = str_replace('{CATALOG_NAME}',$this->getName() ,$productMetaTemplate->getMetaTitle());
        return $metaTitle;
        //}
    }

    public function getActive()
    {
        return (bool) $this->active;
    }

}
