<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 02.03.18
 * Time: 11:18
 */

namespace App\Entity;


interface OrderPaymentInterface
{
    public function getOrder(): ?OrderInterface;
    public function getAmount(): float ;
    public function isStatus(): bool;
    public function getType(): OrderPaymentType;
    public function setOrder(OrderInterface $order) :void;
    public function isOrigin() : bool;
    public function getBankPayUrl() : ?string;
    public function setStatus(bool $status) : void;
}