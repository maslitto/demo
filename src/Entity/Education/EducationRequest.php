<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 16.03.18
 * Time: 17:33
 */

namespace App\Entity\Education;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Hashids\Hashids;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Education\EducationRequestRepository")
 * @ORM\Table(name="education_requests")
 */

class EducationRequest
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|NULL
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string|NULL
     * @ORM\Column(type="string")
     */
    private $phone;

    /**
     * @var string|NULL
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @var string|NULL
     * @ORM\Column(type="string")
     */
    private $city;

    /**
     * @var bool|null
     * @ORM\Column(type="integer")
     */
    private $subscribe;
    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $educationId;

    /**
     * @var Education
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Education\Education")
     * @ORM\JoinColumn(name="education_id", referencedColumnName="id")
     */
    private $education;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return NULL|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param NULL|string $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return NULL|string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param NULL|string $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return NULL|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param NULL|string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return NULL|string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @return bool|null
     */
    public function getSubscribe(): ?bool
    {
        return $this->subscribe;
    }

    /**
     * @param bool|null $subscribe
     */
    public function setSubscribe(?bool $subscribe): void
    {
        $this->subscribe = $subscribe;
    }

    /**
     * @param NULL|string $city
     */
    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return int
     */
    public function getEducationId(): int
    {
        return $this->educationId;
    }

    /**
     * @param int $educationId
     */
    public function setEducationId(int $educationId): void
    {
        $this->educationId = $educationId;
    }

    /**
     * @return Education
     */
    public function getEducation(): Education
    {
        return $this->education;
    }

    /**
     * @param Education $education
     */
    public function setEducation(Education $education): void
    {
        $this->education = $education;
    }

}