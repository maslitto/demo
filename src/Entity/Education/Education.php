<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 16.03.18
 * Time: 17:33
 */

namespace App\Entity\Education;


use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Education\EducationRepository")
 * @ORM\Table(name="educations")
 */

class Education
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $educationTypeId;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $lectorFio;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $profession;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $phone;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $city;
    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $address;
    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $isSingleDay;
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $dateStart;
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $dateEnd;
    /**
     * @var string|NUll
     * @ORM\Column(type="string")
     */
    private $time;
    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $isFree;
    /**
     * @var integer|null
     * @ORM\Column(type="integer")
     */
    private $price;
    /**
     * @var integer|null
     * @ORM\Column(type="integer")
     */
    private $oldPrice;
    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private $program;
    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $photo;
    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private $relativePosts;
    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private $relativeProducts;

    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $userId;

    /**
     * @var integer|null
     * @ORM\Column(type="integer")
     */
    private $educationDirectionId;

    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $showOnBanner;

    /**
     * @var EducationStatus
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Education\EducationStatus")
     * @ORM\JoinColumn(name="education_status_id", referencedColumnName="id")
     */
    private $educationStatus;

    /**
     * @var EducationType
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Education\EducationType")
     * @ORM\JoinColumn(name="education_type_id", referencedColumnName="id")
     */
    private $educationType;

    /**
     * @var EducationDirection|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Education\EducationDirection")
     * @ORM\JoinColumn(name="education_direction_id", referencedColumnName="id")
     */
    private $educationDirection;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getEducationTypeId(): int
    {
        return $this->educationTypeId;
    }

    /**
     * @param int $educationTypeId
     */
    public function setEducationTypeId(int $educationTypeId): void
    {
        $this->educationTypeId = $educationTypeId;
    }

    /**
     * @return null|string
     */
    public function getLectorFio(): ?string
    {
        return $this->lectorFio;
    }

    /**
     * @param null|string $lectorFio
     */
    public function setLectorFio(?string $lectorFio): void
    {
        $this->lectorFio = $lectorFio;
    }

    /**
     * @return null|string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param null|string $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param null|string $city
     */
    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return null|string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param null|string $address
     */
    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return bool|null
     */
    public function getisSingleDay(): ?bool
    {
        return $this->isSingleDay;
    }

    /**
     * @param bool|null $isSingleDay
     */
    public function setIsSingleDay(?bool $isSingleDay): void
    {
        $this->isSingleDay = $isSingleDay;
    }

    /**
     * @return \DateTime
     */
    public function getDateStart(): \DateTime
    {
        return $this->dateStart;
    }

    /**
     * @param \DateTime $dateStart
     */
    public function setDateStart(\DateTime $dateStart): void
    {
        $this->dateStart = $dateStart;
    }

    /**
     * @return \DateTime
     */
    public function getDateEnd(): \DateTime
    {
        return $this->dateEnd;
    }

    /**
     * @param \DateTime $dateEnd
     */
    public function setDateEnd(\DateTime $dateEnd): void
    {
        $this->dateEnd = $dateEnd;
    }

    /**
     * @return NUll|string
     */
    public function getTime(): ?string
    {
        return $this->time;
    }

    /**
     * @param NUll|string $time
     */
    public function setTime(?string $time): void
    {
        $this->time = $time;
    }

    /**
     * @return bool|null
     */
    public function getisFree(): ?bool
    {
        return $this->isFree;
    }

    /**
     * @param bool|null $isFree
     */
    public function setIsFree(?bool $isFree): void
    {
        $this->isFree = $isFree;
    }

    /**
     * @return int|null
     */
    public function getPrice(): ?int
    {
        return $this->price;
    }

    /**
     * @param int|null $price
     */
    public function setPrice(?int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return null|string
     */
    public function getProgram(): ?string
    {
        return $this->program;
    }

    /**
     * @param null|string $program
     */
    public function setProgram(?string $program): void
    {
        $this->program = $program;
    }

    /**
     * @return null|string
     */
    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    /**
     * @param null|string $photo
     */
    public function setPhoto(?string $photo): void
    {
        $this->photo = $photo;
    }

    /**
     * @return null|string
     */
    public function getRelativePosts(): ?string
    {
        return $this->relativePosts;
    }

    /**
     * @param null|string $relativePosts
     */
    public function setRelativePosts(?string $relativePosts): void
    {
        $this->relativePosts = $relativePosts;
    }

    /**
     * @return null|string
     */
    public function getRelativeProducts(): ?string
    {
        return $this->relativeProducts;
    }

    /**
     * @param null|string $relativeProducts
     */
    public function setRelativeProducts(?string $relativeProducts): void
    {
        $this->relativeProducts = $relativeProducts;
    }

    /**
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool|null $active
     */
    public function setActive(?bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return bool|null
     */
    public function getShowOnBanner(): ?bool
    {
        return $this->showOnBanner;
    }

    /**
     * @param bool|null $showOnBanner
     */
    public function setShowOnBanner(?bool $showOnBanner): void
    {
        $this->showOnBanner = $showOnBanner;
    }

    /**
     * @return EducationStatus
     */
    public function getEducationStatus(): EducationStatus
    {
        return $this->educationStatus;
    }

    /**
     * @param EducationStatus $educationStatus
     */
    public function setEducationStatus(EducationStatus $educationStatus): void
    {
        $this->educationStatus = $educationStatus;
    }

    /**
     * @return EducationType
     */
    public function getEducationType(): EducationType
    {
        return $this->educationType;
    }

    /**
     * @param EducationType $educationType
     */
    public function setEducationType(EducationType $educationType): void
    {
        $this->educationType = $educationType;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return null|string
     */
    public function getProfession(): ?string
    {
        return $this->profession;
    }

    /**
     * @param null|string $profession
     */
    public function setProfession(?string $profession): void
    {
        $this->profession = $profession;
    }

    /**
     * @return EducationDirection|null
     */
    public function getEducationDirection(): ?EducationDirection
    {
        return $this->educationDirection;
    }

    /**
     * @param EducationDirection|null $educationDirection
     */
    public function setEducationDirection(?EducationDirection $educationDirection): void
    {
        $this->educationDirection = $educationDirection;
    }

    /**
     * @return int|null
     */
    public function getEducationDirectionId(): ?int
    {
        return $this->educationDirectionId;
    }

    /**
     * @param int|null $educationDirectionId
     */
    public function setEducationDirectionId(?int $educationDirectionId): void
    {
        $this->educationDirectionId = $educationDirectionId;
    }

    /**
     * @return int|null
     */
    public function getOldPrice(): ?int
    {
        return $this->oldPrice;
    }

    /**
     * @param int|null $oldPrice
     */
    public function setOldPrice(?int $oldPrice): void
    {
        $this->oldPrice = $oldPrice;
    }

}