<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 16.03.18
 * Time: 17:33
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ManagerRepository")
 * @ORM\Table(name="managers")
 */

class Manager
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $lastName;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @var integer|null
     * @ORM\Column(type="integer")
     */
    private $versionId;

    /**
     * @var integer|null
     * @ORM\Column(type="integer")
     */
    private $lastOrder;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $timeCreate;

    /**
     * @var boolean|null
     * @ORM\Column(type="smallint")
     */
    private $active;


    /**
     * @var SiteVersion|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\SiteVersion")
     * @ORM\JoinColumn(name="version_id", referencedColumnName="id")
     */
    private $siteVersion;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param null|string $lastName
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return int|null
     */
    public function getVersionId(): ?int
    {
        return $this->versionId;
    }

    /**
     * @param int|null $versionId
     */
    public function setVersionId(?int $versionId): void
    {
        $this->versionId = $versionId;
    }

    /**
     * @return SiteVersion|null
     */
    public function getSiteVersion(): ?SiteVersion
    {
        return $this->siteVersion;
    }

    /**
     * @param SiteVersion|null $siteVersion
     */
    public function setSiteVersion(?SiteVersion $siteVersion): void
    {
        $this->siteVersion = $siteVersion;
    }

    /**
     * @return \DateTime
     */
    public function getTimeCreate(): \DateTime
    {
        return $this->timeCreate;
    }

    /**
     * @param \DateTime $timeCreate
     */
    public function setTimeCreate(\DateTime $timeCreate): void
    {
        $this->timeCreate = $timeCreate;
    }

    /**
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool|null $active
     */
    public function setActive(?bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return null|string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param null|string $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return int|null
     */
    public function getLastOrder(): ?int
    {
        return $this->lastOrder;
    }

    /**
     * @param int|null $lastOrder
     */
    public function setLastOrder(?int $lastOrder): void
    {
        $this->lastOrder = $lastOrder;
    }

}