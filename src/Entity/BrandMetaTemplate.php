<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BrandMetaTemplateRepository")
 * @ORM\Table(name="brand_meta_templates")
 *
 */

class BrandMetaTemplate
{
    const NUM_ITEMS = 25;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $metaTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $metaKeywords;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $metaDescription;

    /**
     * @var SiteVersion
     *
     * @ORM\OneToOne(targetEntity="App\Entity\SiteVersion", inversedBy="brandMetaTemplate")
     * @ORM\JoinColumn(name="version_id", referencedColumnName="id")
     */
    private $siteVersion;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    /**
     * @param null|string $metaTitle
     */
    public function setMetaTitle(?string $metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @return null|string
     */
    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    /**
     * @param null|string $metaKeywords
     */
    public function setMetaKeywords(?string $metaKeywords): void
    {
        $this->metaKeywords = $metaKeywords;
    }

    /**
     * @return null|string
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param null|string $metaDescription
     */
    public function setMetaDescription(?string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return SiteVersion
     */
    public function getSiteVersion(): SiteVersion
    {
        return $this->siteVersion;
    }

    /**
     * @param SiteVersion $siteVersion
     */
    public function setSiteVersion(SiteVersion $siteVersion): void
    {
        $this->siteVersion = $siteVersion;
    }



}
