<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 16.03.18
 * Time: 17:33
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubscribeRepository")
 * @ORM\Table(name="subscribe")
 */

class Subscribe
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $confirmKey;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $confirmed;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getConfirmKey(): string
    {
        return $this->confirmKey;
    }

    /**
     * @param string $confirmKey
     */
    public function setConfirmKey(string $confirmKey): void
    {
        $this->confirmKey = $confirmKey;
    }

    /**
     * @return bool
     */
    public function isConfirmed(): bool
    {
        return $this->confirmed;
    }

    /**
     * @param bool $confirmed
     */
    public function setConfirmed(bool $confirmed): void
    {
        $this->confirmed = $confirmed;
    }

}