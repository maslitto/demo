<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="orders_payments")
 */

class OrderPayment
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal")
     */
    private $amount;


    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $timeCreate;

    /**
     * @var string|NULL
     *
     * @ORM\Column(type="text")
     */
    private $bankPayUrl;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $bankId;

    /**
     * @var bool
     *
     * @ORM\Column(type="smallint")
     */
    public $isOrigin;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="payments")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @var boolean
     *
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @var OrderPaymentType
     *
     * @ORM\OneToOne(targetEntity="App\Entity\OrderPaymentType")
     * @ORM\JoinColumn(name="type", referencedColumnName="id")
     */
    private $type;
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return \DateTime
     */
    public function getTimeCreate(): \DateTime
    {
        return $this->timeCreate;
    }

    /**
     * @param \DateTime $timeCreate
     */
    public function setTimeCreate(\DateTime $timeCreate): void
    {
        $this->timeCreate = $timeCreate;
    }

    /**
     * @return NULL|string
     */
    public function getBankPayUrl(): ?string
    {
        return $this->bankPayUrl;
    }

    /**
     * @param NULL|string $bankPayUrl
     */
    public function setBankPayUrl(?string $bankPayUrl): void
    {
        $this->bankPayUrl = $bankPayUrl;
    }

    /**
     * @return string
     */
    public function getBankId(): string
    {
        return $this->bankId;
    }

    /**
     * @param string $bankId
     */
    public function setBankId(string $bankId): void
    {
        $this->bankId = $bankId;
    }

    /**
     * @return bool
     */
    public function isOrigin(): bool
    {
        return $this->isOrigin;
    }

    /**
     * @param bool $isOrigin
     */
    public function setIsOrigin(bool $isOrigin): void
    {
        $this->isOrigin = $isOrigin;
    }

    /**
     * @return Order
     */
    public function getOrder(): ?Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(?Order $order): void
    {
        $currentOrder = $this->getOrder();
        if ($currentOrder === $order) {
            return;
        }
        $this->order = null;
        if (null !== $currentOrder) {
            $currentOrder->removePayment($this);
        }
        if (null === $order) {
            return;
        }
        $this->order = $order;
        if (!$order->hasPayment($this)) {
            $order->addPayment($this);
        }
    }

    /**
     * @return OrderPaymentType
     */
    public function getType(): OrderPaymentType
    {
        return $this->type;
    }

    /**
     * @param OrderPaymentType $type
     */
    public function setType(OrderPaymentType $type): void
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }


}
