<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Command;

use App\Entity\Product;
use App\Entity\User;
use App\Repository\ProductRepository;
use App\Services\ConfigService;
use App\Services\ImageService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;


class RegenerateThumbnailsCommand extends Command
{
    // to make your command lazily loaded, configure the $defaultName static property,
    // so it will be instantiated only when the command is actually called.
    protected static $defaultName = 'app:regenerate-thumbnails';

    /**
     * @var SymfonyStyle
     */
    private $io;

    private $entityManager;
    private $configService;
    private $imageService;
    private $productRepository;
    private $publicPath;

    public function __construct(string $publicPath,EntityManagerInterface $em, ProductRepository $productRepository,ConfigService $configService, ImageService $imageService)
    {
        parent::__construct();

        $this->entityManager = $em;
        $this->configService = $configService;
        $this->imageService = $imageService;
        $this->productRepository = $productRepository;
        $this->publicPath = $publicPath;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Lists all the existing users')
            ->setHelp(<<<'HELP'
The <info>%command.name%</info> command lists all the users registered in the application:

  <info>php %command.full_name%</info>

By default the command only displays the 50 most recent users. Set the number of
results to display with the <comment>--max-results</comment> option:

  <info>php %command.full_name%</info> <comment>--max-results=2000</comment>

In addition to displaying the user list, you can also send this information to
the email address specified in the <comment>--send-to</comment> option:

  <info>php %command.full_name%</info> <comment>--send-to=fabien@symfony.com</comment>

HELP
            )
            // commands can optionally define arguments and/or options (mandatory and optional)
            // see https://symfony.com/doc/current/components/console/console_arguments.html
            ->addOption('max-results', null, InputOption::VALUE_OPTIONAL, 'Limits the number of users listed', 50)
            ->addOption('send-to', null, InputOption::VALUE_OPTIONAL, 'If set, the result is sent to the given email address')
        ;
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Product[] $products */
        $products = $this->productRepository->getActiveProducts();
        $config = $this->configService->getConstants();
        $sizes = $config['images']['product'];

        foreach ($products as $product){
            if(file_exists($this->publicPath.$product->getOriginalImage())){
                $this->imageService->createThumbnails($this->publicPath.$product->getOriginalImage(),$sizes);
                $output->write('id='.$product->getId().' success!'."\n");
            } else {
                $output->write('id='.$product->getId().' error!'."\n");
            }
        }
        // instead of just displaying the table of users, store its contents in a variable


    }


}
