<?php

namespace App\EventSubscriber;

use App\Entity\Comment;
use App\Entity\Order;
use App\Events;
use App\Services\VersionService;
use App\Utils\Sms;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Services\CartService;
/**
 * Notifies customer about new order.
 *
 */
class OrderNotificationSubscriber implements EventSubscriberInterface
{
    private $mailer;
    private $cartService;
    private $sender;
    private $templating;
    private $sms;
    private $versionService;

    public function __construct(
        \Swift_Mailer $mailer,
        CartService $cartService,
        string $sender,
        \Twig_Environment $templating,
        Sms $sms,
        VersionService $versionService
    )
    {
        $this->mailer = $mailer;
        $this->cartService = $cartService;
        $this->sender = $sender;
        $this->templating = $templating;
        $this->sms = $sms;
        $this->versionService = $versionService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            Events::ORDER_CREATED => 'onOrderCreated',
        ];
    }

    public function onOrderCreated(GenericEvent $event): void
    {
        /** @var Order $order */
        $order = $event->getSubject();
        $this->sendUserNotification($order);
        $this->sendManagerNotification($order);

    }

    private function sendUserNotification(Order $order)
    {
        // Symfony uses a library called SwiftMailer to send emails. That's why
        // email messages are created instantiating a Swift_Message class.
        // See https://symfony.com/doc/current/email.html#sending-emails
        $subject = 'Новый заказ №'.$order->getEncryptedId();
        if(!empty($order->getEmail())){
            $message = (new \Swift_Message())
                ->setSubject($subject)
                ->setTo($order->getEmail())
                ->setFrom($this->sender)
                ->setBody($this->templating->render('email/new_order.html.twig', [
                    'order' => $order,
                    'year' => date('Y'),
                    'server' => $_SERVER['HTTP_HOST'],
                ]), 'text/html');

            // In app/config/config_dev.yml the 'disable_delivery' option is set to 'true'.
            // That's why in the development environment you won't actually receive any email.
            // However, you can inspect the contents of those unsent emails using the debug toolbar.
            // See https://symfony.com/doc/current/email/dev_environment.html#viewing-from-the-web-debug-toolbar
            $this->mailer->send($message);
        }

        $message = "Интернет-магазин \"Стоммаркет\". Наш менеджер скоро с вами свяжется. Номер вашего заказа ".$order->getEncryptedId().", сумма ".$order->getPrice()." р.";
        $this->sms->send($order->getPhone(), $message);
    }

    private function sendManagerNotification(Order $order)
    {
        // Symfony uses a library called SwiftMailer to send emails. That's why
        // email messages are created instantiating a Swift_Message class.
        // See https://symfony.com/doc/current/email.html#sending-emails
        $subject = 'Новый заказ №'.$order->getEncryptedId();
        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setTo($order->getSiteVersion()->getEmail())
            ->setFrom($this->sender)
            ->setBody($this->templating->render('email/new_order_manager.html.twig', [
                'order' => $order,
                'server' => $_SERVER['HTTP_HOST'],
            ]), 'text/html');

        $this->mailer->send($message);
        $message = "Интернет-магазин \"Стоммаркет\". Новый заказ на сайте.";
        if($this->versionService->getSiteVersion()->getMobilePhone()){
            $this->sms->send($this->versionService->getSiteVersion()->getMobilePhone(), $message);
        }

    }
}
