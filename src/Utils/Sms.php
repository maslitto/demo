<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 15.06.18
 * Time: 11:31
 */

namespace App\Utils;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
class Sms
{
    const LOGIN    = 'pk137583';
    const PASSWORD = '288876';
    const URL      = 'http://api.prostor-sms.ru/messages/v2/send/?';

    public function send(string $phone, string $message)
    {
        $client = new Client();
        $response = $client->request('GET', self::URL,[
            'auth' => [self::LOGIN, self::PASSWORD],
            'query' => [
                'phone' =>$this->prepareNumberForSms($phone),
                'text' => $message,
            ]
        ]);

        return $response;
    }
    private function prepareNumberForSms($phone) {
        $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);
        $len = strlen($phone);
        if ($len === 11 && $phone{0} === '8') {
            $phone = '+7' . substr($phone, 1);
        } else if (strpos($phone, '+') === false && $len === 9) {
            $phone = '+7' . $phone;
        }

        return $phone;
    }
}