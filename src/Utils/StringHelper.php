<?php
namespace App\Utils;

class StringHelper
{
    /**
     * поддержка Multibyte String
     */
    protected $mbString = false;

    /**
     * Кодировка текста
     */
    protected $encoding = 'UTF-8';

    public function randomString($lenght = 15)
    {
        return substr(md5(rand()), 0, $lenght);
    }

    /**
     * Обрезка строк по словам
     */
    public function cutByWords($text, $maxLength = 150, $needEllipsis = true, array $allowTags = array('<br>'))
    {
        if (empty($text)) {
            return '';
        }

        if (!empty($allowTags)) {
            $allowTags = implode('', $allowTags);
            $text      = strip_tags($text, $allowTags);
        }

        $text = str_replace(array('&nbsp;', '  '), '', $text);
        if (empty($text)) {
            return '';
        }

        $text = trim($text, '<br />');
        $text = trim($text, ' ');
        $text = trim($text, '<br />');

        $this->mbString= (function_exists('mb_strlen')) ? true : false;
        $strlen = ($this->mbString) ? mb_strlen($text, $this->encoding) : strlen($text);

        if ($maxLength > 0 && $maxLength < $strlen) {
            $substr = ($this->mbString) ? mb_substr($text, $maxLength, $strlen, $this->encoding)
                : substr($text, $maxLength);

            $pos = ($this->mbString) ? mb_strpos($substr, ' ', 0, $this->encoding) : strpos($substr, ' ');

            if ($pos !== false) {
                $text = ($this->mbString) ? mb_substr($text, 0, ($maxLength + $pos), $this->encoding)
                    : substr($text, 0, ($maxLength + $pos));
            }

            if ($needEllipsis) {
                $text .= '...';
            }
        }

        return $text;
    }

    public function textPrepare($text) {
        $text = iconv('utf-8', 'windows-1251', $text);

        $counter = 0;
        $result = '<p>';

        for($i=0; $i <= strlen($text)-1 ; $i++ ) {
            $result .= $text[$i];

            if($counter > 250 && ($text[$i] . $text[$i + 1] == '. ')) {
                $tmp1 = iconv('windows-1251', 'utf-8', $text[$i - 2] . $text[$i - 1] . $text[$i]);
                $tmp2 = iconv('windows-1251', 'utf-8', $text[$i - 3]) . $tmp1;
                $tmp3 = iconv('windows-1251', 'utf-8', $text[$i + 2] . $text[$i + 3] . $text[$i + 4] . $text[$i + 5]);

                if(!in_array($tmp1, array(' л.', ' ч.', '.д.', '.ч.')) && !in_array($tmp2, array(' мл.', ' ок.')) && !in_array($tmp3, array('Для ', 'Это '))) {
                    $result .= '</p><p>';
                    $counter = 0;
                }
            }
            $counter++;
        }
        $result .= '</p>';

        return iconv('windows-1251', 'utf-8', $result);
    }

    public function escapeXml($string)
    {
        $string  = strip_tags($string);

        $search  = array('&laquo;', '&apos;', '&nbsp;', '&copy;', '&raquo;', '&ndash;', '&mdash;', '&', '>', '<', '—');
        $replace = array('&#171;', '&#146;', '&#160;', '&#169;', '&#187;', '&#8211;', '&#8212;', '&amp;', '&gt;', '&lt;', '-');
        $string  = str_replace($search, $replace, $string);

        $string  = trim($string);

        return $string;
    }

    public function escapeQuotes($string)
    {
        return addslashes(str_replace('"', "'", $string));
    }

    public function removeEmptyLines($string)
    {
        return preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $string);
    }
    /**
     * Convert a snake-cased string to a camel-cased one.
     *
     * @param   string  $snakeString  The original snake-cased string.
     * @param   bool    $first        Whether the first letter should be capitalized
     *                                (useful for classnames vs. properties).
     * @return  string                The converted string.
     */
    public function snake2Camel($snakeString, $first = false)
    {
        $camel = implode('', array_map(function($piece) {
            return empty($piece) ? '_' : ucfirst(strtolower($piece));
        }, explode('_', $snakeString)));
        return $first ? $camel : lcfirst($camel);
    }
}