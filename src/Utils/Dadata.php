<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 27.04.18
 * Time: 13:06
 */

namespace App\Utils;


class Dadata
{
    const API_KEY = '0f7e1c73aef9b778e8f4feaa0c8b926fb1503043';

    public function suggest($type, $fields)
    {
        $result = false;
        if ($ch = curl_init("http://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/$type"))
        {
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: Token '.self::API_KEY
            ));
            curl_setopt($ch, CURLOPT_POST, 1);
            // json_encode
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            $result = json_decode($result, true);
            curl_close($ch);
        }
        return $result;
    }
}
