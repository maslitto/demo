<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 07.05.18
 * Time: 11:45
 */

namespace App\Utils;


class Morpher
{
    public function morph(string $word, string $case)
    {
        $response = json_decode(file_get_contents('https://ws3.morpher.ru/russian/declension?s='.$word.'&format=json'));
        return $response->$case;
    }
}