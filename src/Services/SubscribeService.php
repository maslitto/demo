<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 06.06.18
 * Time: 13:52
 */

namespace App\Services;


use App\Entity\Subscribe;
use App\Repository\SubscribeRepository;
use Doctrine\ORM\EntityManagerInterface;

class SubscribeService
{
    private $entityManager;
    private $subscribeRepository;
    private $mailer;
    private $sender;
    private $templating;

    public function __construct(
        EntityManagerInterface $entityManager,
        SubscribeRepository $subscribeRepository,
        \Swift_Mailer $mailer,
        \Twig_Environment $templating,
        string $sender
    )
    {
        $this->entityManager = $entityManager;
        $this->subscribeRepository = $subscribeRepository;
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->sender = $sender;
    }

    public function addSubscribe($email)
    {
        $subscribe = new Subscribe();
        $subscribe->setEmail($email);
        $subscribe->setConfirmKey(md5($email.mt_rand()));
        $subscribe->setConfirmed(false);

        $this->entityManager->persist($subscribe);
        $this->entityManager->flush();

        return $subscribe;
    }

    public function confirmSubscribe(string $confirmKey)
    {
        if($this->hasSubscribeByField('confirmKey', $confirmKey)) {

            $subscribe = $this->subscribeRepository->findOneBy(['confirmKey' => $confirmKey]);
            $subscribe->setConfirmed(true);

            $this->entityManager->persist($subscribe);
            $this->entityManager->flush();

            return true;
        }
        else {
            return false;
        }
    }

    public function sendConfirmEmail(Subscribe $subscribe)
    {
        $message = (new \Swift_Message('Подтверждение подписки'))
            ->setFrom($this->sender)
            ->setTo($subscribe->getEmail())
            ->setBody(
                $this->templating->render(
                    'email/confirm_subscribe.html.twig',
                    [
                        'link' => 'https://'.$_SERVER['HTTP_HOST'].'/subscribe/confirm/'.$subscribe->getConfirmKey()
                    ]
                ),
                'text/html'
        );
        $this->mailer->send($message);

    }

    public function hasSubscribeByField(string $field, $value)
    {
        if($subscribe = $this->subscribeRepository->findOneBy([$field => $value])){
            return true;
        } else {
            return false;
        }
    }
}