<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 21.03.18
 * Time: 10:47
 */

namespace App\Services;


use App\Entity\Cart;
use App\Entity\DeliveryType;
use App\Entity\Manager;
use App\Entity\Order;
use App\Entity\OrderPayment;
use App\Entity\OrderProduct;
use App\Entity\OrderStatus;
use App\Entity\Product;
use App\Entity\Promocode;
use App\Repository\DeliveryTypeRepository;
use App\Repository\ManagerRepository;
use App\Repository\OrderPaymentRepository;
use App\Repository\OrderPaymentTypeRepository;
use App\Repository\OrderStatusRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Services\Bank\BankService;
use App\Utils\Sms;
use Doctrine\ORM\EntityManagerInterface;
use PhpCsFixer\Config;
use SunCat\MobileDetectBundle\DeviceDetector\MobileDetector;

class OrderService implements OrderServiceInterface
{
    private $entityManager;

    private $cartService;

    private $versionService;

    private $productRepository;

    private $orderPaymentTypeRepository;

    private $orderStatusRepository;

    private $managerRepository;

    private $constants;

    private $bankService;

    private $orderRepository;

    private $deliveryTypeRepository;

    /**
     * OrderService constructor.
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        CartService $cartService,
        VersionService $versionService,
        OrderStatusRepository $orderStatusRepository,
        ProductRepository $productRepository,
        OrderPaymentTypeRepository $orderPaymentTypeRepository,
        ConfigService $configService,
        BankService $bankService,
        OrderRepository $orderRepository,
	    ManagerRepository $managerRepository,
        DeliveryTypeRepository $deliveryTypeRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->cartService = $cartService;
        $this->versionService = $versionService;
        $this->productRepository = $productRepository;
        $this->orderPaymentTypeRepository = $orderPaymentTypeRepository;
        $this->orderStatusRepository = $orderStatusRepository;
        $this->constants = $configService->getConfig('constants');
        $this->bankService = $bankService;
        $this->orderRepository = $orderRepository;
	    $this->managerRepository = $managerRepository;
        $this->deliveryTypeRepository = $deliveryTypeRepository;
    }

    /**
     * @param array $data
     * @param Cart $cart
     * @return Order
     *
     */
    public function createOrder(array $data, Cart $cart): Order
    {
        //dump($this->constants);die();
        /** @var Manager $manager */

        $manager = $this->managerRepository->getCurrentManager($this->versionService->getId());
        $total = $this->cartService->getTotal($cart->getHashId());

        $orderStatus = $this->orderStatusRepository->findOneBy(['id' => $this->constants->order->status->new]);

        //create order
        $order = new Order();
        $order->setAddress($data['address']);
        $order->setAgreement($data['agreement']);

        //$order->setDeliveryType($data['delivery_type']);

        /** @var  $deliveryType  DeliveryType*/
        $deliveryType = $this->deliveryTypeRepository->findOneBy(['cmsCode' => $data['delivery_type']]);
        $order->setDelivery($deliveryType);

        $order->setPhone($data['phone']);
        $order->setEmail($data['email']);
        $order->setUsername($data['username']);
        $promocode = $cart->getPromocode();
        if($promocode){
            $order->setDescription('Оповещение системы: К этому заказу был применен промокод: '.$promocode->getCode());
            $order->setPromoCode($promocode->getCode());
        }

        /** @var OrderStatus $orderStatus */
        $orderStatus = $this->orderStatusRepository->findOneBy(['id' => $this->constants->order->status->new]);
        $order->setOrderStatus($orderStatus);

        $order->setSync(0);
        $order->setPrice($total);
        $order->setAgreementTime(\Carbon\Carbon::now());
        $order->setManager($manager);
        $order->setSiteVersion($this->versionService->getSiteVersion());

        $mobileSourceType = $this->constants->order->source_type->mobile;
        $desktopSourceType = $this->constants->order->source_type->desktop;

        $md = new MobileDetector();
        $order->setSourceType($md->isMobile() ? $mobileSourceType : $desktopSourceType);

        if(isset($data['user_id'])){
            $order->setUserId($data['user_id']);
        }
        $this->entityManager->persist($order);

        $payment = new OrderPayment();
        $payment->setOrder($order);
        $payment->setAmount($total);
        $payment->setType($this->orderPaymentTypeRepository->findOneBy(['id' => $data['origin_payment-type']]));

        $payment->setStatus(false);
        $payment->setIsOrigin(true);
        $this->entityManager->persist($payment);

        //create order_carts
        foreach($cart->getCartProducts() as $cartProduct){
            $orderProduct = new OrderProduct();
            /** @var Product $product */
            $product = $cartProduct->getProduct();

            $orderProduct->setProduct($product);
            $orderProduct->setOrder($order);
            $promocode = $cart->getPromocode();

            //$price = $product->getPrice($this->versionService->getSiteVersion());
            $price = $cartProduct->getPrice();
            if($promocode){
                /** @var bool $promocodeHasBrand */
                $promocodeHasBrand = $promocode->getBrands()->contains($product->getProductBrand());
                /** @var bool $promocodeHasProduct */
                $promocodeHasProduct = $promocode->getProducts()->contains($product);

                if($promocodeHasBrand || $promocodeHasProduct ){
                    if($promocode->getType() == Promocode::TYPE_PERCENTS){
                        $price = $price - ($price * $promocode->getDiscount() / 100) ;
                    } elseif ($promocode->getType() == Promocode::TYPE_ROUBLES) {
                        $price = $price - $promocode->getDiscount();
                    }
                }
                if($promocode->isDisposable()){
                    $promocode->setActive(false);
                }
                $this->entityManager->persist($promocode);

            }
            $orderProduct->setPrice((int) ceil($price));
            $orderProduct->setCount($cartProduct->getCount());
            $this->entityManager->persist($orderProduct);
        }
        $this->entityManager->flush();

        $manager->setLastOrder($order->getId());
        $this->entityManager->persist($manager);
        $this->entityManager->flush();

        $cashlessPaymentType = $this->constants->payment->type->cashless;

        if($cashlessPaymentType  === (int) $data['origin_payment-type']) {
            $this->sendBankOrder($order);
        }

        return $order;

    }

    /**
     * Create one-click-order(OCB)
     * @param array $data
     * @return Order
     */
    public function createOcbOrder(array $data): Order
    {
        /** @var Manager $manager */
        $manager = $this->managerRepository->getCurrentManager($this->versionService->getId());
        $product = $this->productRepository->findOneBy(['id'=> $data['pid']]);
        $price = $product->getPrice($this->versionService->getSiteVersion());
        $total = $price * $data['count'];

        $order = new Order();
        $order->setPhone($data['phone']);

        $orderStatus = $this->orderStatusRepository->findOneBy(['id' => $this->constants->order->status->new]);
        $order->setOrderStatus($orderStatus);

        /** @var  $deliveryType  DeliveryType*/
        $deliveryType = $this->deliveryTypeRepository->findOneBy(['cmsCode' => 0]);
        $order->setDelivery($deliveryType);
        $order->setSync(0);
        $order->setPrice($total);
        $order->setManager($manager);
        $order->setAgreement(0);
        $order->setAgreementTime(\Carbon\Carbon::now());
        $order->setSiteVersion($this->versionService->getSiteVersion());

        $mobileSourceType = $this->constants->order->source_type->mobile;
        $desktopSourceType = $this->constants->order->source_type->desktop;

        $md = new MobileDetector();
        $order->setSourceType($md->isMobile() ? $mobileSourceType : $desktopSourceType);

        $this->entityManager->persist($order);

        $payment = new OrderPayment();
        $payment->setOrder($order);
        $payment->setAmount($total);
        $payment->setType($this->orderPaymentTypeRepository->findOneBy(['cmsCode' => 0]));

        $payment->setStatus(false);
        $payment->setIsOrigin(true);
        $this->entityManager->persist($payment);
        $orderProduct = new OrderProduct();
        $orderProduct->setProduct($product);
        $orderProduct->setOrder($order);
        $orderProduct->setPrice($price);
        $orderProduct->setCount($data['count']);

        $this->entityManager->persist($orderProduct);
        $this->entityManager->flush();


        $manager->setLastOrder($order->getId());
        $this->entityManager->persist($manager);
        $this->entityManager->flush();


        return $order;
    }

    /**
     * Get callback data from bank and change order payment status from it
     * @param array $callbackData
     * @return bool
     */
    public function updatePaymentFromBankCallback(array $callbackData)
    {
        $bankService = $this->bankService;
        $bankClient = $bankService->getClient();
        $response = false;

        if($bankClient->isCorrectCallbackHash($callbackData) &&
            $bankClient->isCallbackPaymentSuccess($callbackData)) {

            $orderEncryptedNumber = $bankClient->getCallbackOrderNumber($callbackData);

            /** @var Order $order */
            $order = $this->orderRepository->findOneBy(['encryptedId' => $orderEncryptedNumber]);

            $originPayment = $order->getOriginPayment();

            if($originPayment) {
                $originPayment->setStatus($this->constants->payment->status->paid);
                $this->entityManager->persist($originPayment);
                $this->entityManager->flush();
            }

            $response = true;
        }

        return $response;
    }

    /**
     * Send payment request to the bank and save payment urls at OrderPayment entity
     * @param Order $order
     * @return bool
     */
    private function sendBankOrder(Order $order)
    {
        $bankService = $this->bankService;
        $bankClient = $bankService->getClient();

        $regResp = $bankClient->registerOrder($order);

        $bankId = $bankClient->getRegRespBankId($regResp);
        $bankPayUrl = $bankClient->getRegRespPayUrl($regResp);

        if(!$bankClient->hasError($regResp) &&
            !empty($bankId) &&
            !empty($bankPayUrl)) {

            $orderPayment = $order->getOriginPayment();

            $orderPayment->setBankId($bankId);
            $orderPayment->setBankPayUrl($bankPayUrl);

            $this->entityManager->persist($orderPayment);
            $this->entityManager->flush();

            return $bankPayUrl;
        }

        return false;
    }
}
