<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 02.03.18
 * Time: 14:44
 */

namespace App\Services;


use App\Entity\Cart;

interface CartServiceInterface
{
    /**
     * Add CartProduct to Cart
     * @param array $insert
     * @param string $hashId
     * @return Cart
     */
    public function add(array $insert, string $hashId): Cart;
    /**
     * Update item in cart
     * @param array $update
     */
    public function update(array $update, string $hashId): Cart ;

    /**
     * Delete item from cart
     * @param int $id
     * @return bool
     */
    public function delete(int $id, string $hashId): Cart;

    /**
     * Get cart object
     * @param string $hashId
     * @return Cart
     */
    public function getCart(string $hashId): ?Cart;

    /**
     * Get distinct CartProducts
     * @param string $hashId
     * @return int
     */
    public function getCount(string $hashId = NULL): int;

    /**
     * Get Cart Total Price
     * @param string $hashId
     * @return int
     */
    public function getTotal(string $hashId): int;

    /**
     * Check is allow cashless payment for current products in cart
     * @param string $hashId
     * @return bool
     */
    public function isAllowCashlessPayment(string $hashId) : bool;
}