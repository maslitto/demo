<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 29.11.18
 * Time: 11:24
 */

namespace App\Services;


class RedisService
{
    public function set(string $key, $value)
    {
        $redis = new \Predis\Client();
        $redis->set($key, $value);
    }

    public function get(string $key)
    {
        $redis = new \Predis\Client();
        return $redis->get($key);
    }
}