<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 07.06.18
 * Time: 12:19
 */

namespace App\Services;


use App\Entity\Product;
use App\Entity\ProductImage;
use Doctrine\ORM\EntityManagerInterface;
use Intervention\Image\Image;
use Intervention\Image\ImageManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Yaml\Yaml;


class ImageService
{
    private $configService;
    private $entityManager;
    private $rootPath;
    private $productImagesPath;

    /**
     * ImageService constructor.
     * @param string $rootPath
     * @param ConfigService $configService
     * @param EntityManagerInterface $entityManager
     */
    public function __construct( string $rootPath, ConfigService $configService, EntityManagerInterface $entityManager)
    {
        $this->configService = $configService;
        $this->entityManager = $entityManager;
        $this->rootPath = $rootPath;
        $this->productImagesPath = $rootPath.'/public/'. ProductImage::PATH_DIR;
    }

    /**
     * @param Product $product
     * @param UploadedFile $file
     * @return ProductImage
     */
    public function createProductImage(Product $product, UploadedFile $file){

        $productImage = new ProductImage();
        $productImage->setProduct($product);
        $productImage->setImageFile($file);

        $this->entityManager->persist($productImage);
        $this->entityManager->flush();

        $filepath = $this->productImagesPath . $productImage->getFilename();
        $config = $this->configService->getConstants();
        $sizes = $config['images']['product'];
        $this->createThumbnails($filepath, $sizes);

        return $productImage;
    }

    public function createThumbnails(string $filepath, array $sizes)
    {
        //dump($sizes);die();
        // create an image manager instance with favored driver
        $manager = new ImageManager(array('driver' => 'imagick'));

        $image = $manager->make($filepath);
        $filename = basename($filepath);

        // now you are able to resize the instance
        foreach ($sizes as $k => $size) {
            $thumbPath = $this->productImagesPath.$k.'/';
            if (!file_exists($thumbPath)) {
                mkdir($thumbPath, 0777, true);
            }
            $background = $manager->canvas($size['width'], $size['height'],'#fff');
            $resized = $manager->make($filepath)->resize($size['width'], $size['height'], function ($c) {
                $c->aspectRatio();
                $c->upsize();
            });
            $background->insert($resized, 'center');
            if (file_exists($thumbPath . $filename)) {
                unlink($thumbPath . $filename);
            }
            $background->save($thumbPath . $filename);

            /*
            $image->resize($size['width'], $size['height']);
            $image->save($this->productImagesPath.$k.'/'.$filename);
            */
        }
    }

    public function removeImage(ProductImage $image)
    {
        $filename = $image->getFilename();
        $filePath = $this->productImagesPath . $filename;
        if(file_exists($filePath)){
            unlink($filePath);
        }
        $config = $this->configService->getConstants();
        $sizes = $config['images']['product'];
        foreach ($sizes as $k => $size) {
            $thumbPath = $this->productImagesPath.$k.'/'. $filename;
            if(file_exists($thumbPath )){
                unlink($thumbPath);
            }
        }
        $this->entityManager->remove($image);
        $this->entityManager->flush();
    }
}