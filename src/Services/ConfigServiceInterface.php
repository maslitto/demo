<?php
namespace App\Services;

interface ConfigServiceInterface
{
    public function getConstants();
}