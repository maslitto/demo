<?php

namespace App\Services;

use App\Entity\Catalog;
use App\Entity\Product;
use App\Repository\CatalogRepository;
use App\Repository\ProductRepository;
use App\Utils\StringHelper;

class YandexMarketService
{
    private $productRepository;
    private $catalogRepository;
    private $versionService;
    private $stringHelper;
    private $domain = 'https://stommarket.ru';

    public function __construct(
        CatalogRepository $catalogRepository,
        ProductRepository $productRepository,
        VersionService $versionService,
        StringHelper $stringHelper
    )
    {
        $this->productRepository = $productRepository;
        $this->catalogRepository = $catalogRepository;
        $this->versionService = $versionService;
        $this->stringHelper = $stringHelper;
    }

    public function getYml()
    {
        @ini_set("memory_limit", "2048M");
        set_time_limit(0);

        $rootXML = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><yml_catalog></yml_catalog>');
        $rootXML->addAttribute('date', date('Y-m-d H:i:s'));

        $shopXML = $rootXML->addChild('shop');
        $shopXML->addChild('name', 'Stommarket');
        $shopXML->addChild('company', 'Stommarket');
        $shopXML->addChild('url', $this->domain);

        $currenciesXML = $shopXML->addChild('currencies');
        $currencyXML = $currenciesXML->addChild('currency');
        $currencyXML->addAttribute('id', 'RUR');
        $currencyXML->addAttribute('rate', 1);
        $currencyXML->addAttribute('plus', 0);

        $this->setCatalog($shopXML);
        $this->setProducts($shopXML);

        return $rootXML->asXML();
    }

    public function setCatalog( $shopXML)
    {
        $catalogXML = $shopXML->addChild('categories');

        /**
         * @var Catalog[] $catalogs
         */
        $catalogs = $this->catalogRepository->findBy(['active' => true]);

        foreach($catalogs as $catalog) {
            if(in_array($catalog->getId(), array(19, 15, 131, 20))) {
                continue;
            }

            $categoryXML = $catalogXML->addChild('category', $this->stringHelper->escapeXml($catalog->getName()));
            $categoryXML->addAttribute('id', $catalog->getId());

            if($catalog->getParentId()) {
                $categoryXML->addAttribute('parentId', $catalog->getParentId());
            }
        }
        return $shopXML;
    }

    public function setProducts(\SimpleXMLElement $shopXML)
    {
        /**
         * Adds a CDATA property to an XML document.
         *
         * @param string $name
         *   Name of property that should contain CDATA.
         * @param string $value
         *   Value that should be inserted into a CDATA child.
         * @param object $parent
         *   Element that the CDATA child should be attached too.
         */

        $offersXML = $shopXML->addChild('offers');
        /**
         * @var Product[] $products
         */
        $products = $this->productRepository->findBy(['active'=> true]);
        foreach($products as $product) {

            if($product->getCount($this->versionService->getId()) < 1) continue;
            /** @var Catalog $catalog */
            $catalog = $product->getCatalog();
            if (!$catalog->getActive()){continue;}
            if (!$product->getPrice($this->versionService->getSiteVersion()) ||
                !$product->getName() ||
                (int) $product->getId() === 5655 ||
                in_array($product->getCatalogId(), array(19, 15, 131, 20))) {
                continue;
            }

            if(in_array($product->getCatalogId(), array(163, 164, 165, 166, 167, 168, 16, 19, 15, 131, 20))) {
                $isProductAvailable = 'false';
            }
            else {
                $isProductAvailable = $product->getCount($this->versionService->getId()) > 0 ? 'true' : 'false';
            }

            if('false' === $isProductAvailable) {
                continue;
            }

			$price = (int) $product->getPrice($this->versionService->getSiteVersion());
            $productName = $this->stringHelper->escapeXml($product->getName());
			if($price < 1 || !$product->getCatalogId()) {
				continue;
			}

            $description = false;
            if($product->getPreview()) {
                $description = $product->getPreview();
                $description = preg_replace('#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', '', $description);
                if (mb_strlen($description, 'UTF-8') >= 3000) {
                    $description = false;
                }
            }
            if(!$description){
                $description = 'У товара пока нет описания.';
            }
            /**
             * Adds a CDATA property to an XML document.
             *
             * @param string $name
             *   Name of property that should contain CDATA.
             * @param string $value
             *   Value that should be inserted into a CDATA child.
             * @param object $parent
             *   Element that the CDATA child should be attached too.
             */
            $addCdata = function($name, $value, &$parent) {
                $child = $parent->addChild($name);

                if ($child !== NULL) {
                    $child_node = dom_import_simplexml($child);
                    $child_owner = $child_node->ownerDocument;
                    $child_node->appendChild($child_owner->createCDATASection($value));
                }

                return $child;
            };
            $offerXML = $offersXML->addChild('offer');
            $offerXML->addAttribute('id', $product->getId());
            $offerXML->addAttribute('available', $isProductAvailable);

            $offerXML->addChild('name', $productName);
            $offerXML->addChild('url', $this->domain . '/catalog/' . $product->getUrl() );
            $offerXML->addChild('price', $product->getPrice($this->versionService->getSiteVersion()));
            //$offerXML->addChild('description', $description);
            $addCdata('description', $description, $offerXML);
            $offerXML->addChild('currencyId', 'RUR');
            $offerXML->addChild('categoryId', $product->getCatalogId());
            $offerXML->addChild('pickup', 'true');
            $offerXML->addChild('delivery', 'true');

            if(!empty($product->getImage('hr'))) {
                $offerXML->addChild('picture', $this->domain . $product->getImage('hr'));
            }
            
            if($product->getProductBrand()->getName()) {
                $offerXML->addChild('vendor', $this->stringHelper->escapeXml($product->getProductBrand()->getName()));
            }
        }
        return $shopXML;
    }
}