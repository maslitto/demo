<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 29.08.18
 * Time: 17:14
 */

namespace App\Services\Api;


use App\Entity\ApiKey;
use App\Entity\Catalog;
use App\Entity\Product;
use App\Entity\ProductBrand;
use App\Entity\SiteVersion;
use App\Repository\CatalogRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductBrandRepository;
use App\Repository\ProductRepository;
use App\Repository\SiteVersionRepository;
use App\Services\Sync1c\OrderService;
use App\Utils\StringHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;

class ApiService

{
    const PRICE_CODE_SMALL_WHOLESALE = '000000005';

    private $stringHelper;
    private $productRepository;
    private $filePath;
    private $fileName;
    private $catalogRepository;
    private $productBrandRepository;
    private $siteVersionRepository;
    private $orderRepository;
    private $orderService;
    private $entityManager;

    public function __construct(
        string $varDir,
        StringHelper $stringHelper,
        ProductRepository $productRepository,
        CatalogRepository $catalogRepository,
        ProductBrandRepository $productBrandRepository,
        SiteVersionRepository $siteVersionRepository,
        OrderRepository $orderRepository,
        OrderService $orderService,
        EntityManagerInterface $entityManager
    )

    {
        $this->stringHelper = $stringHelper;
        $this->productRepository = $productRepository;
        $this->catalogRepository = $catalogRepository;
        $this->productBrandRepository = $productBrandRepository;
        $this->siteVersionRepository = $siteVersionRepository;
        $this->orderRepository = $orderRepository;
        $this->orderService = $orderService;
        $this->entityManager = $entityManager;
        $this->filePath = $varDir . '/sync/ecommerce-api';
        $this->fileName = $this->filePath . '/nomenclature.xml';
    }
    public function getNomenclature($apiKey = false)
    {
        if (file_exists($this->fileName)) {
            return file_get_contents($this->fileName);
        }
        else {
            return false;
        }
    }

    public function prepareNomenclature()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);

        $nomenclatureXml = $this->getNomenclatureXmlWrap();

        $classifierNode = $nomenclatureXml->addChild('Классификатор');
        $classifierNode = $this->addBrands($classifierNode);
        $classifierNode = $this->addCatalog($classifierNode, null, null, true);
        $classifierNode = $this->addPriceTypes($classifierNode);

        $catalogNode = $nomenclatureXml->addChild('Каталог');
        $catalogNode = $this->addProducts($catalogNode);

        $this->saveNomenclature($nomenclatureXml);
    }

    public function setNomenclature(ApiKey $apiKey, string $productsJson)
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        /** @var SiteVersion $siteVersion */
        $siteVersion = $this->siteVersionRepository->findOneBy(['id' => $apiKey->getVersionId()]);
        $products = json_decode($productsJson);
        //dump($products);die();
        foreach ($products as $productPost) {
              $product = $this->productRepository->findOneBy(['syncCode' => $productPost->code]);
              if($product){
                  //$product->setPriceRet();
                  $product->setCount($siteVersion, (int) $productPost->count);
                  $this->entityManager->persist($product);
              }
        }
        $this->entityManager->flush();
    }

    public function getOrders(ApiKey $apiKey, $fromTime = NULL)
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $siteVersion = $this->siteVersionRepository->findOneBy(['id' => $apiKey->getVersionId()]);

        $ordersXml = $this->orderService->getOrdersXmlWrapper();
        if(NULL === $fromTime) {
            $fromDateTime = (new \DateTime())->modify('-2 weeks');
        }
        else {
            $fromDateTime = new \DateTime($fromTime);
        }
        $orders = $this->orderRepository->getOrdersFor1cExchange($siteVersion->getId(), $fromDateTime);
        $ordersXml = $this->orderService->addToOrdersXml($ordersXml, $orders);
        return $ordersXml->asXml();
    }

    private function getNomenclatureXmlWrap() : \SimpleXMLElement
    {
        $nomenclatureXml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><КоммерческаяИнформация></КоммерческаяИнформация>');
        $nomenclatureXml->addAttribute('ВерсияСхемы', '2.05');
        $nomenclatureXml->addAttribute('ДатаФормирования', date('Y-m-d'));

        return $nomenclatureXml;
    }

    private function addPriceTypes(\SimpleXMLElement $parentNode) : \SimpleXMLElement
    {
        $pricesTypes = $parentNode->addChild('ТипыЦен');

        $smallWholesale = $pricesTypes->addChild('ТипЦены');
        $smallWholesale->addChild('Код', self::PRICE_CODE_SMALL_WHOLESALE);
        $smallWholesale->addChild('Наименование', 'Мелкоптовые цены');
        $smallWholesale->addChild('Валюта', 'руб');

        $smallWholesaleTax = $smallWholesale->addChild('Налог');
        $smallWholesaleTax->addChild('Наименование', 'НДС');
        $smallWholesaleTax->addChild('УчтеноВСумме', 'true');

        return $parentNode;
    }

    private function addProducts(\SimpleXMLElement $parentNode) : \SimpleXMLElement
    {
        $productsParentNode = $parentNode->addChild('Товары');

        /**
         * @var $products Product[]
         */
        $products = $this->productRepository->getActiveProducts();

        foreach ($products as $product) {

            $productNode = $productsParentNode->addChild('Товар');

            $article = $this->stringHelper->escapeXml($product->getArticle());
            $productNode->addChild('Артикул', $article);

            $name1c = $this->stringHelper->escapeXml($product->getName1c());
            $productNode->addChild('Наименование', $name1c);

            $name = $this->stringHelper->escapeXml($product->getName());
            $productNode->addChild('НаименованиеДляИнтернетМагазина', $name);

            $fullName1c = $this->stringHelper->escapeXml($product->getFullName1c());
            $productNode->addChild('ПолноеНаименование', $fullName1c);

            $packageType = $this->stringHelper->escapeXml($product->getPackageType());
            $productNode->addChild('БазоваяЕдиница', $packageType);

            $productNode->addChild('Код', $product->getSyncCode());

            $productNode = $this->setProductParentName($product, $productNode);
            $productNode = $this->setProductEvents($product, $productNode);
            $productNode = $this->setProductBrandSyncCode($product, $productNode);
            $productNode = $this->setProductCatalogSyncCode($product, $productNode);
            $productNode = $this->setProductTax($product, $productNode);

            $priceNode = $productNode->addChild('Цены')->addChild('Цена');

            $priceNode->addChild('Код', self::PRICE_CODE_SMALL_WHOLESALE);
            $priceNode->addChild('ЦенаЗаЕдиницу', $product->getPriceRet());

        }

        return $parentNode;
    }

    private function setProductTax(Product $product,\SimpleXMLElement  $productNode)
    {
        $taxNode = $productNode->addChild('СтавкиНалогов')->addChild('СтавкаНалога');

        $taxNode->addChild('Наименование', 'НДС');

        $taxValue = $product->getValueAddedTax();
        $taxNode->addChild('Ставка', $taxValue);

        return $productNode;
    }

    private function setProductBrandSyncCode(Product $product, \SimpleXMLElement $productNode)
    {
        /**
         * @var $brand ProductBrand
         */
        $brand = $product->getProductBrand();
        $brandSyncCode = $brand->getSyncCode();

        $productNode->addChild('Производители')->addChild('Код', $brandSyncCode);

        return $productNode;
    }

    private function setProductCatalogSyncCode(Product $product, \SimpleXMLElement  $productNode)
    {
        /**
         * @var $catalog Catalog
         */
        $catalog = $product->getCatalog();
        $catalogSyncCode = $catalog->getSyncCode();

        $productNode->addChild('Группы')->addChild('Код', $catalogSyncCode);

        return $productNode;
    }

    private function setProductParentName(Product $product,\SimpleXMLElement  $productNode)
    {
        $name = $this->stringHelper->escapeXml($product->getName1c());

        if(0 === (int) $product->getParent()) {
            $parentName = $name;
        }
        else {
            $parentProduct = $this->productRepository->findOneBy(['id' => $product->getParent()]);

            $parentName = $this->stringHelper->escapeXml($parentProduct->getName1c());
        }

        $productNode->addChild('ГоловнойТовар', $parentName);

        return $productNode;
    }

    private function setProductEvents(Product $product, \SimpleXMLElement $productNode)
    {
        $newValue = 'false';
        $topValue = 'false';
        $saleValue = 'false';

        switch ($product->getEvent()) {
            case 'sale':
                $saleValue = 'true';
                break;
            case 'top':
                $topValue = 'true';
                break;
            case 'new':
                $newValue = 'true';
                break;
        }

        $productNode->addChild('Новинка', $newValue);
        $productNode->addChild('Распродажа', $saleValue);
        $productNode->addChild('ЛидерПродаж', $topValue);

        return $productNode;
    }

    private function addCatalog(\SimpleXMLElement $rootNode,\SimpleXMLElement $parentNode = NULL,Catalog $catalog = NULL, $firstCall = false) : \SimpleXMLElement
    {
        if($firstCall) {
            /*$catalog = new Catalog;
            $catalog->setParentId(0);
            $catalog->select()->where(array('active' => 1));*/
            $catalog = $this->catalogRepository->findOneBy(['parentId' => NULL]);

            $parentNode = $rootNode->addChild('Группы');
        }

        foreach ($catalog->getChildren() as $subCategory) {
            /** @var $subCategory Catalog */
            $group = $parentNode->addChild('Группа');

            $group->addChild('Код', $subCategory->getSyncCode());
            $group->addChild('Наименование', $this->stringHelper->escapeXml($subCategory->getName()));

            if($subCategory->hasChildren()) {
                $groups = $group->addChild('Группы');
                $rootNode = $this->addCatalog($rootNode, $groups, $subCategory);
            }
        }

        return $rootNode;
    }

    private function addBrands(\SimpleXMLElement $parentNode) : \SimpleXMLElement
    {
        $brandsParentNode = $parentNode->addChild('Производители');
        /** @var $brands ProductBrand[] */
        $brands = $this->productBrandRepository->findBy(['active' => true]);

        foreach ($brands as $brand) {

            $brandNode = $brandsParentNode->addChild('Производитель');
            $brandNode->addChild('Код', $brand->getSyncCode());
            $brandNode->addChild('Наименование', $this->stringHelper->escapeXml($brand->getName()));
        }

        return $parentNode;
    }

    private function saveNomenclature(\SimpleXMLElement $nomenclatureXml)
    {
        $fileSystem = new Filesystem();
        if(!$fileSystem->exists($this->filePath)){
            $fileSystem->mkdir($this->filePath);
        }
        $nomenclatureXml->asXml($this->fileName);
    }

}