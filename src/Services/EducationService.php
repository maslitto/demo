<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 21.03.18
 * Time: 10:47
 */

namespace App\Services;

use App\Entity\Education\Education;
use App\Entity\Education\EducationDirection;
use App\Entity\Education\EducationStatus;
use App\Entity\Education\EducationType;
use App\Entity\User;
use App\Repository\Education\EducationDirectionRepository;
use App\Repository\Education\EducationRepository;
use App\Repository\Education\EducationStatusRepository;
use App\Repository\Education\EducationTypeRepository;
use App\Utils\CropAvatar;
use Doctrine\ORM\EntityManagerInterface;

class EducationService
{
    private $educationTypeRepository;
    private $educationRepository;
    private $educationStatusRepository;
    private $em;
    private $publicDir;

    public function __construct(
        EducationTypeRepository $educationTypeRepository,
        EducationDirectionRepository $educationDirectionRepository,
        EducationRepository $educationRepository,
        EducationStatusRepository $educationStatusRepository,
        EntityManagerInterface $entityManager,
        string $publicDir
    )
    {
        $this->educationRepository = $educationRepository;
        $this->educationTypeRepository = $educationTypeRepository;
        $this->educationDirectionRepository = $educationDirectionRepository;
        $this->educationStatusRepository = $educationStatusRepository;
        $this->em = $entityManager;
        $this->publicDir = $publicDir;
    }
    public function saveEducation(User $user, array $data, $files)
    {
        if($files){
            $uploadPath = $this->publicDir .'/uploads/education/';
            $crop = new CropAvatar(
                isset($data['photo_src']) ? $data['photo_src'] : null,
                isset($data['photo_data']) ? $data['photo_data'] : null,
                isset($files['photo_file']) ? $files['photo_file'] : null,
                $uploadPath,
                260,
                260
            );
            $photo = str_replace($this->publicDir,'',$crop->getResult());
        }
        if($data['id'] > 0){
            $education = $this->educationRepository->findOneBy(['id' => $data['id']]);
        } else {
            $education = new Education();
        }
        /** @var EducationType $educationType */
        $educationType = $this->educationTypeRepository->findOneBy(['id' =>(int) $data['education_type_id']]);
        $educationDirection = null;
        if(isset($data['education_direction_id'])){
            /** @var EducationDirection $educationDirection */
            $educationDirection = $this->educationDirectionRepository->findOneBy(['id' =>(int) $data['education_direction_id']]);
        }
        /** @var EducationStatus $educationStatus */
        $educationStatus = $this->educationStatusRepository->findOneBy(['id' => EducationStatus::TYPE_MODERATION]);
        //dd($educationType->getId());
        $education->setEducationType($educationType);
        $education->setLectorFio($data['lector_fio']);
        $education->setProfession($data['profession']);
        $education->setPhone($data['phone']);
        $education->setEmail($data['email']);
        $education->setName($data['name']);
        $education->setCity($data['city']);
        $education->setAddress($data['address']);
        $education->setIsSingleDay($data['is_single_day']);
        $education->setDateStart(new \DateTime($data['date_start']));
        $education->setDateEnd(new \DateTime($data['date_end']));
        $education->setTime($data['time']);
        $education->setIsFree((bool) $data['is_free']);
        $education->setPrice((int) $data['price']);
        $education->setProgram($data['program']);
        $education->setUser($user);
        $education->setEducationStatus($educationStatus);
        if($educationDirection) $education->setEducationDirection($educationDirection);
        $education->setActive(false);

        if(isset($photo)){
            $education->setPhoto($photo);
        }
        $this->em->persist($education);
        $this->em->flush();

        return $education;
    }
}