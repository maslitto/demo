<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 07.03.18
 * Time: 11:56
 */

namespace App\Services;

use App\Entity\Product;

interface ProductServiceInterface
{
    public function getRecentlyViewedProducts(): array;

    public function pushToRecentlyViewedProducts(Product $product): array;

    //public function getSimilarProducts(Product $product): array;

    public function getAlsoBoughtProducts(Product $product) : array;
}