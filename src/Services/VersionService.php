<?php
/**
 * Created by PhpStorm.
 * User: veteran
 * Date: 16.02.18
 * Time: 0:13
 */

namespace App\Services;


use App\Entity\SiteVersion;
use App\Repository\SiteVersionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use SypexGeo\Reader;
use Detection\MobileDetect;

class VersionService implements VersionServiceInterface
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var SiteVersionRepository
     */
    private $siteVersionRepository;
    /**
     * @var SiteVersion
     */
    private $siteVersion;
    /**
     * @var string
     */
    private $suggestVersion;
    /**
     * @var string
     */
    private $cookie;

    /**
     * @var string
     */
    private $host;
    /**
     * @var string
     */
    private $ip;
    /**
     * VersionService constructor.
     */
    private $request;

    public function __construct(SiteVersionRepository $siteVersionRepository,string $geoPath, RequestStack $requestStack)
    {
        $this->siteVersionRepository = $siteVersionRepository;
        $this->request = $request = $requestStack->getCurrentRequest();
        //dump($request->getHttpHost());die();
        if($request){
            $ip = $this->ip = $request->getClientIp();
            $this->host = $host = $request->getHost();
            $cookie = $request->cookies->get('site_version',NULL);
            if($cookie){
                $this->id = $cookie;
            } else {
                $key = str_replace(['https://','m.','www.','stommarket.ru','.'],'', $host);
                $siteVersion = $this->siteVersionRepository->findOneBy(['key' => $key,'active' => true]);

                if($siteVersion){
                    $this->id = $siteVersion->getId();
                } else {
                    $this->id = 1;
                }
            }
            $this->siteVersion = $siteVersionRepository->findOneBy(['id' => $this->id]);

            $reader = new Reader($geoPath);
            if( $suggestVersion = $siteVersionRepository->findOneBy(['active' => true,'sxgeoName' => $reader->getGeo($ip)['region']['ru']])){
                $this->suggestVersion = $suggestVersion ;
            } else{
                $this->suggestVersion = $siteVersionRepository->findOneBy(['id' => 1]);
            }
        }
    }

    /**
     * Version id getter
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Version id setter
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isSetVersion()
    {
        $cookie = $this->request->cookies->get('switch-city',NULL);
        if($cookie) return true;
        if($this->siteVersion->getId() == $this->suggestVersion->getId()) return true;
        else return false;
    }
    /**
     * @return SiteVersion
     */
    public function getSiteVersion()
    {
        return $this->siteVersion;
    }
    /**
     * @return array
     */
    public function getAll()
    {
        return $this->siteVersionRepository->findBy(['active' => true]);
    }
    /**
     * @return string
     */
    public function getCity(): ?string
    {
        return $this->siteVersion->getName();
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->siteVersion->getEmail();
    }

    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->siteVersion->getPhone();
    }

    /**
     * @return string
     */
    public function getShortPhone(): ?string
    {
        return str_replace([' ','-'],'',$this->getPhone());
    }

    /**
     * @return string
     */
    public function getRussiaPhone(): ?string
    {
        return '8-800-35-55-55';
    }
    /**
     * @return bool
     */
    public function isMobile(): bool
    {
        // TODO: Implement isMobile() method.
        return false;
    }

    /**
     * @return string
     */
    public function getWorkTime(): ?string
    {
        return $this->siteVersion->getWorkTime();
    }

    /**
     * @return string
     */
    public function getMapLInk(): ?string
    {
        return $this->siteVersion->getMapAddress();
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->siteVersion->getAddress();
    }

    /**
     * @return string
     */
    public function getFb(): ?string
    {
        return $this->siteVersion->getFb();
    }

    /**
     * @return string
     */
    public function getVk(): ?string
    {
        return $this->siteVersion->getVk();
    }

    /**
     * @return string
     */
    public function getFooterLine1(): ?string
    {
        return $this->siteVersion->getFooterLine1();
    }

    /**
     * @return string
     */
    public function getFooterLine2(): ?string
    {
        return $this->siteVersion->getFooterLine2();
    }

    /**
     * @return string
     */
    public function getSuggestVersion(): ?SiteVersion
    {
        return $this->suggestVersion;
    }


    /**
     * @return string
     */
    public function getStoreName()
    {
        return $this->siteVersion->getStoreName();
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $_SERVER['HTTP_HOST'];
    }
    public function getIp()
    {
        return $this->ip;
    }

    public function getCanonical()
    {
        $host = $this->siteVersion->getUrl();
        $canonical = $host .$this->request->getPathInfo();
        return $canonical;
    }

    public function getAlternate()
    {
        $host = $this->siteVersion->getMobileUrl();
        $alternate = $host .$this->request->getPathInfo();
        return $alternate;
    }
}