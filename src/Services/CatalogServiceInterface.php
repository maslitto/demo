<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 07.03.18
 * Time: 12:46
 */

namespace App\Services;


use App\Entity\Catalog;
use App\Entity\MetaInterface;

interface CatalogServiceInterface
{
    public function getFilter(MetaInterface $page, array $filter) : array;

    public function getBrands(Catalog $catalog, array $filter) : ?array;
}