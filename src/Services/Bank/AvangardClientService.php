<?php

namespace App\Services\Bank;

use App\Entity\Order;
use App\Services\ConfigService;
use GuzzleHttp\Client;
use App\Entity\OrderPayment;

/**
 * Send, receive and check info from "Avangard" bank about orders payments
 * Class AvangardClientService
 * @package App\Services\Bank
 */
class AvangardClientService extends BankClientAbstract
{
    const ORDER_STATUS_NOT_FOUND      = 0;
    const ORDER_STATUS_PROCESSING     = 1;
    const ORDER_STATUS_REJECT         = 2;
    const ORDER_STATUS_SUCCESS        = 3;
    const ORDER_STATUS_PARTIAL_RETURN = 4;
    const ORDER_STATUS_RETURN         = 5;

    const ERROR_CODE_NOERROR           = 0;
    const ERROR_CODE_SHOP_ID_EMPTY     = 1;
    const ERROR_CODE_SHOP_PASSWD_EMPTY = 2;
    const ERROR_CODE_WRONG_AUTH_DATA   = 3;
    const ERROR_CODE_INTERNAL_ERROR    = 4;
    const ERROR_CODE_EMPTY_TICKET      = 5;
    const ERROR_CODE_DISALLOWED_IP     = 6;
    const ERROR_CODE_WRONG_XML         = 7;
    const ERROR_CODE_EMPTY_XML         = 8;
    const ERROR_CODE_WRONG_ENCODING    = 9;
    const ERROR_CODE_WRONG_SUMM        = 10;

    /**
     * Init current auth data and bank signature
     * @param string $env
     * @param ConfigService $configService
     */
    function __construct(string $env, ConfigService $configService)
    {
        $authDataAllEnv = $configService->getBankAuthData()['avangard'];

        if('prod' === $env) {
            $authDataCurrentEnv = $authDataAllEnv[$env];
        }
        else {
            $authDataCurrentEnv = $authDataAllEnv['dev'];
        }

        $this->server = $authDataCurrentEnv['server'];
        $this->bankSignature = $authDataCurrentEnv['bankSignature'];

        $this->shopId = $authDataCurrentEnv['shopId'];
        $this->shopPass = $authDataCurrentEnv['shopPass'];

        $this->siteUri = 'http://' . $_SERVER['HTTP_HOST'];
    }

    /**
     * Check callback checksumm
     * @param array $callbackData
     * @return bool
     */
    public function isCorrectCallbackHash(array $callbackData) : bool
    {
        $dataHash = $callbackData['signature'];
        $calcHash = $this->calculateCallbackHash($callbackData);

        if($dataHash === $calcHash) {
            return true;
        }

        return false;
    }

    /**
     * Check was payment successfully passed
     * @param array $callbackData
     * @return bool
     */
    public function isCallbackPaymentSuccess(array $callbackData) : bool
    {
        if(self::ORDER_STATUS_SUCCESS === (int) $callbackData['status_code']) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Get order number from callback data
     * @param array $callbackData
     * @return string
     */
    public function getCallbackOrderNumber(array $callbackData) : string
    {
        return $callbackData['order_number'];
    }

    /**
     * Get payment status from bank server
     * @param OrderPayment $orderPayment
     * @return array
     */
    public function getPaymentStatus(OrderPayment $orderPayment) : array
    {
        $url = $this->server.'/iacq/h2h/get_order_info';

        $xmlRequest = '
        <?xml version="1.0" encoding="utf-8"?>
        <get_order_info>
            <shop_id>'.$this->shopId.'</shop_id>
            <shop_passwd>'.$this->shopPass.'</shop_passwd>
            <ticket>'.$orderPayment->getBankId().'</ticket>
        </get_order_info>';

        $params = ['xml' => $xmlRequest];

        $client = new Client();
        $response = $client->request('POST', $url, [
            'form_params' => $params
        ]);

        return (array) simplexml_load_string($response->getBody());
    }

    /**
     * Register new order payment at the bank.
     * Transfer rubles to kopecks in amount.
     * @param Order $order
     * @return array
     */
    public function registerOrder(Order $order) : array
    {
        $encryptedOrderNumber = $order->getEncryptedId();
        $orderAmount = $order->getOriginPayment()->getAmount();

        $url = $this->server.'/iacq/h2h/reg';

        $orderXml = '
        <?xml version="1.0" encoding="utf-8"?>
        <NEW_ORDER>
            <SHOP_ID>'.$this->shopId.'</SHOP_ID>
            <SHOP_PASSWD>'.$this->shopPass.'</SHOP_PASSWD>
            <AMOUNT>'.($orderAmount * 100).'</AMOUNT>
            <ORDER_NUMBER>'.$encryptedOrderNumber.'</ORDER_NUMBER>
            <ORDER_DESCRIPTION>Заказ с сайта</ORDER_DESCRIPTION>
            <LANGUAGE>RU</LANGUAGE>
            <client_name>'.$order->getUsername().'</client_name>
            <client_email>'.$order->getEmail().'</client_email>
            <client_phone>'.$order->getPhone().'</client_phone>
            <BACK_URL>'.$this->siteUri.'</BACK_URL>
            <BACK_URL_OK>'.$this->getSuccessUrl().'</BACK_URL_OK>
            <BACK_URL_FAIL>'.$this->getFailUrl().'</BACK_URL_FAIL>
        </NEW_ORDER>';

        $params = ['xml' => $orderXml];

        $client = new Client();
        $response = $client->request('POST', $url, [
            'form_params' => $params
        ]);

        return (array) simplexml_load_string($response->getBody());
    }

    /**
     * Check error in bank response
     * @param array $responseData
     * @return bool
     */
    public function hasError(array $responseData) : bool
    {
        if((!isset($responseData['response_code']) or
            self::ERROR_CODE_NOERROR !== (int) $responseData['response_code'])) {

            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Get order id from bank response
     * @param array $responseData
     * @return null|string
     */
    public function getRegRespBankId(array $responseData) : ?string
    {
        if(isset($responseData['ticket']) and
            !empty($responseData['ticket'])) {

            return $responseData['ticket'];
        }
    }

    /**
     * Get payment url from the bank response
     * @param array $responseData
     * @return null|string
     */
    public function getRegRespPayUrl(array $responseData) : ?string
    {
        $baseUrl = 'https://www.avangard.ru/iacq/pay?ticket=';
        $ticket  = $this->getRegRespBankId($responseData);

        if(empty(!$ticket)) {
            return $baseUrl.$ticket;
        }
    }

    /**
     * Generate checksum from some of callback fields and bank signature
     * @param array $callbackData
     * @return string
     */
    protected function calculateCallbackHash(array $callbackData) : string
    {
        $shopId   = $callbackData['shop_id'];
        $orderNum = $callbackData['order_number'];
        $amount   = $callbackData['amount'];

        $orderDataStr = $shopId.$orderNum.$amount;

        return strtoupper(md5(strtoupper(md5($this->bankSignature).md5($orderDataStr))));
    }
}