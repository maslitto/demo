<?php
namespace App\Services\Bank;
use App\Entity\Order;
use App\Entity\OrderPayment;

abstract class BankClientAbstract implements BankClientInterface
{
    protected $server;
    protected $siteUri;

    protected $shopId;
    protected $shopPass;

    protected $bankSignature;

    abstract public function isCorrectCallbackHash(array $callbackData) : bool;
    abstract public function isCallbackPaymentSuccess(array $callbackData) : bool;
    abstract public function getCallbackOrderNumber(array $callbackData) : string;
    abstract public function getPaymentStatus(OrderPayment $orderPayment) : array;
    abstract public function registerOrder(Order $order) : array;
    abstract public function hasError(array $responseData) : bool;
    abstract public function getRegRespBankId(array $responseData) : ?string;
    abstract public function getRegRespPayUrl(array $responseData) : ?string;

    abstract protected function calculateCallbackHash(array $callbackData) : string;

    protected function getSuccessUrl()
    {
        return $this->siteUri.'/cart/success';
    }

    protected function getFailUrl()
    {
        return $this->siteUri.'/cart/fail';
    }
}