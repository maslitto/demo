<?php

namespace App\Services\Bank;

use App\Services\Bank\BankClientAbstract;
use App\Services\Bank\BankService;

interface BankServiceInterface
{
    public function getClient() : BankClientAbstract;
}