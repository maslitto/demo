<?php

namespace App\Services\Bank;
use App\Services\ConfigService;
use App\Services\VersionService;
use Doctrine\ORM\Version;
use App\Services\Bank\SberbankClientService;
use App\Services\Bank\AvangardClientService;
use App\Services\Bank\BankClientAbstract;

/**
 * Class Bank
 * @package App\Services\Bank
 */

class BankService implements BankServiceInterface
{
    private $client;

    public function __construct(
        VersionService $versionService,
        SberbankClientService $sberbankClientService,
        AvangardClientService $avangardClientService,
        ConfigService $configService
    )
    {
        $currentId = $versionService->getId();
        $constants = $configService->getConfig('constants');

        $kirovId = $constants->version->kirov;
        $yaroslavlIds = $constants->version->yaroslavlIds;
        $mainIds = $constants->version->mainIds;

        if(in_array($currentId, $yaroslavlIds) || in_array($currentId, $mainIds)){
            $this->setClient($sberbankClientService);
        } elseif($currentId == $kirovId){
            $this->setClient($avangardClientService);
        }
        else{
            $this->setClient($sberbankClientService);
        }
    }

    private function setClient(BankClientAbstract $client) : BankService
    {
        $this->client = $client;
        return $this;
    }

    public function getClient() : BankClientAbstract
    {
        return $this->client;
    }
}