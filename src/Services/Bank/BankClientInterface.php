<?php

namespace App\Services\Bank;
use App\Entity\Order;
use App\Entity\OrderPayment;

interface BankClientInterface
{
    public function isCorrectCallbackHash(array $callbackData) : bool;
    public function isCallbackPaymentSuccess(array $callbackData) : bool;
    public function getCallbackOrderNumber(array $callbackData) : string;
    public function getPaymentStatus(OrderPayment $orderPayment) : array;
    public function registerOrder(Order $order) : array;
    public function hasError(array $responseData) : bool;
    public function getRegRespBankId(array $responseData) : ?string;
    public function getRegRespPayUrl(array $responseData) : ?string;
}