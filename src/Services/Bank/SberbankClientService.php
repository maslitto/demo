<?php

namespace App\Services\Bank;

use App\Services\ConfigService;
use App\Services\VersionService;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\RequestStack;
use App\Entity\Order;
use App\Entity\OrderPayment;

/**
 * Send, receive and check info from "Sberbank" bank about orders payments
 * Class SberbankClientService
 * @package App\Services\Bank
 */
class SberbankClientService extends BankClientAbstract
{
    const CALLBACK_OPERATION_DEPOSITED = 'deposited';
    const CALLBACK_OPERATION_APPROVED  = 'approved';
    const CALLBACK_OPERATION_REVERSED  = 'reversed';
    const CALLBACK_OPERATION_REFUNDED  = 'refunded';

    const CALLBACK_STATUS_ERROR = 0;
    const CALLBACK_STATUS_OK 	= 1;

    const ERROR_CODE_NO_ERROR = 0;

    /**
     * Init current auth data and bank signature
     * @param string $env
     * @param ConfigService $configService
     */
    function __construct(string $env, ConfigService $configService, RequestStack $requestStack,VersionService $versionService)
    {
        $request = $requestStack->getCurrentRequest();
        $contantsConfig = $configService->getConfig('constants');
        $banksConfig = $configService->getConfig('bank_auth');

        $yaroslavlIds = $contantsConfig->version->yaroslavlIds;

        if(in_array($versionService->getId(),$yaroslavlIds )){
            $authDataAllEnv = $banksConfig->sberbank->yaroslavl;
        } else{
            $authDataAllEnv = $banksConfig->sberbank->main;
        }

        if('prod' === $env) {
            $authDataCurrentEnv = $authDataAllEnv->prod;
        }
        else {
            $authDataCurrentEnv = $authDataAllEnv->dev;
        }

        $this->server = $authDataCurrentEnv->server;
        $this->bankSignature = $authDataCurrentEnv->bankSignature;

        $this->shopId = $authDataCurrentEnv->shopId;
        $this->shopPass = $authDataCurrentEnv->shopPass;

        $this->siteUri = 'http://' . $request->getHttpHost();
    }

    /**
     * Check callback checksumm
     * @param array $callbackData
     * @return bool
     */
    public function isCorrectCallbackHash(array $callbackData) : bool
    {
        $dataHash = $callbackData['checksum'];
        $calcHash = $this->calculateCallbackHash($callbackData);
        if($dataHash === $calcHash) {
            return true;
        }

        return false;
    }

    /**
     * Check was payment successfully passed
     * @param array $callbackData
     * @return bool
     */
    public function isCallbackPaymentSuccess(array $callbackData) : bool
    {
        if(self::CALLBACK_OPERATION_DEPOSITED === $callbackData['operation'] &&
            self::CALLBACK_STATUS_OK === (int) $callbackData['status']) {

            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Get order number from callback data
     * @param array $callbackData
     * @return string
     */
    public function getCallbackOrderNumber(array $callbackData) : string
    {
        return $callbackData['orderNumber'];
    }

    /**
     * Get payment status from bank server
     * @param string $orderEncryptedId
     * @return array
     */
    public function getPaymentStatus(OrderPayment $orderPayment) : array
    {
        $url = $this->server.'/payment/rest/getOrderStatus.do';
        $params = [
            'orderId'  => $orderPayment->getOrder()->getEncryptedId(),
            'userName' => $this->shopId,
            'password' => $this->shopPass,
        ];

        $client = new Client();
        $response = $client->request('GET', $url, [
            'form_params' => $params
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * Register new order payment at the bank.
     * Transfer rubles to kopecks in amount (sberbank stores order summ in kopecks).
     * Payment available for 24 hours
     * @param $order
     * @return mixed
     */
    public function registerOrder(Order $order) : array
    {
        $encryptedOrderNumber = $order->getEncryptedId();
        $orderAmount = $order->getOriginPayment()->getAmount();

        $url = $this->server.'/payment/rest/register.do';

        $params = [
            'returnUrl'           => $this->getSuccessUrl(),
            'failUrl'             => $this->getFailUrl(),
            'orderNumber'         => $encryptedOrderNumber,
            'amount'              => $orderAmount * 100,
            'sessionTimeoutSecs'  => 86400,
            'userName' 			  => $this->shopId,
            'password' 			  => $this->shopPass,
        ];

        $client = new Client();
        $response = $client->request('POST', $url, [
            'form_params' => $params
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * Check error in bank response
     * @param array $responseData
     * @return bool
     */
    public function hasError(array $responseData) : bool
    {
        if((isset($responseData['errorCode']) and
            self::ERROR_CODE_NO_ERROR != $responseData['errorCode'])) {

            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Get order id from bank response
     * @param array $responseData
     * @return null|string
     */
    public function getRegRespBankId(array $responseData) : ?string
    {
        if(isset($responseData['orderId']) and
            !empty($responseData['orderId'])) {

            return $responseData['orderId'];
        }
    }

    /**
     * Get payment url from the bank response
     * @param array $responseData
     * @return null|string
     */
    public function getRegRespPayUrl(array $responseData) : ?string
    {
        if(isset($responseData['formUrl']) and
            !empty($responseData['formUrl'])) {

            return $responseData['formUrl'];
        }
    }

    /**
     * Generate checksum from all callback data (except checksumm) and bank signature
     * @param array $callbackData
     * @return string
     */
    protected function calculateCallbackHash(array $callbackData) : string
    {
        unset($callbackData['checksum']);
        ksort($callbackData);
        $string = http_build_query($callbackData, null, ';');
        $string = str_replace('=', ';', $string).';';
        $hmac = hash_hmac('sha256', $string, $this->bankSignature);
        $upperHmac = strtoupper($hmac);
        return $upperHmac;
    }
}