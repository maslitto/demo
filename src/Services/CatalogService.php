<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 07.03.18
 * Time: 12:48
 */

namespace App\Services;


use App\Entity\Catalog;
use App\Entity\MetaInterface;
use App\Entity\ProductBrand;
use App\Entity\Tag;
use App\Repository\ProductBrandRepository;
use App\Repository\ProductRepository;

class CatalogService implements CatalogServiceInterface
{
    private $productRepository;
    private $versionService;
    private $productBrandRepository;

    public function __construct(ProductRepository $productRepository, VersionService $versionService, ProductBrandRepository $productBrandRepository)
    {
        $this->productRepository = $productRepository;
        $this->versionService = $versionService;
        $this->productBrandRepository = $productBrandRepository;
    }

    /**
     * @param Catalog $catalog
     * @param array $filter
     * @return array
     */
    public function getFilter(MetaInterface $page, array $filter) : array
    {
        $qb = $this->productRepository->filterProductsQuery($page, $filter, $this->versionService->getId());
        if($page instanceof Catalog){
            return [
                'brands' => $this->getBrands($page, $filter),
                'min' => $this->productRepository->getMinPrice($qb),
                'max' => $this->productRepository->getMaxPrice($qb),
            ];

        } elseif($page instanceof ProductBrand){
            return [
                'min' => $this->productRepository->getMinPrice($qb),
                'max' => $this->productRepository->getMaxPrice($qb),
            ];

        } elseif($page instanceof Tag){
            return [
                'min' => $this->productRepository->getMinPrice($qb),
                'max' => $this->productRepository->getMaxPrice($qb),
            ];
        }
    }

    /**
     * @param Catalog $catalog
     * @param array $filter
     * @return array
     */
    public function getBrands(Catalog $catalog, array $filter) : ?array
    {
        $brandIds = $this->productRepository->getBrandIds($catalog, $filter, $this->versionService->getId());
        $brands = $this->productBrandRepository->findBy(['id' => $brandIds]);
        return $brands;
    }
}