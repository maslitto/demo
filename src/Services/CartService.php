<?php
/**
 * Created by PhpStorm.
 * User: veteran
 * Date: 16.02.18
 * Time: 0:13
 */

namespace App\Services;
use App\Entity\Cart;
use App\Entity\CartProduct;
use App\Entity\Product;
use App\Entity\Promocode;
use App\Entity\User;
use App\Repository\CartProductRepository;
use App\Repository\CartRepository;
use App\Repository\ProductRepository;
use App\Services\VersionService;
use Doctrine\ORM\EntityManagerInterface;
use SunCat\MobileDetectBundle\DeviceDetector\MobileDetector;
use Detection\MobileDetect;

class CartService implements CartServiceInterface
{
    private $cart;
    private $productRepository;
    private $cartRepository;
    private $versionService;
    private $cartProductRepository;
    private $entityManager;

    public function __construct(
        ProductRepository $productRepository,
        VersionService $versionService,
        CartRepository $cartRepository,
        CartProductRepository $cartProductRepository,
        EntityManagerInterface $entityManager
    )
    {
        $this->productRepository = $productRepository;
        $this->cartRepository = $cartRepository;
        $this->cartProductRepository = $cartProductRepository;
        $this->versionService = $versionService;
        $this->entityManager = $entityManager;
    }

    /**
     * @return Cart
     */
    public function createCart()
    {
        $md = new MobileDetector();
        $cart = new Cart();
        $cart->setFinished(0);
        $cart->setSiteDeviceVersion($md->isMobile() ? 0 : 1);
        $cart->setUserAgent($md->getUserAgent() );
        $cart->setSiteVersion($this->versionService->getSiteVersion());
        $cart->setMailSended(false);
        $cart->setIp($this->versionService->getIp());
        do {
            $hashId = uniqid();
            $cartExists = $this->cartRepository->findBy(['hashId' => $hashId]);
        } while (count($cartExists) > 0);
        $cart->setHashId($hashId);
        $this->entityManager->persist($cart);
        $this->entityManager->flush();

        return $cart;
    }

    /**
     * @param Cart $cart
     * @param Product $product
     * @param int $count
     * @return CartProduct
     */
    private function createCartProduct(Cart $cart, Product $product, int $count)
    {
        $cartProduct = new CartProduct();
        $cartProduct->setProduct($product);
        $cartProduct->setCount($count);
        $cartProduct->setCart($cart);
        //мутный кусок хардкода для скидки на реставрины, в корзине на баннере
        $restavrinIds = $this->productRepository->getWhereNameLikeIds('Реставрин - шприц 4г');
        if(in_array($product->getId(),$restavrinIds) && $this->getTotal($cart->getHashId()) >= 4000) {
            $cartProduct->setPrice(272);
        } else {
            $cartProduct->setPrice($product->getPrice($this->versionService->getSiteVersion()));
        }
        $this->entityManager->persist($cartProduct);
        $this->entityManager->flush();
        return $cartProduct;
    }

    /**
     * @param array $insert
     * @param string|NULL $hashId
     * @return Cart
     */
    public function add(array $insert, string $hashId = NULL): Cart
    {
        if(!$hashId){
            $cart = $this->createCart();
            $hashId = $cart->getHashId();
        }
        $product = $this->productRepository->findOneBy(['id' => $insert['id']]);
        $count = $insert['count'];
        $cart = $this->cartRepository->findOneBy(['hashId' => $hashId]);
        $cartProduct = $this->cartProductRepository->checkProductInCart($hashId, $insert['id']);
        if($cartProduct){
            $cartProduct->setCount($cartProduct->getCount() + $insert['count']);
            $this->entityManager->persist($cartProduct);
        } else {
            $cartProduct = $this->createCartProduct($cart, $product, $count);
        }

        $this->entityManager->flush();
        return $cart;
    }

    /**
     * Update item in cart
     * @param array $update
     * @return bool|Cart
     */
    public function update(array $update, string $hashId): Cart
    {

        $cart = $this->cartRepository->findOneBy(['hashId' => $hashId]);

        $cartProduct = $this->cartProductRepository->checkProductInCart($hashId, $update['id']);
        if($cartProduct){
            $cartProduct->setCount($update['count']);
            $this->entityManager->persist($cartProduct);
        } else {
            return false;
        }
        $this->entityManager->flush();
        return $cart;
    }

    /**
     * Apply Promocode to Cart Entity
     * @param array $update
     * @return bool|Cart
     */
    public function applyPromocode(Promocode $promocode, string $hashId): Cart
    {
        /** @var Cart $cart */
        $cart = $this->cartRepository->findOneBy(['hashId' => $hashId]);

        $cart->setPromocode($promocode);

        $this->entityManager->persist($cart);
        $this->entityManager->flush();
        return $cart;
    }

    /**
     * Delete CartProduct from cart
     * @param int $id
     * @return bool|Cart
     */
    public function delete(int $id, string $hashId): Cart
    {
        $cart = $this->cartRepository->findOneBy(['hashId' => $hashId]);
        $cartProduct = $this->cartProductRepository->checkProductInCart($hashId, $id);
        if($cartProduct){
            $this->entityManager->remove($cartProduct);
        } else {
            return false;
        }
        $this->entityManager->flush();
        return $cart;
    }

    public function finish(string $hashId): Cart
    {
        $cart = $this->cartRepository->findOneBy(['hashId' => $hashId]);
        $cart->setFinished(1);
        $this->entityManager->persist($cart);
        $this->entityManager->flush();
        return $cart;
    }
    /**
     * Get Cart object
     * @param string|NULL $hashId
     * @return bool|null|object
     */
    public function getCart(string $hashId): ?Cart
    {
        $cart = $this->cartRepository->findOneBy(['hashId' => $hashId]);
        if($cart == NULL){
            setcookie("cart", '', time() - 60*60*24*30, "/");
            return NULL;
        } else{
            $this->cart = $cart;
            return $cart;
        }

    }

    /**
     * Get CartProducts count
     * @param string|NULL $hashId
     * @return int
     */
    public function getCount(string $hashId = NULL): int{
        if($hashId == NULL) return 0;
        $cart = $this->getCart($hashId);
        if($cart == NULL) return 0;
        if(!$cart->getCartProducts()->count()){
            return 0;
        } else {
            return $cart->getCartProducts()->count();
        }
    }

    /**
     * Get cart total sum
     * @param string|NULL $hashId
     * @return int
     */
    public function getTotal(string $hashId): int{
        $cart = $this->getCart($hashId);
        $total = 0;
        if(!$cart->getCartProducts()->count()){
            return 0;
        } else {
            $promocode = $cart->getPromocode();
            foreach ($cart->getCartProducts() as $cartProduct){
                /** @var Product $product */
                $product = $cartProduct->getProduct();
                $price = $cartProduct->getPrice($this->versionService->getSiteVersion());

                if($promocode){
                    /** @var bool $promocodeHasBrand */
                    $promocodeHasBrand = $promocode->getBrands()->contains($product->getProductBrand());
                    /** @var bool $promocodeHasProduct */
                    $promocodeHasProduct = $promocode->getProducts()->contains($product);

                    if($promocodeHasBrand || $promocodeHasProduct ){
                        if($promocode->getType() == Promocode::TYPE_PERCENTS){
                            $price = $price - ($price * $promocode->getDiscount() / 100) ;
                        } elseif ($promocode->getType() == Promocode::TYPE_ROUBLES) {
                            $price = $price - $promocode->getDiscount();
                        }
                    }
                }
                $total += (int) ceil($price * $cartProduct->getCount());
            }
            return $total;
        }
    }

    /**
     * Get total cart discount
     * @param string|NULL $hashId
     * @return int
     */
    public function getDiscount(string $hashId): int{
        $versionId = $this->versionService->getId();
        $cart = $this->getCart($hashId);
        $total = 0;
        if(!$cart->getCartProducts()->count()){
            return 0;
        } else {
            $promocode = $cart->getPromocode();
            foreach ($cart->getCartProducts() as $cartProduct){
                /** @var Product $product */
                $product = $cartProduct->getProduct();
                $price = $product->getPrice($this->versionService->getSiteVersion());
                $discount = 0;
                if($promocode){
                    /** @var bool $promocodeHasBrand */
                    $promocodeHasBrand = $promocode->getBrands()->contains($product->getProductBrand());
                    /** @var bool $promocodeHasProduct */
                    $promocodeHasProduct = $promocode->getProducts()->contains($product);
                    if($promocodeHasBrand || $promocodeHasProduct ){
                        if($promocode->getType() == Promocode::TYPE_PERCENTS){
                            $discount = $price * $promocode->getDiscount() / 100 ;
                        } elseif ($promocode->getType() == Promocode::TYPE_ROUBLES) {
                            $discount = $promocode->getDiscount();
                        }
                    }
                }
                $total += $discount * $cartProduct->getCount() + $product->getDiscount($versionId) * $cartProduct->getCount();
            }
            return (int) $total;
        }
    }

    /**
     * Get cart total bonus
     * @param string|NULL $hashId
     * @return int
     */
    public function getBonus(string $hashId): int{
        $versionId = $this->versionService->getId();
        $cart = $this->getCart($hashId);
        $total = 0;
        if(!$cart->getCartProducts()->count()){
            return 0;
        } else {
            foreach ($cart->getCartProducts() as $cartProduct){
                $total +=$cartProduct->getProduct()->getBonus($versionId) * $cartProduct->getCount();
            }
            return $total;
        }
    }

    /**
     * @param Product $product
     * @param $hashId
     * @return int
     */
    public function getProductCountInCart(Product $product, string $hashId = NULL)
    {
        if($hashId == NULL) return 0;

        $cartProduct = $this->cartProductRepository->checkProductInCart($hashId, $product->getId());

        if($cartProduct) return $cartProduct->getCount();
        else return 0;
    }

    /**
     * Check is allow cashless payment for current products in cart
     * @param string $hashId
     * @return bool
     */
    public function isAllowCashlessPayment(?string $hashId): bool
    {
        if($hashId == NULL){
            return false;
        }
        $cart = $this->getCart($hashId);
        $versionId = $this->versionService->getId();
        $anesthesiaCatalogId = 131;

        if($cart->getPrice() < 1000 && in_array($cart->getSiteVersion()->getId(),[1,2])){
            return false;
        }
        foreach ($cart->getCartProducts() as $cartProduct) {
            $product = $cartProduct->getProduct();

             if((0 == $product->getCount($versionId)) ||
                ($anesthesiaCatalogId == $product->getCatalogId())) {
                 return false;
             }
        }

        return true;
    }
    /**
     * Check is allow delivery
     * @param string $hashId
     * @return bool
     */
    public function isAllowDelivery(?string $hashId): bool
    {
        if($hashId == NULL){
            return false;
        }
        $cart = $this->getCart($hashId);
        if($cart->getPrice() < 2000 && in_array($cart->getSiteVersion()->getId(),[1])){
            return false;
        }
        return true;
    }
}