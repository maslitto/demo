<?php

namespace App\Services;

use App\Entity\Catalog;
use App\Entity\Product;
use Doctrine\Common\Collections\ArrayCollection;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class PriceService
{
    private $versionService;

    private $columnsAliases = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
        'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
        'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE',
        'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL'
    ];

    public function __construct(VersionService $versionService)
    {
        $this->versionService = $versionService;
    }

    public static function monthName($unixTimeStamp = false) {
      
      // Если не задано время в UNIX, то используем текущий
      if (!$unixTimeStamp) {
        $mN = date('m');
      
      
      // Если задано определяем месяц времени
      } else {
        $mN = date('m', (int)$unixTimeStamp);
      }
      
      $monthAr = array(
        1 => 'Января',
        2 => 'Февраля',
        3 => 'Марта',
        4 => 'Апреля',
        5 => 'Мая',
        6 => 'Июня',
        7 => 'Июля',
        8 => 'Августа',
        9 => 'Сентября',
        10=> 'Октября',
        11=> 'Ноября',
        12=> 'Декабря'
      );
      
      return $monthAr[(int)$mN];
    }

    public function getExcel(Catalog $catalog)
    {
        if(0 === $catalog->getId()) {
            $catalogName = 'Корень каталога';
        }
        else {
            $catalogName = $catalog->getName();
        }
        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Стоммаркет')
            ->setTitle('Коммерческое предложение Стоммаркет')
            ->setDescription('прайс-лист')
            ->setCategory('стоматология');

            $spreadsheet->getActiveSheet()->insertNewRowBefore(1,1);
            $spreadsheet->setActiveSheetIndex(0);

            $spreadsheet->getActiveSheet()->getDefaultColumnDimension()
            ->setWidth(3);
            $spreadsheet->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
            $spreadsheet->getActiveSheet()->getStyle("A1:CZ6")->getAlignment()->setVertical(Alignment::VERTICAL_TOP);

            // --------------------------------------------------------------

            $styleArrayReq = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => Border::BORDER_THIN,
                        'color' => array('argb' => '000000'),
                    ),
                ),
            );

            $styleArrayLigntTop = array(
                'borders' => array(
                    'top' => array(
                        'style' => Border::BORDER_THIN,
                        'color' => array('argb' => 'aaaaaa'),
                    ),
                ),
            );

            $styleArrayLigntRight = array(
                'borders' => array(
                    'right' => array(
                        'style' => Border::BORDER_THIN,
                        'color' => array('argb' => 'aaaaaa'),
                    ),
                ),
            );

             $styleArrayHorisontalCenter = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                )
            );

            $spreadsheet->getActiveSheet()->getStyle('A1:AL6')->applyFromArray($styleArrayReq);
            $spreadsheet->getActiveSheet()->getStyle('A3')->applyFromArray($styleArrayLigntTop);
            $spreadsheet->getActiveSheet()->getStyle('A6')->applyFromArray($styleArrayLigntTop);
            $spreadsheet->getActiveSheet()->getStyle('A4')->applyFromArray($styleArrayLigntRight);
            $spreadsheet->getActiveSheet()->getStyle('J4')->applyFromArray($styleArrayLigntRight);

            // --------------------------------------------------------------

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('A1:R2');
            $spreadsheet->getActiveSheet()->getStyle("A1")->getAlignment()->setWrapText(true);
            $spreadsheet->getActiveSheet()->SetCellValue('A1', 'СТ-ПЕТЕРБУРГСКИЙ Ф-Л ПАО "ПРОМСВЯЗЬБАНК" Г. САНКТ-ПЕТЕРБУРГ');

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('A3:R3');
            $spreadsheet->getActiveSheet()->SetCellValue('A3', 'Банк получателя');

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('A4:B4');
            $spreadsheet->getActiveSheet()->SetCellValue('A4', 'ИНН');

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('C4:I4');
            $spreadsheet->getActiveSheet()->setCellValueExplicit('C4', '7811549186', DataType::TYPE_STRING);

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('J4:K4');
            $spreadsheet->getActiveSheet()->SetCellValue('J4', 'КПП');

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('L4:R4');
            $spreadsheet->getActiveSheet()->setCellValueExplicit('L4', '780601001', DataType::TYPE_STRING);

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('A5:R5');
            $spreadsheet->getActiveSheet()->SetCellValue('A5', 'Общество с ограниченной ответственностью "Стоммаркет"');

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('A6:R6');
            $spreadsheet->getActiveSheet()->SetCellValue('A6', 'Получатель');

            // -----------------------------------------------------

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('S1:T1');
            $spreadsheet->getActiveSheet()->SetCellValue('S1', 'БИК');

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('S2:T3');
            $spreadsheet->getActiveSheet()->SetCellValue('S2', 'Сч. №');

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('S4:T6');
            $spreadsheet->getActiveSheet()->SetCellValue('S4', 'Сч. №');

            // -----------------------------------------------------

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('U1:AL1');
            $spreadsheet->getActiveSheet()->setCellValueExplicit('U1', '44030920', DataType::TYPE_STRING);

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('U2:AL3');
            $spreadsheet->getActiveSheet()->setCellValueExplicit('U2', '30101810000000000920', DataType::TYPE_STRING);

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('U4:AL6');
            $spreadsheet->getActiveSheet()->setCellValueExplicit('U4', '40702810206000003639', DataType::TYPE_STRING);


            // -----------------------------------------------------

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('A8:AF8');
            $spreadsheet->getActiveSheet()->getRowDimension('8')->setRowHeight(26);
            $spreadsheet->getActiveSheet()->getStyle("A8")->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle("A8")->getFont()->setSize(14);
            $spreadsheet->getActiveSheet()->getStyle("A8")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            $spreadsheet->getActiveSheet()->SetCellValue('A8', 'Коммерческое предложение от '.date("d")." ".self::monthName()." ".date("Y"));

            // -----------------------------------------------------
            $spreadsheet->getActiveSheet()->getRowDimension('10')->setRowHeight(36);
            $spreadsheet->getActiveSheet()->getStyle("A10:AF10")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('A10:F10');
            $spreadsheet->getActiveSheet()->SetCellValue('A10', 'Поставщик:');

            $spreadsheet->getActiveSheet()->getStyle("G10")->getAlignment()->setWrapText(true);
            $spreadsheet->setActiveSheetIndex(0)->mergeCells('G10:AF10');
            $spreadsheet->getActiveSheet()->SetCellValue('G10', 'Общество с ограниченной ответственностью "Стоммаркет", ИНН 7811549186, КПП 780601001, 195112, Санкт-Петербург г, Республиканская ул, дом № 22, корпус А');

            // -----------------------------------------------------

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('A12:H12');
            $spreadsheet->getActiveSheet()->getStyle("A12")->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->SetCellValue('A12', 'Контактные данные');

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('J12:R12');
            $spreadsheet->getActiveSheet()->SetCellValue('J12', 'Телефон: 8 800 700-83-43');

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('U12:AF12');
            $spreadsheet->getActiveSheet()->SetCellValue('U12', 'Email: sale@stommarket.ru');


            /* ~~~~~~~~~~~~~~~~~~      CATEGORY DESCRIPTION      ~~~~~~~~~~~~~~~~~~ */
            // -----------------------------------------------------

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('A14:AF14');
            $spreadsheet->getActiveSheet()->getRowDimension('14')->setRowHeight(26);
            $spreadsheet->getActiveSheet()->getStyle("A14")->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle("A14")->getFont()->setSize(14);
            $spreadsheet->getActiveSheet()->getStyle("A14")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            $spreadsheet->getActiveSheet()->SetCellValue('A14', 'Раздел: '.$catalogName);

            /* ~~~~~~~~~~~~~~~~~~      TABLE HEADER      ~~~~~~~~~~~~~~~~~~ */
            // -----------------------------------------------------
            $spreadsheet->getActiveSheet()->getStyle("A16:AL16")->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle('A16:AL16')->applyFromArray($styleArrayHorisontalCenter);
            $spreadsheet->setActiveSheetIndex(0)->mergeCells('A16:B16');
            $spreadsheet->getActiveSheet()->SetCellValue('A16', '№');   

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('C16:V16');
            $spreadsheet->getActiveSheet()->SetCellValue('C16', 'Наименование');

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('W16:AB16');
            $spreadsheet->getActiveSheet()->SetCellValue('W16', 'Производитель');

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('AC16:AG16');
            $spreadsheet->getActiveSheet()->SetCellValue('AC16', 'Цена (руб)');

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('AH16:AL16');
            $spreadsheet->getActiveSheet()->SetCellValue('AH16', 'В наличии');

            /* ~~~~~~~~~~~~~~~~~~      TABLE DATA      ~~~~~~~~~~~~~~~~~~ */
            // -----------------------------------------------------
            $startRow           = 17;
            $startColumn        = 0;
            $spreadsheet        = $this->drawCategoryTree($spreadsheet, $catalog, $startColumn, $startRow)['excelObj'];

            // Redirect output to a client’s web browser (Excel5) 
            /*header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$fileName.'-stommarket-price.xlsx"'); 
            header('Cache-Control: max-age=0'); 


            die();*/
            $writer = new Xlsx($spreadsheet);

            return $writer;
    }

    private function drawCategoryTree(Spreadsheet $spreadsheet, Catalog $catalog, int $currentColumnNum, int $currentRow)
    {
        $catalogChildren       = $catalog->getChildren();
        $currentColumnStartChar = $this->columnsAliases[$currentColumnNum];
        $currentColumnEndChar   = $this->columnsAliases[$currentColumnNum + 31];
        $nextColumnNum          = $currentColumnNum + 1;

        $currentColumnStartIndex = $currentColumnStartChar.$currentRow;
        $currentColumnEndIndex   = $currentColumnEndChar.$currentRow;

        $excelData = $this->drawExcelTitleStrings($spreadsheet, $currentColumnStartIndex, $currentColumnEndIndex, $currentRow, $catalog->getName());
        $spreadsheet = $excelData['excelObj'];
        $currentRow = $excelData['currentRow'];

        $excelData = $this->drawExcelProducts($spreadsheet, $currentRow, $catalog);
        $spreadsheet = $excelData['excelObj'];
        $currentRow = $excelData['currentRow'];

        
        foreach ($catalogChildren as $catalogChild) {
            $currentColumnStartIndex = $currentColumnStartChar.$currentRow;
            $currentColumnEndIndex   = $currentColumnEndChar.$currentRow;

            $excelData = $this->drawExcelTitleStrings($spreadsheet, $currentColumnStartIndex, $currentColumnEndIndex, $currentRow, $catalogChild->getName());
            $spreadsheet = $excelData['excelObj'];
            $currentRow = $excelData['currentRow'];

            $excelData = $this->drawExcelProducts($spreadsheet, $currentRow, $catalogChild);
            $spreadsheet = $excelData['excelObj'];
            $currentRow = $excelData['currentRow'];

            if($catalogChild->hasChildren()) {
                $excelData     = $this->drawCategoryTree($spreadsheet, $catalogChild, $nextColumnNum, $currentRow);
                $spreadsheet  = $excelData['excelObj'];
                $currentRow   = $excelData['currentRow'];
            }
            $currentRow += 1;
        }

        return [
            'excelObj'    => $spreadsheet,
            'currentRow' => $currentRow
        ];
    }

    private function drawExcelTitleStrings($spreadsheet, $columnStartIndex, $columnEndIndex, $currentRow, $string)
    {
        $spreadsheet->setActiveSheetIndex(0)->mergeCells($columnStartIndex.':'.$columnEndIndex);
        $spreadsheet->getActiveSheet()->getRowDimension((string) $currentRow)->setRowHeight(26);
        $spreadsheet->getActiveSheet()->getStyle($columnStartIndex)->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getStyle($columnStartIndex)->getFont()->setSize(14);
        $spreadsheet->getActiveSheet()->getStyle($columnStartIndex)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->SetCellValue($columnStartIndex, $string);

        $currentRow += 1;

        return [
            'excelObj'    => $spreadsheet,
            'currentRow' => $currentRow
        ];
    }

    private function drawExcelProducts($spreadsheet, $currentRow,Catalog $catalog)
    {
        $productCounter = 1;
        $products       = $catalog->getProducts();

        foreach($products as $product) {
            
            $spreadsheet->setActiveSheetIndex(0)->mergeCells('A'.$currentRow.':B'.$currentRow);
            $spreadsheet->getActiveSheet()->SetCellValue('A'.$currentRow, $productCounter);   

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('C'.$currentRow.':V'.$currentRow);
            $spreadsheet->getActiveSheet()->SetCellValue('C'.$currentRow, $product->getName());

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('W'.$currentRow.':AB'.$currentRow);
            $spreadsheet->getActiveSheet()->SetCellValue('W'.$currentRow, $product->getProductBrand()->getName());

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('AC'.$currentRow.':AG'.$currentRow);
            $spreadsheet->getActiveSheet()->SetCellValue('AC'.$currentRow, $product->getPrice($this->versionService->getSiteVersion()));

            $spreadsheet->setActiveSheetIndex(0)->mergeCells('AH'.$currentRow.':AL'.$currentRow);
            $spreadsheet->getActiveSheet()->SetCellValue('AH'.$currentRow, $product->getCount($this->versionService->getId()) > 0 ? 'да' : 'нет');

            $currentRow++;
            $productCounter++;
        }

        return [
            'excelObj'    => $spreadsheet,
            'currentRow' => $currentRow
        ];
    }

    public function getFranchizePrice($products){
        /** @var Product[] $products */
        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Стоммаркет')
            ->setTitle('Прайс франчайзи')
            ->setDescription('прайс-лист')
            ->setCategory('стоматология');

        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->getColumnDimension('A')->setWidth(100);
        $sheet->getColumnDimension('B')->setWidth(10);
        $sheet->setCellValue('A1', 'Наименование');
        $sheet->setCellValue('B1', 'Цена (руб.)');
        foreach ($products as $k => $product) {
            $sheet->setCellValue('A'.(4+$k), $product->getName());
            $sheet->setCellValue('B'.(4+$k), $product->getPriceFranchize());
        }
        $writer = new Xlsx($spreadsheet);

        return $writer;
    }
    public function getDummyPrice(){
        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Стоммаркет')
            ->setTitle('Прайс')
            ->setDescription('прайс-лист')
            ->setCategory('стоматология');

        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->getColumnDimension('A')->setWidth(100);
        $sheet->setCellValue('A1', 'Внимание!');
        $sheet->setCellValue('A2','По вопросам получения прайс-листа обращайтесь на е-мэйл sale@stommarket.ru или по телефону 8 800 700-83-43');
        $writer = new Xlsx($spreadsheet);

        return $writer;
    }
}