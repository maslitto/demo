<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 30.08.19
 * Time: 10:23
 */

namespace App\Services;


use FOS\ElasticaBundle\Finder\FinderInterface;

class ElasticService
{

    private $productFinder;
    private $catalogFinder;
    private $brandFinder;
    private $tagFinder;
    private $search;
    /**
     * SearchController constructor.
     * @param $finder
     */
    public function __construct(
        FinderInterface $productFinder,
        FinderInterface $catalogFinder,
        FinderInterface $brandFinder,
        FinderInterface $tagFinder
    )

    {
        $this->productFinder = $productFinder;
        $this->catalogFinder = $catalogFinder;
        $this->brandFinder = $brandFinder;
        $this->tagFinder = $tagFinder;
    }

    public function setSearch(string $search){
        $this->search = $search;
    }

    /**
     * @param int $fuzziness
     * @return \Elastica\Query\BoolQuery
     */
    private function getQuery(int $fuzziness = 2)
    {
        $search = $this->search;
        $translitService = new \Denismitr\Translit\Translit($search);
        $translit = $translitService->getTranslit();
        $fields = ['name', 'url','searchTags'];
        $searches = [$search, $translit];
        $boolQuery = new \Elastica\Query\BoolQuery();

        $fieldQuery = new \Elastica\Query\Term();
        $fieldQuery->setTerm('active',true);
        $boolQuery->addFilter($fieldQuery);

        /*foreach ($fields as $field){
            foreach ($searches as $search) {
                $fieldQuery = new \Elastica\Query\MatchPhrasePrefix();
                $fieldQuery->setFieldQuery($field, $search);
                $boolQuery->addShould($fieldQuery);

                $fuzzyQuery = new \Elastica\Query\Fuzzy();
                $fuzzyQuery->setField($field, $search);
                $fuzzyQuery->setFieldOption("fuzziness", $fuzziness);
                $boolQuery->addShould($fuzzyQuery);
            }
        }*/
        return $boolQuery;
    }
    private function getTranslit(){
        $translitService = new \Denismitr\Translit\Translit($this->search);
        $translit = $translitService->getTranslit();
        return $translit;
    }
    /**
     * @param int $count
     * @param array $options
     * @return array
     */
    public function findProducts(int $count, $options = [])
    {
        if(strlen($this->search) <= 4){
            $fuzziness = 0;
        } elseif(strlen($this->search) > 4 && strlen($this->search) <= 8) {
            $fuzziness = 1;
        } else {
            $fuzziness = 2;
        }
        $boolQuery = $this->getQuery();
        $fieldQuery = new \Elastica\Query\Term();
        $fieldQuery->setTerm('status',1);
        $boolQuery->addMust($fieldQuery);


        $fieldQuery = new \Elastica\Query\MatchPhrasePrefix();
        $fieldQuery->setFieldQuery('name', $this->search);
        $boolQuery->addShould($fieldQuery);

        $fuzzyQuery = new \Elastica\Query\Fuzzy();
        $fuzzyQuery->setField('name', $this->search);
        $fuzzyQuery->setFieldOption("fuzziness", $fuzziness);
        $boolQuery->addShould($fuzzyQuery);



        $fuzzyQuery = new \Elastica\Query\Fuzzy();
        $fuzzyQuery->setField('name', $this->getTranslit());
        $fuzzyQuery->setFieldOption("fuzziness", $fuzziness);
        $boolQuery->addShould($fuzzyQuery);

        $fuzzyQuery = new \Elastica\Query\Fuzzy();
        $fuzzyQuery->setField('searchTags', $this->search);
        $fuzzyQuery->setFieldOption("fuzziness", $fuzziness);
        $boolQuery->addShould($fuzzyQuery);

        $fuzzyQuery = new \Elastica\Query\Fuzzy();
        $fuzzyQuery->setField('url', $this->getTranslit());
        $fuzzyQuery->setFieldOption("fuzziness", $fuzziness);
        $boolQuery->addShould($fuzzyQuery);

        $boolQuery->setMinimumShouldMatch(1);

        $products = $this->productFinder->find($boolQuery, $count, $options);
        return $products;
    }

    /**
     * @param int $count
     * @param array $options
     * @return array
     */
    public function findTags(int $count, $options = [])
    {
        $boolQuery = $this->getQuery();

        $fuzzyQuery = new \Elastica\Query\Fuzzy();
        $fuzzyQuery->setField('name', $this->search);
        $fuzzyQuery->setFieldOption("fuzziness", 2);
        $boolQuery->addShould($fuzzyQuery);

        $fuzzyQuery = new \Elastica\Query\Fuzzy();
        $fuzzyQuery->setField('name', $this->getTranslit());
        $fuzzyQuery->setFieldOption("fuzziness", 2);
        $boolQuery->addShould($fuzzyQuery);
        $boolQuery->setMinimumShouldMatch(1);
        /*$fuzzyQuery = new \Elastica\Query\Fuzzy();
        $fuzzyQuery->setField('name', $this->getTranslit());
        $fuzzyQuery->setFieldOption("fuzziness", 2);
        $boolQuery->addFilter($fuzzyQuery);*/

        $tags = $this->tagFinder->find($boolQuery,$count,[]);
        return $tags;
    }

    /**
     * @param int $count
     * @param array $options
     * @return mixed
     */
    public function findBrands(int $count, $options = [])
    {
        $boolQuery = $this->getQuery();

        $fuzzyQuery = new \Elastica\Query\Fuzzy();
        $fuzzyQuery->setField('name', $this->search);
        $fuzzyQuery->setFieldOption("fuzziness", 2);
        $boolQuery->addMust($fuzzyQuery);

        $fuzzyQuery = new \Elastica\Query\Fuzzy();
        $fuzzyQuery->setField('name', $this->getTranslit());
        $fuzzyQuery->setFieldOption("fuzziness", 2);
        $boolQuery->addShould($fuzzyQuery);

        $boolQuery->setMinimumShouldMatch(1);
        $brands = $this->brandFinder->find($boolQuery,$count,$options);
        return $brands;
    }

    /**
     * @param int $count
     * @param array $options
     * @return array
     */
    public function findCatalogs(int $count, $options = [])
    {
        $boolQuery = $this->getQuery();

        $fieldQuery = new \Elastica\Query\MatchPhrasePrefix();
        $fieldQuery->setFieldQuery('name', $this->search);
        $boolQuery->addShould($fieldQuery);


        $fieldQuery = new \Elastica\Query\MatchPhrasePrefix();
        $fieldQuery->setFieldQuery('name', $this->getTranslit());
        $boolQuery->addShould($fieldQuery);

        /*$fuzzyQuery = new \Elastica\Query\Fuzzy();
        $fuzzyQuery->setField('name', $this->search);
        $fuzzyQuery->setFieldOption("fuzziness", 2);
        $boolQuery->addShould($fuzzyQuery);

        $fuzzyQuery = new \Elastica\Query\Fuzzy();
        $fuzzyQuery->setField('name', $this->getTranslit());
        $fuzzyQuery->setFieldOption("fuzziness", 2);
        $boolQuery->addShould($fuzzyQuery);
        $boolQuery->setMinimumShouldMatch(1);*/

        $boolQuery->setMinimumShouldMatch(1);


        $catalogs = $this->catalogFinder->find($boolQuery,$count,$options);
        return $catalogs;
    }
}
