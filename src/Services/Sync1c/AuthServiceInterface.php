<?php
/**
 * Created by PhpStorm.
 * User: kip
 * Date: 29/05/18
 * Time: 13:04
 */

namespace App\Services\Sync1c;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface AuthServiceInterface
{
    public function hasSuccessAuth(string $passwordCookie): bool;
    public function authenticateClient(): bool;
    public function getAuthPassword(): string;
}