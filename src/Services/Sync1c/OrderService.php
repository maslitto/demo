<?php
/**
 * Created by PhpStorm.
 * User: kip
 * Date: 28/05/18
 * Time: 16:49
 */

namespace App\Services\Sync1c;

use App\Entity\Order;
use App\Entity\OrderPayment;
use App\Entity\OrderProduct;
use App\Repository\OrderRepository;
use App\Services\Sync1c\OrderServiceInerface;
use App\Services\VersionService;
use App\Utils\StringHelper;

class OrderService implements OrderServiceInerface
{
    private $orderRepository;
    private $versionService;

    public function __construct(OrderRepository $orderRepository,VersionService $versionService)
    {
        $this->orderRepository = $orderRepository;
        $this->versionService = $versionService;
    }
    public function getXmlOrders(string $startTime): string
    {
        if(false === $startTime) {
            $fromDateTime = (new \DateTime())->modify('-2 weeks');
        }
        else {
            $fromDateTime = new \DateTime($startTime);
        }
        $orders = $this->orderRepository->getOrdersFor1cExchange($this->versionService->getId(),$fromDateTime);
        //dump($orders);die();
        $rootXML = $this->getOrdersXmlWrapper();
        $xml = $this->addToOrdersXml($rootXML, $orders/*,$this->versionService->getCity()*/);
        return $xml->asXML();
    }

    public function getOrdersXmlWrapper():\SimpleXMLElement
    {
        //create xml doc and add tech data
        $rootXML = new \SimpleXMLElement("<КоммерческаяИнформация></КоммерческаяИнформация>");
        $rootXML->addAttribute('ВерсияСхемы', '2.08');
        $rootXML->addAttribute('ДатаФормирования', date('Y-m-d'));

        return $rootXML;
    }

    public function addToOrdersXml(\SimpleXMLElement $rootXML, $orders, $versionKey = false)
    {
        $stringHelper = new StringHelper();
        foreach($orders as $order) {
            /** @var  $order Order */
            $orderManager = $order->getManager();

            $comment = '';
            //$commentSep = '; ';
            $commentSep = ' ';

            //shielding from html entities
            $name       = $stringHelper->escapeXml($order->getUsername());
            if(empty($name)) {
                $name = 'John Smith';
            }
            $address    = $stringHelper->escapeXml($order->getAddress());
            $city       = $stringHelper->escapeXml($order->getCity());
            $postIndex  = $stringHelper->escapeXml($order->getZipCode());
            $phone      = $stringHelper->escapeXml($order->getPhone());
            $email      = $stringHelper->escapeXml($order->getEmail());
            $descr      = $stringHelper->escapeXml($order->getDescription());
            //$promoCode  = $stringHelper->escapeXml($order->getPr);
            $totalPrice = $order->getPrice();

            $comment .=
                'Менеджер: ' . $orderManager->getLastName().' '.$orderManager->getName() . $commentSep;

            if($order->getBonus()) {
                $comment .= 'Использован бонус ' . $order->getBonus() . ' руб.' . $commentSep;
            }

            $comment .=
                'Телефон: ' . ($phone ? $phone : '') . $commentSep
                .'E-mail: ' . ($email ? $email : '') .$commentSep
                .'Комментарий: ' . ($descr ?  $descr . $commentSep : '');

            //$comment .= 'Промокод: '.$promoCode.$commentSep;

            /*$payments = $order->getPayments();
            if(0 < count($payments)) {
                foreach ($payments as $payment) {*/
                    $comment .=
                        'Оплата: ' . $order->getPaymentTitle() . $commentSep;
            /*    }
            }*/

            $comment .=
                'Тип доставки: ' . $order->getDelivery()->getName() . $commentSep;

            if($address) {
                $comment .= 'Адрес:  ' . $address . $commentSep;
            }

            if($name) {
                $username = $name;
            }
            else {
                $username = "John Smith";
            }

            $orderXML = $rootXML->addChild('Документ');

            $idPostfix = (false === $versionKey) ? '' : $versionKey;

            $orderId = $order->getId().'-'.$idPostfix;
            $orderXML->addChild('Ид', $order->getId());

            $orderNumber = '0' !== (string) $order->getEncryptedId() ? $order->getEncryptedId().'-'.$idPostfix : $orderId;
            $orderXML->addChild('Номер', $orderNumber);
            $orderXML->addChild('Менеджер', $orderManager->getLastName().' '.$orderManager->getName());
            $orderXML->addChild('МенеджерИмя', $orderManager->getName());
            $orderXML->addChild('МенеджерФамилия', $orderManager->getLastName());

            $orderXML->addChild('ХозОперация', 'Заказ товара');
            $orderXML->addChild('Валюта', 'руб');
            $orderXML->addChild('Курс', '1');
            $orderXML->addChild('Сумма', $totalPrice*100);

            //set date and comment to xml doc
            $orderXML->addChild('Дата',$order->getTimeCreate()->format('Y-m-d'));
            $orderXML->addChild('Комментарий', $comment);

            //add user info as counterparty (id, role, full name)
            $clientXML = $orderXML->addChild('Контрагенты')->addChild('Контрагент');
            $clientXML->addChild('Ид', $order->getUserId() ? 'Незарегистрированный пользователь'.'-'.$idPostfix : $order->getId().'-'.$idPostfix);
            $clientXML->addChild('Роль', 'Покупатель');
            $clientXML->addChild('Имя', $order->getUsername());
            $clientXML->addChild('Фамилия', $order->getUsername());
            $clientXML->addChild('ПолноеНаименование', $name);
            $clientXML->addChild('Наименование', $name);

            //add user address to xml
            $clientAddressXML = $clientXML->addChild('АдресРегистрации');
            $clientAddressXML->addChild('Представление', $postIndex . ' ' . $address);

            //add user city to xml
            $clientXML->addChild('Город', $city);

            //set contact parent node in xml doc
            $clientContactsXML = $clientXML->addChild('Контакты');

            //add phone to xml
            if($phone) {
                $clientContactXML = $clientContactsXML->addChild('Контакт');
                $clientContactXML->addChild('Тип', 'Телефон рабочий');
                $clientContactXML->addChild('Значение', $phone);
            }

            //add email to xml
            if($email) {
                $clientContactXML = $clientContactsXML->addChild('Контакт');
                $clientContactXML->addChild('Тип', 'Почта');
                $clientContactXML->addChild('Значение', $email);
            }

            //add address as 'address field' in xml
            $addressFieldXML = $clientAddressXML->addChild('АдресноеПоле');
            $addressFieldXML->addChild('Тип', 'Адрес');
            $addressFieldXML->addChild('Значение', $address);

            //set client agent in xml
            $clientAgentXML = $clientXML->addChild('Представители')->addChild('Представитель')->addChild('Контрагент');

            $clientAgentXML->addChild('Отношение', 'Контактное лицо');
            $clientAgentXML->addChild('Наименование', $name);
            $clientAgentXML->addChild('Ид', 'b342955a9185c40132d4c1df6b30af2f');

            //add time to xml as separate node
            $orderXML->addChild('Время', $order->getTimeCreate()->format('H:i:s'));

            $productsXML = $orderXML->addChild('Товары');

            //get current cart items
            $orderProducts = $order->getOrderProducts();
            //loop thru cart items and add them to xml
            foreach($orderProducts as $orderProduct) {
                /** @var  $orderProduct OrderProduct */
                //add product node to xml
                $product = $orderProduct->getProduct();

                $productPrice = $orderProduct->getPrice() + 0;

                $productXML = $productsXML->addChild('Товар');

                //add product amount to xml
                $productXML->addChild('Ид', $product->getSyncId());
                $productXML->addChild('Код', $product->getSyncCode());
                $productXML->addChild('ИдКаталога', $product->getCatalog()->getSyncId());
                $productXML->addChild('Наименование', htmlspecialchars($product->getName()));
                $productXML->addChild('ЦенаЗаЕдиницу', $productPrice);
                $productXML->addChild('Количество', $orderProduct->getCount());
                $productXML->addChild('Сумма', $productPrice * $orderProduct->getCount());

                //add tech info
                $productPropsXML = $productXML->addChild('ЗначенияРеквизитов');
                $productPropertyXML = $productPropsXML->addChild('ЗначениеРеквизита');
                $productPropertyXML->addChild('Наименование', 'ВидНоменклатуры');
                $productPropertyXML->addChild('Значение', 'Товар');

                $productPropertyXML = $productPropsXML->addChild('ЗначениеРеквизита');
                $productPropertyXML->addChild('Наименование', 'ТипНоменклатуры');
                $productPropertyXML->addChild('Значение', 'Товар');
            }

            //add payment type to xml
            $orderPropertiesXML = $orderXML->addChild('ЗначенияРеквизитов');
            $propertyXML = $orderPropertiesXML->addChild('ЗначениеРеквизита');

            //----------------------------------------------------------------------------------------------------------
            /** @var  $originPayment OrderPayment*/
            $originPayment = $order->getOriginPayment();
            if($originPayment) {
                $orderPaymentStatus = ((int) $originPayment->isStatus() === Order::STATUS_PAID ? 'true' : 'false');
                $orderPaymentMethod = $order->getPaymentTitle();
            }
            else {
                $orderPaymentStatus = 'false';
                $orderPaymentMethod = 'Не задан';
            }

            //payment type
            $propertyXML->addChild('Наименование', 'Метод оплаты');
            $propertyXML->addChild('Значение', $orderPaymentMethod);

            //has paid
            $propertyXML = $orderPropertiesXML->addChild('ЗначениеРеквизита');
            $propertyXML->addChild('Наименование', 'Заказ оплачен');
            $propertyXML->addChild('Значение', $orderPaymentStatus);
            //----------------------------------------------------------------------------------------------------------


            //add is delivery allowed
            $propertyXML = $orderPropertiesXML->addChild('ЗначениеРеквизита');
            $propertyXML->addChild('Наименование', 'Доставка разрешена');

            $allowedDeliveryTypes = [
                Order::DELIVERY_TYPE_SPB,
                Order::DELIVERY_TYPE_LEN_REG,
                Order::DELIVERY_TYPE_MSK,
                Order::DELIVERY_TYPE_MSK_REG,
                Order::DELIVERY_TYPE_DIMITROVGRAD,
                Order::DELIVERY_TYPE_ULJANOVSK,
                Order::DELIVERY_TYPE_ULJANOVSK_REG,
            ];
            $propertyXML->addChild('Значение', in_array($order->getDeliveryType(), $allowedDeliveryTypes) ? 'true' : 'false');

            //has canceled
            $propertyXML = $orderPropertiesXML->addChild('ЗначениеРеквизита');
            $propertyXML->addChild('Наименование', 'Отменен');
            $propertyXML->addChild('Значение', ($order->getStatus() == Order::STATUS_CANCELED ? 'true' : 'false'));

            //has final
            $propertyXML = $orderPropertiesXML->addChild('ЗначениеРеквизита');
            $propertyXML->addChild('Наименование', 'Финальный статус');
            $propertyXML->addChild('Значение', ($order->getStatus() == Order::STATUS_COMPLETE ? 'true' : 'false'));

            $propertyXML = $orderPropertiesXML->addChild('ЗначениеРеквизита');
            $propertyXML->addChild('Наименование', 'Статус заказа');
            $propertyXML->addChild('Значение', $order->getOrderStatus()->getName());

            //last update time
            $propertyXML = $orderPropertiesXML->addChild('ЗначениеРеквизита');
            $propertyXML->addChild('Наименование', 'Дата изменения статуса');
            $propertyXML->addChild('Значение', $order->getTimeUpdate()->format('Y-m-d H:i:s'));
        }

        return $rootXML;
    }

}