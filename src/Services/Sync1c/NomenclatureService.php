<?php
/**
 * Created by PhpStorm.
 * User: kip
 * Date: 28/05/18
 * Time: 16:49
 */

namespace App\Services\Sync1c;

use App\Entity\ProductBrand;
use App\Entity\Catalog;
use App\Entity\Product;
use App\Entity\ProductVersion;
use App\Repository\CatalogRepository;
use App\Repository\ProductBrandRepository;
use App\Repository\ProductRepository;
use App\Repository\SiteVersionRepository;
use App\Services\RedisService;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Proxies\__CG__\App\Entity\SiteVersion;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Services\Sync1c\NomenclatureServiceInterface;
use Denismitr\Translit\Translit;
use Doctrine\ORM\EntityManagerInterface;


class NomenclatureService implements NomenclatureServiceInterface
{
    const NOMENCLATURE_FILENAME = "import.xml";
    const PRICE_FILENAME = "offers.xml";

    private const INIT_MODE = 'init';
    private const FILE_MODE = 'file';
    private const IMPORT_MODE = 'import';

    private const PRICE_ROZN_CODE = '000000005';
    private const PRICE_OPT_CODE = '000000002';
    private const PRICE_FRANCHIZE_CODE = '000000026';

    private $sync1cPath;
    private $productBrandRepository;
    private $entityManager;
    private $catalogRepository;
    private $productRepository;
    private $siteVersionRepository;
    private $redisService;
    private $templating;

    public function __construct(
        string $sync1cPath,
        ProductBrandRepository $productBrandRepository,
        CatalogRepository $catalogRepository,
        EntityManagerInterface $entityManager,
        ProductRepository $productRepository,
        RedisService $redisService,
        \Twig_Environment $templating
    )
    {
        $this->sync1cPath = $sync1cPath;
        $this->productBrandRepository = $productBrandRepository;
        $this->entityManager = $entityManager;
        $this->catalogRepository = $catalogRepository;
        $this->productRepository = $productRepository;
        $this->redisService = $redisService;
        $this->templating = $templating;
    }

    public function syncNomenclature($mode, $fileName = NULL): string
    {
        $message = 'nomenclature sync error';

        switch ($mode) {
            case self::INIT_MODE:
                $this->deleteSyncFiles();
                $message = "zip=no"."\n". "file_limit=100000000"."\n";
                break;
            case self::FILE_MODE:
                if($this->saveSyncFile($fileName)) {
                    $message = "success"."\n".$fileName."\n";
                }
                break;
            case self::IMPORT_MODE:
                if($this->allowedFileName($fileName)) {
                    $this->parseFile($fileName);
                    $message = "success"."\n";
                }
                break;
        }

        return $message;
    }

    private function allowedFileName(string $fileName): bool
    {
        $allowedFileNames = [self::NOMENCLATURE_FILENAME, self::PRICE_FILENAME];

        if(in_array(trim($fileName), $allowedFileNames)) {
            return true;
        }
        else {
            return false;
        }
    }

    private function parseFile(string $fileName)
    {
        switch ($fileName) {
            case self::NOMENCLATURE_FILENAME:
                $this->nomenclatureParser($fileName);
                break;
            case self::PRICE_FILENAME:
                $this->priceParser($fileName);
                break;
        }
    }

    private function saveSyncFile(string $fileName): bool
    {

        if($this->allowedFileName($fileName)) {
            return copy("php://input", $this->sync1cPath.'/'.$fileName);
        }

        return false;
    }

    private function deleteSyncFiles()
    {
        $nomenclatureFullName = $this->sync1cPath.'/'.self::NOMENCLATURE_FILENAME;
        if(file_exists($nomenclatureFullName)) {
            unlink($nomenclatureFullName);
        }

        $priceFullName = $this->sync1cPath.'/'.self::PRICE_FILENAME;
        if(file_exists($priceFullName)) {
            unlink($priceFullName);
        }
    }

    private function deactivateProducts(){
        /** @var Product[] $products */
        $products = $this->productRepository->findAll();
        foreach ($products as $product){
            $product->setStatus(0);
            $this->entityManager->persist($product);
        }
        $this->entityManager->flush();
    }

    private function nomenclatureParser(string $fileName): bool
    {
        $fileFullName = $this->sync1cPath.'/'.$fileName;

        if (!file_exists($fileFullName)) {
            return false;
        }
        $fileSimpleXmlObj = simplexml_load_string(stripslashes(file_get_contents($fileFullName)));
        $fullSync = ('false' == (string) $fileSimpleXmlObj->Каталог['СодержитТолькоИзменения']);

        //consistency is important
        $brands = $fileSimpleXmlObj->Классификатор->Производители->Производитель;
        $brandsIds = $this->importBrands($brands);
        $this->entityManager->flush();
        $catalog = $fileSimpleXmlObj->Классификатор->Группы->Группа;
        $catalogIds = $this->importCatalog($catalog);

        $products = $fileSimpleXmlObj->Каталог->Товары->Товар;
        $now = new \DateTime();
        $hour = (int) $now->format('H');
        if($hour < 11){
            $this->deactivateProducts();
        }
        $this->importProducts($products);
        if($fullSync) {
            $this->disableBrandsNotInIds($brandsIds);
            $this->disableCategoriesNotInIds($catalogIds);
        }
        return true;
    }

    private function importBrands(\SimpleXMLElement $brands): array
    {
        $brandsIds = [];
        foreach ($brands as $brand) {

            if(!$this->isAllowedBrand($brand->Наименование)) {
                continue;
            }

            $syncCode = trim($brand->Код);
            $syncId = trim($brand->Ид);
            $brandModel = $this->productBrandRepository->findOneBy(['syncCode' => $syncCode]);

            if(!$brandModel) {
                $brandModel = new ProductBrand();

                $translitUrl = (new Translit($brand->Наименование))->getSlug();
                $brandModel->setUrl($translitUrl);
            }

            $brandModel->setSyncCode($syncCode);
            $brandModel->setSyncId($syncId);
            $brandModel->setName($brand->Наименование);
            $brandModel->setActive(1);
            $brandModel->setText('');
            $brandModel->setIsSale(false);

            $this->entityManager->persist($brandModel);
            $this->entityManager->flush();

            $brandsIds[] = $brandModel->getId();
        }

        return $brandsIds;
    }

    private function isAllowedBrand(string $brandName): bool
    {
        $disallowedBrands = [
            "рекламная продукция",
            "помойка"
        ];

        $brandNameLowCase = mb_strtolower($brandName);

        if(!in_array($brandNameLowCase, $disallowedBrands)) {
            return true;
        }
        else {
            return false;
        }
    }

    private function disableBrandsNotInIds(array $brandsIds)
    {
        /** @var  $brandsToDeactivate ProductBrand[]*/

        $brandsToDeactivate = $this->productBrandRepository->getAllExceptIds($brandsIds);
        foreach ($brandsToDeactivate as $brandToDeactivate) {
            if($brandToDeactivate->getName() == 'Разное') continue;
            $brandToDeactivate->setActive(0);

            $this->entityManager->persist($brandToDeactivate);
            $this->entityManager->flush();
        }

    }

    private function importCatalog(\SimpleXMLElement $catalog, $parentId = 0, $parentUrl = ''): array
    {
        $categoryIds = [];

        foreach ($catalog as $category) {
            if(!$this->isAllowedCategory($category->Наименование)){
                continue;
            }

            $syncCode = trim($category->Код);
            $syncId = trim($category->Ид);
            $catalogModel = $this->catalogRepository->findOneBy(['syncCode' => $syncCode]);

            $url = (new Translit($category->Наименование))->getSlug();
            $urlPath = $parentUrl.$url;

            if(!$catalogModel) {
                $catalogModel = new Catalog();

                $catalogModel->setUrl($url);
                $catalogModel->setUrlPath($urlPath);
                $catalogModel->setText('');
                $catalogModel->setSyncId($syncId);
                $catalogModel->setSyncCode($syncCode);
                $catalogModel->setShowBrand(false);

            }

            $catalogModel->setSyncCode($syncCode);
            $catalogModel->setName(trim($category->Наименование));
            if($parentId == 0){
                $parent = $this->catalogRepository->findOneBy(['name' => 'Каталог']);
            } else{
                $parent = $this->catalogRepository->find($parentId);
            }

            $catalogModel->setParent($parent);

            if($this->isInactiveCategory($category->Наименование)) {
                $catalogModel->setActive(false);
            }
            else {
                $catalogModel->setActive(true);
            }
            $this->entityManager->persist($catalogModel);
            $this->entityManager->flush();

            $categoryIds[] = $catalogModel->getId();

            if(isset($category->Группы)) {
                $childCategoryIds = $this->importCatalog($category->Группы->Группа, $catalogModel->getId(), $urlPath.'/');
                $categoryIds = array_merge($categoryIds, $childCategoryIds);
            }
        }

        $menu = $this->templating->render('layouts/_menu.html.twig',['lvl' => 1,'menu' => $this->catalogRepository]);
        $this->redisService->set('menu', $menu);

        return $categoryIds;
    }

    private function isInactiveCategory(string $categoryName): bool
    {
        $inactiveCategories = [
            "тмц и услуги",
            "акционные товары",
        ];

        if(in_array(mb_strtolower(trim($categoryName)), $inactiveCategories)) {
            return true;
        }
        else {
            return false;
        }
    }

    private function isAllowedCategory(string $categoryName): bool
    {
        $disallowedCategories = [
            "помойка",
            "услуги",
            "основные средства",
            "рекламная продукция",
            "на обработку"
        ];

        $categoryNameLowCase = mb_strtolower(trim($categoryName));

        if(!in_array($categoryNameLowCase, $disallowedCategories)) {
            return true;
        }

        return false;
    }

    private function disableCategoriesNotInIds(array $categoriesIds)
    {
        /** @var $categoriesToDeactivate Catalog[] */
        $categoriesToDeactivate = $this->catalogRepository->getAllExceptIds($categoriesIds);
        //dump($categoriesToDeactivate);die();

        foreach ($categoriesToDeactivate as $categoryToDeactivate) {
            if($categoryToDeactivate->getName() == 'Каталог'){continue;}
            $categoryToDeactivate->setActive(false);
            $categoryToDeactivate->setCatalogVersions(new ArrayCollection());
            $categoryToDeactivate->setTags(new ArrayCollection());
            $this->entityManager->persist($categoryToDeactivate);
        }
        $this->entityManager->flush();
    }

    private function importProducts(\SimpleXMLElement $products)
    {
        ini_set('memory_limit', '-1');
        set_time_limit(1200);

        foreach($products as $product) {
            if(!$this->isAllowedProduct($product)) {
                continue;
            }
            $syncCode = trim($product->Код);
            $syncId = trim($product->Ид);
            $productModel = $this->productRepository->findOneBy(['syncId' => $syncId , 'active' => true]);
            //$productModel = $this->productRepository->getLastUpdatedProductBySyncId($syncId);

            if(!$productModel) {
                $productModel = new Product();
                $productModel->setUrl($this->getProductUrl($product));
                $productModel->setCountYaroslavl(0);
                $productModel->setCountVolgograd(0);
                $productModel->setCountKirov(0);
            }

            $productModel->setCountSpb(0);
            $productModel->setCountMsk(0);
            $productModel->setCountUlyanovsk(0);
            $productModel->setName($this->getProductName($product));
            $productModel->setName1c($product->Наименование);
            $productModel->setFullName1c($this->getProduct1cFullName($product));
            $productModel->setLastParseTime(new \DateTime());
            $productModel->setText('');
            /*if($productModel->getActive() === NULL){
                $productModel->setActive(true);
            }*/
            $productModel->setActive(true);
            $productModel->setStatus(1);
            $productModel->setPackageType(trim($product->БазоваяЕдиница));
            $productModel->setSyncCode($syncCode);
            $productModel->setSyncId($syncId);
            $productModel->setArticle(trim($product->Артикул));
            $productModel->setDivType(str_replace(array('Номер'), array('Упаковка'), $product->ВидРазделения));
            $productModel->setDivValue($product->ЗначениеРазделения);
            $productModel->setValueAddedTax((int) $this->getProductValueAddedTax($product));
            $productModel->setEvent($this->getProductEvent($product));
            $productModel->setParent($this->getProductParentId($product));
            $productModel->setSearchTags($product->КлючевыеСлова);
            if(isset($product->Артикул3М)){
                //die((int)trim($product->Артикул3М));
                $productModel->setArticle3m(trim($product->Артикул3М));
            }
            //$productModel->setPreview(trim($product->Описание));
            $catalogSyncId = trim($product->Группы->Ид);
            $category = $this->catalogRepository->findOneBy(['syncId' => $catalogSyncId]);
            $productModel->setCatalog($category);

            $brandSyncCode = trim($product->Производители->Код);
            $brandSyncId = trim($product->Производители->Ид);
            /** @var $brand ProductBrand */
            $brand = $this->productBrandRepository->findOneBy(['syncId' => $brandSyncId]);
            if(!$brand) {die($brandSyncCode);}
            $productModel->setProductBrand($brand);
            $zeroSpb = true;
            $zeroMsk = true;
            for ($i = 1; $i <= 10; $i++) {
                $strName = 'НаименованиеСклада'.$i;
                $strVal = 'ОстатокПоСкладу'.$i;
                $stockName = $product->$strName;
                $stockValue = (int) $product->$strVal;
                switch($stockName) {
                    case 'Склад Санкт-Петербург':
                        $productModel->setCountSpb($stockValue);
                        $zeroSpb = false;
                        break;
                    case 'Склад Москва':
                        $productModel->setCountMsk($stockValue);
                        $zeroMsk = false;
                        break;
                    case 'Склад Димитровград':
                        $productModel->setCountUlyanovsk($stockValue);
                        break;


                }
            }
            if($zeroSpb){
                $productModel->setCountSpb(0);
            }
            if($zeroMsk){
                $productModel->setCountMsk(0);
            }
            $this->entityManager->persist($productModel);

        }
        $this->entityManager->flush();

    }

    private function getProductName(\SimpleXMLElement $product): string
    {
        return $product->НаименованиеДляИнтернетМагазина ? : $product->Наименование;
    }

    private function getProductUrl(\SimpleXMLElement $product)
    {
        $productName = $this->getProductName($product);
        $syncCode = preg_replace('/[^A-Za-z0-9\-]/', '', trim($product->Код));
        $baseProductUrl = (new Translit($productName))->getSlug();
        $productUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $baseProductUrl);
        $productUrlPostfix = 1;

        while($this->productRepository->findWithEqualUrlAndDifferentCode($productUrl, $syncCode)) {
            $productUrl = $baseProductUrl . '-' . $productUrlPostfix;
            $productUrlPostfix++;
        }

        return preg_replace('/[^A-Za-z0-9\-]/', '', $productUrl);
    }

    private function getProductParentId(\SimpleXMLElement $product): string
    {
        $productName = $this->getProductName($product);
        $parentName = (string) $product->ГоловнойТовар;
        $parentId = 0;

        if(!empty($parentName) &&
            $parentName != $productName &&
            $parentProductModel = $this->productRepository->findOneBy(['name' => $parentName])) {

            $parentId = $parentProductModel->getId();
        }

        return $parentId;
    }

    private function getProductValueAddedTax(\SimpleXMLElement $product): ?string
    {
        if(isset($product->СтавкиНалогов)){
            $taxName = $product->СтавкиНалогов->СтавкаНалога->Наименование;
            $taxValue = $product->СтавкиНалогов->СтавкаНалога->Ставка;
            $result = '';

            if('НДС' == $taxName and !empty($taxValue)) {
                $result = $taxValue;
            }
            return $result;
        } else {
            return NULL;
        }

    }

    private function getProduct1cFullName(\SimpleXMLElement $product): string
    {
        $product1cFullName = '';

        foreach ($product->ЗначенияРеквизитов->ЗначениеРеквизита as $props) {
            $propsName = $props->Наименование;
            $propsValue = $props->Значение;

            if('Полное наименование' === (string) $propsName) {
                $product1cFullName = $propsValue;
                break;
            }
        }

        return $product1cFullName;
    }

    private function getProductEvent(\SimpleXMLElement $product): string
    {
        $event = '';

        if('true' === (string) $product->Новинка) {
            $event = 'new';
        }
        elseif('true' === (string) $product->Распродажа) {
            $event = 'sale';
        }
        elseif('true' === (string) $product->ЛидерПродаж) {
            $event = 'top';
        }

        return $event;
    }

    private function isAllowedProduct($product)
    {
        $categorySyncCode = trim($product->Группы->Код);

        if((!isset($product->Статус) or
            trim($product->Статус) != 'Удален') and
            $this->catalogRepository->findOneBy(['syncCode' => $categorySyncCode])) {

            return true;
        }
        else {
            return false;
        }
    }

    private function priceParser(string $fileName): bool
    {
        $offerFullName = $this->sync1cPath.'/'.$fileName;
        $fileSimpleXmlObj = simplexml_load_string(stripslashes(file_get_contents($offerFullName)));
        $fullSync = ('false' == (string) $fileSimpleXmlObj->ПакетПредложений['СодержитТолькоИзменения']);

        //consistency is important
        $offers = $fileSimpleXmlObj->ПакетПредложений->Предложения->Предложение;
        foreach ($offers as $offer) {
            $priceOpt = 0;
            $priceFranchize = 0;
            $syncCode = trim((string) $offer->Код);
            $syncId = trim((string) $offer->Ид);
            //echo $syncCode;
            if((string) $offer->Статус === 'Удален') {
                $productToDisactivate = $this->productRepository->findOneBy(['syncCode' => $syncCode]);
                $productToDisactivate->setActive(false);
                $this->entityManager->persist($productToDisactivate);
                $this->entityManager->flush();
                continue;
            }
            if($offer->Цены->Цена){
                foreach($offer->Цены->Цена as $price) {
                    if(!trim($price->ЦенаЗаЕдиницу)) {
                        continue;
                    }
                    //обычная цена
                    $priceCode = self::PRICE_ROZN_CODE;
                    //цены для черной пятницы
                    //$priceCode = self::PRICE_OPT_CODE;
                    $priceOptCode = self::PRICE_OPT_CODE;
                    $priceFranchizeCode = self::PRICE_FRANCHIZE_CODE;
                    $priceRetailCode = self::PRICE_ROZN_CODE;
                    switch((string) $price->Код) {
                        case $priceCode:
                            $priceRet = (int) $price->ЦенаЗаЕдиницу;
                            break;
                        case $priceFranchizeCode:
                            $priceFranchize = (int) $price->ЦенаЗаЕдиницу;
                            break;
                        case $priceRetailCode:
                            $priceRetail = (int) $price->ЦенаЗаЕдиницу;
                            break;
                        case $priceOptCode:
                            $priceOpt = (int) $price->ЦенаЗаЕдиницу;
                            break;
                    }
                }
            }
            if(empty($priceRet)) {
                continue;
            }
            $product = $this->productRepository->findOneBy(['syncId' => $syncId]);
            if($product){
                $product->setPriceRet($priceRet);
                $product->setPriceOpt($priceOpt);
                $product->setPriceFranchize($priceFranchize);
                //$product->setPriceRetail($priceRetail);
                $this->entityManager->persist($product);
            }

        }
        $this->entityManager->flush();
        return true;
    }
}