<?php
/**
 * Created by PhpStorm.
 * User: kip
 * Date: 29/05/18
 * Time: 13:04
 */

namespace App\Services\Sync1c;

use App\Services\Sync1c\AuthServiceInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthService
{
    private const CHECK_AUTH_MODE = 'checkauth';

    private $sync1cLogin;
    private $sync1cPassword;

    public function __construct(
        string $sync1cLogin,
        string $sync1cPassword
    )
    {
        $this->sync1cLogin = $sync1cLogin;
        $this->sync1cPassword = $sync1cPassword;
    }

    public function hasSuccessAuth(string $passwordCookie = NULL, string $mode = NULL): bool
    {
        if((self::CHECK_AUTH_MODE === $mode and
            $this->sync1cPassword === $passwordCookie)) {

            return true;
        }

        return false;
    }

    public function authenticateClient(): bool
    {
        if (isset($_SERVER['PHP_AUTH_USER']) and
            trim($_SERVER['PHP_AUTH_USER']) === $this->sync1cLogin and
            trim($_SERVER['PHP_AUTH_PW']) === $this->sync1cPassword) {

            return true;
        }

        return false;
    }

    public function getAuthPassword(): string
    {
        return $this->sync1cPassword;
    }
}