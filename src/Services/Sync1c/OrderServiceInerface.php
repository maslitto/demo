<?php
/**
 * Created by PhpStorm.
 * User: kip
 * Date: 28/05/18
 * Time: 16:49
 */

namespace App\Services\Sync1c;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface OrderServiceInerface
{
    public function getXmlOrders(string $startTime): string;
}