<?php
/**
 * Created by PhpStorm.
 * User: kip
 * Date: 10.04.18
 * Time: 12:57
 */

namespace App\Services;

use Symfony\Component\Yaml\Yaml;
use App\Services\ConfigServiceInterface;

class ConfigService implements ConfigServiceInterface
{

    private $rootPath;

    public function __construct(string $rootPath)
    {
        $this->rootPath = $rootPath;
    }

    public function getConfig(string $config)
    {
        return  Yaml::parseFile($this->rootPath.'/config/'. $config .'.yaml', Yaml::PARSE_OBJECT_FOR_MAP);
    }

    public function getConstants()
    {
        return Yaml::parse(
            file_get_contents($this->rootPath.'/config/constants.yaml')
        );
    }

    public function getBankAuthData()
    {
        return Yaml::parse(
            file_get_contents($this->rootPath.'/config/bank_auth.yaml')
        );
    }
}