<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 02.03.18
 * Time: 10:07
 */

namespace App\Services;


interface VersionServiceInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @param int $versionId
     */
    public function setId(int $versionId): void;

    /**
     * @return string
     */
    public function getEmail(): ?string;

    /**
     * @return string
     */
    public function getCity(): ?string;

    /**
     * @return bool
     */
    public function isMobile():bool;

    /**
     * @return string
     */
    public function getPhone(): ?string;

    /**
     * @return string
     */
    public function getShortPhone(): ?string;
    /**
     * @return string
     */
    public function getRussiaPhone(): ?string;

    /**
     * @return string
     */
    public function getWorkTime(): ?string;

    /**
     * @return string
     */
    public function getMapLInk(): ?string;

    /**
     * @return string
     */
    public function getAddress(): ?string;

    /**
     * @return string
     */
    public function getVk(): ?string;

    /**
     * @return string
     */
    public function getFb(): ?string;

    /**
     * @return string
     */
    public function getFooterLine1(): ?string;

    /**
     * @return string
     */
    public function getFooterLine2(): ?string;
}