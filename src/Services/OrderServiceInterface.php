<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 21.03.18
 * Time: 10:20
 */

namespace App\Services;


use App\Entity\Cart;
use App\Entity\Order;
use App\Repository\ManagerRepository;

interface OrderServiceInterface
{
    public function createOrder(array $data ,Cart $cart): Order;
}