<?php
/**
 * Created by PhpStorm.
 * User: veteran
 * Date: 15.02.18
 * Time: 23:57
 */

namespace App\Services;

use App\Repository\ProductViewRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Repository\OrderProductRepository;
use App\Entity\ProductView;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ProductService implements ProductServiceInterface
{
    private $session;
    private $productRepository;
    private $productViewRepository;
    private $entityManager;
    private $versionService;

    public function __construct(
        ProductRepository $productRepository,
        ProductViewRepository $productViewRepository,
        EntityManagerInterface $entityManager,
        SessionInterface $session,
        VersionService $versionService
    )
    {
        $this->productRepository = $productRepository;
        $this->productViewRepository = $productViewRepository;
        $this->entityManager = $entityManager;
        $this->session = $session;
        $this->versionService = $versionService;
    }

    /**
     * Returns recently viewed productRepository from session
     * @return array
     */

    public function getRecentlyViewedProducts(int $limit = 4) : array
    {
        $recent = [];
        if($this->session->has('recent')){
            $ids = $this->session->get('recent');
            $recent = $this->productRepository->findByIds($ids, $limit);
        }
        return $recent;
    }

    /**
     * This function push product to session (recently viewed products)
     * @param $product
     * @return array
     */
    public function pushToRecentlyViewedProducts(Product $product) : array
    {
        $recent = [];
        if($this->session->has('recent')){
            $recent = $this->session->get('recent');
            if(!in_array($product->getId(), $recent)){
                array_push($recent, $product->getId());
                $this->session->set('recent', $recent);
            }
        } else{
            array_push($recent, $product->getId());
            $this->session->set('recent', $recent);
        }
        return $recent;
    }

    /**
     * This function increment Product Views
     * @param $product
     * @return array
     */
    public function incrementProductViews(Product $product) : void
    {
        $recent = $this->session->get('recent',[]);
        if(!in_array($product->getId(), $recent)){
            $count = (int) $product->getViewsCount();
            $product->setViewsCount($count + 1);
            $this->entityManager->persist($product);
            $this->entityManager->flush();
        }
    }

    /**
     * @param Product $product
     * @return array
     */
    public function getSimilarProducts(Product $product) : array
    {
        $versionId = $this->versionService->getSiteVersion()->getId();
        $field = Product::getCountField($versionId);
        $qb = $this->productRepository->filterProductsQuery(null, [],$versionId);
        $qb
            ->setParameter('catalogId',$product->getCatalogId())
            ->setParameter('id',$product->getId())
            ->andWhere('p.catalogId = :catalogId')
            ->andWhere('p.id <> :id')
            ->andWhere("p.$field > 0")
            ->setMaxResults(4);
        $similar = $qb->getQuery()->getResult();
        return $similar;
    }

    /**
     * @param Product $product
     * @return array
     */
    public function getAlsoBoughtProducts(Product $product) : array
    {
        return $this->productRepository->findAlsoBoughtProducts($product , 4);
    }




}
