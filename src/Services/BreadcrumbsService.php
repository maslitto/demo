<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 14.03.18
 * Time: 12:31
 */

namespace App\Services;


use App\Repository\CatalogRepository;
use App\Repository\ProductBrandRepository;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\RequestStack;

class BreadcrumbsService
{
    private $request;
    /**
     * @var CatalogRepository
     */
    private $catalogRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    private $productBrandRepository;
    /**
     * @return null|\Symfony\Component\HttpFoundation\Request
     */
    public function getRequest(): ?\Symfony\Component\HttpFoundation\Request
    {
        return $this->request;
    }
    /**
     * BreadcrumbsService constructor.
     */
    public function __construct(
        RequestStack $requestStack,
        CatalogRepository $catalogRepository,
        ProductRepository $productRepository,
        ProductBrandRepository $productBrandRepository
    )
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->catalogRepository = $catalogRepository;
        $this->productRepository = $productRepository;
        $this->productBrandRepository = $productBrandRepository;
    }

    public function getBreadCrumbs()
    {
        if($this->request){
            $uri = $this->request->getRequestUri();
            $path = explode('/',trim($uri,'/'));
            $breadcrumbs[] = (object) ['name' => 'Главная','href' => '/'];
            switch ($path[0]){
                case 'catalog':
                    $breadcrumbs[] = (object) ['name' => 'Каталог','href' => '/catalog/'];
                    $breadcrumbs = $this->getCatalogCrumbs($breadcrumbs, $path);
                    break;
                case 'reviews':
                    $breadcrumbs[] = (object) ['name' => 'Отзывы','href' => '/reviews/'];
                    break;
                case 'price':
                    $breadcrumbs[] = (object) ['name' => 'Прайс-лист','href' => '/price/'];
                    break;
                case 'about':
                    $breadcrumbs[] = (object) ['name' => 'О компании','href' => '/about/'];
                    break;
                case 'contacts':
                    $breadcrumbs[] = (object) ['name' => 'Контакты','href' => '/contacts/'];
                    break;
                case 'help':
                    switch ($path[1]){
                        case 'delivery':
                            $breadcrumbs[] = (object) ['name' => 'Доставка','href' => '/help/delivery/'];
                            break;
                        case 'payment':
                            $breadcrumbs[] = (object) ['name' => 'Оплата','href' => '/help/payment/'];
                            break;
                        case 'dogovor-publichnoy-ofertyi-postavki':
                            $breadcrumbs[] = (object) ['name' => 'ДОГОВОР ПУБЛИЧНОЙ ОФЕРТЫ','href' => '/help/dogovor-publichnoy-ofertyi-postavki/'];
                            break;
                        case 'politika-konfidencialnosti':
                            $breadcrumbs[] = (object) ['name' => 'Политика конфиденциальности','href' => '/help/politika-konfidencialnosti/'];
                            break;

                    }

                    break;

            }
            //dump($path);die();
        }

        ;
        return $breadcrumbs;
    }

    private function getCatalogCrumbs(array $breadcrumbs, array $path)
    {
        unset($path[0]);
        //dump($path);die();

        foreach ($path as $item) {
            if($catalog = $this->catalogRepository->findOneBy(['url' => $item, 'active'=> true])){
                $breadcrumbs[] = (object) ['name' => $catalog->getName(),'href' =>'/catalog/'. $catalog->getUrlPath()];
            } elseif ($product = $this->productRepository->findOneBy(['url' => $item, 'active'=> true])){
                $parent = $product->getCatalog();
                $productPath[] = (object) ['name' => $parent->getName(),'href' => '/catalog/'.$parent->getUrlPath()];
                while( $parent->getParent()->getParentId() !== NULL)
                {
                    $parent = $parent->getParent();
                    $productPath[] = (object) ['name' => $parent->getName(),'href' => '/catalog/'.$parent->getUrlPath()];
                };
                foreach (array_reverse($productPath) as $item) {
                    $breadcrumbs[] = $item;
                }
                break;
            }

        }
        return $breadcrumbs;
    }
}