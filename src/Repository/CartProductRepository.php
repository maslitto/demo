<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\Cart;
use App\Entity\CartProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class CartProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CartProduct::class);
    }

    public function checkProductInCart(string $hashId, int $productId)
    {
        $qb = $this->createQueryBuilder('cp')
            ->leftJoin('App:Cart', 'c',   'WITH',  'c.id = cp.cartId')
            ->where('cp.productId = :productId')
            ->andWhere('c.hashId = :hashId')
            ->setParameter('productId', $productId)
            ->setParameter('hashId', $hashId);
        try {
            $cartProduct = $qb->getQuery()->getSingleResult();
            return $cartProduct;
        } catch (NoResultException $e) {
            return false;
        } catch (NonUniqueResultException $e){
            return false;
        }
    }
}
