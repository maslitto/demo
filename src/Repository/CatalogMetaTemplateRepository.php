<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\CatalogMetaTemplate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class CatalogMetaTemplateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CatalogMetaTemplate::class);
    }
    public function getCatalogMetaTemplatesQueryBuilder() : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('cmr');
        $qb = $queryBuilder->addSelect('cmr');
        return $qb->orderBy('cmr.id','ASC');
    }


    public function getCatalogMetaTemplatesPaginated(int $page = 1)
    {
        $qb = $this->getCatalogMetaTemplatesQueryBuilder();
        $query = $qb->getQuery();
        return $this->createPaginator($query, $page, CatalogMetaTemplate::NUM_ITEMS);
    }

    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

}
