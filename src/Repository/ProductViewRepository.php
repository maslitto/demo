<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\ProductView;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ManagerRegistry;
//

class ProductViewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductView::class);
    }

    public function getLastPeriodViews(int $productId)
    {
        $qb = $this->createQueryBuilder('pv')
            ->where('pv.productId = :productId')
            ->andWhere('pv.timeCreate >= :today')
            ->setParameter('productId', $productId)
            ->setParameter('today', \Carbon\Carbon::now()->addMonth(-6))
        ;
        $result = $qb->getQuery()->getResult();
        if(is_array($result) && sizeOf($result) > 0){
            return $result[0];
        } else return NULL;
    }
}
