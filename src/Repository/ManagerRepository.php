<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\Manager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class ManagerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Manager::class);
    }

    public function getManagersQueryBuilder(array $filter, int $versionId) : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('m');
        $qb = $queryBuilder->addSelect('m');
        return $qb->orderBy('m.timeCreate','DESC');
    }


    public function getManagersPaginated(array $filter, int $page, int $versionId,  int $perPage = 20)
    {
        $qb = $this->getManagersQueryBuilder($filter, $versionId);
        $query = $qb->getQuery();
        return $this->createPaginator($query, $page, $perPage);
    }

    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }
    public function getCurrentManager(int $versionId)
    {
        $qb = $this->createQueryBuilder('m')
            ->addSelect('m')
            ->where('m.versionId = :versionId')
            ->andWhere('m.active = true')
            ->setParameter('versionId', $versionId)
            ->orderBy('m.lastOrder','ASC');
        return $qb->getQuery()->getResult()[0];
    }

    public function getManagersByVersion(int $versionId)
    {
        $yaroslavlIds = [5,6,7,8,9];
        $mainIds = [1,2];
        $qb = $this->createQueryBuilder('m')
            ->addSelect('m')
            ->where('m.active = true');
    ;
        if(in_array($versionId, $yaroslavlIds) ){
            $qb
                ->andWhere('m.versionId IN(:versionIds)')
                ->setParameter('versionIds', $yaroslavlIds);
        } elseif(in_array($versionId, $mainIds) ){
            $qb
                ->andWhere('m.versionId IN(:versionIds)')
                ->setParameter('versionIds', $mainIds);
        }
        else {
            $qb
                ->andWhere('m.versionId = :versionId')
                ->setParameter('versionId', $versionId);
        }
        return $qb->getQuery()->getResult();
    }
}
