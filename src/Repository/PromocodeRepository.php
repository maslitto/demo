<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\Promocode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class PromocodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Promocode::class);
    }

    public function getPromocodesQueryBuilder(array $filter, int $versionId) : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('p');
        $qb = $queryBuilder->addSelect('p');

        return $qb->orderBy('p.disposable','ASC');
    }

    public function getPromocodesPaginated(int $page, int $versionId,  int $perPage = 20, array $filter = [])
    {
        $qb = $this->getPromocodesQueryBuilder($filter, $versionId);
        $query = $qb->getQuery();
        return $this->createPaginator($query, $page, $perPage);
    }

    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    //Возвращаем все группы промокодов
    public function getPromocodesAllGroups(){
      $conn = $this->getEntityManager()->getConnection();

      $sql = '
      SELECT DISTINCT group_name
      FROM promocodes
      WHERE group_name IS NOT NULL
      ';

      $stmt = $conn->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll();
    }

    //Возвращаем сериализированный массив\обьект для промокодов(групп)
    public function getPromocodesByGroup( string $groupName){
      $qb = $this->createQueryBuilder('p');
      $qb->addSelect('p')
        ->andWhere('p.groupName = :groupName')
        ->setParameter('groupName',$groupName);
      ;
      $results = $qb->getQuery()->getResult();

      return $results;
    }

    //Возвращаем массив|обьект для использованных промокодов по группе
    public function getUsedPromocodesByGroup( string $groupName ){

      $conn = $this->getEntityManager()->getConnection();

      $sql = '
      SELECT orders.username , orders.email, orders.time_create, orders.promo_code, orders.price
      FROM promocodes as promocodes
      INNER JOIN orders as orders ON orders.promo_code = promocodes.code
      WHERE promocodes.active = 0 AND promocodes.group_name = :groupName
      ';
      //
      $stmt = $conn->prepare($sql);
      $stmt->execute(['groupName'=>$groupName]);
      return $stmt->fetchAll();


    }
}
