<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class TagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tag::class);
    }

    public function getTagsByIds(array $ids)
    {
        $qb = $this->createQueryBuilder('t');
        $qb->where('t.id IN (:ids)')->setParameter('ids', $ids);
        return $qb->getQuery()->getResult();
    }
    /**
     * @param array $filter
     * @param int $versionId
     * @return QueryBuilder
     */
    public function getTagsQueryBuilder(array $filter = NULL, int $versionId) : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('t');
        $qb = $queryBuilder->addSelect('t');

        return $qb->orderBy('t.timeCreate','DESC');
    }

    
    /**
     * @param array $filter
     * @param int $page
     * @param int $versionId
     * @param int $perPage
     * @return Pagerfanta
     */
    public function getTagsPaginated(array $filter = null, int $page, int $versionId,  int $perPage = 20)
    {
        $qb = $this->getTagsQueryBuilder($filter, $versionId);
        $query = $qb->getQuery();
        return $this->createPaginator($query, $page, $perPage);
    }

    /**
     * @param Query $query
     * @param int $page
     * @param int $perPage
     * @return Pagerfanta
     */
    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }
}
