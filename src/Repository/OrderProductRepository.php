<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\OrderProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for Product information.
 *
 */
class OrderProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderProduct::class);
    }

    public function findTopAlsoBoughtProductIds(string $productId, int $limit): array
    {
        $orderIds = $this->createQueryBuilder('oc')
            ->select('oc.orderId')
            ->where('oc.productId = :id')
            ->setParameter('id', $productId)
            ->getQuery()
            ->getArrayResult();
        $productIds = $this->createQueryBuilder('oc')
            ->select('oc.productId')
            ->andWhere("oc.orderId IN(:ids)")
            ->andWhere("oc.productId <> :id")
            ->setParameter('ids', $orderIds)
            ->setParameter('id', $productId)
            ->groupBy("oc.productId")
            ->having("COUNT(oc.productId) > 1")
            ->orderBy('COUNT(oc.productId)', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
        return $productIds;

    }

}
