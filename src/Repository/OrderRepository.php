<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\Yaml\Yaml;

class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * Get array ['date','count'] by last $period days and site version
     * @param int $period
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getOrdersCountByDaysAndVersion(int $period,int $versionId)
    {
        $yaroslavlIds = [5,6,7,8,9];
        if(in_array($versionId, $yaroslavlIds)){
            $in = $yaroslavlIds;
        } else{
            $in = [$versionId];
        }
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
SELECT count(*) as count, DATE_FORMAT(time_create, "%d.%m(%a)") as date
FROM orders
WHERE DATE(time_create) >= (DATE(NOW()) - INTERVAL :period DAY) AND version_id IN(:ids)
GROUP BY DATE(time_create)
ORDER BY time_create ASC';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['period'=> $period, 'ids' => implode($in,',')]);
        return $stmt->fetchAll();
    }

    /**
     * Get array ['date','count'] by last $period days by all site versions
     * @param int $period
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getOrdersCountByDaysAllVersions(int $period)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
SELECT count(*) as count, DATE_FORMAT(time_create, "%d.%m(%a)") as date
FROM orders
WHERE DATE(time_create) >= (DATE(NOW()) - INTERVAL :period DAY)
GROUP BY DATE(time_create)
ORDER BY time_create ASC';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['period'=> $period]);
        return $stmt->fetchAll();
    }
    public function getOrdersQueryBuilder(array $filter = [], int $versionId = NULL) : QueryBuilder
    {
        $yaroslavlIds = [5,6,7,8,9];
        $mainIds = [1,2];
        $queryBuilder = $this->createQueryBuilder('o');
        $qb = $queryBuilder->addSelect('o');
        if(isset($filter['managerId'])){
            $qb->andWhere('o.managerId = :managerId')
                ->setParameter('managerId', $filter['managerId']);
        }
        if(isset($filter['query'])){
            $qb->andWhere('o.encryptedId LIKE :query OR o.phone LIKE :query OR o.email LIKE :query  OR o.username LIKE :query')
                ->setParameter('query', '%'.$filter['query'].'%');
        }
        if($versionId){
            if(in_array($versionId, $yaroslavlIds)){
                $qb->andWhere('o.versionId  IN(:versionIds)')
                    ->setParameter('versionIds', $yaroslavlIds);
            }elseif(in_array($versionId, $mainIds)){
                $qb->andWhere('o.versionId  IN(:versionIds)')
                    ->setParameter('versionIds', $mainIds);
            }
            else{
                $qb->andWhere('o.versionId = :versionId')
                    ->setParameter('versionId', $versionId);
            }

        }
        return $qb->orderBy('o.timeCreate','DESC');
    }

    public function getOrdersPaginated(array $filter, int $page, int $versionId,  int $perPage = 20)
    {
        $qb = $this->getOrdersQueryBuilder($filter, $versionId);
        $query = $qb->getQuery();
        return $this->createPaginator($query, $page, $perPage);
    }

    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    public function getOrdersFor1cExchange(int $versionId, \DateTime $fromDateTime)
    {
        //Yaml::parseFile()
        $yarVersions = [5,6,7,8,9];
        $queryBuilder = $this->createQueryBuilder('o');
        $qb = $queryBuilder->addSelect('o');
        $qb
            ->where('o.timeCreate > :fromDateTime')
            ->setParameter('fromDateTime', $fromDateTime);
        if(in_array($versionId, $yarVersions) ){
            $qb
                ->andWhere("o.versionId IN(:versionIds)")
                ->setParameter('versionIds', $yarVersions);
        } else {
            $qb
                ->andWhere('o.versionId = :versionId')
                ->setParameter('versionId', $versionId);
        }
        return $qb->getQuery()->getResult();

    }

    public function getEmails()
    {
        $qb = $this->createQueryBuilder('o')->addSelect('o');

        $qb->select('DISTINCT(o.email)')->expr()->neq('o.email','');
        return $qb->getQuery()->getResult();
    }

    //Возвращаем данные о использованных промокодах в заказах
    public function getPromocodeWithOrders()
    {

        $this_day = date('Y-m-d');
        $date_limits = date('Y-m-d H:i:s', strtotime($this_day . " -1 days"));

        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT promocodes.group_name , promocodes.code, promocodes.disposable
        FROM promocodes as promocodes
        INNER JOIN orders as orders ON orders.promo_code = promocodes.code
        WHERE orders.time_create >= :date_limit
        ';
        //
        $stmt = $conn->prepare($sql);
        $stmt->execute(['date_limit'=>$date_limits]);
        return $stmt->fetchAll();

    }
}
