<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }
    public function getUsersQueryBuilder(array $filter, int $versionId) : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('u');
        $qb = $queryBuilder->addSelect('u');
        if(isset($filter['query'])){
            $qb->leftJoin('App:UserProfile', 'up',   'WITH',  'u.id = up.userId')
                ->where('u.email LIKE :query')
                ->orWhere('up.name LIKE :query')
                ->orWhere('up.surname LIKE :query')
                ->orWhere('up.phone LIKE :query')
                ->orWhere('up.skype LIKE :query')
                ->orWhere('up.city LIKE :query')
                ->orWhere('up.comInn LIKE :query')
                ->setParameter('query', '%'.$filter['query'].'%');
        }
        return $qb->orderBy('u.timeCreate','DESC');
    }


    public function getUsersPaginated(array $filter, int $page, int $versionId,  int $perPage = 20)
    {
        $qb = $this->getUsersQueryBuilder($filter, $versionId);
        $query = $qb->getQuery();
        return $this->createPaginator($query, $page, $perPage);
    }

    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }
}
