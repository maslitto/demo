<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\ProductReview;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class ProductReviewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductReview::class);
    }

    /**
     * @param array $filter
     * @param int $versionId
     * @return QueryBuilder
     */
    public function getProductReviewsQueryBuilder(array $filter = NULL, int $versionId) : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('r');
        $qb = $queryBuilder->addSelect('r');
        /*if($filter == NULL){
            $qb->where('r.status = 1');
        }*/
        return $qb->orderBy('r.timeCreate','DESC');
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $versionId
     * @param int $perPage
     * @return Pagerfanta
     */
    public function getProductReviewsPaginated(array $filter = null, int $page, int $versionId,  int $perPage = 20)
    {
        $qb = $this->getProductReviewsQueryBuilder($filter, $versionId);
        $query = $qb->getQuery();
        return $this->createPaginator($query, $page, $perPage);
    }

    /**
     * @param Query $query
     * @param int $page
     * @param int $perPage
     * @return Pagerfanta
     */
    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }
}
