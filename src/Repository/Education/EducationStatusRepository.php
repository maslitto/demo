<?php

namespace App\Repository\Education;

use App\Entity\Education\EducationStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class EducationStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EducationStatus::class);
    }

    public function getEducationsPaginated(array $filter, int $page, int $perPage = 15)
    {
        $qb = $this->getEducationsQueryBuilder($filter);
        $query = $qb->getQuery();
        return $this->createPaginator($query, $page, $perPage);
    }

    public function getEducationsQueryBuilder(array $filter) : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('e');
        $qb = $queryBuilder->addSelect('e');
        return $qb->orderBy('e.timeCreate','DESC');
    }

    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }


}
