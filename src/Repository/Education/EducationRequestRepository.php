<?php

namespace App\Repository\Education;

use App\Entity\Education\EducationRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class EducationRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EducationRequest::class);
    }

    public function getEducationRequestsPaginated(array $filter, int $page, int $perPage = 50)
    {
        $qb = $this->getEducationRequestQueryBuilder($filter);
        $query = $qb->getQuery();
        return $this->createPaginator($query, $page, $perPage);
    }

    public function getEducationRequestQueryBuilder(array $filter) : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('er');
        $qb = $queryBuilder->addSelect('er');
        return $qb->orderBy('er.id','DESC');
    }

    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }


}
