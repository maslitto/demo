<?php

namespace App\Repository\Education;

use App\Entity\Education\EducationDirection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class EducationDirectionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EducationDirection::class);
    }

    public function getEducationDirectionsPaginated(array $filter, int $page, int $perPage = 15)
    {
        $qb = $this->getEducationDirectionsQueryBuilder($filter);
        $query = $qb->getQuery();
        return $this->createPaginator($query, $page, $perPage);
    }

    public function getEducationDirectionsQueryBuilder(array $filter) : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('e');
        $qb = $queryBuilder->addSelect('e');
        return $qb->orderBy('e.id','DESC');
    }

    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }


}
