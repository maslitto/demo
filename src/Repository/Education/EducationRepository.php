<?php

namespace App\Repository\Education;

use App\Entity\Education\Education;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class EducationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Education::class);
    }
    public function getEducationAsArray(int $id)
    {
        $RAW_QUERY = 'SELECT * FROM educations where id = :id ;';
        $statement = $this->getEntityManager()->getConnection()->prepare($RAW_QUERY);
        // Set parameters
        $statement->bindValue('id', $id);
        $statement->execute();

        $result = $statement->fetchAll();
        return $result[0];
    }
    public function getEducationsPaginated(array $filter, int $page, int $perPage = 15)
    {
        $qb = $this->getEducationsQueryBuilder($filter);
        $query = $qb->getQuery();
        return $this->createPaginator($query, $page, $perPage);
    }

    public function getEducationsQueryBuilder(array $filter = []) : QueryBuilder
    {
        $qb = $this->createQueryBuilder('e');

        if(isset($filter['query'])){
            $qb->andWhere('e.name LIKE :query OR e.lectorFio LIKE :query OR e.program LIKE :query')
                ->setParameter('query','%'.$filter['query'].'%');
        }
        if(isset($filter['types'])){
            $qb->andWhere('e.educationTypeId IN (:types)')->setParameter('types',$filter['types']);
        }
        if(isset($filter['directions'])){
            $qb->andWhere('e.educationDirectionId IN (:directions)')->setParameter('directions',$filter['directions']);
        }
        if(isset($filter['free'])){
            $qb->andWhere('e.price = 0');
        }
        if(isset($filter['cities'])){
            $qb->andWhere('e.city IN (:cities)')->setParameter('cities',$filter['cities']);
        }
        if(isset($filter['date'])){
            $qb->andWhere('e.dateStart >= :dateStart')->setParameter('dateStart',$filter['date']);
        }
        if(isset($filter['admin'])){
            $qb->addOrderBy('e.active','ASC');
            //$qb->addOrderBy('e.educationStatus','ASC');
            $qb->addOrderBy('e.id','DESC');

        } else{
            $qb->orderBy('e.dateStart','ASC');
            $qb
                ->andWhere('e.active = true')
                ->setParameter('now',new \DateTime('-1 day'))
                ->andWhere('e.dateStart >= :now');
        }
        return $qb;
    }

    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    public function getDatesString():string
    {
        $qb = $this->getEducationsQueryBuilder();
        /** @var Education[] $educations */
        $educations = $qb->getQuery()->getResult();
        if(sizeOf($educations)){
            $dates = '[';
            foreach ($educations as $education){
                $dates .= '"'.$education->getDateStart()->format('Y-m-d').'",';
            }
            $dates = substr($dates, 0,-1);
            $dates = $dates.']';
        } else {
            $dates = '[]';
        }
        return $dates;
    }

    public function getCities(): array
    {
        $qb = $this->getEducationsQueryBuilder();
        $qb->select('distinct(e.city)');
        $qb->andWhere('e.city <> :empty ')->setParameter('empty','')->orderBy('e.city','ASC');
        /** @var Education[] $educations */
        $educations = $qb->getQuery()->getResult();
        foreach ($educations as $k=> $education) {
            $cities[] = $education[1];
        }
        return $cities;
    }

}
