<?php
/**
 * Created by PhpStorm.
 * User: kip
 * Date: 11.04.18
 * Time: 12:18
 */

namespace App\Repository;

use App\Entity\OrderPayment;
use App\Entity\ResetPassword;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class OrderPaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderPayment::class);
    }
}
