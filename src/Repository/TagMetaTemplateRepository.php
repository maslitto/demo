<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\TagMetaTemplate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class TagMetaTemplateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TagMetaTemplate::class);
    }
    public function getTagMetaTemplatesQueryBuilder() : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('tmr');
        $qb = $queryBuilder->addSelect('tmr');
        return $qb->orderBy('tmr.id','ASC');
    }


    public function getTagMetaTemplatesPaginated(int $page = 1)
    {
        $qb = $this->getTagMetaTemplatesQueryBuilder();
        $query = $qb->getQuery();
        return $this->createPaginator($query, $page, TagMetaTemplate::NUM_ITEMS);
    }

    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

}
