<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\Catalog;
use App\Entity\MetaInterface;
use App\Entity\Product;
use App\Entity\ProductBrand;
use App\Entity\Tag;
use App\Services\ElasticService;
use App\Services\VersionService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use FOS\ElasticaBundle\Finder\FinderInterface;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use App\Repository\OrderProductRepository;
use App\Repository\CatalogRepository;
use App\Repository\ProductBrandRepository;
use Denismitr\Translit\Translit;
use Doctrine\ORM\QueryBuilder as QueryBuilder;
/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for Product information.
 *
 */
class ProductRepository extends ServiceEntityRepository
{
    //TODO добавить версии в репозиторий
    /**
     * @var \App\Repository\OrderProductRepository
     */
    private $orderProductRepository;
    private $catalogRepository;
    private $versionService;
    private $elasticService;

    /**
     * ProductRepository constructor.
     * @param ManagerRegistry $registry
     * @param \App\Repository\OrderProductRepository $orderProductRepository
     * @param \App\Repository\CatalogRepository $catalogRepository
     */
    public function __construct(
        ManagerRegistry $registry,
        OrderProductRepository $orderProductRepository,
        CatalogRepository $catalogRepository,
        VersionService $versionService,
        ElasticService $elasticService
    )
    {
        $this->orderProductRepository = $orderProductRepository;
        $this->catalogRepository = $catalogRepository;
        $this->versionService = $versionService;
        $this->elasticService = $elasticService;

        parent::__construct($registry, Product::class);
    }

    /**
     * @param string $syncId
     * @return mixed|null
     */
    public function getLastUpdatedProductBySyncId(string $syncId)
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->addSelect('p')
            ->andWhere('p.syncId = :syncId')
            ->setParameter('syncId', $syncId)
            ->orderBy('p.lastParseTime','DESC')
            ->setMaxResults(1)
        ;
        $results = $qb->getQuery()->getResult();
        if(count($results) > 0){
            return $results[0];
        } else {
            return null;
        }
        //return $qb->getQuery()->getSingleResult();
    }
    /**
     * @return QueryBuilder
     */
    public function getActiveProductsQueryBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('p');
        $qb = $queryBuilder
            ->addSelect('p')
            ->andWhere('p.priceRet > 0')
            ->andWhere('p.status > 0')
            ->andWhere('p.active = 1');
        return $qb;
    }

    public function getActiveProducts()
    {
        $qb = $this->getActiveProductsQueryBuilder();
        $products = $qb->getQuery()->getResult();
        return $products;
    }
    /**
     * @param MetaInterface $page
     * @param array $filter
     * @param int $versionId
     * @return QueryBuilder
     */
    public function filterProductsQuery(MetaInterface $page = NULL, array $filter, int $versionId): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('p');
        $qb = $queryBuilder
            ->addSelect('p');
            //->leftJoin('App:ProductVersion', 'v', 'WITH', 'p.id = v.productId')
            //->leftJoin('App:ProductView', 'pvs', 'WITH', 'p.id = pvs.productId')
            //->where('v.versionId = :versionId')
        if ($page !== NULL) {

            if ($page instanceof Catalog) {

                //get all parents ids
                $ids = $this->catalogRepository->getActiveChildrenIds($page);
                $qb->andWhere('p.catalogId IN (:ids)')->setParameter('ids', $ids);

            } elseif ($page instanceof ProductBrand) {
                $products = $page->getProducts();
                $qb->andWhere('p.id IN (:ids)')->setParameter('ids', $products);

            } elseif ($page instanceof Tag) {

                $products = $page->getProducts();
                $qb->andWhere('p.id IN (:ids)')->setParameter('ids', $products);
            }
        }
        /*if (isset($filter['versionId'])) {
            $qb->setParameter('versionId', $filter['versionId']);
        } else {
            $qb->setParameter('versionId', $versionId);
        }*/
        if(isset($filter['admin'])){

        }else {
            $qb->andWhere('p.status > 0')->andWhere('p.active = 1');
        }

        if(isset($filter['tag_id'])){
            $qb->innerJoin('p.tags', 't')
                ->where('t.id = :tag_id')
                ->setParameter('tag_id', $filter['tag_id']);
        }
        if (isset($filter['query'])) {
            if(strlen($filter['query'])!= ''){
                $search = $filter['query'];
                if(count(explode(" ", $search)) <= 3){
                    $this->elasticService->setSearch($search);
                    $products = $this->elasticService->findProducts(1000);
                    $catalogs = $this->elasticService->findCatalogs(1);
                    $brands = $this->elasticService->findBrands(1);
                    //dd($catalogs);
                    $qb
                        ->innerJoin('App:Catalog', 'c', \Doctrine\ORM\Query\Expr\Join::WITH, 'p.catalog = c.id')
                        ->innerJoin('App:ProductBrand', 'pb', \Doctrine\ORM\Query\Expr\Join::WITH, 'p.productBrand = pb.id')
                        ;
                    if(count($catalogs) > 0 ) {
                        $catalogStr =' OR c.id in (:catalogs)';
                        $qb->setParameter('catalogs', $catalogs);
                    } else {
                        $catalogStr = '';
                    };
                    if(count($brands) > 0 ) {
                        $brandStr =' OR pb.id in (:brands)';
                        $qb->setParameter('brands', $brands);
                    } else {
                        $brandStr = '';
                    };
                    $qb->andWhere('p.id IN (:products)'. $brandStr . $catalogStr)->setParameter('products', $products);

                }
                else{
                    $translit = new Translit($filter['query']);
                    $translitQuery = $translit->getTranslit();
                    $qb
                        ->setParameter('query', '%' . $filter['query'] . '%')
                        ->setParameter('translit', '%' . $translitQuery . '%')
                        ->andWhere("p.name LIKE :query OR p.name LIKE :translit");
                }
            }
        }
        if (isset($filter['no-preview'])) {
            $qb
                ->setParameter('empty','')
                ->andWhere('p.preview IS NULL or p.preview = :empty');
        }
        if (isset($filter['no-price'])) {
            $qb
                ->andWhere('p.priceRet IS NULL or p.priceRet < 1');
        } else{
            $qb->andWhere('p.priceRet > 0');
        }
        if (isset($filter['no-image'])) {
            $qb
                ->leftJoin('App:ProductImage', 'pvi', 'WITH', 'p.id = pvi.productId')
                ->andWhere('pvi.productId IS NULL');
        }

        if(isset($filter['pageUrl'])){
            $urlPath = str_replace('/catalog/','',$filter['pageUrl']);
            if(strlen($urlPath) > 1){
                $urlPath = substr($urlPath, 0, -1);
                $category = $this->catalogRepository->findOneBy(['urlPath'=> $urlPath]);
                if($category instanceof Catalog){
                    $children = $this->catalogRepository->getActiveChildren($category);
                    $ids = [];
                    $ids[] = $category->getId();
                    if(count($children) > 0){
                        foreach ($children as $child){
                            $ids[] = $child->getId();
                        }
                    }
                    //dump(implode(',',$ids));die();
                    $qb->setParameter('catalogIds', $ids)->andWhere("p.catalogId IN(:catalogIds)");
                }
            }
        }
        if(isset($filter['category']) && isset($filter['pageUrl'])){
            if(strpos($filter['pageUrl'],'brands')){
                $qb->andWhere('p.brandId = :categoryId')->setParameter('categoryId',(int)$filter['category']);
            }

        }
        if (isset($filter['brands'])) {
            $qb->setParameter('brands', array_values($filter['brands']))->andWhere("p.brandId IN(:brands)");
        }
        if (isset($filter['receiveTime'])) {
            if (count($filter['receiveTime']) == 1) {
                $field = Product::getCountField($this->versionService->getId());
                if ($filter['receiveTime'][0] == 'today') {
                    $qb->andWhere("p.$field > 0");
                } else {
                    $qb->andWhere("p.$field = 0");
                }
            }
        }
        if (isset($filter['minMax'])) {
            $minMax = explode(';', $filter['minMax']);
            $qb->setParameter('min', $minMax[0])
                ->setParameter('max', $minMax[1])
                ->andWhere("p.priceRet <= :max")
                ->andWhere("p.priceRet >= :min");
        }
        if (isset($filter['sort'])) {
            $field = Product::getCountField($this->versionService->getId());
            switch ($filter['sort']) {
                case 'top':
                    $qb->orderBy('p.viewsCount', 'DESC');
                    break;
                case 'price-less':
                    $qb->orderBy('p.priceRet', 'DESC')->addOrderBy("p.$field", 'ASC');
                    break;
                case 'price-more':
                    $qb->orderBy('p.priceRet', 'ASC')->addOrderBy("p.$field", 'ASC');
                    break;
                case 'new':
                    $qb->addOrderBy("p.$field", 'DESC')->addOrderBy('p.timeCreate', 'DESC');
                    break;
            }
        } else {
            $qb->orderBy('p.viewsCount', 'DESC');
        }
        return $qb;
    }

    /**
     * @param MetaInterface $page
     * @param array $filter
     * @param int $versionId
     * @return Pagerfanta
     */
    public function filterProducts(MetaInterface $page = NULL, array $filter, int $versionId, int $pageNumber = 1): Pagerfanta
    {
        $qb = $this->filterProductsQuery($page, $filter, $versionId);
        return $this->createPaginator($qb->getQuery(), $pageNumber);
    }

    /**
     * @param QueryBuilder $qb
     * @return mixed
     */
    public function getMaxPrice(QueryBuilder $qb)
    {
        $max = (int)$qb->select('MAX(p.priceRet) as max')->getQuery()->getSingleScalarResult();
        if ($max == 0) $max = 1;
        return $max;
    }

    /**
     * @param QueryBuilder $qb
     * @return mixed
     */
    public function getMinPrice(QueryBuilder $qb)
    {
        return (int)$qb->select('MIN(p.priceRet) as min')->getQuery()->getSingleScalarResult();
    }

    /**
     * @param string $url
     * @param string $syncCode
     * @return mixed
     */
    public function findWithEqualUrlAndDifferentCode(string $url, string $syncCode)
    {
        $queryBuilder = $this->createQueryBuilder('p');
        $expr = $queryBuilder->expr();
        $products = $queryBuilder
            ->addSelect('p')
            ->where("p.url = :url")
            ->andWhere($expr->neq('p.syncCode', $syncCode))
            ->setParameter('url', $url)
            //->setParameter('syncCode', $syncCode)
            ->getQuery()
            ->getResult();

        return $products;
    }

    /**
     * @param $catalog
     * @return mixed
     */
    public function getBrandIds($catalog, $filter, $versionId)
    {
        $qb = $this->filterProductsQuery($catalog, [], $versionId);
        $results = $qb->select('p.brandId')->distinct(true)->getQuery()->getResult();
        return array_map('current', $results);
    }

    /**
     * @param Query $query
     * @param int $page
     * @return Pagerfanta
     */
    private function createPaginator(Query $query, int $page): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage(Product::NUM_ITEMS);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    /**
     * Find products bu array of ids
     * @param array $ids
     * @param int $limit
     * @return array
     */
    public function findByIds(array $ids, int $limit = NULL ): array
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $products = $queryBuilder
            ->addSelect('p')
            ->where("p.id IN(:ids)")
            ->andWhere('p.active = 1')
            ->setParameter('ids', array_values($ids))
            ->orderBy('p.id','DESC');
        if($limit){
            $products->setMaxResults($limit);
        }
        $products = $products->getQuery()->getResult();

        return $products;
    }

    /**
     * @param Product $product
     * @param int $limit
     * @return array
     */
    public function findAlsoBoughtProducts(Product $product, int $limit): array
    {
        $productIds = $this->orderProductRepository->findTopAlsoBoughtProductIds($product->getId(),4);
        $products = $this->findByIds($productIds,4);
        return $products;
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return null|Product
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return parent::findOneBy($criteria, $orderBy); // TODO: Change the autogenerated stub
    }

    /**
     * @param int $versionId
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getAvailableProductsCount(int $versionId): int
    {
        $qb = $this->createQueryBuilder('p');
        $countField = Product::getCountField($versionId);
        $products = $qb
            ->select('count(p.id)')
            ->where("p.$countField >0")
            ->andWhere('p.priceRet > 0')
            ->andWhere('p.status > 0')
            ->andWhere('p.active = 1');
        $count = $products->getQuery()->getSingleScalarResult();

        return $count;
    }

    /**
     * @param int $versionId
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getStorageFullnessForSparkline(int $versionId): array
    {
        $activeProductsCount = count($this->getActiveProducts());
        $productsAvailableCount = $this->getAvailableProductsCount($versionId);
        $availablePercent = ($productsAvailableCount/$activeProductsCount) * 100;
        return [(int)$availablePercent, 100 - (int)$availablePercent];
    }


    public function getWhereNameLikeIds(string $name): array
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $products = $queryBuilder
            ->addSelect('p')
            ->where("p.name like :name")
            ->andWhere('p.active = 1')
            ->setParameter('name', '%'.$name.'%')
        ;

        $products = $products->getQuery()->getResult();
        $ids = [];
        foreach ($products as $product) {
            $ids[] = $product->getId();
        }
        return $ids;
    }

}
