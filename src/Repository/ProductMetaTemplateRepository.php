<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\ProductMetaTemplate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class ProductMetaTemplateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductMetaTemplate::class);
    }
    public function getProductMetaTemplatesQueryBuilder() : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('pmr');
        $qb = $queryBuilder->addSelect('pmr');
        return $qb->orderBy('pmr.id','ASC');
    }


    public function getProductMetaTemplatesPaginated(int $page = 1)
    {
        $qb = $this->getProductMetaTemplatesQueryBuilder();
        $query = $qb->getQuery();
        return $this->createPaginator($query, $page, ProductMetaTemplate::NUM_ITEMS);
    }

    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

}
