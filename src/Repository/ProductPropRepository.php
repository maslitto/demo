<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\ProductProp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class ProductPropRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductProp::class);
    }

    public function getProductPropsQueryBuilder(array $filter) : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('pp');
        $qb = $queryBuilder->addSelect('pp');

        return $qb->orderBy('pp.id','DESC');
    }


    public function getProductPropsPaginated(int $page, int $perPage = 20, array $filter = [])
    {
        $qb = $this->getProductPropsQueryBuilder($filter);
        $query = $qb->getQuery();
        return $this->createPaginator($query, $page, $perPage);
    }

    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }
}
