<?php

namespace App\Repository;

use App\Entity\ProductBrand;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class ProductBrandRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductBrand::class);
    }

    public function getActiveBrandsQueryBuilder()
    {
        $queryBuilder = $this->createQueryBuilder('b');
        $qb = $queryBuilder
            ->addSelect('b')
            ->where('b.active = 1');

        return $qb;
    }
    /**
     * @return Query
     */
    public function getActiveBrandsQuery()
    {
        $qb = $this->getActiveBrandsQueryBuilder();
        return $qb->getQuery();
    }

    public function getActiveBrands(int $page = 1): Pagerfanta
    {
        $query = $this->getActiveBrandsQuery();
        return $this->createPaginator($query, $page);
    }

    /**
     * Find brands except given as param by array of ids
     * @param array $ids
     * @return array
     */
    public function getAllExceptIds(array $ids): array
    {
        $queryBuilder = $this->createQueryBuilder('b');

        $brands = $queryBuilder
            ->addSelect('b')
            ->where("b.id NOT IN(:ids)")
            ->andWhere('b.active = 1')
            ->setParameter('ids', array_values($ids))
            ->getQuery()
            ->getResult();

        return $brands;
    }

    /**
     * @param Query $query
     * @param int $page
     * @return Pagerfanta
     */
    private function createPaginator(Query $query, int $page): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage(ProductBrand::NUM_ITEMS);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    public function getProductBrandQueryBuilder(array $filter, int $versionId) : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('pb');
        $qb = $queryBuilder->addSelect('pb');

        return $qb->orderBy('pb.active','DESC');
    }


    public function getProductBrandPaginated(array $filter, int $page, int $versionId)
    {
        $qb = $this->getProductBrandQueryBuilder($filter, $versionId);
        $query = $qb->getQuery();
        return $this->createPaginator($query, $page);
    }
}
