<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 14.03.18
 * Time: 17:06
 */

namespace App\Twig;


class InstanceOfExtension extends \Twig_Extension {

    public function getTests() {
        return array(
            new \Twig_SimpleTest('instanceof', array($this, 'isInstanceOf')),
        );
    }

    public function isInstanceOf($var, $instance) {
        $reflectionClass = new \ReflectionClass($instance);
        return $reflectionClass->isInstance($var);
    }
}