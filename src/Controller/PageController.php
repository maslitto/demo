<?php
/**
 * Created by PhpStorm.
 * User: kip
 * Date: 25/05/18
 * Time: 17:24
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Services\VersionService;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\PageRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Page controller
 *
 * @Route("/page")
 */

class PageController extends AbstractController
{
    private $versionService;

    public function __construct(VersionService $versionService)
    {
        $this->versionService = $versionService;
    }

    /**
     * @Route("/delivery/", name="page_delivery")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function deliveryInfo(Request $request, PageRepository $pageRepository)
    {
        $page = $pageRepository->findOneBy(['url' => 'delivery','versionId' => $this->versionService->getId()]);

        if($request->isXmlHttpRequest()){
            return $this->json($this->render('page/_ajax_delivery.html.twig',['page' => $page])->getContent());
        }

        return $this->render('page/delivery.html.twig',[
            'page' => $page,
            'meta' => $page
        ]);
    }
    /**
     * @Route("/pickup/", name="page_pickup")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function pickupInfo(Request $request, PageRepository $pageRepository)
    {
        $page = $pageRepository->findOneBy(['url' => 'pickup','versionId' => $this->versionService->getId()]);

        if($request->isXmlHttpRequest()){
            return $this->json($this->render('page/_ajax_pickup.html.twig',['page' => $page])->getContent());
        }

        return $this->render('page/pickup.html.twig',[
            'page' => $page,
            'meta' => $page
        ]);
    }
    /**
     * @Route("/new-year/", name="new_year")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function newYearInfo(Request $request, PageRepository $pageRepository)
    {
        $page = $pageRepository->findOneBy(['url' => 'new-year','versionId' => 1]);

        return $this->render('page/new-year.html.twig',[
            'page' => $page,
            'meta' => $page
        ]);
    }
}