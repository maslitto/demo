<?php


namespace App\Controller;

use App\Entity\Feedback;
use App\Repository\ProductRepository;
use App\Services\VersionService;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class RobotsController extends AbstractController
{
    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route({"robots.txt/"}, name="robots", defaults={"_format"="txt"})
     * @Route({"robots.txt"}, name="robots1", defaults={"_format"="txt"})
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(ProductRepository $productRepository,Request $request)
    {
        /*@ini_set("memory_limit", "2048M");
        set_time_limit(0);
        $sFile = $this->getParameter('rootDir').'/../3.xls';
        $oReader = IOFactory::createReaderForFile($sFile);
        $oSpreadsheet = $oReader->load($sFile);
        $oCells = $oSpreadsheet->getActiveSheet()->getCellCollection();
        $urls = [];
        for ($iRow = 22; $iRow <= $oCells->getHighestRow(); $iRow++) {
            for ($iCol = 'D'; $iCol <= 'D'; $iCol++) {
                $oCell = $oCells->get($iCol.$iRow);
                if($oCell) {
                    $title = $oCell->getValue();
                    if($product = $productRepository->findOneBy(['name' => $title])){
                        $urls[] = $product->getUrl();
                        echo 'rewrite /catalog/'.$product->getUrl().'  / permanent;'.PHP_EOL.'<br>';
                    }
                }
            }
        }*/

        $host = $request->getHost();
        if (strpos($host, 'm.') === false) {
            $url = $this->versionService->getSiteVersion()->getUrl();
        } else {
            $url = $this->versionService->getSiteVersion()->getMobileUrl();
        }
        return $this->render('robots/robots.txt.twig',[
            'url' => $url
        ]);
    }

}
