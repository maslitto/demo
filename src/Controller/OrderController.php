<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Services\OrderService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\VersionService;
use App\Services\CartService;
use App\Entity\Order;
use App\Entity\OrderProduct;
use App\Entity\OrderPayment;
use App\Repository\ProductRepository;
use App\Repository\OrderRepository;
use App\Repository\OrderPaymentRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Order Controller
 *
 * @Route("/order")
 *
*/
class OrderController extends AbstractController
{
    private $cartService;
    private $versionService;
    private $productRepository;
    private $orderRepository;
    private $orderPaymentRepository;
    private $orderService;
    /**
     * OrderController constructor.
     */
    public function __construct(

        CartService $cartService,
        VersionService $versionService,
        ProductRepository $productRepository,
        OrderRepository $orderRepository,
        OrderService $orderService,
        orderPaymentRepository $orderPaymentRepository
    )
    {
        $this->cartService = $cartService;
        $this->versionService = $versionService;
        $this->productRepository = $productRepository;
        $this->orderRepository = $orderRepository;
        $this->orderService = $orderService;
        $this->orderPaymentRepository = $orderPaymentRepository;
    }

    /**
     * @Route("/add/", name="order_add")
     * @Method("POST")
     */
    public function add(Request $request, EventDispatcherInterface $eventDispatcher): Response
    {
        $hashId = $request->cookies->get('cart',NULL);
        if($hashId == NULL){
            return $this->json(['success' => false, 'message' => 'Не указан идентификатор корзины']);
        }
        $data = $request->request->all();
        if($user = $this->getUser()){
            $data['user_id'] = $user->getId();
        }
        $cart = $this->cartService->getCart($hashId);
        $order = $this->orderService->createOrder($data, $cart);
        $event = new GenericEvent($order);
        $eventDispatcher->dispatch(\App\Events::ORDER_CREATED, $event);

        $this->cartService->finish($hashId);
        $items = [];
        foreach($cart->getCartProducts() as $cartProduct){
            $el['product_id'] = $cartProduct->getProduct()->getId();
            $el['product_name'] = $cartProduct->getProduct()->getName();
            $el['price'] = $cartProduct->getProduct()->getPrice($this->versionService->getSiteVersion());
            $el['qnt'] = $cartProduct->getCount();
            $el['product_url'] = 'https://stommarket.ru/'.$cartProduct->getProduct()->getUrl();
            $el['product_image_url'] = 'https://stommarket.ru/'.$cartProduct->getProduct()->getImage('hr');
            $items[] =  $el;
        }
        $response = [
            'success' => true,
            'orderId' => $order->getEncryptedId(),
            'summ' => $order->getPrice(),
            'name' => $data['username'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'items' => $items,
        ];

        $orderPayment = $order->getOriginPayment();

        if($orderPayment->getBankPayUrl()) {
            $response['redirect'] = $orderPayment->getBankPayUrl();
        }
        /*else {
            $response['redirect'] = 'none';
        }*/

        return $this->json(
            $response,
            Response::HTTP_OK,[
                'Set-Cookie'=> 'cart=; Max-Age=-2592000; path=/'
        ]);
    }

    /**
     * Add One-Click-Buy(ocb) order
     * @Route("/add-ocb-order/", name="order_add_ocb")
     * @Method("POST")
     */
    public function addOcbOrder(Request $request, EventDispatcherInterface $eventDispatcher): Response
    {
        $data = $request->request->all();
        $order = $this->orderService->createOcbOrder($data);
        $event = new GenericEvent($order);
        $eventDispatcher->dispatch(\App\Events::ORDER_CREATED, $event);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        $response = [
            'success' => true,
            'id' => $order->getEncryptedId()
        ];
        return $this->json($response);
    }

    /**
     * Avangard callback
     * @Route("/avangard-callback/", name="avangard_callback")
     * @Method("POST")
     */
    public function avangardCallback(Request $request)
    {
        $callbackParams = $request->request->all();

        if($this->orderService->updatePaymentFromBankCallback($callbackParams)) {
            return new Response('', Response::HTTP_ACCEPTED);
        }
        else {
            return new Response('', Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * Sberbank callback
     * @Route("/sberbank-callback/", name="sberbank_callback")
     * @Method("GET")
     */
    public function sberbankCallback(Request $request)
    {
        $callbackParams = $request->query->all();
        file_put_contents('/var/sites/symfony/callback.txt',print_r($callbackParams,true));
        if($this->orderService->updatePaymentFromBankCallback($callbackParams)) {
            return new Response('', Response::HTTP_OK);
        }
        else {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

    }

    /**
     * Page with success payment message for return from bank
     * @Route("/success/", name="success_payment")
     * @Method("GET")
     */
    public function success(Request $request)
    {
        $orderBankId = $request->query->get('orderId');
        $orderPayment = $this->orderPaymentRepository->findOneBy(['bankId' => $orderBankId]);
        /** @var Order $order */
        $order = $orderPayment ? $orderPayment->getOrder() : NULL;
        $orderEncryptedId = $order ? $order->getEncryptedId() : NULL;

        return $this->render('order/success.html.twig', [
            'header' => 'Оплата заказа успешно принята',
            'orderId' => $orderEncryptedId
        ]);
    }

    /**
     * Page with fail payment message for return from bank
     * @Route("/fail/", name="fail_payment")
     * @Method("GET")
     */
    public function fail(Request $request)
    {
        $orderBankId = $request->query->get('orderId');
        $orderPayment = $this->orderPaymentRepository->findOneBy(['bankId' => $orderBankId]);
        $order = $orderPayment ? $orderPayment->getOrder() : NULL;
        $orderEncryptedId = $order ? $order->getEncryptedId() : NULL;
        $bankPayUrl = $orderPayment ? $orderPayment->getBankPayUrl() : NULL;

        return $this->render('order/fail.html.twig', [
            'header' => 'Оплата заказа не была принята',
            'orderId' => $orderEncryptedId,
            'bankPayUrl' => $bankPayUrl
        ]);
    }

    /**
     * Returns user's emails from orders
     * @Route("/emails.csv/", name="users_emails")
     * @Method("GET")
     */
    public function getEmails(OrderRepository $orderRepository)
    {
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=emails.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        $orders = $orderRepository->getEmails();
        //dump($orders);
        foreach ($orders as $k => $order){
            echo $order[1].PHP_EOL;
        }
        die;
    }
}
