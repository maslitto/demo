<?php


namespace App\Controller;

use App\Entity\CartProduct;
use App\Entity\Education\Education;
use App\Entity\Education\EducationType;
use App\Entity\Feedback;
use App\Entity\OrderProduct;
use App\Entity\User;
use App\Entity\UserProfile;
use App\Repository\CartProductRepository;
use App\Repository\Education\EducationDirectionRepository;
use App\Repository\Education\EducationRepository;
use App\Repository\Education\EducationTypeRepository;
use App\Repository\OrderProductRepository;
use App\Repository\OrderRepository;
use App\Services\CartService;
use App\Services\EducationService;
use App\Services\VersionService;
use App\Utils\Dadata;
use App\Utils\StringHelper;
use Detection\MobileDetect;
use Doctrine\Common\Collections\Criteria;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * User's personal page Controller
 *
 * @Route("/personal")
*/

class PersonalController extends AbstractController
{
    use \App\Traits\Meta;

    private $versionService;
    private $cartService;

    public function __construct(VersionService $versionService, CartService $cartService){
        $this->versionService = $versionService;
        $this->cartService = $cartService;

    }

    /**
     * @Route("/", name="personal_page")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(
        Request $request,
        OrderRepository $orderRepository,
        EducationRepository $educationRepository,
        EducationTypeRepository $educationTypeRepository,
        EducationDirectionRepository $educationDirectionRepository
    )
    {
        $meta = $this->getMetaObject('Личный кабинет');
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $hashId = $request->cookies->get('cart',NULL);
        $cart = [];
        $discount = 0;
        $total = 0;
        $criteria = new Criteria();
        $criteria->where($criteria->expr()->neq('encryptedId', 0))->andWhere($criteria->expr()->eq('userId',$user->getId() ));
        $orders = $orderRepository->matching($criteria);//findBy(['userId' => $user->getId()]);
        $educations = $educationRepository->findBy(['userId' => $user->getId()]);/*findBy(['userId' => $user->getId()]);*/
        $educationTypes = $educationTypeRepository->findBy(['active' => true]);
        $educationDirections = $educationDirectionRepository->findBy(['active' => true]);
        if($hashId !== NULL) {
            $content = $this->cartService->getCart($hashId);
            foreach($content->getCartProducts() as $cartProduct){
                $el['product'] = $cartProduct->getProduct();
                $el['qty'] = $cartProduct->getCount();
                $cart[] = (object) $el;
                $discount +=  $cartProduct->getProduct()->getDiscount($this->versionService->getId()) * $cartProduct->getCount();
                $total += $cartProduct->getProduct()->getPrice($this->versionService->getSiteVersion()) * $cartProduct->getCount();
            }
        }
        $md = new MobileDetect();
        if($md->isMobile()||$md->isTablet()){
            $view = 'personal/index_mobile.html.twig';
        } else{
            $view = 'personal/index.html.twig';
        }
        return $this->render($view, [
            'cartItems' => $cart,
            'total' => $total,
            'preTotal' => $total + $discount,
            'discount' => $discount,
            'orders' => $orders,
            'educations' => $educations,
            'educationTypes' => $educationTypes,
            'educationDirections' => $educationDirections,
            'user' => $user,
            'meta' => $meta,
            'isAllowDelivery' => $this->cartService->isAllowDelivery($hashId),
            'isAllowCashlessPayment' => $this->cartService->isAllowCashlessPayment($hashId)
        ]);
    }

    /**
     *
     * @Route("/get-info-by-inn/", name="info_by_inn_json")
     * @Method("POST")
     *
     */
    public function getInfoByInn(Request $request)
    {

        $inn = $request->get('inn');
        $dadata = new Dadata();
        $response = $dadata->suggest('party', ['query' => $inn, 'count' => 1]);
        if(sizeOf($response)>0){
            return $this->json($response['suggestions'][0]);
        } else {
            return $this->json([]);
        }

    }

    /**
     *
     * @Route("/save-personal-form/", name="save_personal_form")
     * @Method("POST")
     *
     */
    public function savePersonalForm(Request $request)
    {
        $user = $this->getUser();
        /** @var UserProfile $userProfile */
        $userProfile = $user->getUserProfile();
        $userProfile->setName($request->get('udata-name'));
        $userProfile->setPhone($request->get('udata-phone'));
        $userProfile->setSkype($request->get('udata-skype'));
        $userProfile->setCity($request->get('udata-city'));
        $userProfile->setCountry($request->get('udata-country'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($userProfile);
        $em->flush();

        return $this->json(['ret' => 1]);
    }
    /**
     *
     * @Route("/save-company-form/", name="save_company_form")
     * @Method("POST")
     *
     */
    public function saveCompanyForm(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        $userProfile = $user->getUserProfile();
        $userProfile->setComInn($request->get('udata-com_inn'));
        $userProfile->setComName($request->get('udata-com_name'));
        $userProfile->setComKpp($request->get('udata-com_kpp'));
        $userProfile->setComOgrn($request->get('udata-com_ogrn'));
        $userProfile->setComOkved($request->get('udata-com_okved'));
        $userProfile->setComUridAddress($request->get('udata-com_urid_address'));
        $userProfile->setComFactAddress($request->get('udata-com_fact_address'));
        $userProfile->setComPhone($request->get('udata-com_com-phone'));
        $userProfile->setComRc($request->get('udata-com_rc'));
        $userProfile->setComKc($request->get('udata-com_kc'));
        $userProfile->setComBik($request->get('udata-com_bik'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($userProfile);
        $em->flush();

        return $this->json(['ret' => 1]);
    }
    /**
     *
     * @Route("/save-feed-form/", name="save_feed_form")
     * @Method("POST")
     *
     */
    public function saveFeedForm(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        $userFeed = $user->getUserFeed();
        $userFeed->setNews($request->get('ufeed-news',0));
        $userFeed->setEvents($request->get('ufeed-events',0));
        $userFeed->setSales($request->get('ufeed-sales',0));
        $userFeed->setNewProducts($request->get('ufeed-new_products',0));

        $em = $this->getDoctrine()->getManager();
        $em->persist($userFeed);
        $em->flush();

        return $this->json(['ret' => 1]);
    }

    /**
    * @Route("/save-education-form/", name="save_education_form")
    * @Method("POST")
    *
    */
    public function saveEducationForm(
        Request $request,
        EducationService $educationService,
        EducationRepository $educationRepository,
        EducationTypeRepository $educationTypeRepository
)
    {
        /** @var User $user */
        $user = $this->getUser();
        if($request->isMethod('POST')) {
            $data = $request->request->all();
            $files = $_FILES;
            //dd($files);
            if($files["photo_file"]["tmp_name"]!==''){
                $check = getimagesize($files["photo_file"]["tmp_name"]);
                if($check !== false) {
                    $education = $educationService->saveEducation($user, $data, $files);
                    $response['ret'] =  1;
                    $response['educationId'] = $education->getId();
                } else {
                    $response['ret'] = (int) false;
                }
            }
            else{
                $education = $educationService->saveEducation($user, $data, null);
                $response['ret'] = 1;
                $response['educationId'] = $education->getId();
            }
        }

        return $this->json($response);
    }

    /**
     * @Route("/copy-order-to-cart/{id}/", name="copy-order-to-cart")
     * @Method("GET")
     *
     */
    public function copyOrderToCart(Request $request, int $id, CartService $cartService, OrderProductRepository $orderProductRepository)
    {
        /** @var OrderProduct[] $orderProducts */
        $orderProducts = $orderProductRepository->findBy(['order' => $id]);
        $hashId = $request->cookies->get('cart',NULL);
        if($hashId == NULL){
            $hashId = $this->cartService->createCart()->getHashId();
            $this->getDoctrine()->getManager()->flush();
        }
        foreach ($orderProducts as $orderProduct) {
            $insert = [
                'count' => $orderProduct->getCount(),
                'id' => $orderProduct->getProduct()->getId(),
            ];
            $cart = $cartService->add($insert, $hashId);
        }

        return $this->json([
            'success' => true,
            'count' => $cartService->getCount($hashId),
        ],Response::HTTP_OK,[
            'Set-Cookie'=> 'cart='. $cart->getHashId() .'; Max-Age=2592000; path=/'
        ]);
    }
}
