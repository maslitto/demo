<?php


namespace App\Controller;

use App\Entity\Feedback;
use App\Services\VersionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Contacts controller
 *
 * @Route("/contacts")
*/

class ContactsController extends AbstractController
{
    use \App\Traits\Meta;
    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("/", name="contacts_page")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index()
    {
        $siteVersion = $this->versionService->getSiteVersion();
        $meta = $this->getMetaObject('Контакты');
        return $this->render('contacts/index.html.twig',['meta' => $meta,'siteVersion' => $siteVersion]);
    }

    /**
     * @Route("/feedback/", name="feedback_add")
     * @Method("POST")
     * @Cache(smaxage="10")
     *
     */
    public function feedback(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();

        $feedback = new Feedback();
        $feedback->setName($data['name']);
        $feedback->setEmail($data['email']);
        $feedback->setPhone($data['phone']);
        $feedback->setQuestion($data['question']);
        $feedback->setAgreement($data['agreement']);
        $feedback->setAgreementTime(new \DateTime());
        $feedback->setTimeCreate(new \DateTime());
        $feedback->setStatus(0);
        $feedback->setVersionId($this->versionService->getId());

        $errors = [];
        try{
            $em->persist($feedback);
            $em->flush();
        }catch (\Exception $e){
            $errors[] = $e->getMessage();
        }

        return $this->json(['errors' => $errors]);
    }
}
