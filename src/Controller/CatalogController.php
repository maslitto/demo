<?php

namespace App\Controller;


use App\Entity\MetaInterface;
use App\Entity\Tag;
use App\Repository\TagRepository;
use App\Services\CartService;
use App\Services\CatalogService;
use App\Services\VersionService;
use Detection\MobileDetect;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Catalog;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Product;
use App\Services\ProductService;
use App\Repository\CatalogRepository;
use App\Repository\ProductRepository;
use App\Repository\ProductBrandRepository;

/**
 * @Route("/catalog")
*/

class CatalogController extends AbstractController
{
    use \App\Traits\Meta;
    const PRODUCT_PAGE_ROUTES = ['props','comments','instruction'];

    private $productService;
    private $versionService;
    private $catalogRepository;
    private $catalogService;
    private $cartService;

    public function __construct(
        ProductService $productService,
        VersionService $versionService,
        CatalogRepository $catalogRepository,
        CatalogService $catalogService,
        CartService $cartService
    )
    {
        $this->productService = $productService;
        $this->versionService = $versionService;
        $this->catalogRepository = $catalogRepository;
        $this->catalogService = $catalogService;
        $this->cartService = $cartService;

    }
    /**
     * @Route("/get-menu/", name="catalog-menu")
     * @Method("GET")
     */
    public function getMenuAction(Request $request, CatalogRepository $catalogRepository)
    {
        $catalogId = $request->get('catalogId');  // From GET
        /**
         * @var Catalog $catalog
         */
        $catalog = $catalogRepository->find($catalogId);
        return $this->json([
            'catalogs'   => $this->render('catalog/_catalog_menu_mobile.html.twig',['catalog' => $catalog])->getContent(),
            'catalogId'  => $catalogId,
            'parentId'   => $catalog->getParentId(),
            'level' => $catalog->getLvl()
        ]);
    }
    /**
     * //@Route("/brands/",defaults={"page": "1"}, name="brand_index")
     */
    public function brands(ProductBrandRepository $productBrandRepository, Request $request)
    {
        $page = $request->query->get('page',1);
        $brands = $productBrandRepository->getActiveBrands($page);
        $meta = $this->getMetaObject('Бренды производителей представленных в интернет-магазине «Стоммаркет»');
        return $this->render('brands/index.html.twig', [
            'brands' => $brands,
            'meta' => $meta
        ]);
    }

    /**
     * //@ParamConverter("product")
     * //@Route("{url}/", name="product_view")
     * //@Route("{url}/props/", name="product_props")
     * //@Route("{url}/comments/", name="product_comments")
     * //@Route("{url}/instruction/", name="product_instruction")
     * //@Method("GET")
     *
     */
    public function view(Product $product, Request $request, string $postfix = NULL): Response
    {
        //dump($product->getProductBrand()->getActive());die();
        if(!$product->getProductBrand()->getActive()){
            throw new NotFoundHttpException('Товар в помойке!');
        }
        $hashId = $request->cookies->get('cart',NULL);
        $this->productService->incrementProductViews($product);
        $this->productService->pushToRecentlyViewedProducts($product);
        $similarProducts = $this->productService->getSimilarProducts($product);
        $recentlyViewedProducts = $this->productService->getRecentlyViewedProducts();
        $alsoBoughtProducts = $this->productService->getAlsoBoughtProducts($product);
        $tags = $product->getTags();
        $currentCount = $this->cartService->getProductCountInCart($product, $hashId);
        $vueInject = [
            'inCart' => $currentCount > 0 ? true : false,
            'product' => [
                'id' => $product->getId(),
                'count' => $product->getCount($this->versionService->getId()),
                'remain' => $product->getCount($this->versionService->getId()),
                'price' => (int)$product->getPrice($this->versionService->getSiteVersion()),
                'oldPrice' => (int)$product->getOldPrice($this->versionService->getId()),
            ],
            'currentCount' => $currentCount ? $currentCount : 1,
        ];
        $md = new MobileDetect();
        if($md->isMobile()||$md->isTablet()){
            $view = 'product/view-mobile.html.twig';
        }else{
            $view = 'product/view.html.twig';
        }
        /*$reviews = $product->getProductReviews();
        if(count($reviews) > 0){
            $sum = 0;
            foreach ($reviews as $review) {
                $sum = $sum + $review->getRating();
            }
            $rating = $sum / count($reviews);
            $rating = (int) $rating;
        } else {
            $rating = 5;
        }*/

        return $this->render($view, [
            'product' => $product,
            //'productVersion' => $product->getProductVersion(1),
            'similarProducts' => $similarProducts,
            'alsoBoughtProducts' => $alsoBoughtProducts,
            'recentlyViewedProducts' => $recentlyViewedProducts,
            'tags' => $tags,
            'vueInject' => json_encode($vueInject,JSON_UNESCAPED_SLASHES),
            //'productVersions' => $product->getProductVersions(),
            'meta' => $product,
            'postfix' => $postfix
        ]);
    }

    /**
     * Это очередной костыль для диспечеризация роутов, потому что нельзя менять url из-за seo.
     * Такой роутинг несомненно изобрели пидарасы.
     * //
     * @Route("/{slug}/", defaults={"slug":""}, requirements={"slug": "[a-zA-Z0-9/_/.-]+"}, name="dispatcher_index")
     * @Route("/", name="catalog_dispatcher")
     * @Method("GET")
     */
    public function dispatcher(
        string $slug = '',
        ProductRepository $productRepository,
        ProductBrandRepository $productBrandRepository,
        TagRepository $tagRepository,
        CatalogRepository $catalogRepository,
        Request $request
    )
    {
        //dump($request->getPathInfo());die();
        // trim "/" from end of string
        if($request->get('pageUrl') !== NULL){
            $slug = str_replace('/catalog','',$request->getPathInfo());
            /*str_replace('/catalog','',$request->get('pageUrl'));*/
        }
        if(substr($slug, -1)=='/') $slug = rtrim($slug,"/");

        if($slug == '') {
            $page = $this->catalogRepository->findOneBy(['parentId' => NULL]);
            return $this->catalog( $page, $productRepository, $productBrandRepository, $request );
        }
        if($request->isXmlHttpRequest()) {
            $catalogId = $request->get('category', NULL);

            if($catalogId !== NULL){
                $page = $this->catalogRepository->findOneBy(['id' => $catalogId]);
            } else{
                $page = $this->catalogRepository->findOneBy(['parentId' => NULL]);
            }
            return $this->catalog( $page, $productRepository, $productBrandRepository, $request );
        }

        $route = explode('/',$slug);

        $url = array_pop($route);
        //dump($slug);die();
        if ($brand = $productBrandRepository->findOneBy(['url' => $url, 'active' => true])){

            return $this->catalog( $brand, $productRepository, $productBrandRepository, $request );

        } elseif ($tag = $tagRepository->findOneBy(['url' => $url, 'active' => true])){
            return $this->catalog( $tag, $productRepository, $productBrandRepository, $request );


        }elseif($catalog = $catalogRepository->findOneBy(['url' => $url, 'active' => true])){
            if($catalogRepository->findOneBy(['urlPath' => $slug, 'active' => true])){
                return $this->catalog( $catalog, $productRepository, $productBrandRepository, $request );
            } else{
                throw new NotFoundHttpException('Страница не найдена. Ошибка 404!');
            }

        } elseif($url == 'brands'){
            return $this->brands($productBrandRepository, $request);

        } elseif ($product = $productRepository->findOneBy(['url' => $url, 'active' => true])){
            return $this->view($product, $request);
        } elseif(in_array($url,self::PRODUCT_PAGE_ROUTES)){
            $postfix = $url;
            $url = $route[count($route)-1];
            if($product = $productRepository->findOneBy(['url' => $url, 'active' => true])){
                return $this->view($product, $request, $postfix);
            };
        }
        else {
            throw new NotFoundHttpException('Страница не найдена. Ошибка 404!');
        }
    }

    /**
     * @Route( name="catalog")
     * //@Method("GET")
     * //@Cache(smaxage="10")
     */

    public function catalog(
        MetaInterface $page = NULL,
        ProductRepository $productRepository ,
        ProductBrandRepository $productBrandRepository,
        Request $request
    ): Response

    {
        if($page->getUrl() == '3m-espe/'){
            $populars = $productRepository->findByIds([42028,5336,6856,5381],4);
            $sales = $productRepository->findBy(['brandId' => $page->getId(),'event' => 'sale']);
            return $this->render('brands/3m.html.twig', [
                'meta' => $page,
                'page' => $page,
                'populars' => $populars,
                'sales' => $sales
            ]);
        }
        $md = new MobileDetect();
        if($md->isMobile()||$md->isTablet()){
            $view = 'catalog/catalog_mobile.html.twig';
        }else{
            $view = 'catalog/catalog.html.twig';
        }

        //set catalog page
        if(!$page) $page = $this->catalogRepository->findOneBy(['parentId' => NULL]);
        $pageNumber = $request->query->get('page', 1);
        //get filter fields from request
        $filter = $request->query->all();
        //get products

        $products = $productRepository->filterProducts($page, $filter, $this->versionService->getId(),$pageNumber);
        //return json response for filter

        if($request->isXmlHttpRequest()){

            return $this->json([
                'html' => $this->render('catalog/ajax_response.html.twig',['products' => $products])->getContent(),
                'count' =>'Найдено '.count($products). ' товаров',
                'page' => $pageNumber
            ]);
        }
        $filters = $this->catalogService->getFilter($page, $filter, $this->versionService->getId());
        if(($page instanceof Catalog)||($page instanceof Product)){
            $tags = $page->getTags();
        } elseif($page instanceof Tag) {
            $tags = $page->getCatalogs()[0]->getTags();
        }
        else{
            $tags = new ArrayCollection();
        }
        $search = $request->get('query',false);
        // dd($search);
        return $this->render($view, [
            'products' => $products,
            'catalog' => $page,
            'filter' => $filters,
            'tags' => $tags,
            'search' => $search,
            'meta' => $page
        ]);
    }


}
