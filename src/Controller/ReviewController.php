<?php


namespace App\Controller;

use App\Entity\Review;
use App\Repository\ProductRepository;
use App\Repository\ReviewRepository;
use App\Services\VersionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\ProductReview;

/**
 * Controller used to manage index page
 *
 * @Route("/reviews")
*/

class ReviewController extends AbstractController
{

    private $versionService;

    /**
     * IndexController constructor.
     */
    public function __construct(
        VersionService $versionService
    )
    {
        $this->versionService = $versionService;
    }
    /**
     * @Route("/", name="reviews_page")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(ReviewRepository $reviewRepository, Request $request)
    {
        $page = $request->query->get('page',1);
        $reviews = $reviewRepository->getReviewsPaginated(null, $page,1, 5);
        if($request->isXmlHttpRequest()){
            return $this->json([
                'html' => $this->render('reviews/ajax_response.html.twig',['reviews' => $reviews])->getContent(),
                'isEnd' => !$reviews->hasNextPage(),
            ]);
        }
        return $this->render('reviews/index.html.twig',[
            'reviews' => $reviews,
        ]);
    }
    /**
     * @Route("/add/", name="add_review")
     * @Method("POST")
     *
     */
    public function add(Request $request)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();

        $review = new Review();
        $review->setStatus(0);
        $review->setName($data['name']);
        $review->setRating($data['rating']);
        $review->setReview($data['review']);

        $errors = [];
        try{
            $em->persist($review);
            $em->flush();
        }catch (\Exception $e){
            $errors[] = $e->getMessage();
        }
        return $this->json([
            'errors' => $errors
        ]);
    }

}
