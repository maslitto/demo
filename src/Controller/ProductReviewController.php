<?php


namespace App\Controller;

use App\Repository\ProductRepository;
use App\Repository\ProductReviewRepository;
use App\Services\VersionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\ProductReview;
/**
 * Controller used to manage index page
 *
 * @Route("/product-reviews")
*/

class ProductReviewController extends AbstractController
{

    private $versionService;

    /**
     * IndexController constructor.
     */
    public function __construct(
        VersionService $versionService
    )
    {
        $this->versionService = $versionService;
    }

    /**
     * @Route("/add/", name="add_product_review")
     * @Method("POST")
     *
     */
    public function add(Request $request, ProductRepository $productRepository)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();

        $product = $productRepository->findOneBy(['id' => $data['product_id']]);
        $productReview = new ProductReview();
        $productReview->setStatus(0);
        $productReview->setName($data['name']);
        $productReview->setProduct($product);
        $productReview->setRating($data['rating']);
        $productReview->setReview($data['review']);

        $errors = [];
        try{
            $em->persist($productReview);
            $em->flush();
        }catch (\Exception $e){
            $errors[] = $e->getMessage();
        }
        return $this->json([
            'errors' => $errors
        ]);
    }

}
