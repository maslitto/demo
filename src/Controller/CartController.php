<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Promocode;
use App\Entity\User;
use App\Repository\PromocodeRepository;
use Detection\MobileDetect;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProductRepository;
use App\Services\VersionService;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Services\CartService;

/**
 * Controller used to cart.
 *
 * @Route("/cart")
 *
*/
class CartController extends AbstractController
{
    use \App\Traits\Meta;
    private $cartService;
    private $productRepository;
    private $versionService;

    public function __construct(
        ProductRepository $productRepository,
        VersionService $versionService,
        CartService $cartService
    )
    {
        $this->productRepository = $productRepository;
        $this->versionService = $versionService;
        $this->cartService = $cartService;

    }

    /**
     * @Route("/", name="cart_index")
     * @Method("GET")
     */
    public function index(Request $request): Response
    {
        $meta = $this->getMetaObject('Корзина');
        $turboId = $request->query->get('turboId', NULL);
        $hashId = $request->cookies->get('cart',NULL);
        if(!$hashId){
            $cart = $this->cartService->createCart();
            return new RedirectResponse('/cart/',302,[
                'Set-Cookie'=> 'cart='. $cart->getHashId() .'; Max-Age=2592000; path=/'
            ]);
        }
        $cart = [];
        $parameters = [];
        $discount = 0;
        $total = 0;
        /** @var Product $product */
        $product = $this->productRepository->find(4653);
        $alsoBoughtProducts =  $this->productRepository->findAlsoBoughtProducts($product ,4);
        if($turboId){
            $insert = [
                'id' => $turboId,
                'count' => 1
            ];
            $newCart = $this->cartService->add($insert, $hashId);
            $hashId = $newCart->getHashId();
            $parameters = [
                'Set-Cookie'=> 'cart='. $hashId .'; Max-Age=2592000; path=/'
            ];
            return new RedirectResponse($this->generateUrl('cart_index'),Response::HTTP_FOUND, $parameters);
        }
        if($hashId !== NULL) {
            $content = $this->cartService->getCart($hashId);
            foreach($content->getCartProducts() as $cartProduct){
                $el['product'] = $cartProduct->getProduct();
                $el['qty'] = $cartProduct->getCount();
                $el['price'] = $cartProduct->getPrice();
                $cart[] = (object) $el;
                $discount +=  $cartProduct->getProduct()->getDiscount($this->versionService->getId()) * $cartProduct->getCount();
                $total += $cartProduct->getPrice() * $cartProduct->getCount();
            }
        }
        $md = new MobileDetect();
        if($md->isMobile()||$md->isTablet()){
            $view = 'cart/index-mobile.html.twig';
        }else{
            $view = 'cart/index.html.twig';
        }
        $response = new Response(
            $this->renderView($view, [
                'alsoBoughtProducts' => $alsoBoughtProducts,
                'cartItems' => $cart,
                'total' => $total,
                'preTotal' => $total + $discount,
                'discount' => $discount,
                'meta' => $meta,
                'cart' => $this->cartService->getCart($hashId),
                'isAllowCashlessPayment' => $this->cartService->isAllowCashlessPayment($hashId),
                'isAllowDelivery' => $this->cartService->isAllowDelivery($hashId),
            ]),
            Response::HTTP_OK,
            $parameters
        );
        return $response;

    }

    /**
     * @Route("/update/", name="cart_update")
     * @Method("POST")
     */

    public function update(Request $request, PromocodeRepository $promocodeRepository): Response
    {
        $hashId = $request->cookies->get('cart', NULL);
        if($hashId == NULL){
            return $this->json(['success' => false,'message' => 'Не указан идентификатор корзины']);
        }
        $update = $request->request->all();
        if(isset($update['promocode'])){
            /** @var Promocode|null $promocode */
            $promocode = $promocodeRepository->findOneBy(['code' => $update['promocode']]);
            if(!$promocode){
                return $this->json([
                    'success' => false,
                    'promocode' => 'Неверный промокод',
                ],Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            elseif($promocode->isExpired()){
                return $this->json([
                    'success' => false,
                    'promocode' => 'Просроченный промокод',
                ],Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            elseif($promocode->isDisposable() && !$promocode->isActive()){
                return $this->json([
                    'success' => false,
                    'promocode' => 'Промокод уже был применен',
                ],Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            else{
                $cart = $this->cartService->applyPromocode($promocode, $hashId);
            }
        }
        else{
            $cart = $this->cartService->update($update, $hashId);
        }
        return $this->json([
            'success' => true,
            'count' => $this->cartService->getCount($hashId),
            'sum' => $this->cartService->getTotal($hashId),
            'fullSum' => $this->cartService->getTotal($hashId) + $this->cartService->getDiscount($hashId),
            'discount' => $this->cartService->getDiscount($hashId),
            'bonus' => $this->cartService->getBonus($hashId),
            'isAllowCashlessPayment' => $this->cartService->isAllowCashlessPayment($hashId),
            'isAllowDelivery' => $this->cartService->isAllowDelivery($hashId),
            'promocode' => isset($promocode) ? true : false
        ]);

    }

    /**
     * @Route("/callback/avangard/", name="callback_avangard_alias")
     * @Method("POST")
     */
    public function callbackAvangard(Request $request)
    {
        $postParams = $request->request->all();
        return $this->redirectToRoute('avangard_callback', $postParams);
    }

    /**
     * @Route("/callback/", name="callback_sberbank_alias")
     * @Method("GET")
     */
    public function callbackSberbank(Request $request)
    {
        $getParams = $request->query->all();
        return $this->redirectToRoute('sberbank_callback', $getParams);
    }

    /**
     * @Route("/success/", name="success_payment_alias")
     * @Method("GET")
     */
    public function success(Request $request)
    {
        $getParams = $request->query->all();
        return $this->redirectToRoute('success_payment', $getParams);
    }

    /**
     * @Route("/fail/", name="fail_payment_alias")
     * @Method("GET")
     */
    public function fail(Request $request)
    {
        $getParams = $request->query->all();
        return $this->redirectToRoute('fail_payment', $getParams);
    }

    /**
     * @Route("/add/", name="cart_add")
     * @Method("POST")
     */
    public function add(Request $request): Response
    {
        $hashId = $request->cookies->get('cart',NULL);
        /*if($hashId == NULL){
            $hashId = $this->cartService->createCart()->getHashId();
            $this->getDoctrine()->getManager()->flush();
        }*/
        $insert = $request->request->all();
        $cart = $this->cartService->add($insert, $hashId);
        $hashId = $cart->getHashId();
        if((int)$request->get('bonus', 0) > 0){
            $this->addFlash('success','Товар добавлен в корзину');
        }
        return $this->json([
            'success' => true,
            'count' => $this->cartService->getCount($hashId),
            'sum' => $this->cartService->getTotal($hashId),
            'fullSum' => $this->cartService->getTotal($hashId) + $this->cartService->getDiscount($hashId),
            'discount' => $this->cartService->getDiscount($hashId),
            'bonus' => $this->cartService->getBonus($hashId),
            'isAllowCashlessPayment' => $this->cartService->isAllowCashlessPayment($hashId),
        ],Response::HTTP_OK,[
            'Set-Cookie'=> 'cart='. $cart->getHashId() .'; Max-Age=2592000; path=/'
        ]);
    }

    /**
     * @Route("/delete/", name="cart_delete")
     * @Method("POST")
     */
    public function delete(Request $request): Response
    {
        $hashId = $request->cookies->get('cart',NULL);
        if($hashId == NULL){
            return $this->json(['success' => false,'message' => 'Не указан идентификатор корзины']);
        }
        $id = $request->get('id');
        $cart = $this->cartService->delete($id, $hashId);
        return $this->json([
            'success' => true,
            'count' => $this->cartService->getCount($hashId),
            'sum' => $this->cartService->getTotal($hashId),
            'fullSum' => $this->cartService->getTotal($hashId) + $this->cartService->getDiscount($hashId),
            'discount' => $this->cartService->getDiscount($hashId),
            'bonus' => $this->cartService->getBonus($hashId),
            'isAllowCashlessPayment' => $this->cartService->isAllowCashlessPayment($hashId),
        ]);

    }


    /**
     * @Route("/json/", name="cart_json")
     * @Method("GET")
     */
    public function getJsonCart(Request $request): Response
    {
        $hashId = $request->cookies->get('cart',NULL);
        $cart = [];
        if($hashId !== NULL) {
            $content = $this->cartService->getCart($hashId);
            foreach($content->getCartProducts() as $cartProduct){
                $el['id'] = $cartProduct->getProduct()->getId();
                $el['name'] = $cartProduct->getProduct()->getName();
                $el['price'] = $cartProduct->getProduct()->getPrice($this->versionService->getSiteVersion());
                $el['count'] = $cartProduct->getCount();
                $cart[] =  $el;
            }
        }
        if($this->getUser()){
            $email = $this->getUser()->getEmail();
        } else {
            $email = NULL;
        }
        return $this->json([
            'cart' => $cart,
            'email' => $email
        ]);
    }

    /**
     * @Route("/convead/", name="cart_convead")
     * @Method("GET")
     */
    public function getConvead(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $response = [];
        if($user){
            $response['email'] = $user->getEmail();
        } else{
            $response['email'] = NULL;
        }
        return $this->json($response);
    }

    /**
     * @Route("/convead-json/", name="cart_convead_json")
     * @Method("GET")
     */
    public function getConveadCart(Request $request): Response
    {
        $hashId = $request->cookies->get('cart',NULL);
        $cart = [];
        if($hashId !== NULL) {
            $content = $this->cartService->getCart($hashId);
            foreach($content->getCartProducts() as $cartProduct){
                $el['product_id'] = $cartProduct->getProduct()->getId();
                $el['product_name'] = $cartProduct->getProduct()->getName();
                $el['price'] = $cartProduct->getProduct()->getPrice($this->versionService->getSiteVersion());
                $el['qnt'] = $cartProduct->getCount();
                $el['product_url'] = 'https://stommarket.ru/'.$cartProduct->getProduct()->getUrl();
                $el['product_image_url'] = 'https://stommarket.ru/'.$cartProduct->getProduct()->getImage('hr');
                $cart[] =  $el;
            }
        }
        if($this->getUser()){
            $email = $this->getUser()->getEmail();
        } else {
            $email = NULL;
        }
        return $this->json([
            'cart' => $cart,
            'email' => $email
        ]);
    }

    /**
     * @Route("/product-json/", name="product_json")
     * @Method("GET")
     */
    public function getProductJson(Request $request, ProductRepository $productRepository)
    {
        $product = $productRepository->find((int)$request->get('id'));
        $serializer = $this->get('serializer');
        $return = $serializer->serialize($product,'json', ['groups' => ['json']]);
        return $this->json($return);
    }
}
