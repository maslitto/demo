<?php


namespace App\Controller;

use App\Repository\CatalogRepository;
use App\Repository\ProductRepository;
use App\Services\PriceService;
use App\Services\VersionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * Price list controller
 *
 * @Route("/price")
*/

class PricelistController extends AbstractController
{

    use \App\Traits\Meta;
    private $versionService;
    private $session;
    public function __construct(VersionService $versionService)
    {
        $this->versionService = $versionService;
        $this->session = new Session(new PhpBridgeSessionStorage());
        $this->session->start();
    }

    /**
     * @Route("/", name="pricelist_page")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(CatalogRepository $catalogRepository)
    {
        $catalogs = $catalogRepository->findBy(['lvl' => 1, 'active' => 1]);
        $meta = $this->getMetaObject(
            'Цены на стоматологические материалы, стоимость товаров для стоматологов в СПб - интернет-магазин «Стоммаркет»',
            'Интернет-магазин «Стоммаркет» предлагает оптовые цены на стоматологические материалы от ведущих производителей. Товары для стоматологов с доставкой по Санкт-Петербургу и России',
            'цены на стоматологические материалы, стоимость, товары для стоматологии, интернет-магазин стоммаркет'
        );
        return $this->render('pricelist/index.html.twig',[
            'catalogs' => $catalogs,
            'meta' => $meta
        ]);
    }

    /**
     * @Route("/get/{id}/", name="pricelist_get",defaults={"id" : 0} ,requirements={"id"="\d+"})
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function getPrice(int $id = 0, CatalogRepository $catalogRepository, PriceService $priceService)
    {
        if($id == 0){
            $catalog = $catalogRepository->findOneBy(['lvl' => 0]);
        } else {
            $catalog = $catalogRepository->findOneBy(['id' => $id]);
        }
        //$writer = $priceService->getExcel($catalog, 'catalog');
        $writer = $priceService->getDummyPrice();

        $fileName = (new \Denismitr\Translit\Translit)->forString($catalog->getName())->getSlug();
        ob_start();
        $writer->save('php://output');
        return new Response(
            ob_get_clean(),  // read from output buffer
            200,
            array(
                'Content-Type' => 'text/vnd.ms-excel; charset=utf-8',
                'Content-Disposition' =>  'attachment; filename="'.$fileName.'-stommarket-price.xlsx"',
            )
        );

    }

    /**
     * @Route("/franchize/", name="pricelist_franchize")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function getPricelistFranchize(PriceService $priceService,ProductRepository $productRepository,Request $request)
    {
        $ids = $this->session->get('price',[]);
        if(sizeof($ids) > 0){
            $products = $productRepository->findByIds($ids);
        } else {
            $this->addFlash('error','Прайс пустой');
            $referer = $request->headers->get('referer');
            return $this->redirect($referer);
        }
        $writer = $priceService->getFranchizePrice($products);
        $fileName = 'prise_franchize_all.xlsx';
        ob_start();
        $writer->save('php://output');
        return new Response(
            ob_get_clean(),  // read from output buffer
            200,
            array(
                'Content-Type' => 'text/vnd.ms-excel; charset=utf-8',
                'Content-Disposition' =>  'attachment; filename="'.$fileName.'"',
            )
        );

    }

    /**
     * @Route("/franchize/all/", name="pricelist_franchize_all")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function getPricelistFranchizeAll(PriceService $priceService,ProductRepository $productRepository)
    {

        $criteria = new \Doctrine\Common\Collections\Criteria();
        $criteria->where($criteria->expr()->gt('priceFranchize', 0))->andWhere($criteria->expr()->eq('active',1 ));
        $products = $productRepository->matching($criteria);

        $writer = $priceService->getFranchizePrice($products);
        $fileName = 'prise_franchize_all.xlsx';
        ob_start();
        $writer->save('php://output');
        return new Response(
            ob_get_clean(),  // read from output buffer
            200,
            array(
                'Content-Type' => 'text/vnd.ms-excel; charset=utf-8',
                'Content-Disposition' =>  'attachment; filename="'.$fileName.'"',
            )
        );

    }



}
