<?php


namespace App\Controller;

use App\Entity\Feedback;
use App\Repository\CatalogRepository;
use App\Repository\ProductBrandRepository;
use App\Repository\ProductRepository;
use App\Services\VersionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class SitemapController extends AbstractController
{

    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route({"/sitemap.xml","/sitemap.xml/"}, name="sitemap", defaults={"_format"="xml"})
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(
        Request $request,
        ProductRepository $productRepository,
        CatalogRepository $catalogRepository,
        ProductBrandRepository $productBrandRepository
    )
    {
        $urls = [];
        $hostname = 'https://'.$request->getHost();
        $urls[] = ['loc' => $this->get('router')->generate('index_page'), 'changefreq' => 'weekly', 'priority' => '1.0'];
        $products = $productRepository->findBy(['active' => 1]);
        $catalogs = $catalogRepository->findBy(['active' => 1]);
        $brands = $productBrandRepository->findBy(['active' => 1]);
        foreach ($products as $product) {
            $urls[] = ['loc' => '/catalog/'.$product->getUrl(), 'changefreq' => 'weekly', 'priority' => '1.0'];
            $urls[] = ['loc' => '/catalog/'.$product->getUrl().'comments/', 'changefreq' => 'weekly', 'priority' => '1.0'];
        }

        foreach ($catalogs as $catalog) {
            $urls[] = ['loc' => '/catalog/'.$catalog->getUrlPath(), 'changefreq' => 'weekly', 'priority' => '1.0'];
        }

        foreach ($brands as $brand) {
            $urls[] = ['loc' => '/catalog/brands/'.$brand->getUrl(), 'changefreq' => 'weekly', 'priority' => '1.0'];
        }

        /*foreach ($em->getRepository('AcmeSampleStoreBundle:Product')->findAll() as $product) {
            $urls[] = array('loc' => $this->get('router')->generate('home_product_detail',
                array('productSlug' => $product->getSlug())), 'priority' => '0.5');
        }*/
        return $this->render('sitemap/sitemap.xml.twig',[
            'hostname' => $hostname,
            'urls' => $urls
        ]);
    }


}
