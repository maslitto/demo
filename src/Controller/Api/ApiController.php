<?php


namespace App\Controller\Api;

use App\Entity\ApiKey;
use App\Entity\Product;
use App\Repository\ApiKeyRepository;
use App\Repository\ProductRepository;
use App\Services\Api\ApiService;
use App\Services\VersionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Franchise api controller
 *
 * @Route("/api/")
*/

class ApiController extends AbstractController
{
    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("nomenclature/", name="nomenclature_get_api")
     * @Method("GET")
     */

    public function getNomenclature(ApiKeyRepository $apiKeyRepository, Request $request, ApiService $apiService)
    {
        //return new Response('сегодня в связи с распродажей цены не выгружаем',200, ['Content-Type' => 'text/html']);

        $key = $request->query->get('key',NULL);
        $apiKey = $apiKeyRepository->findOneBy(['key' => $key]);
        if(!$apiKey){
            return new Response('wrong api key',403);
        }
        else{
            $apiService->prepareNomenclature();
            $nomenclatureXmlData = $apiService->getNomenclature();
            return new Response($nomenclatureXmlData,200, ['Content-Type' => 'text/xml']);
        }
    }

    /**
     * @Route("orders/", name="orders_get_api")
     * @Method("GET")
     */

    public function getOrders(ApiKeyRepository $apiKeyRepository, Request $request, ApiService $apiService)
    {
        $key = $request->query->get('key',NULL);
        $fromTime = $request->query->get('from-time',NULL);
        /** @var $apiKey ApiKey */
        $apiKey = $apiKeyRepository->findOneBy(['key' => $key]);
        if(!$apiKey){
            return new Response('wrong api key',403);
        }
        else{
            $ordersXmlData = $apiService->getOrders($apiKey, $fromTime);
            return new Response($ordersXmlData,200, ['Content-Type' => 'text/xml']);
        }
    }

    /**
     * @Route("nomenclature/products/update/", name="nomenclature_post_api")
     * @Method("POST")
     */

    public function postNomenclature(ApiKeyRepository $apiKeyRepository, Request $request, ApiService $apiService)
    {

        //file_put_contents('/var/sites/symfony/log.txt', print_r($request, true));
        $key = $request->request->get('key', NULL);
        $productsJson = $request->request->get('products', NULL);
        /** @var $apiKey ApiKey */
        $apiKey = $apiKeyRepository->findOneBy(['key' => $key]);
        if(!$apiKey){
            return new Response('wrong api key',403);
        }
        else{
            $apiService->setNomenclature($apiKey, $productsJson);
            return new Response('success',200);
        }
    }

}
