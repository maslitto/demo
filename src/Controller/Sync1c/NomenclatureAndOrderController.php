<?php
/**
 * Created by PhpStorm.
 * User: kip
 * Date: 29/05/18
 * Time: 12:07
 */

namespace App\Controller\Sync1c;

use App\Services\VersionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\Sync1c\AuthService;
use App\Services\Sync1c\NomenclatureService;
use App\Services\Sync1c\OrderService;
/**
 * Sync 1c nomenclature and order controller
 *
 * @Route("/sync")
 */

class NomenclatureAndOrderController extends AbstractController
{
    private const NOMENCLATURE_SYNC_TYPE = 'catalog';
    private const ORDER_SYNC_TYPE = 'sale';

    private const ORDER_INIT_MODE = 'init';
    private const ORDER_FILE_MODE = 'file';
    private const ORDER_QUERY_MODE = 'query';
    private const ORDER_SUCCESS_MODE = 'success';

    private const AUTH_USER = 'test';
    private const AUTH_PASSWORD = 'test';

    private $authService;
    private $orderService;
    private $nomenclatureService;

    public function __construct(
        AuthService $authService,
        OrderService $orderService,
        NomenclatureService $nomenclatureService
    )
    {
        $this->authService = new AuthService('test','test');
        $this->orderService = $orderService;
        $this->nomenclatureService = $nomenclatureService;
    }


    /**
     * Entry point to all sync processes
     * @Route("/", name="nomenclature_and_order_index")
     * @Method({"GET","POST"})
     */
    public function index(Request $request,VersionService $versionService): Response
    {
        ini_set('memory_limit', '-1');
        set_time_limit(1200);
        if(!in_array($versionService->getId(), [1,2]) ){
            return new Response('', Response::HTTP_OK);
        }
        $passwordCookie = $request->cookies->get('secret', NULL);
        $mode = $request->query->get('mode');
        $syncType = $request->query->get('type');

        if($mode =='checkauth'){
            $user = trim($request->server->get('PHP_AUTH_USER',''));
            $password = trim($request->server->get('PHP_AUTH_PW',''));
            if($user != self::AUTH_USER || $password != self::AUTH_PASSWORD){
                return new Response('Неверный логин или пароль!',Response::HTTP_FORBIDDEN);
            } else {
                $cookie = password_hash(microtime().'tr4jg', PASSWORD_DEFAULT);
               return new Response('success'."\n"."secret"."\n". $cookie ."\n");
            }
        }

        if(self::NOMENCLATURE_SYNC_TYPE == $syncType) {
            $fileName = $request->query->get('filename',NULL);
            $message = $this->nomenclatureService->syncNomenclature($mode, $fileName);
            $response = new Response($message);
        }
        elseif(self::ORDER_SYNC_TYPE == $syncType) {
            switch ($mode) {
                case self::ORDER_INIT_MODE:
                    $response = new Response("zip=yes"."\n"."file_limit=100000000");
                    break;
                case self::ORDER_FILE_MODE:
                    $response = new Response("success");
                    break;
                case self::ORDER_QUERY_MODE:
                    $fromTime = $request->query->get('from-time');
                    $orderResponseMessage = $this->orderService->getXmlOrders($fromTime);

                    header('Content-Type: text/html; charset=cp1251');
                    die(iconv("utf-8", "cp1251", $orderResponseMessage));
                    break;
                case self::ORDER_SUCCESS_MODE:
                    die("success");
                    break;
            }
        }

        return $response;
    }
}