<?php


namespace App\Controller;

use App\Entity\Feedback;
use App\Repository\ProductRepository;
use App\Services\VersionService;
use App\Services\YandexMarketService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Контроллер выгружает csv с товарами для facebook и vk. Будто бы это говно кому-то нужно?
 */
class TargetController extends AbstractController
{

    /**
     * @Route("/fbtaglistik/", name="facebook_csv")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function facebooksCsv(ProductRepository $productRepository)
    {
        $products = $productRepository->getActiveProducts();
        $response = new Response();
        $response->setContent($this->renderView('target/facebook.csv.twig',[
            'products' => $products
        ]));
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="facebook.csv"');

        return $response;
    }
    /**
     * @Route("/vktaglistt/", name="vk_yml")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function vkYml(ProductRepository $productRepository)
    {
        $products = $productRepository->getActiveProducts();
        $response = new Response();
        $response->setContent($this->renderView('target/vk.xml.twig',[
            'products' => $products,
            'now' => new \DateTime()
        ]));
        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }

}
