<?php


namespace App\Controller;

use App\Entity\Feedback;
use App\Services\VersionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Price list controller
 *
 * @Route("/about")
*/

class AboutController extends AbstractController
{
    use \App\Traits\Meta;
    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("/", name="about_page")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index()
    {
        $meta = $this->getMetaObject('О компании');
        return $this->render('about/index.html.twig',[
            'meta' => $meta
        ]);
    }


}
