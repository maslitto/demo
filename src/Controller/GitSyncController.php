<?php


namespace App\Controller;

use App\Entity\Feedback;
use App\Services\VersionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Git synchronization controller
 *
 * Вызывается хуком битбакета, после того как репозиторий был запушен.
 * Запускает через шелл команду "git pull"
 *
 * @Route("/gitsync")
*/

class GitSyncController extends AbstractController
{

    /**
     * @Route("/", name="git_sync_page")
     * @Method({"GET","POST"})
     *
     */
    public function index()
    {
        //adgasfa sg ssgd a
        echo  shell_exec('/home/k1paris/backup_scripts/gitsync.sh 2>&1');
        return $this->json('Success sync!');
    }


}
