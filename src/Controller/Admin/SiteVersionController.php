<?php


namespace App\Controller\Admin;

use App\Entity\Manager;
use App\Entity\SiteVersion;
use App\Repository\ManagerRepository;
use App\Repository\SiteVersionRepository;
use App\Services\VersionService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Admin SiteVersion controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/site-version")
*/

class SiteVersionController extends AbstractController
{

    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("/", name="admin_site_version_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(SiteVersionRepository $siteVersionRepository, ManagerRepository $managerRepository, Request $request)
    {
        $versionId = 1;
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $siteVersions = $siteVersionRepository->getSiteVersionsPaginated($filter, $page, $versionId);
        return $this->render('admin/site_version/index.html.twig',[
            'siteVersions' => $siteVersions
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_site_version_edit")
     * @Route("/add/", name="admin_site_version_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, SiteVersionRepository $siteVersionRepository)
    {
        if($id){
            $siteVersion = $siteVersionRepository->findOneBy(['id' => $id]);
        } else{
            $siteVersion = new SiteVersion();
        }
        $form = $this->createFormBuilder($siteVersion)
            ->add('active', CheckboxType::class,['label'=>'Активный','required' => false])
            ->add('isOptPrice', ChoiceType::class,[
                'label'=>'Тип цен(не менять если не знаешь что к чему!)',
                'required' => true,
                'choices'=>[
                    'Розничные цены' => 0,
                    'Оптовые цены' => 1,
                ]
            ])
            ->add('name', TextType::class,['label'=>'Название','required' => false])
            ->add('mobileName', TextType::class,['label'=>'Мобильное название','required' => false])
            ->add('key', TextType::class,['label'=>'Ключ','required' => false])
            ->add('sxgeoName', TextType::class,['label'=>'Название(Sx Geo)','required' => false])
            ->add('storeName', TextType::class,['label'=>'Название склада','required' => false])
            ->add('email', TextType::class,['label'=>'E-mail','required' => false])
            ->add('phone', TextType::class,['label'=>'Телефон','required' => false])
            ->add('mobilePhone', TextType::class,['label'=>'Viber, WhatsApp','required' => false])
            ->add('workTime', TextType::class,['label'=>'Часы работы','required' => false])
            ->add('address', TextType::class,['label'=>'Адрес','required' => false])
            ->add('mapAddress', TextType::class,['label'=>'Ссылка на адрес','required' => false])
            ->add('fb', TextType::class,['label'=>'Facebook страница','required' => false])
            ->add('vk', TextType::class,['label'=>'Vk страница','required' => false])
            ->add('footerLine1', TextType::class,['label'=>'Footer line 1','required' => false])
            ->add('footerLine2', TextType::class,['label'=>'Footer line 1','required' => false])
            //->add('deliveryPrice', TextType::class,['label'=>'Цена доставки','required' => false])
            ->add('head', TextAreaType::class,['label'=>'Head','required' => false, 'attr' => ['class' => 'js-ace']])
            ->add('metrika', TextAreaType::class,['label'=>'Metrika','required' => false, 'attr' => ['class' => 'js-ace']])
            ->add('footer', TextAreaType::class,['label'=>'Footer scripts','required' => false, 'attr' => ['class' => 'js-ace']])

            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $siteVersion = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($siteVersion);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_site_version_edit',['id' => $siteVersion->getId()]);
        }

        return $this->render('admin/site_version/edit.html.twig',[
            'form' => $form->createView(),
            'siteVersion' => $siteVersion
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_site_version_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, SiteVersionRepository $siteVersionRepository)
    {
        $siteVersion = $siteVersionRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($siteVersion);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_site_version_index');
    }

}
