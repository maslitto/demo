<?php


namespace App\Controller\Admin;

use App\Entity\SiteVersion;
use App\Entity\Slider;
use App\Form\CkfinderType;
use App\Repository\OrderRepository;
use App\Repository\SiteVersionRepository;
use App\Repository\SliderRepository;
use App\Services\VersionService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Admin Slider controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/slider")
*/

class SliderController extends AbstractController
{

    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("/", name="admin_slider_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(SliderRepository $sliderRepository)
    {
        $sliders = $sliderRepository->getSlidersSortedByActive();
        return $this->render('admin/slider/index.html.twig',['sliders' => $sliders]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_slider_edit")
     * @Route("/add/", name="admin_slider_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, SliderRepository $sliderRepository, SiteVersionRepository $siteVersionRepository)
    {
        if($id){
            $slider = $sliderRepository->findOneBy(['id' => $id]);
        } else{
            $slider = new Slider();
        }
        $versions = $siteVersionRepository->findBy(['active' => true]);
        foreach ($versions as $k =>$version) {
            $choices[$version->getName()] = $version->getId();
        }
        $form = $this->createFormBuilder($slider)
            ->add('active', CheckboxType::class,['label'=>'Активный','required' => false])
            //->add('image', TextType::class,['label'=>'image'])
            ->add('image', CkfinderType::class,['label'=>'путь к картинке','required' => false])
            ->add('mobileImage', CkfinderType::class,['label'=>'Картинка для мобильной','required' => false])
            ->add('header', TextType::class,['label'=>'Заголовок','required' => false])
            ->add('description', TextareaType::class,['label'=>'Описание','required' => false])
            ->add('url', TextType::class,['label'=>'Url','required' => false])
            ->add('btnText', TextType::class,['label'=>'Заголовок кнопки','required' => false])
            ->add('sort', TextType::class,['label'=>'Сортировка','required' => false])
            ->add('backgroundStyle', TextAreaType::class,['label'=>'Background style','required' => false])
            ->add('btnInlineJs', TextAreaType::class,['label'=>'Button inline JS','required' => false, 'attr' => ['class' => 'js-ace']])
            ->add('btnInlineCss', TextAreaType::class,['label'=>'Button inline CSS','required' => false, 'attr' => ['class' => 'js-ace']])
            ->add('buttonClass', TextAreaType::class,['label'=>'Button class','required' => false, 'attr' => ['class' => 'js-ace']])
            ->add('versions', ChoiceType::class,[
                'choices'  => $choices,
                'label'=>'Отображать для версий',
                'multiple' => true,
                'required' => false
            ])
            /*->add('siteVersion', EntityType::class, array(
                'class' => SiteVersion::class,
                'choice_label' => 'name',
                'label' => 'Город(версия)'
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ))*/
            ->add('save', SubmitType::class, array('label' => 'Сохранить слайд'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $slider = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($slider);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_slider_edit',['id' => $slider->getId()]);
        }

        return $this->render('admin/slider/edit.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_slider_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, SliderRepository $sliderRepository)
    {
        $slider = $sliderRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($slider);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_slider_index');
    }

}
