<?php


namespace App\Controller\Admin;

use App\Entity\Catalog;
use App\Entity\SiteVersion;
use App\Entity\Tag;
use App\Repository\CatalogRepository;
use App\Repository\TagRepository;
use App\Services\VersionService;
use Denismitr\Translit\Translit;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Admin Site Tags controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/tag")
*/

class TagController extends AbstractController
{

    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("/", name="admin_tag_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(TagRepository $tagRepository, Request $request)
    {
        $versionId = 1;
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $tags = $tagRepository->getTagsPaginated($filter, $page, $versionId);
        return $this->render('admin/tag/index.html.twig',[
            'tags' => $tags,
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_tag_edit")
     * @Route("/add/", name="admin_tag_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, TagRepository $tagRepository)
    {
        if($id){
            $tag = $tagRepository->findOneBy(['id' => $id]);
        } else{
            $tag = new Tag();
        }
        $form = $this->createFormBuilder($tag)
            ->add('active', ChoiceType::class,[
                'choices'  => [
                    'Да' => 1,
                    'Нет' => 0,
                ],
                'label'=>'Показать на сайте?'])
            ->add('name', TextType::class,['label'=>'Название','required' => false])
            ->add('url', TextType::class,['label'=>'Url','required' => false])
            ->add('title', TextType::class,['label'=>'Title','required' => false])
            ->add('text', TextAreaType::class,['label'=>'Текст','required' => false,'attr' => ['class' => 'js-ckeditor']])
            ->add('catalogs', EntityType::class, [
                'class' => Catalog::class,
                'choice_label' => function ($catalog) {
                    return $catalog->getName();
                },
                'empty_data' => [],
                'required'   => false,
                'placeholder' => 'Пусто',
                'label' => 'Категории',
                'multiple' => true,
                'query_builder' => function (CatalogRepository $catalogRepository) {
                    return $catalogRepository->getCatalogsQueryBuilder([])->orderBy('node.name','ASC');
                },
                'attr' => ['class' => 'js-select2']
            ])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Tag $tag */
            $tag = $form->getData();
            $slug = (new Translit($tag->getName()))->getSlug();
            $tag->setUrl($slug);
            $entityTag = $this->getDoctrine()->getManager();
            $entityTag->persist($tag);
            $entityTag->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_tag_edit',['id' => $tag->getId()]);
        }

        return $this->render('admin/tag/edit.html.twig',[
            'form' => $form->createView(),
            'tag' => $tag
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_tag_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, TagRepository $tagRepository)
    {
        $tag = $tagRepository->findOneBy(['id' => $id]);
        $entityTag = $this->getDoctrine()->getManager();
        $entityTag->remove($tag);
        $entityTag->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_tag_index');
    }

}
