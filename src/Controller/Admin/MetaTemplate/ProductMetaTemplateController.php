<?php


namespace App\Controller\Admin\MetaTemplate;

use App\Entity\ProductMetaTemplate;
use App\Repository\ProductMetaTemplateRepository;
use App\Services\VersionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Product meta template controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("admin/seo/product-meta-template")
*/

class ProductMetaTemplateController extends AbstractController
{


    /**
     * @Route("/", name="admin_product_meta_template_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(ProductMetaTemplateRepository $productMetaTemplateRepository)
    {
        $productMetaTemplates = $productMetaTemplateRepository->getProductMetaTemplatesPaginated();
        return $this->render('admin/product_meta_templates/index.html.twig',[
            'productMetaTemplates' => $productMetaTemplates
        ]);

    }

    /**
     * @Route("/edit/{id}/", name="admin_product_meta_template_edit")
     * @Route("/add/", name="admin_product_meta_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, ProductMetaTemplateRepository $productMetaTemplateRepository)
    {
        if($id){
            $productMetaTemplate = $productMetaTemplateRepository->findOneBy(['id' => $id]);
        } else{
            $productMetaTemplate = new ProductMetaTemplate();
        }
        $form = $this->createFormBuilder($productMetaTemplate)

            ->add('metaTitle', TextType::class,['label'=>'Meta Title','required' => false])
            ->add('metaKeywords', TextType::class,['label'=>'Meta Keywords','required' => false])
            ->add('metaDescription', TextType::class,['label'=>'Meta Description','required' => false])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productMetaTemplate = $form->getData();
            $entityReview = $this->getDoctrine()->getManager();
            $entityReview->persist($productMetaTemplate);
            $entityReview->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_product_meta_template_edit',['id' => $productMetaTemplate->getId()]);
        }

        return $this->render('admin/product_meta_templates/edit.html.twig',[
            'form' => $form->createView(),
            'productMetaTemplate' => $productMetaTemplate
        ]);
    }


}
