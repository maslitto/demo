<?php


namespace App\Controller\Admin\MetaTemplate;

use App\Entity\BrandMetaTemplate;
use App\Repository\BrandMetaTemplateRepository;
use App\Services\VersionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Brand meta template controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("admin/seo/brand-meta-template")
*/

class BrandMetaTemplateController extends AbstractController
{


    /**
     * @Route("/", name="admin_brand_meta_template_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(BrandMetaTemplateRepository $brandMetaTemplateRepository)
    {
        $brandMetaTemplates = $brandMetaTemplateRepository->getBrandMetaTemplatesPaginated();
        return $this->render('admin/brand_meta_templates/index.html.twig',[
            'brandMetaTemplates' => $brandMetaTemplates
        ]);

    }

    /**
     * @Route("/edit/{id}/", name="admin_brand_meta_template_edit")
     * @Route("/add/", name="admin_brand_meta_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, BrandMetaTemplateRepository $brandMetaTemplateRepository)
    {
        if($id){
            $brandMetaTemplate = $brandMetaTemplateRepository->findOneBy(['id' => $id]);
        } else{
            $brandMetaTemplate = new BrandMetaTemplate();
        }
        $form = $this->createFormBuilder($brandMetaTemplate)

            ->add('metaTitle', TextType::class,['label'=>'Meta Title','required' => false])
            ->add('metaKeywords', TextType::class,['label'=>'Meta Keywords','required' => false])
            ->add('metaDescription', TextType::class,['label'=>'Meta Description','required' => false])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $brandMetaTemplate = $form->getData();
            $entityReview = $this->getDoctrine()->getManager();
            $entityReview->persist($brandMetaTemplate);
            $entityReview->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_brand_meta_template_edit',['id' => $brandMetaTemplate->getId()]);
        }

        return $this->render('admin/brand_meta_templates/edit.html.twig',[
            'form' => $form->createView(),
            'brandMetaTemplate' => $brandMetaTemplate
        ]);
    }


}
