<?php


namespace App\Controller\Admin\MetaTemplate;

use App\Entity\CatalogMetaTemplate;
use App\Repository\CatalogMetaTemplateRepository;
use App\Services\VersionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Catalog meta template controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("admin/seo/catalog-meta-template")
*/

class CatalogMetaTemplateController extends AbstractController
{


    /**
     * @Route("/", name="admin_catalog_meta_template_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(CatalogMetaTemplateRepository $catalogMetaTemplateRepository)
    {
        $catalogMetaTemplates = $catalogMetaTemplateRepository->getCatalogMetaTemplatesPaginated();
        return $this->render('admin/catalog_meta_templates/index.html.twig',[
            'catalogMetaTemplates' => $catalogMetaTemplates
        ]);

    }

    /**
     * @Route("/edit/{id}/", name="admin_catalog_meta_template_edit")
     * @Route("/add/", name="admin_catalog_meta_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, CatalogMetaTemplateRepository $catalogMetaTemplateRepository)
    {
        if($id){
            $catalogMetaTemplate = $catalogMetaTemplateRepository->findOneBy(['id' => $id]);
        } else{
            $catalogMetaTemplate = new CatalogMetaTemplate();
        }
        $form = $this->createFormBuilder($catalogMetaTemplate)

            ->add('metaTitle', TextType::class,['label'=>'Meta Title','required' => false])
            ->add('metaKeywords', TextType::class,['label'=>'Meta Keywords','required' => false])
            ->add('metaDescription', TextType::class,['label'=>'Meta Description','required' => false])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $catalogMetaTemplate = $form->getData();
            $entityReview = $this->getDoctrine()->getManager();
            $entityReview->persist($catalogMetaTemplate);
            $entityReview->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_catalog_meta_template_edit',['id' => $catalogMetaTemplate->getId()]);
        }

        return $this->render('admin/catalog_meta_templates/edit.html.twig',[
            'form' => $form->createView(),
            'catalogMetaTemplate' => $catalogMetaTemplate
        ]);
    }


}
