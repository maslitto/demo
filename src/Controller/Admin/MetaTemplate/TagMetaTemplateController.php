<?php


namespace App\Controller\Admin\MetaTemplate;

use App\Entity\TagMetaTemplate;
use App\Repository\TagMetaTemplateRepository;
use App\Services\VersionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Tag meta template controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("admin/seo/tag-meta-template")
*/

class TagMetaTemplateController extends AbstractController
{


    /**
     * @Route("/", name="admin_tag_meta_template_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(TagMetaTemplateRepository $tagMetaTemplateRepository)
    {
        $tagMetaTemplates = $tagMetaTemplateRepository->getTagMetaTemplatesPaginated();
        return $this->render('admin/tag_meta_templates/index.html.twig',[
            'tagMetaTemplates' => $tagMetaTemplates
        ]);

    }

    /**
     * @Route("/edit/{id}/", name="admin_tag_meta_template_edit")
     * @Route("/add/", name="admin_tag_meta_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, TagMetaTemplateRepository $tagMetaTemplateRepository)
    {
        if($id){
            $tagMetaTemplate = $tagMetaTemplateRepository->findOneBy(['id' => $id]);
        } else{
            $tagMetaTemplate = new TagMetaTemplate();
        }
        $form = $this->createFormBuilder($tagMetaTemplate)
            ->add('metaTitle', TextType::class,['label'=>'Meta Title','required' => false])
            ->add('metaKeywords', TextType::class,['label'=>'Meta Keywords','required' => false])
            ->add('metaDescription', TextType::class,['label'=>'Meta Description','required' => false])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tagMetaTemplate = $form->getData();
            $entityReview = $this->getDoctrine()->getManager();
            $entityReview->persist($tagMetaTemplate);
            $entityReview->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_tag_meta_template_edit',['id' => $tagMetaTemplate->getId()]);
        }

        return $this->render('admin/tag_meta_templates/edit.html.twig',[
            'form' => $form->createView(),
            'tagMetaTemplate' => $tagMetaTemplate
        ]);
    }


}
