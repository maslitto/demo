<?php


namespace App\Controller\Admin;

use App\Entity\DeliveryType;
use App\Entity\Manager;
use App\Entity\OrderSms;
use App\Entity\OrderStatus;
use App\Entity\SiteVersion;
use App\Entity\Order;
use App\Repository\ManagerRepository;
use App\Repository\OrderRepository;
use App\Services\VersionService;
use App\Utils\Sms;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Admin Order controller
 *
 * @Route("/admin/order")
*/

class OrderController extends AbstractController
{

    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("/", name="admin_order_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(OrderRepository $orderRepository, ManagerRepository $managerRepository, Request $request)
    {
        $versionId = $this->versionService->getId();
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $managers = $managerRepository->getManagersByVersion($versionId);
        $orders = $orderRepository->getOrdersPaginated($filter, $page, $versionId);
        return $this->render('admin/order/index.html.twig',[
            'orders' => $orders,
            'managers' => $managers
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_order_edit")
     * @Route("/add/", name="admin_order_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, OrderRepository $orderRepository)
    {
        if($id){
            $order = $orderRepository->findOneBy(['id' => $id]);
        } else{
            $order = new Order();
        }
        $form = $this->createFormBuilder($order)
            ->add('timeCreate', DateTimeType::class,['label'=>'Дата создания','disabled'=>true])
            ->add('encryptedId', TextType::class,['label'=>'Номер заказа','required' => false,'disabled'=>true])
            ->add('orderStatus', EntityType::class, array(
                'class' => OrderStatus::class,
                'choice_label' => 'name',
                'label' => 'Статус заказа'
            ))
            ->add('manager', EntityType::class, array(
                'class' => Manager::class,
                'choice_label' => 'name',
                'label' => 'Менеджер',
            ))
            ->add('username', TextType::class,['label'=>'Имя','required' => false])
            ->add('email', TextType::class,['label'=>'Email','required' => false])
            ->add('phone', TextType::class,['label'=>'Контактный телефон','required' => false])
            ->add('description', TextAreaType::class,['label'=>'Комментарий','required' => false])
            ->add('delivery', EntityType::class, array(
                'class' => DeliveryType::class,
                'choice_label' => 'name',
                'label' => 'Доставка',
            ))
            ->add('address', TextAreaType::class,['label'=>'Адрес','required' => false])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $order = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($order);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_order_edit',['id' => $order->getId()]);
        }

        return $this->render('admin/order/edit.html.twig',[
            'form' => $form->createView(),
            'order' => $order
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_order_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, OrderRepository $orderRepository)
    {
        $order = $orderRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($order);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_order_index');
    }

    /**
     * @Route("/send-sms/", name="admin_order_send_sms")
     * @Method("POST")
     *
     */
    public function sendSms(Request $request, OrderRepository $orderRepository,Sms $sms)
    {
        $data = $request->request->all();
        /** @var Order $order */
        $order = $orderRepository->findOneBy(['id' =>(int) $data['order_id']]);

        $sms->send($order->getPhone(),$data['text']);

        $orderSms = new OrderSms();
        $orderSms->setOrder($order);
        $orderSms->setText($data['text']);


        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($orderSms);
        $entityManager->flush();
        $this->addFlash('success','SMS отправлена');
        return $this->redirectToRoute('admin_order_edit',['id' => $order->getId()]);
    }

}
