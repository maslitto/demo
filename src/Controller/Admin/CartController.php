<?php


namespace App\Controller\Admin;

use App\Entity\Manager;
use App\Entity\OrderStatus;
use App\Entity\SiteVersion;
use App\Entity\Order;
use App\Repository\CartRepository;
use App\Repository\ManagerRepository;
use App\Repository\OrderRepository;
use App\Services\VersionService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Admin Cart controller
 * Брошенные корзины
 *
 * @Route("/admin/cart")
*/

class CartController extends AbstractController
{

    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("/", name="admin_cart_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(CartRepository $cartRepository, Request $request)
    {
        $versionId = 1;
        $page = $request->query->get('page',1);
        $carts = $cartRepository->getCartsPaginated($page, $versionId);
        return $this->render('admin/cart/index.html.twig',[
            'carts' => $carts,
        ]);
    }
    /**
     * @Route("/show/{id}/", name="admin_cart_show")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function show(CartRepository $cartRepository, Request $request,int $id)
    {
        $cart = $cartRepository->findOneBy(['id' => $id]);
        return $this->render('admin/cart/show.html.twig',[
            'cart' => $cart,
        ]);
    }

}
