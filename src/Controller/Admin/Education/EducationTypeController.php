<?php


namespace App\Controller\Admin\Education;

use App\Entity\Education\EducationType;
use App\Repository\Education\EducationTypeRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * EducationType controller
 *
 * @Route("/admin/education/type")
*/

class EducationTypeController extends AbstractController
{


    /**
     * @Route("/", name="admin_education_type_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(EducationTypeRepository $educationTypeRepository, Request $request)
    {
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $educationTypes = $educationTypeRepository->getEducationTypesPaginated($filter, $page);
        return $this->render('admin/education/education_type/index.html.twig',[
            'educationTypes' => $educationTypes,
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_education_type_edit")
     * @Route("/add/", name="admin_education_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, EducationTypeRepository $educationTypeRepository)
    {
        if($id){
            $educationType = $educationTypeRepository->findOneBy(['id' => $id]);
        } else{
            $educationType = new EducationType();
        }
        $form = $this->createFormBuilder($educationType)
            ->add('active', ChoiceType::class,[
                'choices'  => [
                    'Да' => 1,
                    'Нет' => 0,
                ],
                'label'=>'Активный?'])
            ->add('name', TextType::class,['label'=>'Имя','required' => false])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $educationType = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($educationType);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_education_type_edit',['id' => $educationType->getId()]);
        }

        return $this->render('admin/education/education_type/edit.html.twig',[
            'form' => $form->createView(),
            'educationType' => $educationType
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_education_type_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, EducationTypeRepository $educationTypeRepository)
    {
        $educationType = $educationTypeRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($educationType);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_education_type_index');
    }

}

