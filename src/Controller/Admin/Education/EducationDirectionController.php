<?php


namespace App\Controller\Admin\Education;

use App\Entity\Education\EducationDirection;
use App\Repository\Education\EducationDirectionRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * EducationDirection controller
 *
 * @Route("/admin/education/direction")
*/

class EducationDirectionController extends AbstractController
{


    /**
     * @Route("/", name="admin_education_direction_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(EducationDirectionRepository $educationDirectionRepository, Request $request)
    {
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $educationDirections = $educationDirectionRepository->getEducationDirectionsPaginated($filter, $page);
        return $this->render('admin/education/education_direction/index.html.twig',[
            'educationDirections' => $educationDirections,
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_education_direction_edit")
     * @Route("/add/", name="admin_education_direction_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, EducationDirectionRepository $educationDirectionRepository)
    {
        if($id){
            $educationDirection = $educationDirectionRepository->findOneBy(['id' => $id]);
        } else{
            $educationDirection = new EducationDirection();
        }
        $form = $this->createFormBuilder($educationDirection)
            ->add('active', ChoiceType::class,[
                'choices'  => [
                    'Да' => 1,
                    'Нет' => 0,
                ],
                'label'=>'Активный?'])
            ->add('name', TextType::class,['label'=>'Имя','required' => false])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $educationDirection = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($educationDirection);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_education_direction_edit',['id' => $educationDirection->getId()]);
        }

        return $this->render('admin/education/education_direction/edit.html.twig',[
            'form' => $form->createView(),
            'educationDirection' => $educationDirection
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_education_direction_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, EducationDirectionRepository $educationDirectionRepository)
    {
        $educationDirection = $educationDirectionRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($educationDirection);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_education_direction_index');
    }

}

