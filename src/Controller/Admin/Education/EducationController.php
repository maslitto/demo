<?php


namespace App\Controller\Admin\Education;

use App\Entity\Education\Education;
use App\Entity\Education\EducationDirection;
use App\Entity\Education\EducationStatus;
use App\Entity\SiteVersion;
use App\Form\CkfinderType;
use App\Repository\Education\EducationRepository;
use Proxies\__CG__\App\Entity\Education\EducationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Education controller
 *
 * @Route("/admin/education")
*/

class EducationController extends AbstractController
{


    /**
     * @Route("/", name="admin_education_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(EducationRepository $educationRepository, Request $request)
    {
        $filter = $request->query->all();
        $filter['admin'] = 1;
        $page = $request->query->get('page',1);
        $educations = $educationRepository->getEducationsPaginated($filter, $page, 20);
        return $this->render('admin/education/index.html.twig',[
            'educations' => $educations,
        ]);
    }
    /**
     * @Route("/copy/{id}/", name="admin_education_copy")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function copy(EducationRepository $educationRepository, int $id)
    {
        $education = $educationRepository->findOneBy(['id' => $id]);
        $copy = clone $education;
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($copy);
        $entityManager->flush();

        $this->addFlash('success','Удачное копирование.Id ='.$copy->getId());
        return $this->redirectToRoute('admin_education_index',['id' => $education->getId()]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_education_edit")
     * @Route("/add/", name="admin_education_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, EducationRepository $educationRepository)
    {
        if($id){
            $education = $educationRepository->findOneBy(['id' => $id]);
        } else{
            $education = new Education();
        }
        $form = $this->createFormBuilder($education)
            ->add('active', ChoiceType::class,[
                'choices'  => [
                    'Да' => 1,
                    'Нет' => 0,
                ],
                'label'=>'Показать на сайте?'])
            ->add('educationStatus', EntityType::class, array(
                'class' => EducationStatus::class,
                'choice_label' => 'name',
                'label' => 'Статус'
            ))
            ->add('educationType', EntityType::class, array(
                'class' => EducationType::class,
                'choice_label' => 'name',
                'label' => 'Тип обучения'
            ))
            ->add('educationDirection', EntityType::class, array(
                'class' => EducationDirection::class,
                'choice_label' => 'name',
                'label' => 'Направление обучения'
            ))
            ->add('isFree', ChoiceType::class,[
                'choices'  => [
                    'Да' => 1,
                    'Нет' => 0,
                ],
                'label'=>'Бесплатный?'])
            ->add('price', TextType::class,['label'=>'Цена','required' => false])
            ->add('oldPrice', TextType::class,['label'=>'Старая цена','required' => false])
            ->add('isSingleDay', ChoiceType::class,[
                'choices'  => [
                    'Да' => 1,
                    'Нет' => 0,
                ],
                'label'=>'Однодневный?'])
            ->add('dateStart', DateType::class,['label'=>'Дата начала','required' => false])
            ->add('dateEnd', DateType::class,['label'=>'Дата окончания','required' => false])
            ->add('time', TextType::class,['label'=>'Время','required' => false])
            ->add('name', TextType::class,['label'=>'Название','required' => false])
            ->add('lectorFio', TextType::class,['label'=>'ФИО лектора','required' => false])
            ->add('profession', TextType::class,['label'=>'Должность лектора','required' => false])
            ->add('email', TextType::class,['label'=>'Email','required' => false])
            ->add('phone', TextType::class,['label'=>'Контактный телефон','required' => false])
            ->add('city', TextType::class,['label'=>'Город','required' => false])
            ->add('address', TextType::class,['label'=>'Адрес','required' => false])
            ->add('photo', CkfinderType::class,['label'=>'путь к фото','required' => false])
            ->add('program', TextareaType::class,['label'=>'Программа','required' => false])
            ->add('educationType', EntityType::class, array(
                'class' => EducationType::class,
                'choice_label' => 'name',
                'label' => 'Тип'
            ))

            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $education = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($education);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_education_edit',['id' => $education->getId()]);
        }

        return $this->render('admin/education/edit.html.twig',[
            'form' => $form->createView(),
            'education' => $education
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_education_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, EducationRepository $educationRepository)
    {
        $education = $educationRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($education);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_education_index');
    }

}

