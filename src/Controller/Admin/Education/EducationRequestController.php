<?php


namespace App\Controller\Admin\Education;

use App\Repository\Education\EducationRequestRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * EducationRequest controller
 *
 * @Route("/admin/education/request")
*/

class EducationRequestController extends AbstractController
{


    /**
     * @Route("/", name="admin_education_request_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(EducationRequestRepository $educationRequestRepository, Request $request)
    {
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $educationRequests = $educationRequestRepository->getEducationRequestsPaginated($filter, $page);
        return $this->render('admin/education/education_request/index.html.twig',[
            'educationRequests' => $educationRequests,
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_education_request_edit")
     * @Route("/add/", name="admin_education_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, EducationRequestRepository $educationRequestRepository)
    {
        if($id){
            $educationRequest = $educationRequestRepository->findOneBy(['id' => $id]);
        } else{
            $educationRequest = new EducationRequest();
        }
        $form = $this->createFormBuilder($educationRequest)
            ->add('active', ChoiceType::class,[
                'choices'  => [
                    'Да' => 1,
                    'Нет' => 0,
                ],
                'label'=>'Принимает заказы?'])
            ->add('name', TextType::class,['label'=>'Имя','required' => false])
            ->add('lastName', TextType::class,['label'=>'Фамилия','required' => false])
            ->add('email', TextType::class,['label'=>'Email','required' => false])
            ->add('phone', TextType::class,['label'=>'Контактный телефон','required' => false])
            ->add('siteVersion', EntityType::class, array(
                'class' => SiteVersion::class,
                'choice_label' => 'name',
                'label' => 'Город(версия)'
            ))
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $educationRequest = $form->getData();
            $entityEducationRequest = $this->getDoctrine()->getEducationRequest();
            $entityEducationRequest->persist($educationRequest);
            $entityEducationRequest->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_education_edit',['id' => $educationRequest->getId()]);
        }

        return $this->render('admin/education/edit.html.twig',[
            'form' => $form->createView(),
            'education' => $educationRequest
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_education_request_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, EducationRequestRepository $educationRequestRepository)
    {
        $educationRequest = $educationRequestRepository->findOneBy(['id' => $id]);
        $entityEducationRequest = $this->getDoctrine()->getEducationRequest();
        $entityEducationRequest->remove($educationRequest);
        $entityEducationRequest->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_education_index');
    }

}

