<?php


namespace App\Controller\Admin;

use App\Entity\Catalog;
use App\Entity\Manager;
use App\Entity\ProductBrand;
use App\Entity\ProductImage;
use App\Entity\ProductProp;
use App\Entity\ProductVersion;
use App\Entity\SiteVersion;
use App\Entity\Product;
use App\Form\ImageType;
use App\Form\ProductVersionType;
use App\Form\TagType;
use App\Repository\CatalogRepository;
use App\Repository\ManagerRepository;
use App\Repository\ProductImageRepository;
use App\Repository\ProductPropRepository;
use App\Repository\ProductRepository;
use App\Repository\SiteVersionRepository;
use App\Repository\TagRepository;
use App\Services\ImageService;
use App\Services\VersionService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Admin Product controller
 *
 * @Route("/admin/product")
*/

class ProductController extends AbstractController
{

    private $versionService;
    private $session;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
        $this->session = new Session(new PhpBridgeSessionStorage());
        $this->session->start();
    }

    /**
     * @Route("/", name="admin_product_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(ProductRepository $productRepository, Request $request)
    {
        $versionId = 1;
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $products = $productRepository->filterProducts(NULL, $filter, $versionId, $page);
        return $this->render('admin/product/index.html.twig',[
            'products' => $products,
            'versionId' => $versionId
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/edit/{id}/", name="admin_product_edit")
     * @Route("/add/", name="admin_product_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(
        Request $request,
        int $id = NULL,
        ProductRepository $productRepository,
        TagRepository $tagRepository,
        ProductPropRepository $productPropRepository
    )
    {
        if($id){
            $product = $productRepository->findOneBy(['id' => $id]);
        } else{
            $product = new Product();
        }
        $productForm = $this->createFormBuilder($product,['attr' => ['class' => 'withPlugin-collection']])
            ->add('name', TextType::class,['label'=>'Название'])
            ->add('url', TextType::class,['label'=>'Url'])
            ->add('video', TextType::class,['label'=>'Видео Youtube','required' => false])
            ->add('sameProductsIds',TextType::class,['label'=>'Связанные товары','required' => false])
            ->add('countSpb',TextType::class,['label'=>'Наличие СПб','required' => false])
            ->add('countMsk',TextType::class,['label'=>'Наличие МСК','required' => false])
            ->add('countYaroslavl',TextType::class,['label'=>'Наличие Ярославль','required' => false])
            ->add('countKirov',TextType::class,['label'=>'Наличие Киров','required' => false])
            ->add('countVolgograd',TextType::class,['label'=>'Наличие Волгоград','required' => false])
            ->add('preview',TextAreaType::class,['label'=>'Описание','required' => false, 'attr' => ['class' => 'js-ckeditor']])
            ->add('searchTags',TextAreaType::class,['label'=>'Теги','required' => false])
            //->add('instruction',FileType::class)
            ->add('pdf', VichFileType::class, [
                'required' => false,
                'allow_delete' => true,
                //'download_uri' => '...',
                'download_label' => 'Скачать',
                'delete_label' => 'Удалить',
                'label' => 'Инструкция'

            ])
            ->add('active', ChoiceType::class,[
                'choices'  => [
                    'Да' => 1,
                    'Нет' => 0,
                ], 'label'=>'Показывать на сайте?'])
            ->add('event', ChoiceType::class,[
                'choices'  => [
                    'Sale' => 'sale',
                    'Hit' => 'top',
                    'New' => 'new',
                ], 'label'=>'Акция','required' => false])
            ->add('catalog', EntityType::class, array(
                'class' => Catalog::class,
                'choice_label' => function ($catalog) {
                    return $catalog->getName().' ('.$catalog->getParent()->getName().')';
                },
                'label' => 'Категория',
                'query_builder' => function (CatalogRepository $er) {
                    return $er->getActiveLeafsQueryBuilder()->orderBy('node.name','ASC');
                },
            ))
            ->add('productBrand', EntityType::class, array(
                'class' => ProductBrand::class,
                'choice_label' => 'name',
                'label' => 'Бренд',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('pb')
                        ->where('pb.active = 1')
                        ->orderBy('pb.name', 'ASC');
                },
            ))
            ->add('videoUrl',TextType::class,['label'=>'Видео Url','required' => false])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();

        $productForm->handleRequest($request);
        /** @var Product $product */
        $product = $productForm->getData();
        if(strlen($product->getPreview()) < 15){$product->setPreview(null);};
        if ($productForm->isSubmitted()) {
            $entityManager = $this->getDoctrine()->getManager();

            $extra = $productForm->getExtraData();
            $tagIds = $extra['tags'];
            $tags = $tagRepository->getTagsByIds($tagIds);
            $product->setTags($tags);
            $propsData = $extra['props'];
            $props = [];
            foreach ($propsData as $propData){
                if(empty($propData['key']) || empty($propData['val'])) continue;
                $prop = $productPropRepository->findOneBy([
                    'key' => $propData['key'],
                    'val' => $propData['val'],
                    'id'=>$product->getId()])
                ;
                if(!$prop){
                    $prop = new ProductProp();
                    $prop->setProduct($product);
                    $prop->setKey($propData['key']);
                    $prop->setVal($propData['val']);
                    $entityManager->persist($prop);
                }
                $props[]= $prop;
            }
            $product->setProductProps($props);
            $entityManager->persist($product);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_product_edit',['id' => $product->getId()]);
        }
        $tags = $tagRepository->findBy(['active' => true]);
        return $this->render('admin/product/edit.html.twig',[
            'productForm' => $productForm->createView(),
            'product' => $product,
            'tags' => $tags
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/delete/{id}/", name="admin_product_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, ProductRepository $productRepository)
    {
        $product = $productRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($product);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_product_index');
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/upload-image/", name="admin_product_upload_image")
     * @Method("POST")
     *
     */
    public function uploadImage(Request $request, ImageService $imageService, ProductRepository $productRepository)
    {
        $productId = $request->get('product_id');
        /**
         * @var UploadedFile
         */
        $file = $request->files->get('file');
        /** @var Product $product */
        $product = $productRepository->find($productId);
        $productImage = $imageService->createProductImage($product, $file);
        $response = [
            'image' => $productImage->getImage('s'),
            'id' => $productImage->getId()
        ];
        return $this->json($response);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/remove-image/", name="admin_product_remove_image")
     * @Method("POST")
     *
     */
    public function removeImage(Request $request, ImageService $imageService,ProductImageRepository $productImageRepository)
    {
        $imageId = $request->get('id');
        /** @var ProductImage $productImage */
        $productImage = $productImageRepository->find($imageId);
        if($productImage){
            $imageService->removeImage($productImage);
        }
        $response = [
            'success' => true,
        ];
        return $this->json($response);
    }

    /**
     * @Route("/add-to-price/", name="admin_product_add_to_price")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function addToPrice(Request $request)
    {

        $productId = $request->get('id');
        $price = $this->session->get('price',[]);
        if(!in_array($productId, $price)){
            array_push($price, $productId);
        }
        $this->session->set('price', $price);
        $this->addFlash('success','Товар добавлен в прайс');
        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }
    /**
     * @Route("/remove-from-price/", name="admin_product_remove_from_price")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function removeFromPrice(Request $request)
    {
        $productId = $request->get('id');
        $price = $this->session->get('price',[]);
        if(in_array($productId, $price)){
            $price = array_diff($price, [$productId]);
        }
        $this->session->set('price', $price);
        $this->addFlash('success','Товар удален из прайса');
        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }
    /**
     * @Route("/clear-price/", name="admin_product_clear_price")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function clearPrice(Request $request)
    {
        $this->session->remove('price');
        $this->addFlash('success','Прайс очищен');
        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

}
