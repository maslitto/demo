<?php


namespace App\Controller\Admin;

use App\Entity\SiteVersion;
use App\Entity\TypicalProp;
use App\Entity\TypicalPropValue;
use App\Form\ImageType;
use App\Form\TypicalPropType;
use App\Form\TypicalPropValueType;
use App\Repository\CatalogRepository;
use App\Repository\ManagerRepository;
use App\Repository\SiteVersionRepository;
use App\Repository\TypicalPropRepository;
use App\Services\VersionService;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Admin TypicalProp controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/typical_prop")
*/

class TypicalPropController extends AbstractController
{


    /**
     * @Route("/", name="admin_typical_prop_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(TypicalPropRepository $typicalPropRepository, Request $request)
    {
        $versionId = 1;
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $typicalProps = $typicalPropRepository->getTypicalPropsPaginated($page, 20, $filter);
        return $this->render('admin/typical_prop/index.html.twig',[
            'typicalProps' => $typicalProps,
            'versionId' => $versionId
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_typical_prop_edit")
     * @Route("/add/", name="admin_typical_prop_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, TypicalPropRepository $typicalPropRepository)
    {
        if($id){
            $typicalProp = $typicalPropRepository->findOneBy(['id' => $id]);
        } else{
            $typicalProp = new TypicalProp();
        }

        $typicalPropForm = $this->createForm(TypicalPropType::class, $typicalProp);
        $typicalPropForm->handleRequest($request);
        $currentPropValues = $typicalProp->getTypicalPropValues();
        if ($typicalPropForm->isSubmitted()) {
            $typicalProp = $typicalPropForm->getData();
            $formPropValues = $typicalProp->getTypicalPropValues();
            $entityManager = $this->getDoctrine()->getManager();
            foreach($currentPropValues as $currentPropValue){
                $entityManager->remove($currentPropValue);
                $entityManager->flush();
            }
            foreach ($formPropValues as $formPropValue) {
                $formPropValue->setTypicalProp($typicalProp);
                $entityManager->persist($formPropValue);
            }
            $entityManager->persist($typicalProp);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_typical_prop_edit',['id' => $typicalProp->getId()]);
        }

        return $this->render('admin/typical_prop/edit.html.twig',[
            'form' => $typicalPropForm->createView(),
            'typicalProp' => $typicalProp,
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_typical_prop_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, TypicalPropRepository $typicalPropRepository)
    {
        $typicalProp = $typicalPropRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($typicalProp);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_typical_prop_index');
    }

}
