<?php


namespace App\Controller\Admin;

use App\Entity\SiteVersion;
use App\Entity\ProductBrand;
use App\Repository\ProductBrandRepository;
use App\Services\VersionService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
/**
 * Admin Product Brand controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/product-brand")
*/

class ProductBrandController extends AbstractController
{

    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("/", name="admin_product_brand_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(ProductBrandRepository $productBrandRepository, Request $request)
    {
        $versionId = 1;
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $productBrands = $productBrandRepository->getProductBrandPaginated($filter, $page, $versionId);
        return $this->render('admin/product_brand/index.html.twig',[
            'productBrands' => $productBrands,
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_product_brand_edit")
     * @Route("/add/", name="admin_product_brand_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, ProductBrandRepository $productBrandRepository)
    {
        if($id){
            $productBrand = $productBrandRepository->findOneBy(['id' => $id]);
        } else{
            $productBrand = new ProductBrand();
        }
        $form = $this->createFormBuilder($productBrand)
            ->add('name', TextType::class,['label'=>'Назавние','required' => false])
            ->add('url', TextType::class,['label'=>'Url','required' => false])
            ->add('isSale', CheckboxType::class,['label'=>'Sale','required' => false])
            ->add('active', CheckboxType::class,['label'=>'Активный','required' => false])
            ->add('text', TextAreaType::class,['label'=>'Описание','required' => false])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productBrand = $form->getData();
            $entityProductBrand = $this->getDoctrine()->getManager();
            $entityProductBrand->persist($productBrand);
            $entityProductBrand->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_product_brand_edit',['id' => $productBrand->getId()]);
        }

        return $this->render('admin/product_brand/edit.html.twig',[
            'form' => $form->createView(),
            'productBrand' => $productBrand
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_product_brand_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, ProductBrandRepository $productBrandRepository)
    {
        $productBrand = $productBrandRepository->findOneBy(['id' => $id]);
        $entityProductBrand = $this->getDoctrine()->getManager();
        $entityProductBrand->remove($productBrand);
        $entityProductBrand->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_product_brand_index');
    }

}
