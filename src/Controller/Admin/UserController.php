<?php


namespace App\Controller\Admin;

use App\Entity\Manager;
use App\Entity\UserStatus;
use App\Entity\SiteVersion;
use App\Entity\User;
use App\Form\UserProfileType;
use App\Repository\ManagerRepository;
use App\Repository\UserRepository;
use App\Services\VersionService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Admin User controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/user")
*/

class UserController extends AbstractController
{

    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("/", name="admin_user_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(UserRepository $userRepository, ManagerRepository $managerRepository, Request $request)
    {
        $versionId = 1;
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $users = $userRepository->getUsersPaginated($filter, $page, $versionId);
        return $this->render('admin/user/index.html.twig',[
            'users' => $users,
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_user_edit")
     * @Route("/add/", name="admin_user_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, UserRepository $userRepository)
    {
        if($id){
            $user = $userRepository->findOneBy(['id' => $id]);
        } else{
            $user = new User();
        }
        $form = $this->createFormBuilder($user)
            ->add('timeCreate', DateTimeType::class,['label'=>'Дата создания','disabled'=>true])
            ->add('email', TextType::class,['label'=>'Email','required' => false])
            ->add('password', TextType::class,['label'=>'Пароль','required' => false])
            ->add('userProfile', UserProfileType::class,['label'=>'Пароль','required' => false])

            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_user_edit',['id' => $user->getId()]);
        }

        return $this->render('admin/user/edit.html.twig',[
            'form' => $form->createView(),
            'user' => $user
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_user_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, UserRepository $userRepository)
    {
        $user = $userRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_user_index');
    }

}
