<?php


namespace App\Controller\Admin;

use App\Entity\Catalog;
use App\Form\ImageType;
use App\Repository\CatalogRepository;
use App\Repository\SiteVersionRepository;
use App\Services\VersionService;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Admin Catalog controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/catalog")
*/

class CatalogController extends AbstractController
{

    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("/", name="admin_catalog_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(CatalogRepository $catalogRepository, Request $request)
    {
        $versionId = 1;
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $catalogs = $catalogRepository->getCatalogsPaginated($page);
        return $this->render('admin/catalog/index.html.twig',[
            'catalogs' => $catalogs
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_catalog_edit")
     * @Route("/add/", name="admin_catalog_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, CatalogRepository $catalogRepository,SiteVersionRepository $siteVersionRepository)
    {
        $versionId = $this->versionService->getId();
        if($id){
            $catalog = $catalogRepository->findOneBy(['id' => $id]);
        } else{
            $catalog = new Catalog();
        }

        $catalogVersion = $catalog->getCatalogVersion($versionId);
        $siteVersion = $siteVersionRepository->find($versionId);

        $catalogForm = $this->createFormBuilder($catalog)
            ->add('name', TextType::class,['label'=>'Название'])
            ->add('url', TextType::class,['label'=>'Url'])
            ->add('text', TextAreaType::class,['label'=>'Описание'])
            ->add('active', ChoiceType::class,[
                'choices'  => [
                    'Да' => 1,
                    'Нет' => 0,
                ], 'label'=>'Показывать на сайте?'])


            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();

        $catalogForm->handleRequest($request);

        if ($catalogForm->isSubmitted()) {
            $catalog = $catalogForm->getData();
            $cv = $catalogForm->getExtraData()['catalogVersion'];

            $catalogVersion->setKeywords($cv['keywords']);
            $catalogVersion->setTitle($cv['title']);
            $catalogVersion->setDescription($cv['description']);


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($catalog);
            $entityManager->persist($catalogVersion);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_catalog_edit',['id' => $catalog->getId()]);
        }

        return $this->render('admin/catalog/edit.html.twig',[
            'catalogForm' => $catalogForm->createView(),
            'catalog' => $catalog,
            'catalogVersion' => $catalogVersion,
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_catalog_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, CatalogRepository $catalogRepository)
    {
        $catalog = $catalogRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($catalog);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_catalog_index');
    }

}
