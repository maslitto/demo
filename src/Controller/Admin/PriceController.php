<?php


namespace App\Controller\Admin;

use App\Entity\Product;
use App\Entity\ProductBrand;
use App\Entity\SiteVersion;
use App\Entity\Promocode;
use App\Repository\ProductBrandRepository;
use App\Repository\ProductRepository;
use App\Repository\PromocodeRepository;
use App\Services\VersionService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Page controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/price")
*/

class PriceController extends AbstractController
{

    /**
     * @Route("/", name="admin_price_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(Request $request)
    {
        return $this->render('admin/price/index.html.twig',[
        ]);
    }


}
