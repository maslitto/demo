<?php


namespace App\Controller\Admin;

use App\Entity\Product;
use App\Entity\ProductBrand;
use App\Entity\SiteVersion;
use App\Entity\Promocode;
use App\Repository\ProductBrandRepository;
use App\Repository\ProductRepository;
use App\Repository\PromocodeRepository;
use App\Services\VersionService;
use App\Utils\CodeGenerator;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\Response;

/**
 * Page controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/promocode")
*/

class PromocodeController extends AbstractController
{

    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("/", name="admin_promocode_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(PromocodeRepository $promocodeRepository, Request $request)
    {
      // dump('test');die();
        $versionId = 1;
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $promocodes = $promocodeRepository->getPromocodesPaginated($page, $versionId);

        //groups
        $groups = $promocodeRepository->getPromocodesAllGroups();


        return $this->render('admin/promocode/index.html.twig',[
            'promocodes' => $promocodes,
            'groups' => $groups
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_promocode_edit")
     * @Route("/add/", name="admin_promocode_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(
        Request $request, int $id = NULL,
        PromocodeRepository $promocodeRepository,
        ProductBrandRepository $productBrandRepository,
        ProductRepository $productRepository
    )
    {
        if($id){
            $promocode = $promocodeRepository->findOneBy(['id' => $id]);
        } else{
            $promocode = new Promocode();
        }
        $form = $this->createFormBuilder($promocode)
            ->add('active', ChoiceType::class,[
                'choices'  => [
                    'Да' => 1,
                    'Нет' => 0,
                ], 'label'=>'Активный','required' => true])
            ->add('code', TextType::class,['label'=>'Промокод','required' => false])
            ->add('expiry', DateTimeType::class,['label'=>'Срок окончания','required' => false])
            ->add('discount', TextType::class,['label'=>'Размер скидки','required' => false])
            ->add('type', ChoiceType::class,[
                'choices'  => [
                    '%' => 'percents',
                    'рублей' => 'roubles',
                ], 'label'=>'Тип скидки','required' => true])
            ->add('brands', EntityType::class, [
                'class' => ProductBrand::class,
                'choice_label' => function ($brand) {
                    return $brand->getName();
                },
                'empty_data' => [],
                'required'   => false,
                'placeholder' => 'Пусто',
                'label' => 'Участвующие бренды',
                'multiple' => true,
                'query_builder' => function (ProductBrandRepository $productBrandRepository) {
                    return $productBrandRepository->getActiveBrandsQueryBuilder()->orderBy('b.name','ASC');
                },
                //'attr' => ['class' => 'js-select2']
            ])
            ->add('products', EntityType::class, [
                'class' => Product::class,
                'choice_label' => function ($product) {
                    return $product->getName().'( ID='.$product->getId().' )';
                },
                'empty_data' => [],
                'required'   => false,
                'label' => 'Участвующие товары',
                'multiple' => true,
                'query_builder' => function (ProductRepository $productRepository) {
                    return $productRepository->getActiveProductsQueryBuilder()->orderBy('p.name','ASC');
                },
                'attr' => ['class' => 'js-select2']
            ])
            ->add('groupName', TextType::class,['label'=>'Название группы','required' => false])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $promocode = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($promocode);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_promocode_edit',['id' => $promocode->getId()]);
        }

        return $this->render('admin/promocode/edit.html.twig',[
            'form' => $form->createView(),
            'promocode' => $promocode
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_promocode_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, PromocodeRepository $promocodeRepository)
    {
        $promocode = $promocodeRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($promocode);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_promocode_index');
    }

    /**
     * @Route("/copy/{promocode}/", name="admin_promocode_copy")
     * @Method("GET")
     *
     */
    public function copy(Promocode $promocode)
    {
        $copy = clone $promocode;
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($copy);
        $entityManager->flush();

        $this->addFlash('success','Удачное копирование. Id ='.$copy->getId());
        return $this->redirectToRoute('admin_promocode_index',['id' => $promocode->getId()]);
    }

    /**
     * @Route("/get-promocodes-by-group/", name="get_promocodes_by_group")
     * @Method("GET")
     *
     */
    public function getPromocodesByGroup(PromocodeRepository $promocodeRepository, Request $request)
    {

        $groupName = $request->query->get('groupName', NULL);

        // @var Promocode[] $promocodes
        $promocodes = $promocodeRepository->getPromocodesByGroup($groupName);

        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Стоммаркет')
            ->setTitle('Промокоды')
            ->setDescription('группы-промокоды')
            ->setCategory('стоматология');

        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->getColumnDimension('A')->setWidth(100);

        $sheetNum = 1;

        foreach ($promocodes as $key => $promocode) {
          $sheetNum += $key;
          $sheet->setCellValue('A'.$sheetNum.'', $promocode->getCode());
        }


        $writer = new Xlsx($spreadsheet);

        ob_start();
        $writer->save('php://output');
        return new Response(
            ob_get_clean(),  // read from output buffer
            200,
            array(
                'Content-Type' => 'text/vnd.ms-excel; charset=utf-8',
                'Content-Disposition' =>  'attachment; filename="'.$groupName.'-promocodes.xlsx"',
            )
        );
    }

    /**
     * @Route("/get-used-promocodes-by-group/", name="get_used_promocodes_by_group")
     * @Method("GET")
     *
     */
    public function getUsedPromocodesByGroup(PromocodeRepository $promocodeRepository, Request $request)
    {

        $groupName = $request->query->get('groupName', NULL);

        // @var Promocode[] $promocodes
        $promocodes = $promocodeRepository->getUsedPromocodesByGroup($groupName);

        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Стоммаркет')
            ->setTitle('Использованные промокоды')
            ->setDescription('группы-промокоды')
            ->setCategory('стоматология');

        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        // $sheet->getColumnDimension('A')->setWidth(100);

        $sheetNum = 1;

        for ($i=0; $i < count($promocodes); $i++) {
          $sheetNum += $i;
          $sheet->setCellValue('A'.$sheetNum.'', $sheetNum);
          $sheet->setCellValue('B'.$sheetNum.'', $promocodes[$i]['promo_code']);
          $sheet->setCellValue('C'.$sheetNum.'', $promocodes[$i]['email']);
          $sheet->setCellValue('D'.$sheetNum.'', $promocodes[$i]['time_create']);
          $sheet->setCellValue('E'.$sheetNum.'', $promocodes[$i]['price']);
          $sheet->setCellValue('F'.$sheetNum.'', $promocodes[$i]['username']);

        }

        $writer = new Xlsx($spreadsheet);

        ob_start();
        $writer->save('php://output');
        return new Response(
            ob_get_clean(),  // read from output buffer
            200,
            array(
                'Content-Type' => 'text/vnd.ms-excel; charset=utf-8',
                'Content-Disposition' =>  'attachment; filename="'.$groupName.'-used-promocodes.xlsx"',
            )
        );
    }

    /**
     * @Route("/generate/", name="admin_promocode_generate")
     * @Method({"GET","POST"})
     *
     */
    public function generate(
        Request $request,
        PromocodeRepository $promocodeRepository,
        ProductBrandRepository $productBrandRepository,
        ProductRepository $productRepository,
        CodeGenerator $codeGenerator
    )
    {
        $promocode = new Promocode();
        $form = $this->createFormBuilder($promocode)
            /*->add('active', ChoiceType::class,[
                'choices'  => [
                    'Да' => 1,
                    'Нет' => 0,
                ], 'label'=>'Активный','required' => true])*/
            ->add('groupName', TextType::class,['label'=>'Название группы','required' => false])
            ->add('expiry', DateTimeType::class,['label'=>'Срок окончания','required' => false])
            ->add('discount', TextType::class,['label'=>'Размер скидки','required' => false])
            ->add('type', ChoiceType::class,[
                'choices'  => [
                    '%' => 'percents',
                    'рублей' => 'roubles',
                ], 'label'=>'Тип скидки','required' => true])
            ->add('brands', EntityType::class, [
                'class' => ProductBrand::class,
                'choice_label' => function ($brand) {
                    return $brand->getName();
                },
                'empty_data' => [],
                'required'   => false,
                'placeholder' => 'Пусто',
                'label' => 'Участвующие бренды',
                'multiple' => true,
                'query_builder' => function (ProductBrandRepository $productBrandRepository) {
                    return $productBrandRepository->getActiveBrandsQueryBuilder()->orderBy('b.name','ASC');
                },
                //'attr' => ['class' => 'js-select2']
            ])
            ->add('products', EntityType::class, [
                'class' => Product::class,
                'choice_label' => function ($product) {
                    return $product->getName().'( ID='.$product->getId().' )';
                },
                'empty_data' => [],
                'required'   => false,
                'label' => 'Участвующие товары',
                'multiple' => true,
                'query_builder' => function (ProductRepository $productRepository) {
                    return $productRepository->getActiveProductsQueryBuilder()->orderBy('p.name','ASC');
                },
                'attr' => ['class' => 'js-select2']
            ])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $extra = $form->getExtraData();
            $count = (int)$extra['count'];
            $promocode = $form->getData();
            $promocode->setActive(true);
            $promocode->setDisposable(true);
            $entityManager = $this->getDoctrine()->getManager();
            for ($i = 1; $i <= $count; $i++){
                do{
                    $code = strtoupper(uniqid());
                    $searchPromocode = $promocodeRepository->findOneBy(['code' => $code]);
                } while ($searchPromocode);

                $promocode->setCode($code);
                $clone = clone $promocode;

                $entityManager->persist($clone);
                $entityManager->flush();
            }

            $this->addFlash('success','Успешная генерирация!');
            return $this->redirectToRoute('admin_promocode_index');
        }

        return $this->render('admin/promocode/generate.html.twig',[
            'form' => $form->createView(),
            'promocode' => $promocode
        ]);
    }
}
