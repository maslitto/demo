<?php


namespace App\Controller\Admin;

use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Services\VersionService;
use App\Utils\Sms;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Admin dashboard controller
 *
 * @Route("/admin")
*/

class DashboardController extends AbstractController
{

    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("/", name="admin_dashboard_page_index")
     * @Method("GET")
     *
     */
    public function index(OrderRepository $orderRepository, ProductRepository $productRepository)
    {
        //dd(unserialize($_SESSION['_sf2_attributes']['_security_main']));
        $token = 'AQAEA7qh-AQAEA7qh-19xAATri0qkJc-sLERAuv0m6rtdtqc';
        $counter = '23244307';
        $dateStart = '30daysAgo';
        $dateEnd = 'today';
        $metrics = 'ym:s:users';

        //$get = file_get_contents('https://api-metrika.yandex.ru/stat/v2/data?metrics='.$metrics.'&sort=ym:s:date&dimensions=ym:s:date&date1='.$dateStart.'&date2='.$dateEnd.'&limit=10000&offset=1&ids='.$counter.'&oauth_token='.$token.'&pretty=true');
        //$metrika = json_decode($get);
        //dump($metrika);die();
        $versionId = $this->versionService->getId();
        $period = 30;
        $lastPeriodOrders = $orderRepository->getOrdersCountByDaysAndVersion($period, $versionId);
        $lastPeriodOrders = str_replace('&quot;','"',json_encode($lastPeriodOrders));

        $lastPeriodOrdersAllVersions = $orderRepository->getOrdersCountByDaysAllVersions($period);
        $lastPeriodOrdersAllVersions = str_replace('&quot;','"',json_encode($lastPeriodOrdersAllVersions));
        $spbSparkline = $productRepository->getStorageFullnessForSparkline(1);
        $spbPercent = $spbSparkline[0];//dd($lastPeriodOrders);
        $spbSparkline = str_replace('&quot;','"',json_encode($spbSparkline));

        $mskSparkline = $productRepository->getStorageFullnessForSparkline(2);
        $mskPercent = $mskSparkline[0];//dd($lastPeriodOrders);
        $mskSparkline = str_replace('&quot;','"',json_encode($mskSparkline));
        //$spbSparkline = '['.implode(',',$spbSparkline).']';
        /*
         foreach ($metrika->data as $k => $data){
            $results[$k]['date'] = $data->dimensions[0]->name;
            $results[$k]['count'] = $data->metrics[0];
        }
        */



        $promocodus = $orderRepository->getPromocodeWithOrders();
        $promocodes_data = [
        ];

        for ($i=0; $i < count($promocodus); $i++) {

          $key = array_search($promocodus[$i]['group_name'], array_column($promocodes_data, 'label'));

          if ( $key === FALSE ) {
            $promocodes_data[] = [
              'label'=>$promocodus[$i]['group_name'],
              'data'=>1
            ];
          } else {
              $promocodes_data[$key]['data']++;
          }

        }
        // dd($promocodes_data);



        return $this->render('admin/dashboard.html.twig',[
            'lastPeriodOrders' => $lastPeriodOrders,
            'lastPeriodOrdersAllVersions' => $lastPeriodOrdersAllVersions,
            'period' => $period,
            'metrika' => "{}",//json_encode($results)
            'spbSparkline' => $spbSparkline,
            'mskSparkline' => $mskSparkline,
            'spbPercent' => $spbPercent,
            'mskPercent' => $mskPercent,
            'promocodus' => json_encode($promocodes_data),
        ]);
    }
    /**
     * @Route("/", name="admin_dashboard_send_sms")
     * @Method("POST")
     *
     */
    public function sendSms(Request $request, Sms $sms)
    {
        $phone = $request->get('phone');
        $text = $request->get('text');
        $sms->send($phone, $text);
        $this->addFlash('success','Сообщение отправлено');
        return $this->redirectToRoute('admin_dashboard_page_index');
    }

}
