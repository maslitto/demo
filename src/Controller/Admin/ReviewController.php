<?php


namespace App\Controller\Admin;

use App\Entity\SiteVersion;
use App\Entity\Review;
use App\Repository\ReviewRepository;
use App\Services\VersionService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * Admin Site Reviews controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/review")
*/

class ReviewController extends AbstractController
{

    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("/", name="admin_review_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(ReviewRepository $reviewRepository, Request $request)
    {
        $versionId = 1;
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $reviews = $reviewRepository->getReviewsPaginated($filter, $page, $versionId);
        return $this->render('admin/review/index.html.twig',[
            'reviews' => $reviews,
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_review_edit")
     * @Route("/add/", name="admin_review_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, ReviewRepository $reviewRepository)
    {
        if($id){
            $review = $reviewRepository->findOneBy(['id' => $id]);
        } else{
            $review = new Review();
        }
        $form = $this->createFormBuilder($review)
            ->add('status', ChoiceType::class,[
                'choices'  => [
                    'Да' => 1,
                    'Нет' => 0,
                ],
                'label'=>'Опубликован'])
            ->add('rating', TextType::class,['label'=>'Рейтинг','required' => false])
            ->add('review', TextAreaType::class,['label'=>'Отзыв','required' => false])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $review = $form->getData();
            $entityReview = $this->getDoctrine()->getManager();
            $entityReview->persist($review);
            $entityReview->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_review_edit',['id' => $review->getId()]);
        }

        return $this->render('admin/review/edit.html.twig',[
            'form' => $form->createView(),
            'review' => $review
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_review_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, ReviewRepository $reviewRepository)
    {
        $review = $reviewRepository->findOneBy(['id' => $id]);
        $entityReview = $this->getDoctrine()->getManager();
        $entityReview->remove($review);
        $entityReview->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_review_index');
    }

}
