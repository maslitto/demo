<?php


namespace App\Controller\Admin;

use App\Entity\SiteVersion;
use App\Entity\Manager;
use App\Repository\HelpPageRepository;
use App\Services\VersionService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Page controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/page")
*/

class PageController extends AbstractController
{

    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("/", name="admin_page_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(HelpPageRepository $helpPageRepository, Request $request)
    {
        $versionId = 1;
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $helpPages = $helpPageRepository->getHelpPagesPaginated($page, $versionId);
        return $this->render('admin/help_page/index.html.twig',[
            'helpPages' => $helpPages,
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_page_edit")
     * @Route("/add/", name="admin_helppage_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, HelpPageRepository $helpPageRepository)
    {
        if($id){
            $helpPage = $helpPageRepository->findOneBy(['id' => $id]);
        } else{
            $helpPage = new Manager();
        }
        $form = $this->createFormBuilder($helpPage)
            ->add('name', TextType::class,['label'=>'Заголовок','required' => false])
            ->add('text', TextAreaType::class,['label'=>'Текст','required' => false/*,'attr' => ['class' => 'js-ckeditor']*/])
            ->add('versionId', TextType::class,['label'=>'Ид версии','required' => false])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $helpPage = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($helpPage);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_help_page_edit',['id' => $helpPage->getId()]);
        }

        return $this->render('admin/help_page/edit.html.twig',[
            'form' => $form->createView(),
            'helpPage' => $helpPage
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_help_page_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, HelpPageRepository $helpPageRepository)
    {
        $helpPage = $helpPageRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($helpPage);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_help_page_index');
    }

}
