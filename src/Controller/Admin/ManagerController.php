<?php


namespace App\Controller\Admin;

use App\Entity\SiteVersion;
use App\Entity\Manager;
use App\Repository\ManagerRepository;
use App\Services\VersionService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
/**
 * Admin Manager controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/manager")
*/

class ManagerController extends AbstractController
{

    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("/", name="admin_manager_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(ManagerRepository $managerRepository, Request $request)
    {
        $versionId = 1;
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $managers = $managerRepository->getManagersPaginated($filter, $page, $versionId);
        return $this->render('admin/manager/index.html.twig',[
            'managers' => $managers,
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_manager_edit")
     * @Route("/add/", name="admin_manager_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, ManagerRepository $managerRepository)
    {
        if($id){
            $manager = $managerRepository->findOneBy(['id' => $id]);
        } else{
            $manager = new Manager();
        }
        $form = $this->createFormBuilder($manager)
            ->add('active', ChoiceType::class,[
                'choices'  => [
                    'Да' => 1,
                    'Нет' => 0,
                ],
                'label'=>'Принимает заказы?'])
            ->add('name', TextType::class,['label'=>'Имя','required' => false])
            ->add('lastName', TextType::class,['label'=>'Фамилия','required' => false])
            ->add('email', TextType::class,['label'=>'Email','required' => false])
            ->add('phone', TextType::class,['label'=>'Контактный телефон','required' => false])
            ->add('siteVersion', EntityType::class, array(
                'class' => SiteVersion::class,
                'choice_label' => 'name',
                'label' => 'Город(версия)'
            ))
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($manager);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_manager_edit',['id' => $manager->getId()]);
        }

        return $this->render('admin/manager/edit.html.twig',[
            'form' => $form->createView(),
            'manager' => $manager
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_manager_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, ManagerRepository $managerRepository)
    {
        $manager = $managerRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($manager);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_manager_index');
    }

}
