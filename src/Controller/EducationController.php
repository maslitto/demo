<?php


namespace App\Controller;

use App\Entity\Education\Education;
use App\Entity\Education\EducationRequest;
use App\Entity\User;
use App\Repository\Education\EducationDirectionRepository;
use App\Repository\Education\EducationRepository;
use App\Repository\Education\EducationTypeRepository;
use Detection\MobileDetect;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Education controller.
 *
 * @Route("/education")
*/

class EducationController extends AbstractController
{
    use \App\Traits\Meta;
    private $sender;
    private $templating;

    public function __construct(string $sender, \Twig_Environment $templating)
    {
        $this->sender = $sender;
        $this->templating = $templating;
    }

    /**
     * @Route("/", name="education_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(
        Request $request,
        EducationRepository $educationRepository,
        EducationTypeRepository $educationTypeRepository,
        EducationDirectionRepository $educationDirectionRepository
    )
    {
        $filter = $request->query->all();
        if(!isset($filter['page'])){
            $filter['page'] = 1;
        }
        /** @var Education[] $educations */
        $educations = $educationRepository->getEducationsPaginated($filter,$filter['page']);
        if($request->isXmlHttpRequest()){

            $response = [
                'html' => $this->render('education/ajax_response.html.twig',['educations' => $educations])->getContent(),
                'page' => $filter['page']
            ];
            return $this->json($response);
        }
        $cities = $educationRepository->getCities();
        $dates = $educationRepository->getDatesString();
        $types = $educationTypeRepository->findBy(['active' => 1]);
        $directions = $educationDirectionRepository->findAll();
        $meta = $this->getMetaObject(
            '«Стоммаркет.Обучение»'
        );
        $md = new MobileDetect();
        if($md->isMobile()||$md->isTablet()){
            $view = 'education/catalog-mobile.html.twig';
        }else{
            $view = 'education/catalog.html.twig';
        }
        return $this->render($view,[
            'educations' => $educations,
            'today' => date('Y-m-d'),
            'dates' => $dates,
            'cities' => $cities,
            'types' => $types,
            'directions' => $directions,
            'meta' => $meta
        ]);
    }

    /**
     * @Route("/search/", name="education_search")
     * @Method("GET")
     *
     */
    public function search(
        Request $request,
        EducationRepository $educationRepository
    )
    {
        $filter = $request->query->all();
        /** @var Education[] $educations */
        $educations = $educationRepository->getEducationsPaginated($filter,1);
        if($request->isXmlHttpRequest()){
            $response = [
                'html' => $this->render('education/search_response.html.twig',['educations' => $educations])->getContent(),
            ];
            return $this->json($response);
        }
    }

    /**
     * @Route("/view/{id}/", name="education_view")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function view(int $id, EducationRepository $educationRepository,Request $request)
    {
        /** @var Education $education */
        $education = $educationRepository->find($id);
        //dd($educationRepository->getEducationAsArray($id));
        if($request->isXmlHttpRequest()){
            /** @var User $user */
            $user = $this->getUser();
            $userId = $user->getId();
            if($education->getUser()->getId() == $userId|| 3154 == $userId){
                return $this->json([
                    'education' => $educationRepository->getEducationAsArray($id)
                ]);
            }
            else return $this->json([]);
        }
        if($education){
            $meta = $this->getMetaObject(
                $education->getName().' — «Cтоммаркет.Обучение»'
            );
            $md = new MobileDetect();
            if($md->isMobile()||$md->isTablet()){
                $view = 'education/view-mobile.html.twig';
            }else{
                $view = 'education/view.html.twig';
            }
            return $this->render($view,[
                'education' => $education,
                'meta' => $meta
            ]);
        } else{
            throw new NotFoundHttpException('Страница не найдена. Ошибка 404!');
        }

    }

    /**
     * @Route("/join/", name="education_join")
     * @Method("POST")
     *
     */
    public function join(Request $request,EducationRepository $educationRepository,\Swift_Mailer $mailer): Response
    {
        $data = $request->request->all();

        /** @var Education $education */
        $education = $educationRepository->findOneBy(['id' => $data['educationId']]);
        $educationRequest = new EducationRequest();
        $educationRequest->setName($data['name']);
        $educationRequest->setEmail($data['email']);
        $educationRequest->setPhone($data['phone']);
        $educationRequest->setCity($data['city']);
        $educationRequest->setEducation($education);
        if(isset($data['subscribe'])){
            $educationRequest->setSubscribe(true);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($educationRequest);
        $em->flush();

        $subject = 'Новый заказ по обучению';
        $message = (new \Swift_Message($subject))
            ->setFrom($this->sender)
            ->setTo($education->getEmail())
            ->setBody(
                $this->renderView(
                    'email/education_request.html.twig', [
                        'request' => $educationRequest,
                        'year' => date('Y'),
                        'server' => $_SERVER['HTTP_HOST'],
                    ]
                ),
                'text/html'
            );
        /*$message = (new \Swift_Message())
            ->setSubject($subject)
            ->setTo($education->getEmail())
            ->setFrom('')
            ->setBody($this->templating->render('email/education_request.html.twig', [
                'request' => $educationRequest,
                'year' => date('Y'),
                'server' => $_SERVER['HTTP_HOST'],
            ]), 'text/html');
*/
        // In app/config/config_dev.yml the 'disable_delivery' option is set to 'true'.
        // That's why in the development environment you won't actually receive any email.
        // However, you can inspect the contents of those unsent emails using the debug toolbar.
        // See https://symfony.com/doc/current/email/dev_environment.html#viewing-from-the-web-debug-toolbar
        $mailer->send($message);

        return $this->json([]);
    }
}
