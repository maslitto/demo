<?php


namespace App\Controller;

use App\Repository\ProductRepository;
use App\Repository\SliderRepository;
use App\Services\ConfigService;
use App\Services\VersionService;
use Detection\MobileDetect;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use SypexGeo\Reader;

/**
 * Controller used to manage index page
 *
 * @Route("/")
*/

class IndexController extends AbstractController
{
    use \App\Traits\Meta;

    /*private $mpIds = [
        42235,
        6285,
        4461,
        6533,
        4032,
        6395,
        6285,
        4252,
        6019,
        3620,
        6273,
        4381,
        5867,
        4652,
        5663,
        6274,
        4643,
    ];*/
    private $mpIds = [
        42235,
        6684,
        6458,
        4653,
        5765,
        6395,
        6285,
        4252,
        6019,
        3620,
        6273,
        4381,
        5867,
        4652,
        5663,
        6274,
        4643,
    ];
    private $mobileList1 = [42235,6684,6533,6395];
    private $mobileList2 = [5867,4653,6285,4381];
    private $productRepository;

    private $sliderRepository;

    private $versionService;

    /**
     * IndexController constructor.
     */
    public function __construct(
        ProductRepository $productRepository,
        SliderRepository $sliderRepository,
        VersionService $versionService
    )
    {
        $this->productRepository = $productRepository;
        $this->sliderRepository = $sliderRepository;
        $this->versionService = $versionService;
    }

    /**
     * @Route("/", name="index_page")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(Request $request)
    {
        //phpinfo();
        $versionId = $this->versionService->getId();
        $city = $this->versionService->getSiteVersion()->getStoreName();
        $slides = $this->sliderRepository->findBy([/*'versionId' => $versionId,*/ 'active' => 1],['sort'=>'DESC']);
        $meta = $this->getMetaObject(
            'Купить стоматологические материалы в '.$city.' — интернет-магазин «Стоммаркет»',
            '«Стоммаркет» — один из крупнейших интернет-магазинов стоматологических товаров. Большой выбор расходных материалов для стоматологии.',
            'купить, стоматологические материалы, Киров, оптом, дешево, расходные материалы, интернет-магазин стоммаркет'
        );
        $llisKit = $this->productRepository->find(42060 );
        $md = new MobileDetect();
        if($md->isMobile()||$md->isTablet()){
            $list1 = $this->productRepository->findByIds($this->mobileList1,4);
            $list2 = $this->productRepository->findByIds($this->mobileList2,4);
            $return = [
                'meta' => $meta,
                'list1' => $list1,
                'list2' => $list2,
                'slides' => $slides,
            ];
            $view = 'index/index-mobile.html.twig';
        } else {
            foreach ($this->mpIds as $id){
                $mainProducts[] = $this->productRepository->find($id);
            }
            $return = [
                'meta' => $meta,
                'products' => $mainProducts,
                'slides' => $slides,
                'llisKit' => $llisKit
            ];
            $view = 'index/index.html.twig';
        }
        return $this->render($view, $return);
    }
}
