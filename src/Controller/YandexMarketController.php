<?php


namespace App\Controller;

use App\Entity\Feedback;
use App\Services\VersionService;
use App\Services\YandexMarketService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Yml for Yandex Market controller
 *
 * @Route("/yandex-marker")
*/

class YandexMarketController extends AbstractController
{
    private $versionService;
    private $yandexMarketService;

    public function __construct(VersionService $versionService,YandexMarketService $yandexMarketService){
        $this->versionService = $versionService;
        $this->yandexMarketService = $yandexMarketService;
    }

    /**
     * @Route("/", name="yandex_market_yml")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index()
    {
        $yml = $this->yandexMarketService->getYml();
        $response = new Response($yml);
        $response->headers->set('Content-Type', 'xml');

        return $response;
    }


}
