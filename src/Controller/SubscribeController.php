<?php


namespace App\Controller;

use App\Repository\CatalogRepository;
use App\Services\PriceService;
use App\Services\SubscribeService;
use App\Services\VersionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
/**
 * Price list controller
 *
 * @Route("/subscribe")
*/

class SubscribeController extends AbstractController
{
    private $subscribeService;

    public function __construct(SubscribeService $subscribeService)
    {
        $this->subscribeService = $subscribeService;
    }
    /**
     * @Route("/add-subscribe/", name="add_subscribe")
     * @Method("POST")
     *
     */
    public function addSubscribe(Request $request)
    {
        $response = [];
        if($request->isXmlHttpRequest()) {
            $email = $request->get('email');
            $hasSubscribeByEmail = $this->subscribeService->hasSubscribeByField('email', $email);

            if(!empty($email) and !$hasSubscribeByEmail) {
                $subscribe = $this->subscribeService->addSubscribe($email);
                $this->subscribeService->sendConfirmEmail($subscribe);

                $response['ret'] = 1;
            }
            else {
                $response['errors']['all-nomessage'] = true;
            }
        }
        else {
            $response['errors']['all-nomessage'] = true;
        }

        return $this->json($response);
    }

    /**
     * @Route("/confirm/{confirmKey}", name="confirm_subscribe")
     * @Method("GET")
     *
     */
    public function confirmSubscribe(Request $request,string $confirmKey)
    {
        $this->subscribeService->confirmSubscribe($confirmKey);
        return new Response('Подписка оформлена.<br> <a href="/">На главную</a>');
    }
}
