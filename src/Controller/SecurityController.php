<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\Promocode;
use App\Entity\ResetPassword;
use App\Entity\UserFeed;
use App\Entity\UserProfile;
use App\Repository\ProductBrandRepository;
use App\Repository\PromocodeRepository;
use App\Repository\ResetPasswordRepository;
use App\Repository\UserRepository;
use App\Utils\CodeGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


class SecurityController extends AbstractController
{
    private $sender;

    public function __construct(string $sender)
    {
        $this->sender = $sender;
    }

    /**
     * @Route("/ajax-login/", name="ajax_login")
     */
    public function ajaxLogin(Request $request)
    {
        return $this->json(['success'=> true]);
    }

    /**
     * @Route("/login/", name="login")
     */
    public function login(AuthenticationUtils $helper): Response
    {
        return $this->render('security/login.html.twig', [
            // last username entered by the user (if any)
            'last_username' => $helper->getLastUsername(),
            // last authentication error (if any)
            'error' => $helper->getLastAuthenticationError(),
        ]);
    }

    /**
     * @Route("/registration/", name="register")
     * @Method({"POST","GET"})
     */
    public function register(
    Request $request,
    UserRepository $userRepository,
    \Twig_Environment $templating,
    \Swift_Mailer $mailer,
    CodeGenerator $codeGenerator,
    PromocodeRepository $promocodeRepository,
    ProductBrandRepository $productBrandRepository
): Response
    {
        if($request->isMethod('post')){
            $data = $request->request->all();
            if($user = $userRepository->findOneBy(['email' => $data['email']])){
                return $this->json([
                    'errors' => [
                        'email' => [
                            'recordFound' => 'Такой e-mail уже зарегистрирован',
                        ]
                    ]
                ],200);
            } elseif($data['password'] !== $data['confirm_password']){
                return $this->json([
                    'errors' => [
                        'confirm_password' => [
                            'recordFound' => 'Введенные пароли не совпадают',
                        ]
                    ]
                ],200);
            } else {
                $user = new User();
                $user->setEmail($data['email']);
                $user->setPassword($data['password']);
                $user->setRoles(['ROLE_USER']);

                $userProfile = new UserProfile();
                $userProfile->setPriceType(1);
                $userProfile->setBonus(0);
                $userProfile->setPersonType(1);
                $userProfile->setUser($user);

                $userFeed = new UserFeed();
                $userFeed->setEvents(0);
                $userFeed->setNews(0);
                $userFeed->setSales(0);
                $userFeed->setNewProducts(0);
                $userFeed->setUser($user);

                $em = $this->getDoctrine()->getManager();
                $em->persist($userProfile);
                $em->persist($userFeed);
                $em->persist($user);
                $em->flush();
                $now = new \DateTime();

                //promocode generator part
                do{
                    $codeGenerator->setMask('XXXX-XXXX-XXXX');
                    $codeGenerator->setAmount(1);
                    $codes = $codeGenerator->getCodes();
                    $promocode = $promocodeRepository->findOneBy(['code' => $codes[0]]);
                } while ($promocode);

                $promocode = new Promocode();
                $promocode->setActive(true);
                $promocode->setCode($codes[0]);
                $promocode->setDiscount(2);
                $promocode->setType('percents');
                $promocode->setDisposable(true);
                $promocode->setGroupName('register');

                $brands = $productBrandRepository->findBy(['active' => true]);
                foreach ($brands as $brand) {
                    if($brand->getId() != 5){
                        $promocode->addBrand($brand);
                    }
                }
                $expiry = new \DateTime();
                $expiry->modify('+7 day');
                $promocode->setExpiry($expiry);
                $em->persist($promocode);
                $em->flush();

                //email new user
                $message = (new \Swift_Message())
                    ->setSubject('Регистрация на сайте stommarket.ru')
                    ->setTo($user->getEmail())
                    ->setFrom($this->getParameter('app.notifications.email_sender'))
                    ->setBody($templating->render('email/register.html.twig', [
                        'server' => $_SERVER['HTTP_HOST'],
                        'user' => $user,
                        'year' => $now->format('Y'),
                        'promocode' => $promocode->getCode()
                    ]), 'text/html');

                $mailer->send($message);
                return $this->json([
                    'success' => true,
                ]);
            }
        }

        else {
            return $this->render('security/register.html.twig', [

            ]);
        }
    }

    /**
     * @Route("/registration/", name="register")
     * @Method("POST")
     */

    /**
     * This is the route the user can use to logout.
     *
     * But, this will never be executed. Symfony will intercept this first
     * and handle the logout automatically. See logout in app/config/security.yml
     *
     * @Route("/logout/", name="logout")
     */
    public function logout(): void
    {
        throw new \Exception('This should never be reached!');
    }

    /**
     * @Route("/forget-password/", name="forget")
     * @Method({"GET","POST"})
     */
    public function forgetPassword(Request $request, UserRepository $userRepository, \Swift_Mailer $mailer): Response
    {
        if($request->isMethod('GET')){
            return $this->render('security/forget_password.html.twig');
        } else {
            if($user = $userRepository->findOneBy(['email' => $request->get('email')])){
                $resetPassword = new ResetPassword();
                $resetPassword->setUser($user);
                $resetPassword->setHash(uniqid());
                $em = $this->getDoctrine()->getManager();
                $em->persist($resetPassword);
                $em->flush();

                $message = (new \Swift_Message('Восстановление пароля'))
                    ->setFrom($this->sender)
                    ->setTo($user->getEmail())
                    ->setBody(
                        $this->renderView(
                            'email/reset_password.html.twig',
                            ['hash' => $resetPassword->getHash()]
                        ),
                        'text/html'
                );

                $mailer->send($message);
                if($request->isXmlHttpRequest()){
                    return $this->json(['success' => true]);
                }
            }
            else{
                if($request->isXmlHttpRequest()){
                    return $this->json([
                        'errors' => [
                            'email' => [
                                'noUser' => 'Пользователь с таким e-mail не найден',
                            ]
                        ]
                    ],200);
                } else{
                    $this->addFlash('error', 'Пользователь с таким e-mail не найден');
                    return $this->render('security/forget_password.html.twig',[]);
                }

            }
            return $this->render('security/forget_password.html.twig',['sended' => true]);
        }

    }

    /**
     * @Route("/reset-password/{hash}/", name="reset_password")
     * @Method({"GET","POST"})
     */
    public function resetPassword(
        string $hash,
        UserRepository $userRepository,
        ResetPasswordRepository $resetPasswordRepository,
        Request $request
    ): Response
    {
        $resetPassword = $resetPasswordRepository->findOneBy(['hash' => $hash]);
        if($request->isMethod('GET')) {
            if ($resetPassword) {
                return $this->render('security/reset_password.html.twig', ['hash' => $hash]);
            } else {
                throw $this->createNotFoundException('Неверная ссылка');
            }
        } else {
            if($request->get('password')!==$request->get('confirm_password')){
                $this->addFlash('error', 'Пароли не совпадают');
                return $this->render('security/reset_password.html.twig', [
                    'hash' => $hash,
                    ]);
            } else {

                $user = $userRepository->findOneBy(['id' => $resetPassword->getUser()->getId()]);
                $user->setPassword($request->get('password'));
                $em = $this->getDoctrine()->getManager();
                $em->remove($resetPassword);
                $em->persist($user);
                $em->flush();
                $this->addFlash('success', 'Пароль успешно изменен!');
                return $this->redirect('/login/');
            }
        }
    }
}
