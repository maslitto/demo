<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\Expr;
use PhpOffice\PhpSpreadsheet\Calculation\DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Csv3mController extends AbstractController
{
    /**
     * @Route("/csv3m/", name="csv3m")
     */
    public function index(ProductRepository $productRepository)
    {
        $criteria = new Criteria();
        $criteria->where($criteria->expr()->neq('article3m', NULL))
            ->andWhere($criteria->expr()->eq('brandId',73 ))
            ->andWhere($criteria->expr()->eq('status',1 ))
            ->andWhere($criteria->expr()->eq('active',true ));
        /** @var Product[] $products */
        $products = $productRepository->matching($criteria);
        /*$empty =  $productRepository->findBy(['brandId'=> 73,'active' => true, 'status' => 1,'article3m'=> NULL]);
        foreach($empty as $product){
            print_r($product->getName().'<br>');
        }*/
        //die();

        $list = array(
            //these are the columns
            ["SKU", "Deeplink","Delivery time","Manufacturer","Last update"]
        );
        $now = new \DateTime();
        foreach ($products as $product){
            $list[] = array(
                $product->getArticle3m(),
                "https://stommarket.ru/".$product->getUrl(),
                "1 Week",
                "3M",
                $now->format('d.m.Y h:i')
            );
        }
        $response = '';
        foreach($list as $row){
            foreach($row as $item){
                $response .= $item.';';
            }
            $response .= PHP_EOL;
        }
        $response = new Response($response);
        $response->headers->set('Content-Type', 'text/csv');
        //it's gonna output in a testing.csv file
        $response->headers->set('Content-Disposition', 'attachment; filename="3m.csv"');

        return $response;
    }
}
