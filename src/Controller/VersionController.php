<?php


namespace App\Controller;

use App\Repository\SiteVersionRepository;
use App\Services\VersionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Version controller
 *
 * @Route("/version/")
*/

class VersionController extends AbstractController
{

    private $versionService;

    public function __construct(VersionService $versionService){
        $this->versionService = $versionService;
    }

    /**
     * @Route("set/", name="set_version")
     *
     * @Method("POST")
     */
    public function setVersion(Request $request,SiteVersionRepository $siteVersionRepository)
    {
        setcookie("site_version", $request->request->get('store'), time() + 60*60*1*1, ".stommarket.ru");
        $setVersion = $siteVersionRepository->findOneBy(['id' => $request->request->get('store')]);
        $domain = $setVersion->getUrl();
        $referer = $request->headers->get('referer');
        if(strpos($referer, 'admin')){
            $this->addFlash('success','Версия изменена');
            return $this->redirect($referer);
        }
        return $this->json([
            'action' => 'redirect',
            'domain' => $domain,
        ]);
    }


}
