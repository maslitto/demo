<?php


namespace App\Controller;

use App\Repository\ProductRepository;
use App\Services\ElasticService;
use App\Services\VersionService;
use Elastica\Request as ElasticaRequest;
use function Sodium\increment;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use FOS\ElasticaBundle\Finder\FinderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 *  search controller
 *
 * @Route("/search")
 *
*/

class SearchController extends AbstractController
{
    private $versionService;

    public function __construct(VersionService $versionService)
    {
        $this->versionService = $versionService;
    }
    /**
     * @Route("/", name="search")
     * @Method("GET")
     *
     */
    public function index(Request $request,ElasticService $elasticService)
    {
        if($request->isXmlHttpRequest()){
            $search = $request->get('query');
            $elasticService->setSearch($search);

            $brands = $elasticService->findBrands(5,[]);
            $catalogs = $elasticService->findCatalogs(5,[]);
            $tags = $elasticService->findTags(5);
            $products = $elasticService->findProducts(50);

            $serializer = $this->get('serializer');
            $k = 0;
            $tagOutput = [];
            foreach($tags as $tag){
                if(isset($tag->getCatalogs()[0])){
                    $tagOutput[$k]['name'] = $tag->getName();
                    $tagOutput[$k]['url'] = $tag->getCatalogs()[0]->getUrlPath().$tag->getUrl().'/';
                    $tagOutput[$k]['active'] = $tag->getActive();
                    $k += 1;
                }
            }
            $items = [];
            $k = 0;
            foreach ($products as $product) {
                if($product->getPrice($this->versionService->getSiteVersion()) > 0){
                    $items[$k]['id'] = $product->getId();
                    $items[$k]['name'] = $product->getName();
                    $items[$k]['url'] = '/catalog/'.$product->getUrl();
                    $items[$k]['image'] = $product->getImage('s');
                    $items[$k]['count'] = $product->getCount($this->versionService->getId());
                    $items[$k]['price'] = $product->getPrice($this->versionService->getSiteVersion());
                    $k+=1;
                }

            }
            $response = [
                'products' => $items,
                'brands' => $serializer->serialize($brands, 'json', ['groups' => ['search']]),
                'catalogs' => $serializer->serialize($catalogs, 'json', ['groups' => ['search']]),
                'tags' => $tagOutput,//$serializer->serialize($tags, 'json', ['groups' => ['search']]),
                'yandexMetrikaId' => 12412125125
                //TODO вставить реальный счетчик
            ];

            return $this->json($response);
        }
        else{
            $query = $request->get('query');
            return $this->redirectToRoute('catalog', array('query' => $query));}

    }


}
