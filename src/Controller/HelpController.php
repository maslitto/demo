<?php


namespace App\Controller;

use App\Entity\Feedback;
use App\Repository\HelpPageRepository;
use App\Services\VersionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
/**
 * Help controller. Delivery & Payment info
 *
 * @Route("/help")
*/

class HelpController extends AbstractController
{

    private $versionService;

    public function __construct(VersionService $versionService)
    {
        $this->versionService = $versionService;
    }
    /**
     * @Route("/delivery/", name="delivery_page")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delivery(Request $request, HelpPageRepository $helpPageRepository)
    {
        $helpPage = $helpPageRepository->findOneBy(['url' => 'delivery','versionId' => $this->versionService->getId() ]);
        if($request->isXmlHttpRequest()){
            return $this->json($this->render('help/_ajax_delivery.html.twig',['helpPage' => $helpPage])->getContent());
        }
        return $this->render('help/delivery.html.twig',[
            'helpPage' => $helpPage,
            'meta' => $helpPage
        ]);
    }

    /**
     * @Route("/payment/", name="payment_page")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function payment(HelpPageRepository $helpPageRepository)
    {
        $helpPage = $helpPageRepository->findOneBy(['url' => 'payment','versionId' => $this->versionService->getId() ]);

        return $this->render('help/payment.html.twig',[
            'helpPage' => $helpPage,
            'meta' => $helpPage
        ]);
    }

    /**
     * @Route("/dogovor-publichnoy-ofertyi-postavki/", name="oferta_page")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function oferta(HelpPageRepository $helpPageRepository)
    {
        $helpPage = $helpPageRepository->findOneBy(['url' => 'dogovor-publichnoy-ofertyi-postavki','versionId' => $this->versionService->getId() ]);

        return $this->render('help/oferta.html.twig',[
            'helpPage' => $helpPage,
            'meta' => $helpPage
        ]);
    }

    /**
     * @Route("/politika-konfidencialnosti/", name="confidential_page")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function confidential(HelpPageRepository $helpPageRepository)
    {
        $helpPage = $helpPageRepository->findOneBy(['url' => 'politika-konfidencialnosti','versionId' => $this->versionService->getId() ]);

        return $this->render('help/confidential.html.twig',[
            'helpPage' => $helpPage,
            'meta' => $helpPage
        ]);
    }

    /**
     * @Route("/pickup/", name="pickup_page")
     * @Method("GET")
     *
     */
    public function pickup(HelpPageRepository $helpPageRepository)
    {
        $helpPage = $helpPageRepository->findOneBy(['url' => 'pickup','versionId' => $this->versionService->getId() ]);

        return $this->json($this->render('help/_ajax_pickup.html.twig',['helpPage' => $helpPage])->getContent());
    }

}
