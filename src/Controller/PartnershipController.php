<?php


namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Partnership controller
 *
 * @Route("/partnership")
*/

class PartnershipController extends AbstractController
{
    use \App\Traits\Meta;

    /**
     * @Route("/", name="partnership_index")
     *
     * @Method("GET")
     */
    public function index()
    {
        $meta = $this->getMetaObject('Партнерская программа');
        return $this->render('partnership/index.html.twig',[
            'meta' => $meta
        ]);
    }


}
