<?php
/**
 * Created by PhpStorm.
 * User: veteran
 * Date: 18.02.18
 * Time: 0:12
 */

namespace App\Traits;


use App\Entity\MetaInterface;
use App\Entity\SiteVersion;
trait Meta
{
    public function getMetaObject(string $title, string $description = NULL, string $keywords = NULL)
    {
        $meta = new class($title, $description, $keywords) implements MetaInterface {

            private $title;
            private $keywords;
            private $description;

            public function __construct($title, $description, $keywords)
            {
                $this->title = $title;
                $this->keywords = $keywords;
                $this->description = $description;
            }

            public function getMetaDescription(SiteVersion $siteVersion): ?string
            {
                if($this->keywords){
                    return $this->keywords;
                } else{
                    return $this->title;
                }
            }
            public function getMetaKeywords(SiteVersion $siteVersion): ?string
            {
                if($this->keywords){
                    return $this->keywords;
                } else{
                    return $this->title;
                }
            }
            public function getMetaTitle(SiteVersion $siteVersion): ?string
            {
                return $this->title;
            }

        };
        return $meta;
    }
}