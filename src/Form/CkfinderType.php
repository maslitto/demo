<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CkfinderType extends AbstractType
{
    /*public function configureOptions(OptionsResolver $resolver)
    {

    }*/

    public function getParent()
    {
        return TextType::class;
    }
}