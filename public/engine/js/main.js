$(function() {
    initWindow();
    initTooltip();
    initPopups();
    $('.tabs-class').tabs();

    var sidebar = $('#sidebar');
    var h1 = $('h1 .actions');
    var marketStatusVal = $('#proc-status').val();
    var serviceInput = $('input#service-name');

    h1.find('.filter').unbind('click').bind('click', function(){
        $('.table-filter').stop().slideToggle(200);
    });

    $('.table-filter').find('.close').bind('click', function(){
        $(this).closest('.table-filter').slideUp(200);
    });

    $('label').each(function(){
        if($(this).children('input[type="checkbox"]').length) {
            initCheckbox($(this));
        }
    });

    initRadio($('form'));

    $('.menu .open .submenu', '#sidebar').css('display', 'block');
    $('.menu li > a', '#sidebar').click(function(){
        var li = $(this).parent();
        var submenu = li.children('.submenu');

        if(submenu.length) {
            li.toggleClass('open');
            submenu.slideToggle(200);
            return false;
        }
    });

    $(document).on('click', 'button.add-handler', function() {
        var btn = $('.add-handler');
        var sel = $('select[name="tags-select"]').find(":selected");
        var module = btn.data('module');
        var section = btn.data('section');

        var products = $('input:checkbox:checked.prod-check').map(function () {
          return $(this).data('id');
        }).get();

        $.ajax({
          method: 'POST',
          url: '/admin/' + module + '/' + section + '/add-ptt/',
          data: {
            tag : sel.val(),
            products : JSON.stringify(products)
          },
          success: function(data) {
            location.reload();
          },
        });
    });

    $(document).on('click', 'i.tag-remove', function() {
        var block = $(this).parent();
        var tagId = block.data('tag-id');
        var prodId = block.data('prod-id');

        var module = block.data('module');
        var section = block.data('section');

        var products = $('input:checkbox:checked.prod-check').map(function () {
          return $(this).data('id');
        }).get();

        $.ajax({
          method: 'POST',
          url: '/admin/' + module + '/' + section + '/remove-ptt/',
          data: {
            tag : tagId,
            product : prodId
          },
          success: function(data) {
            block.remove();
          },
        });
    });

    $(document).on('change', '#proc-status', function() {
        if($(this).val() != marketStatusVal) {
            $('#edit-form').append("<input type='hidden' name='stat-change' value='1'/>"); 
        }
    });

    $(document).on('change', '.market-row *', function() {
        if($(this).val() != marketStatusVal) {
            $('#edit-form').append("<input type='hidden' name='del-change' value='1'/>"); 
        }
    });

    $(document).on('change', '#del-type', function() {
        toggleInput($(this), serviceInput);
    });
});

function toggleInput(handler, input) {
    var acceptedTypes = input.data('types');

    if(acceptedTypes === undefined) {
        return;
    }

    var acceptedTypesArr = acceptedTypes.split(" ");

    if(!$.inArray(handler.val(), acceptedTypesArr)) {
        input.parent().show();
        input.prop('disabled', false);        
    }
    else {
        input.parent().hide();
        input.prop('disabled', true);        

    }
}

function initPopups() {
    if($.fn.fancybox) {
        $(".popup-form").fancybox({
            padding: 0
        });

        $(".popup-image").fancybox({
            padding: 0
        });
    }
}

function initRadio(form) {
    form.find('input[type="radio"]').each(function(){
        var label = $(this).parent();
        var input = $(this);

        label.addClass('radio');

        if(input.is(':checked')) {
            label.addClass('checked');
        }

        label.click(function(){
            var inputs = form.find('input[name="' + input.attr("name") + '"]').removeClass('checked');
            inputs.parent().removeClass('checked');
            inputs.removeAttr('checked');

            label.addClass('checked');
            input.attr('checked', true);
            input.prop('checked', true);
        });
    });
}

function initCheckbox(el) {
    el.addClass('checkbox');
    var input = el.children('input');

    if(input.is(':checked')) {
        el.addClass('checked');
    }

    el.click(function(){
        if(input.is(':checked')) {
            el.addClass('checked');
        } else {
            el.removeClass('checked');
        }
    });
}

function initWindow() {
    //do something
}

function initTooltip() {
    $(document).tooltip({track: true});
    $(document).tooltip({
        items: '.tooltip-icon',
        track: true,
        content: function() {
            return $(this).parent().find('.tooltip-desc').html();
        }
    });
}

function checkAll(bx) {
  var cbs = document.getElementsByTagName('input');
  for(var i=0; i < cbs.length; i++) {
    if(cbs[i].type == 'checkbox') {
      cbs[i].checked = bx.checked;
    }
  }
}
