$(function(){
    initTextEditor();
    initFileManager();
    initPropsList();
    initAttrsList();
    initImagesList();
    initFilesList();
    initCoolectionsList();

    iniDatepicker();
});

function initFileManager() {
    CKFinder.customConfig = function( config ) {
        config.language = 'ru';
        config.basePath = '../';
    };
}

function initCoolectionsList() {
    $('.collection-list').each(function(){
        var collectionBox = $(this);
        var name = collectionBox.data('name');

        $('.list', collectionBox).on('click', '.add-down', function() {
            var html = '<tr>';

            $('input[type="text"], select', $('.form', collectionBox)).each(function() {
                var el = $(this).clone()
                    .attr('name', name + '[add][' + $(this).data('name') + '][]')
                    .val($(this).val());

                html += '<td>' + el.prop('outerHTML') + '</td>';
            });

            html +=
                '<td>' +
                '<input type="hidden" name="price-collection[id][]" value="">' +
                '<span class="btn btn-blue del"><i class="fa fa-trash"></i></span>' +
                '<span class="btn btn-green add-down"><i class="fa fa-plus"></i></span>' +
                '</td>';

            html += '</tr>';

            html = $(html);

            $('input[type="text"], select', $('.form', collectionBox)).each(function() {
                html.find('[data-name="' + $(this).data('name') + '"]').val($(this).val());
            });

            $(this).closest('tr').after(html);
        });

        $('.add', collectionBox).on('click', function(){
            var html = '<tr>';

            $('input[type="text"], select', $('.form', collectionBox)).each(function() {
                var el = $(this).clone()
                    .attr('name', name + '[add][' + $(this).data('name') + '][]')
                    .val($(this).val());

                html += '<td>' + el.prop('outerHTML') + '</td>';
            });

            html +=
                '<td>' +
                '<input type="hidden" name="price-collection[id][]" value="">' +
                '<span class="btn btn-blue del"><i class="fa fa-trash"></i></span>' +
                '<span class="btn btn-green add-down"><i class="fa fa-plus"></i></span>' +
                '</td>';

            html += '</tr>';

            html = $(html);

            $('input[type="text"], select', $('.form', collectionBox)).each(function() {
                html.find('[data-name="' + $(this).data('name') + '"]').val($(this).val());
            });

            $('.list', collectionBox).append(html);
        });

        $('.list', collectionBox).on('click', '.del', function(){
            $(this).closest('tr').remove();
        });
    });
}

function addCollectionRow() {

}

function initFilesList() {
    $('.file-form').each(function(){
        var form = $(this);

        $('.del-file', form).on('click', function() {
            form.text('Файл отмечен на удаление.');
            form.append('<input type="hidden" value="on" name="' + $(this).data('name') + '">');
        })
    });
}

function initImagesList() {
    $('.images-list').each(function(){
        var imagesBox = $(this);
        var name = imagesBox.data('name');

        imagesBox.find('.edit').click(function(){
            var row = $(this).closest('.row').find('input').prop('disabled', false);
        });

        imagesBox.find('.add').click(function(){
            var path = $('.new-img', imagesBox).val();
            $('.new-img', imagesBox).val('');

            if(!path) {
                return false;
            }
            $('.list', imagesBox).append(
                '<div class="img">' +
                '<span class="delete"><i class="fa fa-times-circle"></i></span>' +
                '<img src="' + path + '">' +
                '</div>');

            $('.list', imagesBox).append('<input type="hidden" name="' + name + '[add][id][]" value="' + path + '">');
        });

        imagesBox.on('click', '.delete', function() {
            var img = $(this).closest('.img');
            var id = $(this).data('id');

            img.fadeOut(200);

            if(!id) {
                $('.list', imagesBox).find('input[value="' + img.find('img').attr('src') + '"]').remove();
                return false;
            }

            var name = imagesBox.attr('data-name');
            imagesBox.find('.list').append('<input type="hidden" name="' + name + '[del][]" value="' + id + '">');
        });
    });
}

function initPropsList() {
    $('.props-list').each(function(){
        var propsBox = $(this);
        var name = propsBox.attr('data-name');

        propsBox.find('.edit').click(function(){
            var row = $(this).closest('.row').find('input').prop('disabled', false);
        });

        propsBox.find('.remove').click(function(){
            var row = $(this).closest('.row');
            var id = $(this).attr('data-id');

            if(id) {
                propsBox.find('.list').append('<input type="hidden" name="' + name + '[delete]" value="' + id + '">');
            }

            row.remove();
        });

        propsBox.find('.add').click(function(){
            var name = propsBox.attr('data-name');
            propsBox.find('.list').append(
                '<div class="row"><input type="text" name="' + name + '[]"> <div class="btn btn-blue remove"><i class="fa fa-trash-o"></i></div></div>'
            );
            propsBox.find('.remove').click(function(){
                $(this).closest('.row').remove();
            });
        });
    });
}

function initAttrsList() {
    $('.attrs-list').each(function(){
        var propsBox = $(this);
        var name = propsBox.attr('data-name');

        propsBox.find('.edit').click(function(){
            var row = $(this).closest('.row').find('input').prop('disabled', false);
        });

        propsBox.find('.remove').click(function(){
            var row = $(this).closest('.row');
            var id = $(this).attr('data-id');

            $('input[data-filed="val"]', row).val('');
            row.css('display', 'none');
        });

        propsBox.find('.add').click(function(){
            var name = propsBox.attr('data-name');
            propsBox.find('.list').append(
                '<div class="row">' +
                    '<input type="text" name="' + name + '[keys][]" placeholder="Свойство"> ' +
                    '<input type="text" name="' + name + '[vals][]" placeholder="Значение">' +
                    ' <div class="btn btn-blue remove"><i class="fa fa-trash-o"></i></div>' +
                '</div>'
            );
            propsBox.find('.remove').click(function(){
                $(this).closest('.row').remove();
            });
        });
    });
}

//CKEditor
var editors = [];

function initTextEditor() {
    //var finder = new CKFinder();

    $('textarea.editor').each(function(i){
        var textarea = $(this);
        var editor = CKEDITOR.replace(this, {
            language: 'ru',
            toolbar: [
                { name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source']},
                { name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                { name: 'spellcheck', items: [ 'jQuerySpellChecker' ]},
                { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                { name: 'links', items: ['Link', 'Unlink', 'Anchor' ]},
                { name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'SpecialChar', 'PageBreak', 'Iframe']},
                '/',
                { name: 'styles', items: ['Format']},
                { name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                { name: 'colors', items: ['TextColor', 'BGColor']},
                { name: 'tools', items: ['Maximize']},
                { name: 'others', items: ['-']}
            ],
            contentsCss: '/admin/css/jquery/spellchecker.css'
        });

        CKFinder.setupCKEditor(editor, '/js/admin/ckfinder/');

        editor.textarea = textarea;

        editors.push(editor);
    });

    CKEDITOR.editorConfig = function(config) {
        config.extraPlugins = 'jqueryspellchecker';
    };
}

function updateEditors() {
    $.each(editors, function(value, i) {
        var editor = editors[value];
        if (editor) {
            editor.textarea.val(editor.getData());
        }
    });
}

function removeTextEditor() {
    if (editors.length > 0) {
        $.each(editors, function(value, i) {
            var editor = editors[value];
            if (editor) {
                editor.destroy();
                editor = null;
            }
        });
    }
}

//CKFinder
function showFileManager(setFunctionData)
{
    var finder = new CKFinder();
    finder.selectActionFunction = setFileField;
    finder.selectActionData     = setFunctionData;
    finder.popup();
}

function setFileField(fileUrl, data) {
    $(data['selectActionData']).parent().find('input[type="text"]').val(fileUrl);
    $(data['selectActionData']).parent().find('input[type="text"]').attr('href', fileUrl);
    $(data['selectActionData']).closest('.image-form').find('.pic-box').attr('href', fileUrl);
    $(data['selectActionData']).closest('.image-form').find('.pic-box img').attr('src', fileUrl);


}

function iniDatepicker() {
    if(!$.datepicker) {
        return;
    }

    $.datepicker.regional['ru'] = {
        clearText: 'Очистить',
        clearStatus: '',
        closeText: 'Закрыть',
        closeStatus: '',
        prevText: '',
        prevStatus: '',
        nextText: '',
        nextStatus: '',
        currentText: 'Сегодня',
        currentStatus: '',
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь', 'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн', 'Июл','Авг','Сен','Окт','Ноя','Дек'],
        monthStatus: '',
        yearStatus: '',
        weekHeader: 'Не',
        weekStatus: '',
        dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
        dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        dayStatus: 'DD',
        dateStatus: 'D, M d',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        initStatus: '',
        isRTL: false};

    $.datepicker.setDefaults($.datepicker.regional['ru']);

    $('.datepicker').datepicker();
}