var Cart = function(){
    this.cart = [];
    this.price = 0;
    this.count = 0;
    this.sum   = 0;
    this.cookie = 'cart';

    this.init = function() {
        this.on('update', function() {
            this.save();
            this.sync();
        });

        var jsonCart = $.cookie(this.cookie);
        this.cart = jsonCart ? $.parseJSON(jsonCart) : [];
        this.trigger('update');
    };

    this.clear = function() {
        this.cart = [];
        this.trigger('update');
    };

    this.add = function(data) {
        var exists = false;

        for (var i = 0; i < this.cart.length; i++) {
            if(this.cart[i].id == data.id) {
                this.cart.splice(i, 1, data);
                exists = true;
            }
        }

        if(!exists) {
            this.cart.push(data);
        }

        this.trigger('update');
    };

    this.del = function(id) {
        for (var key in this.cart) {
            if(this.cart[key].id == id) {
                this.cart.splice(key, 1);
            }
        }

        this.trigger('update');
    };

    this.getCart = function() {
        return this.cart;
    };

    this.save = function() {
        var cartJson = null;

        if(this.cart.length) {
            cartJson = JSON.stringify(this.cart);
        }

        $.cookie(this.cookie, cartJson, {expires: 365, path: "/"});
    };

    this.sync = function() {
        var cart = this;

        $.ajax({
            url: '/cart/get-info/',
            success: function(serverCart){
                cart.cart = [];
                for (var key in serverCart.cart) {
                    var product = serverCart.cart[key];
                    cart.cart.push(product)
                }

                cart.price = serverCart.price;
                cart.count = serverCart.count;
                cart.sum   = serverCart.price;
                cart.save();
                cart.trigger('render');
            },
            dataType: 'json'
        });
    };

    this.on = function(event, fn) {
        $(this).on(event, fn);
    };

    this.trigger = function(event) {
        $(this).trigger(event);
    };
};

var globalCart = new Cart();
globalCart.init();

function getCart() {
    return globalCart;
}
