$(function(){
    var searchField = $('.fast-search');

    searchField.autocomplete({
        source: function(request, response ) {
            $.ajax({
                url: '/catalog/search/',
                type: "GET",
                dataType: "json",
                data: {
                    query: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        select: function(event, ui) {
            searchField.val(ui.item.value);
            window.location.href = '/catalog/search/?query=' + ui.item.value;
        },
        open: function(){
            $(this).autocomplete('widget').css('z-index', 999);
            return false;
        }
    });
});