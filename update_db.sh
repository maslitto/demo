#!/bin/bash
start_time=$(date +%s)

#dev data
#DB='demo'
#USER='root'
#PASS='local'

#prod data
DB='stommarket_symfony'
#DB_MAIN = 'stommarket_main'
USER='root'
PASS='v3qUtas4udRu'

echo "Exporting main db..."
mysqldump -u$USER -p$PASS stommarket_main > dump.sql

echo "Exporting db backup..."
mysqldump -u$USER -p$PASS stommarket_symfony > symfony_backup.sql

echo "Cleaning db..."
Q1="DROP DATABASE $DB;"
Q2="CREATE DATABASE $DB;"
SQL="${Q1}${Q2}"
mysql -u$USER -p$PASS -e "$SQL"

echo "Loading database dump..."
mysql -u$USER -p$PASS $DB < dump.sql
echo "Success database load!"

echo "Migrating..."
vendor/bin/phinx migrate
#echo "Meta templates dump..."
#mysql -u$USER -p$PASS $DB < meta_templates.sql
#echo "Help pages dump..."
#mysql -u$USER -p$PASS $DB < help_pages.sql
#echo "Managers dump..."
#mysql -u$USER -p$PASS $DB < managers.sql
echo "Slider dump..."
mysql -u$USER -p$PASS $DB < slider.sql
echo "Data dump..."
mysql -u$USER -p$PASS $DB < data.sql

#echo "Delivery types and versions dump..."
#mysql -u$USER -p$PASS $DB < delivery_versions.sql
echo "All done! Success!"
end_time=$(date +%s)
printf 'Elapsed time is %d seconds\n' $((end_time - start_time))
