<?php


use Phinx\Migration\AbstractMigration;

class CopyDataFromCatalogToVersionProps extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function copySpb()
    {
        $this->execute('
            INSERT INTO `catalog_version_props` (
                catalog_id, 
                title, 
                description,
                keywords,
                version_id
            ) 
            SELECT 
                id, 
                title, 
                description,
                keywords,
                1 
            from `catalog`;');
    }

    public function copyMsk()
    {
        $this->execute('
            INSERT INTO `catalog_version_props` (
                catalog_id, 
                title, 
                description,
                keywords,
                version_id
            ) 
            SELECT 
                id, 
                title, 
                description,
                keywords,
                2
            from `catalog`;');
    }

    public function copyFilials(int $versionId)
    {
        $this->execute("
            INSERT INTO `catalog_version_props` (
                catalog_id, 
                title, 
                description,
                keywords,
                version_id
            ) 
            SELECT 
                id, 
                title, 
                description,
                keywords,
                $versionId 
            from `catalog`;");
    }

    public function change()
    {
        $this->copySpb();
        $this->copyMsk();
        foreach ([4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17] as $versionId) {
            $this->copyFilials($versionId);
        }
    }
}
