<?php


use Phinx\Migration\AbstractMigration;

class CreateEducationTypesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('education_types')
            ->addColumn('name','string')
            ->addColumn('active','boolean')
            ->create();
        $this->execute('insert into education_types (name, active) VALUES 
("Вебинар",1), ("Семинар",1), ("Лекция",1),("Мастер-класс",1),("Статья",1)');
    }
}
