<?php


use Phinx\Migration\AbstractMigration;

class CreateCartProductsTable extends AbstractMigration
{
    public function up()
    {
        $this->table('cart_products')
            ->addColumn('cart_id', 'integer')
            ->addColumn('product_id', 'integer')
            ->addColumn('count', 'integer')
            //->addColumn('price_per_item', 'integer')

            ->addForeignKey('product_id', 'products', 'id', ['delete' => 'RESTRICT', 'update' => 'RESTRICT'])
            ->addForeignKey('cart_id', 'carts', 'id', ['delete' => 'CASCADE', 'update' => 'RESTRICT'])

            ->create();
    }
}
