<?php


use Phinx\Migration\AbstractMigration;

class UpdateSliderTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('slider')
            ->addColumn('btn_inline_css', 'text', ['null' => true])
            ->removeColumn('is_buttom_shadow')
            ->removeColumn('background_color')
            ->removeColumn('button_color')
            ->removeColumn('button_shadow_color')
            ->removeColumn('color_type')
            //->removeColumn('is_full_img')
            ->removeColumn('page_id')
            ->addColumn('image', 'string', ['null' => true])
            ->save();
    }
}
