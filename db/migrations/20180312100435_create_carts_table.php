<?php


use Phinx\Migration\AbstractMigration;

class CreateCartsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->table('carts')
            ->addColumn('hash_id', 'string', ['null' => true])
            ->addColumn('ip', 'string', ['null' => true])
            ->addColumn('user_agent', 'string', ['null' => true])
            ->addColumn('finished', 'boolean', ['default' => false])
            ->addColumn('time_create', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('time_update', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('version_id', 'integer')
            ->addColumn('site_device_version', 'integer')
            //->addColumn('price', 'integer')
            ->addColumn('user_id', 'integer', ['null' => true])
            ->addColumn('mail_sended', 'boolean', ['default' => false])

            ->addIndex('hash_id', ['unique' => true])

            ->create();
    }
}
