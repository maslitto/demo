<?php


use Phinx\Migration\AbstractMigration;

class UpdateUsersDataTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('users_data')
            ->changeColumn('name', 'string', ['null' => true])
            ->changeColumn('surname', 'string', ['null' => true])
            ->changeColumn('patronymic', 'string', ['null' => true])
            ->changeColumn('phone', 'string', ['null' => true])
            ->changeColumn('skype', 'string', ['null' => true])
            ->changeColumn('country', 'string', ['null' => true])
            ->changeColumn('city', 'string', ['null' => true])
            ->changeColumn('description', 'text', ['null' => true])
            ->changeColumn('com_name', 'string', ['null' => true])
            ->changeColumn('com_inn', 'string', ['null' => true])
            ->changeColumn('com_kpp', 'string', ['null' => true])
            ->changeColumn('com_ogrn', 'string', ['null' => true])
            ->changeColumn('com_okved', 'string', ['null' => true])
            ->changeColumn('com_urid_address', 'string', ['null' => true])
            ->changeColumn('com_fact_address', 'string', ['null' => true])
            ->changeColumn('com_phone', 'string', ['null' => true])
            ->changeColumn('com_rc', 'string', ['null' => true])
            ->changeColumn('com_kc', 'string', ['null' => true])
            ->changeColumn('com_bik', 'string', ['null' => true])
            ->changeColumn('phone_confirmed', 'boolean', ['null' => true])
            ->changeColumn('person_type', 'boolean', ['null' => true])
            ->addForeignKey('user_id', 'users', 'id', [
                'delete' => 'CASCADE',
                'update' => 'NO_ACTION',
                'constraint' => 'users_data_pk',
            ])
            ->save();
    }
}
