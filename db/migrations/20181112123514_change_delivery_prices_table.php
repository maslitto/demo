<?php


use Phinx\Migration\AbstractMigration;

class ChangeDeliveryPricesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute('update delivery_prices set version_key =1 WHERE version_key ="spb"');
        $this->execute('update delivery_prices set version_key =2 WHERE version_key ="msk"');
        $this->table('delivery_prices')
            ->changeColumn('version_key','integer')
            ->renameColumn('version_key','version_id')
            ->save();
    }
}
