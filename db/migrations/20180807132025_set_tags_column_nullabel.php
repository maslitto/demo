<?php


use Phinx\Migration\AbstractMigration;

class SetTagsColumnNullabel extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('products')
            ->changeColumn('tags','string',['null'=>true])
            ->changeColumn('guarantee','string',['null'=>true])
            ->changeColumn('article','string',['null'=>true])
            ->changeColumn('url_old','string',['null'=>true])
            ->changeColumn('text','text',['null'=>true])
            ->changeColumn('composition','text',['null'=>true])
            ->changeColumn('instruction','text',['null'=>true])
            ->changeColumn('video','string',['null'=>true])
            ->changeColumn('same_products_ids','string',['null'=>true])
            ->changeColumn('div_type','string',['null'=>true])
            ->changeColumn('div_value','string',['null'=>true])
            ->changeColumn('event','string',['null'=>true])
            ->changeColumn('sort','integer',['null'=>true])
            ->changeColumn('market_id','integer',['null'=>true])
            ->changeColumn('status','integer',['null'=>true])
            ->changeColumn('video_url','string',['null'=>true])
            ->changeColumn('value_added_tax','integer',['null'=>true])
            ->removeColumn('yaroslavl_count')
            ->save();
    }
}
