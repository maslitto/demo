<?php


use Phinx\Migration\AbstractMigration;

class UpdateOrdersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('orders')
            ->renameColumn('index', 'zip_code')
            ->removeColumn('name')
            ->changeColumn('user_id', 'integer', ['null' => true])
            ->changeColumn('delivery_type', 'integer', ['null' => true])
            ->changeColumn('description', 'text', ['null' => true])
            ->changeColumn('bonus', 'integer', ['null' => true])
            ->changeColumn('is_ref', 'integer', ['null' => true])
            ->changeColumn('crm_id', 'integer', ['null' => true])
            ->changeColumn('bonus_has_applied', 'integer', ['null' => true])
            ->changeColumn('sort', 'integer', ['null' => true])
            ->changeColumn('zip_code', 'string', ['null' => true])
            ->changeColumn('email', 'string', ['null' => true])
            ->changeColumn('date', 'date', ['null' => true])
            ->changeColumn('encrypted_id', 'string', ['null' => true]);
    }
}
