<?php


use Phinx\Migration\AbstractMigration;

class AddDeliveryPriceColumnAndVersionIdToDeliveryTypeTable extends AbstractMigration
{

    public function change()
    {
        $this
            ->table('delivery_types')
            ->addColumn('title','string',['null' => true])
            ->addColumn('price','string',['null' => true])
            ->addColumn('version_id','integer',['null' => true])
            ->addColumn('is_delivery','integer',['null' => true])
            ->save();
    }
}
