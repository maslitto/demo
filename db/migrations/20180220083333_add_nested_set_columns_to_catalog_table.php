<?php


use Phinx\Migration\AbstractMigration;

class AddNestedSetColumnsToCatalogTable extends AbstractMigration
{
    public function change()
    {
        $this->table('catalog')
            ->addColumn('parent_id', 'integer', ['null' => true])
            ->addColumn('lft', 'integer')
            ->addColumn('rgt', 'integer')
            ->addColumn('lvl', 'integer')
            ->addColumn('root_id', 'integer', ['null' => true])
            ->save();
        $this->execute('update catalog set parent_id = parent');
    }
}
