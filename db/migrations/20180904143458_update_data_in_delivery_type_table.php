<?php


use Phinx\Migration\AbstractMigration;

class UpdateDataInDeliveryTypeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        //spb
        $this->execute('update delivery_types set version_id = 1, title="Самовывоз",price="бесплатно с Республиканской 22",is_delivery=0 where cms_code=11 ');
        $this->execute('update delivery_types set version_id = 1, title="Доставка по Спб",price="бесплатно",is_delivery=1 where cms_code=23 ');
        //msk
        $this->execute('update delivery_types set version_id = 2, title="Самовывоз",price="бесплатно",is_delivery=0 where cms_code=10 ');
        $this->execute('update delivery_types set version_id = 2, title="Доставка по Москве",price="250 ₽",is_delivery=1 where cms_code=5 ');
        //kirov
        $this->execute('update delivery_types set version_id = 4, title="Самовывоз",price="бесплатно",is_delivery=0 where cms_code=44 ');
        $this->execute('update delivery_types set version_id = 4, title="Доставка по Кирову",price="150 ₽",is_delivery=1 where cms_code=43 ');
        $this->execute('update delivery_types set version_id = 4, title="Доставка в Татарстан",price="150 ₽",is_delivery=1 where cms_code=41 ');
        $this->execute('update delivery_types set version_id = 4, title="Доставка в Коми",price="150 ₽",is_delivery=1 where cms_code=42 ');
        //yaroslavl
        $this->execute('update delivery_types set version_id = 5, title="Доставка по Ярославлю",price="по договоренности",is_delivery=1 where cms_code=45 ');
        $this->execute('update delivery_types set version_id = 5, title="Самовывоз",price="бесплатно",is_delivery=0 where cms_code=46 ');
        //kostroma
        $this->execute('update delivery_types set version_id = 6, title="Доставка по Костроме",price="по договоренности",is_delivery=1 where cms_code=54 ');
        $this->execute('update delivery_types set version_id = 6, title="Самовывоз",price="бесплатно",is_delivery=0 where cms_code=62 ');
        //tver
        $this->execute('update delivery_types set version_id = 7, title="Доставка по Твери",price="по договоренности",is_delivery=1 where cms_code=56 ');
        $this->execute('update delivery_types set version_id = 7, title="Самовывоз",price="бесплатно",is_delivery=0 where cms_code=57 ');
        //ivanovo
        $this->execute('update delivery_types set version_id = 8, title="Доставка по Иваново",price="по договоренности",is_delivery=1 where cms_code=50 ');
        $this->execute('update delivery_types set version_id = 8, title="Самовывоз",price="бесплатно",is_delivery=0 where cms_code=53 ');
        //vologda
        $this->execute('update delivery_types set version_id = 9, title="Доставка по Вологде",price="по договоренности",is_delivery=1 where cms_code=60 ');
        $this->execute('update delivery_types set version_id = 9, title="Самовывоз",price="бесплатно",is_delivery=0 where cms_code=59 ');

    }
}
