<?php


use Phinx\Migration\AbstractMigration;

class AddPromocodePivots extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('promocode_product')->addColumn('promocode_id','integer')->addColumn('product_id','integer')->create();
        $this->table('promocode_product_brand')->addColumn('promocode_id','integer')->addColumn('product_brand_id','integer')->create();
        $this->table('promocodes')->addColumn('expiry','datetime')->save();
    }
}
