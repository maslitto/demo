<?php


use Phinx\Migration\AbstractMigration;

class InsertCountAndPriceColumnsToProductsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('products')
            //->addColumn('count_spb','integer',['null'=>true,'default'=> 0])
            //->addColumn('count_msk','integer',['null'=>true,'default'=> 0])
            ->renameColumn('spb_count','count_spb')
            ->renameColumn('msk_count','count_msk')
            ->addColumn('count_kirov','integer',['null'=>true,'default'=> 0])
            ->addColumn('count_yaroslavl','integer',['null'=>true,'default'=> 0])
            ->addColumn('count_volgograd','integer',['null'=>true,'default'=> 0])
            //->addColumn('price_ret','integer',['null'=>true,'default'=> 0])
            //->addColumn('preview','text',['null'=>true])
            ->save();

    }
}
