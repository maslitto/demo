<?php


use Phinx\Migration\AbstractMigration;

class CreateEducationRequestsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('education_requests')
            ->addColumn('name','string')
            ->addColumn('phone', 'string')
            ->addColumn('email', 'string')
            ->addColumn('city', 'string',['null' => true])
            ->addColumn('education_id','integer')
            ->addForeignKey('education_id','educations','id',[
                'delete'=> 'CASCADE',
                'update'=> 'NO_ACTION',
                'constraint' => 'education_requests_type_pk'
            ])
            ->create();
    }
}
