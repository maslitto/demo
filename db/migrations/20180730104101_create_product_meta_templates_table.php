<?php


use Phinx\Migration\AbstractMigration;

class CreateProductMetaTemplatesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('product_meta_templates')
            ->addColumn('meta_title','string', ['null' => true])
            ->addColumn('meta_keywords','string', ['null' => true])
            ->addColumn('meta_description','string', ['null' => true])
            ->addColumn('version_id','integer')
            /*->addForeignKey('version_id', 'site_versions', 'id', [
                'delete' => 'CASCADE',
                'update' => 'NO_ACTION',
                'constraint' => 'site_versions_pk',
            ])*/
            ->create()
        ;
        for($i = 1; $i <= 17; $i++){
            if(!in_array($i,[3,12,14,15])) $this->execute("INSERT INTO 
product_meta_templates 
(version_id, meta_title,meta_keywords, meta_description) 
VALUES ($i,
'{PRODUCT_NAME}  — купить по выгодной цене в интернет-магазине «Стоммаркет»',
'{PRODUCT_NAME}, москва, санкт-петербург, краснодар, димитровград, цены, характеристики, интернет-магазин',
'{PRODUCT_NAME} — купить сегодня с доставкой по выгодной цене в интернет-магазине «Стоммаркет». Производитель: «{BRAND_NAME}». {PRODUCT_NAME}: характеристики, фото, отзывы покупателей, инструкция, сертификаты.')
");
        }

    }
}
