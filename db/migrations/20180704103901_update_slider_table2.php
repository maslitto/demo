<?php


use Phinx\Migration\AbstractMigration;

class UpdateSliderTable2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('slider')
            ->addColumn('html','text',['null' => true])
            ->addColumn('background_image','string',['null' => true])
            ->save();

        $this->execute('update `slider` as s set s.image = (SELECT si.filename from `slider_images` as si where s.id = si.depend)');
        $this->execute('update `slider` as s set s.background_image = (SELECT sbi.filename from `slider_background_images` as sbi where s.id = sbi.depend)');
    }
}
