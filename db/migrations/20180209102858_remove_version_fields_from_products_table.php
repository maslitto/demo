<?php


use Phinx\Migration\AbstractMigration;

class RemoveVersionFieldsFromProductsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    removeColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('products')
            //->removeColumn('preview')
            //->removeColumn('title')
            //->removeColumn('description')
            //->removeColumn('keywords')

            ->removeColumn('tab1_title')
            ->removeColumn('tab1_header')
            ->removeColumn('tab1_description')
            ->removeColumn('tab1_keywords')
            ->removeColumn('tab1_url')
            ->removeColumn('tab1_text')

            ->removeColumn('tab2_title')
            ->removeColumn('tab2_header')
            ->removeColumn('tab2_description')
            ->removeColumn('tab2_keywords')
            ->removeColumn('tab2_url')
            ->removeColumn('tab2_text')

            ->removeColumn('tab3_title')
            ->removeColumn('tab3_header')
            ->removeColumn('tab3_description')
            ->removeColumn('tab3_keywords')
            ->removeColumn('tab3_url')
            ->removeColumn('tab3_text')

            //->removeColumn('msk_count')
            //->removeColumn('spb_count')
            //->removeColumn('kirov_count')

            //->removeColumn('price_opt')
            //->removeColumn('price_old')
            //->removeColumn('price_ret')

            ->save();
    }
}
