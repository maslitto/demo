<?php


use Phinx\Migration\AbstractMigration;

class UpdateFilialContacts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute('update site_versions set phone= "8 485 260-90-68" WHERE id IN (5,6,7,8,9)');
        $this->execute('update site_versions set email= "yar@stommarket.ru" WHERE id IN (5,6,7,8,9)');
        $this->execute('update site_versions set email= "sale@stommarket.ru" WHERE id IN (10,11,12,13,14,15,16,17)');
        $this->execute('update site_versions set phone= "8 800 700-83-43" WHERE id IN (10,11,12,13,14,15,16,17)');
    }
}
