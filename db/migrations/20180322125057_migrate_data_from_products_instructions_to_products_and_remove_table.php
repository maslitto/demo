<?php


use Phinx\Migration\AbstractMigration;

class MigrateDataFromProductsInstructionsToProductsAndRemoveTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute('
            update products set instruction = NULL'
        );
        $this->execute('
            update products as p 
            LEFT JOIN products_instructions as pi on p.id=pi.depend
            set p.instruction = pi.filename'
        );
        $this->dropTable('products_instructions');
    }
}
