<?php


use Phinx\Migration\AbstractMigration;

class UpdateCatalogTableRepairTree extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("INSERT INTO `catalog` (name,url,active,show_brand ) VALUES ( 'Каталог','',1,0)");
        $id = $this->getAdapter()->getConnection()->lastInsertId();
        $this->execute("UPDATE `catalog` SET parent_id = $id WHERE parent_id = 0");
        $this->execute("UPDATE `catalog` SET root_id = $id");
        $this->execute('DROP PROCEDURE IF EXISTS tree_recover;');
        $this->execute("
CREATE PROCEDURE tree_recover ()
MODIFIES SQL DATA
BEGIN

DECLARE currentId, currentParentId  CHAR(36);
DECLARE currentLeft                 INT;
DECLARE startId                     INT DEFAULT $id;

# Determines the max size for MEMORY tables.
SET max_heap_table_size = 1024 * 1024 * 512;

START TRANSACTION;

# Temporary MEMORY table to do all the heavy lifting in,
# otherwise performance is simply abysmal.
CREATE TABLE `tmp_tree` (
`id`        char(36) NOT NULL DEFAULT '',
`parent_id` char(36)          DEFAULT NULL,
`lft`       int(11)  unsigned DEFAULT NULL,
`rgt`      int(11)  unsigned DEFAULT NULL,
PRIMARY KEY      (`id`),
INDEX USING HASH (`parent_id`),
INDEX USING HASH (`lft`),
INDEX USING HASH (`rgt`)
) ENGINE = MEMORY
SELECT `id`,
`parent_id`,
`lft`,
`rgt`
FROM   `catalog`;

# Leveling the playing field.
UPDATE  `tmp_tree`
SET     `lft`  = NULL,
`rgt` = NULL;

# Establishing starting numbers for all root elements.
WHILE EXISTS (SELECT * FROM `tmp_tree` WHERE `parent_id` IS NULL AND `lft` IS NULL AND `rgt` IS NULL LIMIT 1) DO

UPDATE `tmp_tree`
SET    `lft`  = startId,
`rgt` = startId + 1
WHERE  `parent_id` IS NULL
AND  `lft`       IS NULL
AND  `rgt`      IS NULL
LIMIT  1;
SET startId = startId + 2;

END WHILE;

# Switching the indexes for the lft/rgt columns to B-Trees to speed up the next section, which uses range queries.
DROP INDEX `lft`  ON `tmp_tree`;
DROP INDEX `rgt` ON `tmp_tree`;
CREATE INDEX `lft`  USING BTREE ON `tmp_tree` (`lft`);
CREATE INDEX `rgt` USING BTREE ON `tmp_tree` (`rgt`);

# Numbering all child elements
WHILE EXISTS (SELECT * FROM `tmp_tree` WHERE `lft` IS NULL LIMIT 1) DO

# Picking an unprocessed element which has a processed parent.
SELECT     `tmp_tree`.`id`
INTO     currentId
FROM       `tmp_tree`
INNER JOIN `tmp_tree` AS `parents`
ON `tmp_tree`.`parent_id` = `parents`.`id`
WHERE      `tmp_tree`.`lft` IS NULL
AND      `parents`.`lft`  IS NOT NULL
LIMIT      1;

# Finding the element's parent.
SELECT  `parent_id`
INTO  currentParentId
FROM    `tmp_tree`
WHERE   `id` = currentId;

# Finding the parent's lft value.
SELECT  `lft`
INTO  currentLeft
FROM    `tmp_tree`
WHERE   `id` = currentParentId;

# Shifting all elements to the right of the current element 2 to the right.
UPDATE `tmp_tree`
SET    `rgt` = `rgt` + 2
WHERE  `rgt` > currentLeft;

UPDATE `tmp_tree`
SET    `lft` = `lft` + 2
WHERE  `lft` > currentLeft;

# Setting lft and rgt values for current element.
UPDATE `tmp_tree`
SET    `lft`  = currentLeft + 1,
`rgt` = currentLeft + 2
WHERE  `id`   = currentId;

END WHILE;

# Writing calculated values back to physical table.
UPDATE `catalog`, `tmp_tree`
SET    `catalog`.`lft`  = `tmp_tree`.`lft`,
`catalog`.`rgt` = `tmp_tree`.`rgt`
WHERE  `catalog`.`id`   = `tmp_tree`.`id`;

COMMIT;

DROP TABLE `tmp_tree`;
END ;
");
        $this->execute('CALL `tree_recover`();');
        foreach ([1, 2, 4, 5, 6, 7, 8, 9] as $versionId) {
            $this->execute("
          INSERT INTO `catalog_version_props` 
          
          (catalog_id,title,description,keywords,version_id ) 
          
          VALUES ( $id ,'Каталог','Купить стоматологические материалы в интернет-магазине Стоммаркет','стоматологические материалы, интернет-магазин', $versionId )");
        }

        $nodes = $this->fetchAll('SELECT id, parent_id from catalog');
        $this->reBuildTree($nodes);
    }

    public function reBuildTree(array $nodes, $parentId = null, $lvl = 0)
    {
        $branch = [];
        foreach ($nodes as $node) {
            if ($node['parent_id'] === $parentId) {
                $nid = $node['id'];
                $this->execute("UPDATE catalog SET lvl = $lvl WHERE id = $nid");
                $children = $this->reBuildTree($nodes, $node['id'], $lvl + 1);
                $branch[] = $node;
            }
        }

        return $branch;
    }
}
