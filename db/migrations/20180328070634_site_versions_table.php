<?php


use Phinx\Migration\AbstractMigration;

class SiteVersionsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->table('site_versions')
            ->removeColumn('type')
            ->removeColumn('is_default')
            ->removeColumn('is_current')
            ->removeColumn('http_auth_login')
            ->removeColumn('http_auth_password')
            //->removeColumn('is_visible')
            ->removeColumn('parent_key')

            ->addColumn('metrika', 'integer', ['null' => true])
            ->addColumn('mobile_metrika', 'integer', ['null' => true])
            ->addColumn('yandex_metrika_id', 'integer', ['null' => true])
            ->addColumn('yandex_mobile_metrika_id', 'integer', ['null' => true])
            ->addColumn('jivosite', 'text', ['null' => true])
            ->addColumn('email', 'string', ['null' => true])
            ->addColumn('phone', 'string', ['null' => true])
            ->addColumn('work_time', 'string', ['null' => true])
            ->addColumn('address', 'string', ['null' => true])
            ->addColumn('map_address', 'string', ['null' => true])
            ->addColumn('fb', 'string', ['null' => true])
            ->addColumn('vk', 'string', ['null' => true])
            ->addColumn('oferta', 'text', ['null' => true])
            ->addColumn('confidential', 'text', ['null' => true])
            ->addColumn('delivery', 'text', ['null' => true])
            ->addColumn('payment', 'text', ['null' => true])
            ->addColumn('about', 'text', ['null' => true])
            ->addColumn('footer_line1', 'text', ['null' => true])
            ->addColumn('footer_line2', 'text', ['null' => true])
            ->save();
    }
}
