<?php


use Phinx\Migration\AbstractMigration;

class UpdateTypicalPropsValsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->query('delete from typical_props_vals where depend NOT IN( SELECT id FROM `typical_props`)');
        $this->table('typical_props_vals')
            ->renameColumn('depend', 'typical_prop_id')
            ->renameColumn('val', 'v')
            ->renameColumn('key', 'k')
            ->addForeignKey('typical_prop_id', 'typical_props', 'id', [
                'delete' => 'CASCADE',
                'update' => 'NO_ACTION',
                'constraint' => 'typical_prop_pk',
            ])
            ->addIndex(['typical_prop_id'])
            ->save();
    }
}
