<?php


use Phinx\Migration\AbstractMigration;

class SetAllTablesInnoDbType extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("SET @DATABASE_NAME = 'demo';");
        $rows = $this->getAdapter()->fetchAll("
            SELECT  CONCAT('ALTER TABLE `', table_name, '` ENGINE=InnoDB;') AS sql_statements
            FROM    information_schema.tables AS tb
            WHERE   table_schema = @DATABASE_NAME
            AND     `ENGINE` = 'MyISAM'
            AND     `TABLE_TYPE` = 'BASE TABLE'
            ORDER BY table_name DESC;
        ");
        foreach ($rows as $k => $row) {
            $this->execute($row[0]);
        }
    }
}
