<?php


use Phinx\Migration\AbstractMigration;

class UpdateUsersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('users')
            ->removeColumn('login')
            ->removeColumn('confirm')
            ->removeColumn('sort')
            ->removeColumn('depend')
            ->addColumn('roles', 'json', ['null' => true])
            ->save();

        $this->execute("UPDATE users as u set u.roles = '[\"ROLE_USER\"]' WHERE u.type='user'");

        $this->execute("UPDATE users as u set u.roles = '[\"ROLE_USER\"]' WHERE u.roles is NULL");

        $this->execute("UPDATE users as u set u.roles = '[\"ROLE_MANAGER\",\"ROLE_USER\"]' WHERE u.type='manager' ");

        $this->execute("UPDATE users as u set u.roles = '[\"ROLE_ADMIN\",\"ROLE_USER\"]' WHERE u.type='admin' ");

        $this->table('users')->removeColumn('type')->save();
    }
}
