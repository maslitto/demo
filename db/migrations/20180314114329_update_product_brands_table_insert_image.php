<?php


use Phinx\Migration\AbstractMigration;

class UpdateProductBrandsTableInsertImage extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('products_brands')
            ->addColumn('image_file', 'string', ['null' => true])
            ->save();

        $this->execute('update `products_brands` as pb set pb.image_file = (SELECT pbi.filename from `products_brands_images` as pbi where pb.id = pbi.depend); ');

        $this->table('products_brands_images')->drop();
    }
}
