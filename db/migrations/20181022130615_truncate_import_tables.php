<?php


use Phinx\Migration\AbstractMigration;

class TruncateImportTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('slider')->truncate();
        $this->table('delivery_types')->drop();
        $this->table('site_versions')->drop();
        $this->table('managers')->drop();
        $this->table('help_pages')->drop();
        $this->table('pages')->drop();
        $this->table('product_meta_templates')->drop();
        $this->table('brand_meta_templates')->drop();
        $this->table('tag_meta_templates')->drop();
        $this->table('catalog_meta_templates')->drop();
    }
}
