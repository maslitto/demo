<?php


use Phinx\Migration\AbstractMigration;

class UpdateManagersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->query('update `managers` set version_id = 1 where version_key != "msk"');
        $this->query('update `managers` set version_id = 2 where version_key = "msk"');
        $this->table('managers')->renameColumn('create_time', 'time_create')->save();
    }
}
