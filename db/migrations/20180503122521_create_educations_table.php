<?php


use Phinx\Migration\AbstractMigration;

class CreateEducationsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('educations')

            ->addColumn('education_type_id','integer')

            ->addColumn('lector_fio', 'string')
            ->addColumn('phone', 'string')
            ->addColumn('email', 'string')
            ->addColumn('user_id','integer')

            ->addColumn('name','string')
            ->addColumn('city','string',['null' => true])
            ->addColumn('address','string',['null' => true])

            ->addColumn('is_single_day','boolean',['null' => true])
            ->addColumn('date_start','date')
            ->addColumn('date_end','date',['null' => true])
            ->addColumn('time','string')

            ->addColumn('is_free','boolean',['null' => true])
            ->addColumn('price','integer',['null' => true])

            ->addColumn('program','text',['null' => true])

            ->addColumn('photo','string')

            ->addColumn('relative_posts','text',['null' => true])
            ->addColumn('relative_products','text',['null' => true])

            ->addColumn('active','boolean')
            ->addColumn('show_on_banner','boolean',['null' => true])
            ->addColumn('education_status_id','integer')

            ->addForeignKey('education_type_id','education_types','id',[
                'delete'=> 'CASCADE',
                'update'=> 'NO_ACTION',
                'constraint' => 'educations_type_pk'
            ])
            ->addForeignKey('user_id','users','id',[
                'delete'=> 'CASCADE',
                'update'=> 'NO_ACTION',
                'constraint' => 'educations_users_pk'
            ])
            ->addForeignKey('education_status_id','education_statuses','id',[
                'delete'=> 'CASCADE',
                'update'=> 'NO_ACTION',
                'constraint' => 'educations_status_pk'
            ])
            ->create();
    }
}
