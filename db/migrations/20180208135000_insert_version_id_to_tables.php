<?php

use Phinx\Migration\AbstractMigration;

class InsertVersionIdToTables extends AbstractMigration
{
    public function up()
    {
        $this->table('articles')->addColumn('version_id', 'integer')->save();

        //$this->table('articles_images')->addColumn('version_id', 'integer')->save();

        $this->table('feedback')->addColumn('version_id', 'integer')->save();

        //$this->table('feedback_files')->addColumn('version_id', 'integer')->save();

        $this->table('help_info')->addColumn('version_id', 'integer')->save();

        $this->table('managers')->addColumn('version_id', 'integer')->save();

        $this->table('site_pages')->addColumn('version_id', 'integer')->save();

        $this->table('site_settings')->addColumn('version_id', 'integer')->save();

        $this->table('slider')->addColumn('version_id', 'integer')->save();

        //$this->table('slider_background_images')->addColumn('version_id', 'integer')->save();

        //$this->table('slider_images')->addColumn('version_id', 'integer')->save();

        $this->table('slider_mobile')->addColumn('version_id', 'integer')->save();

        $this->table('store_types')->addColumn('version_id', 'integer')->save();
    }

    public function down()
    {
        $this->table('articles')->dropColumn('version_id')->save();

        //$this->table('articles_images')->dropColumn('version_id')->save();

        $this->table('feedback')->dropColumn('version_id')->save();

        //$this->table('feedback_files')->dropColumn('version_id')->save();

        $this->table('help_info')->dropColumn('version_id')->save();

        $this->table('managers')->dropColumn('version_id')->save();

        $this->table('site_pages')->dropColumn('version_id')->save();

        $this->table('site_settings')->dropColumn('version_id')->save();

        $this->table('slider')->dropColumn('version_id')->save();

        //$this->table('slider_background_images')->dropColumn('version_id')->save();

        //$this->table('slider_images')->dropColumn('version_id')->save();

        $this->table('slider_mobile')->dropColumn('version_id')->save();

        $this->table('store_types')->dropColumn('version_id')->save();
    }
}
