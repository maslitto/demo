<?php


use Phinx\Migration\AbstractMigration;

class CreatePagesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('pages')
            ->addColumn('name', 'string')
            ->addColumn('url', 'string')
            ->addColumn('title', 'string', ['null' => true])
            ->addColumn('keywords', 'string', ['null' => true])
            ->addColumn('description', 'string', ['null' => true])
            ->addColumn('text', 'text', ['null' => true])
            ->addColumn('time_create', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('time_update', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('version_id', 'integer')
            ->create();
    }
}
