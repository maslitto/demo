<?php


use Phinx\Migration\AbstractMigration;

class AddLatitudeAndLongitudeColumnsToSiteVersionsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('site_versions')
            ->addColumn('latitude','string',['null' => true])
            ->addColumn('longitude','string',['null' => true])
            ->save();
        $this->execute('update site_versions set latitude="59.935426675000386", longitude="30.412484791278075" where id=1');
        $this->execute('update site_versions set latitude="55.79895817380641", longitude="37.69559545502095" where id=2');
        $this->execute('update site_versions set latitude="58.61033358736161", longitude="49.67363119125174" where id=4');
        $this->execute('update site_versions set latitude="57.580212344069956", longitude="39.88816022872065" where id=5');
        $this->execute('update site_versions set latitude="57.77797260683761", longitude="40.97141765373721" where id=6');
        $this->execute('update site_versions set latitude="56.84954100845727", longitude="35.9353405237155" where id=7');
        $this->execute('update site_versions set latitude="57.00052156113498", longitude="40.93089580535174" where id=8');
        $this->execute('update site_versions set latitude="59.21860254415693", longitude="39.94178805826996" where id=9');
    }
}
