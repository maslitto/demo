<?php


use Phinx\Migration\AbstractMigration;

class UpdateProductImagesTable extends AbstractMigration
{
    public function change()
    {
        $this->table('products_images')->addColumn('product_id', 'integer')->save();
        $this->execute('delete from products_images where depend not in (select id from products)');
        $this->execute('UPDATE `products_images` SET `product_id`=`depend`');
        $this->table('products_images')
            ->removeColumn('depend')
            ->addIndex(['filename']/*,['unique' => true]*/)
            ->addForeignKey('product_id', 'products', 'id', [
                'delete' => 'CASCADE',
                'update' => 'NO_ACTION',
                'constraint' => 'products_images_pk',
            ])
            ->save();
    }
}
