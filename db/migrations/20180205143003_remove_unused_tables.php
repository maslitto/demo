<?php

use Phinx\Migration\AbstractMigration;

class RemoveUnusedTables extends AbstractMigration
{
    // usage vendor/bin/phoenix migrate -e common --dir common

    public function up()
    {
        $this->table('catalog_properties')->drop();
        $this->table('content')->drop();
        $this->table('content_gallery')->drop();
        $this->table('content_images')->drop();
        $this->table('events')->drop(); //not sure is trash
        $this->table('main_page_banner')->drop();
        $this->table('main_page_images')->drop();
        $this->table('news')->drop();
        $this->table('news_images')->drop();
        $this->table('pages_images')->drop();
        $this->table('products_attributes')->drop();
        $this->table('products_properties')->drop();
        $this->table('request_discount')->drop();
        $this->table('site_domains')->drop();
        $this->table('users_images')->drop();
        $this->table('users_old')->drop();
        $this->table('users_old_old')->drop();
    }

    public function down()/*: void*/
    {
    }
}
