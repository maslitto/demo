<?php


use Phinx\Migration\AbstractMigration;

class CreateCatalogVersionPropsTable extends AbstractMigration
{
    public function up()
    {
        $catalogVersionsProps = $this->table('catalog_version_props');
        $catalogVersionsProps
            ->addColumn('catalog_id', 'integer')
            ->addForeignKey('catalog_id', 'catalog', 'id', [
                'delete' => 'CASCADE',
                'update' => 'NO_ACTION',
                'constraint' => 'catalog_version_props_pk',
            ])
            ->addColumn('title', 'string')
            ->addColumn('description', 'text')
            ->addColumn('keywords', 'text')

            ->addColumn('version_id', 'integer')
            ->addIndex(['version_id'])
            ->create();
    }

    public function down()
    {
        return;
    }
}
