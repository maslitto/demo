<?php


use Phinx\Migration\AbstractMigration;

class UpdateManagersPasswords extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("update users set password='2c502efcaee3da1c822bbd537cd61864', roles='[\"ROLE_MANAGER\",\"ROLE_USER\"]' WHERE email='volgograd@stommarket.ru' ");
        $this->execute("update users set password='87e4bb979e6d1cc3a6bc4853d7b80650', roles='[\"ROLE_MANAGER\",\"ROLE_USER\"]' WHERE email='yar@stommarket.ru' ");
        $this->execute("update users set roles='[\"ROLE_MANAGER\",\"ROLE_USER\"]' WHERE email='sale@stommarket.ru' ");
    }
}
