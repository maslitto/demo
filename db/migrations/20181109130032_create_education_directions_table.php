<?php


use Phinx\Migration\AbstractMigration;

class CreateEducationDirectionsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('education_directions')
            ->addColumn('name','string')
            ->addColumn('active','boolean')
            ->create();
        $this->execute('insert into education_directions (name, active) VALUES 
("терапия",1), ("эндодонтия",1), ("Хирургия и имплантология",1),("Пародонтология",1),("Ортопедия",1),
 ("Ортодонтия",1),("Детская стоматология",1),("Зуботехника",1),("CAD/CAM",1),("Для руководителей",1)');
    }
}
