<?php


use Phinx\Migration\AbstractMigration;

class RemoveDuplicateAndEmptyUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute('DELETE FROM `users`
          WHERE id NOT IN (
            SELECT * FROM (
              SELECT MIN(id) FROM users
                WHERE active=1
                GROUP BY email
            ) 
          x)');

        $this->execute("delete from users where email=''");

        $this->execute('DELETE ud FROM users_data as ud 
          LEFT JOIN users u ON u.id = ud.user_id 
              WHERE u.id IS NULL');
    }
}
