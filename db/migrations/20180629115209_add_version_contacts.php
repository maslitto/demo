<?php


use Phinx\Migration\AbstractMigration;

class AddVersionContacts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->setSpb();
        $this->setMsk();
        $this->setYaroslavl();
        $this->setKostroma();
        $this->setTver();
        $this->setIvanovo();
        $this->setVologda();
        $this->setKirov();
    }

    private function setSpb()
    {
        $this->execute('UPDATE site_versions SET 
phone = "8 812 426-19-39",
email = "sale@stommarket.ru",
yandex_metrika_id = 23244307,
work_time = "Пн.-Сб. с 10:00 до 18:00 (прием заявок и пункт выдачи)",
address = "ул. Республиканская, д. 22",
map_address = "https://2gis.ru/spb/geo/5348763291881427%2C30.412817%2C59.929173/routeSearch/rsType/car/from/30.412918%2C59.92933%7C%D0%9D%D0%BE%D0%B2%D0%BE%D1%87%D0%B5%D1%80%D0%BA%D0%B0%D1%81%D1%81%D0%BA%D0%B0%D1%8F%7C5348810536517690%7Cstation/to/30.412252%2C59.935717%7C%D0%A0%D0%B5%D1%81%D0%BF%D1%83%D0%B1%D0%BB%D0%B8%D0%BA%D0%B0%D0%BD%D1%81%D0%BA%D0%B0%D1%8F%2022%7C5348660212702941%7Cgeo?queryState=center%2F30.405092%2C59.932511%2Fzoom%2F16%2FrouteTab",
fb = "https://www.facebook.com/stommarket/",
vk = "https://vk.com/stommarketru",
footer_line1 = "ООО «Стоммаркет» Адрес: г. Санкт-Петербург, ул. Республиканская, д. 22",
footer_line2 = "Лицензия: № ЛО-78-02-002759 от 8.06.2017 г. ОГРН: 1137847154906"
WHERE id=1');
    }

    private function setMsk()
    {
        $this->execute('UPDATE site_versions SET 
phone = "8 495 120-59-19",
email = "msk@stommarket.ru",
yandex_metrika_id = 49421470,
work_time = "Пн.-Пт. с 9:30 до 18:30 (прием заявок и пункт выдачи)",
address = "Колодезный пер., д.2А",
map_address = "https://2gis.ru/moscow/search/%D1%81%D0%BE%D0%BA%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%D0%B8%20%D0%BC%D0%B5%D1%82%D1%80%D0%BE/routeSearch/tab/transport/rsType/bus/from/37.68017%2C55.788838%7C%D0%A1%D0%BE%D0%BA%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%D0%B8%7C4504385606386364%7Cstation/to/37.695268%2C55.799287%7C%D0%9A%D0%BE%D0%BB%D0%BE%D0%B4%D0%B5%D0%B7%D0%BD%D1%8B%D0%B9%20%D0%BF%D0%B5%D1%80%D0%B5%D1%83%D0%BB%D0%BE%D0%BA%202%D0%B0%7C4504235282992004%7Cgeo?queryState=center%2F37.684886%2C55.794026%2Fzoom%2F16%2FrouteTab",
fb = "https://www.facebook.com/stommarket/",
vk = "https://vk.com/stommarketru",
footer_line1 = "ООО «Стоммаркет» Адрес: г. Санкт-Петербург, ул. Республиканская, д. 22",
footer_line2 = "Лицензия: № ЛО-78-02-002759 от 8.06.2017 г. ОГРН: 1137847154906"
WHERE id=2');
    }

    private function setYaroslavl()
    {
        $this->execute('UPDATE site_versions SET 
phone = "8 485 260-90-68",
email = "yar@stommarket.ru",
yandex_metrika_id = 48031040,
work_time = "Пн - Пт с 9:00 до 18:00",
address = "ул. 1-я Путевая, д. 7",
map_address = "https://2gis.ru/moscow/search/%D1%81%D0%BE%D0%BA%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%D0%B8%20%D0%BC%D0%B5%D1%82%D1%80%D0%BE/routeSearch/tab/transport/rsType/bus/from/37.68017%2C55.788838%7C%D0%A1%D0%BE%D0%BA%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%D0%B8%7C4504385606386364%7Cstation/to/37.695268%2C55.799287%7C%D0%9A%D0%BE%D0%BB%D0%BE%D0%B4%D0%B5%D0%B7%D0%BD%D1%8B%D0%B9%20%D0%BF%D0%B5%D1%80%D0%B5%D1%83%D0%BB%D0%BE%D0%BA%202%D0%B0%7C4504235282992004%7Cgeo?queryState=center%2F37.684886%2C55.794026%2Fzoom%2F16%2FrouteTab",
fb = "https://www.facebook.com/stommarket/",
vk = "https://vk.com/yar.stommarket",
footer_line1 = "Франшиза Стоммаркет, ООО \"АГ Энерго\"",
footer_line2 = "Лицензия № ФС-99-02-005660 от 02.11.2016 г. ОГРН: 1135260009268"
WHERE id=5');
    }

    private function setKostroma()
    {
        $this->execute('UPDATE site_versions SET 
phone = "8 494 241-91-25",
email = "yar@stommarket.ru",
yandex_metrika_id = 48067469,
work_time = "Пн - Пт с 9:00 до 18:00",
address = "ул. Шаговая, д. 152",
map_address = "",
fb = "https://www.facebook.com/stommarket/",
vk = "https://vk.com/yar.stommarket",
footer_line1 = "Франшиза Стоммаркет, ООО \"АГ Энерго\"",
footer_line2 = "Лицензия № ФС-99-02-005660 от 02.11.2016 г. ОГРН: 1135260009268"
WHERE id=6');
    }

    private function setTver()
    {
        $this->execute('UPDATE site_versions SET 
phone = "8 482 271-05-10",
email = "yar@stommarket.ru",
yandex_metrika_id = 48068369,
work_time = "Пн - Пт с 9:00 до 18:00",
address = "ул. Московская, д. 82, строение 3",
map_address = "",
fb = "https://www.facebook.com/stommarket/",
vk = "https://vk.com/yar.stommarket",
footer_line1 = "Франшиза Стоммаркет, ООО \"АГ Энерго\"",
footer_line2 = "Лицензия № ФС-99-02-005660 от 02.11.2016 г. ОГРН: 1135260009268"
WHERE id=7');
    }

    private function setIvanovo()
    {
        $this->execute('UPDATE site_versions SET 
phone = "8 493 226-34-57",
email = "yar@stommarket.ru",
yandex_metrika_id = 48069218,
work_time = "Пн - Пт с 9:00 до 18:00",
address = "Шевченко, 2",
map_address = "",
fb = "https://www.facebook.com/stommarket/",
vk = "https://vk.com/yar.stommarket",
footer_line1 = "Франшиза Стоммаркет, ООО \"АГ Энерго\"",
footer_line2 = "Лицензия № ФС-99-02-005660 от 02.11.2016 г. ОГРН: 1135260009268"
WHERE id=8');
    }

    private function setVologda()
    {
        $this->execute('UPDATE site_versions SET 
phone = "8 817 270-02-45",
email = "yar@stommarket.ru",
yandex_metrika_id = 48069590,
work_time = "Пн - Пт с 9:00 до 18:00",
address = "ул. Северная, дом № 36 А",
map_address = "",
fb = "https://www.facebook.com/stommarket/",
vk = "https://vk.com/yar.stommarket",
footer_line1 = "Франшиза Стоммаркет, ООО \"АГ Энерго\"",
footer_line2 = "Лицензия № ФС-99-02-005660 от 02.11.2016 г. ОГРН: 1135260009268"
WHERE id=9');
    }

    private function setKirov()
    {
        $this->execute('UPDATE site_versions SET 
phone = "8 8332 66-04-39",
email = "kirov@stommarket.ru",
yandex_metrika_id = 46854042,
work_time = "Пн - Пт с 9 до 18, сб с 9 до 15",
address = "Первомайский район, улица МОПРа, 39",
map_address = "",
fb = "https://www.facebook.com/stommarket/",
vk = "https://vk.com/kirov.stommarket",
footer_line1 = "ООО \"Моя аптека\",  ООО \"Стоммаркет Киров\" Адрес: г. Киров, Первомайский район, улица Мопра, 39",
footer_line2 = "Лицензия № ЛО-43-02-001125 от 4 апреля 2018 г.,ОГРН 1174350017753"
WHERE id=4');
    }
}
