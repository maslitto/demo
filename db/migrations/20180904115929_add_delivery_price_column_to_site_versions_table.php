<?php


use Phinx\Migration\AbstractMigration;

class AddDeliveryPriceColumnToSiteVersionsTable extends AbstractMigration
{
    public function change()
    {
        $this
            ->table('site_versions')
            ->addColumn('delivery_price','string',['null' => true])
            ->save();
    }
}
