<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Controller;

use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class OrderControllerTest extends WebTestCase
{
    private $cookie;

    public function testAddProductToCart()
    {
        $client = static::createClient();

        $crawler = $client->request(
            'POST',
            '/cart/add/',
            ['count' => 1, 'id' => 4653],
            [],
            [
                'CONTENT_TYPE'          => 'application/x-www-form-urlencoded; charset=UTF-8',
                'HTTP_REFERER'          => 'http://localhost:8000/catalog/',
                'HTTP_X-Requested-With' => 'XMLHttpRequest',
                'HTTP_USER_AGENT'       => 'MySuperBrowser/1.0',
            ]
        );
        $this->cookie = $client->getCookieJar()->get('cart');
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }
    /**
     * @depends testAddProductToCart
     */
    public function testAddOrder()
    {
        $client = static::createClient();

        $crawler = $client->request(
            'POST',
            '/order/add/',
            [
                'address' => '',
                'agreement' => 	1,
                'delivery_type' => 	11,
                'email' =>	'tester@phpunit.ru',
                'origin_payment-type' =>	3,
                'phone' =>	'+7+(999)+999-99-99',
                'source_type' =>	0,
                'username' =>	'tester',
            ],
            [],
            [
                'COOKIE'                => 'cart='.$this->cookie.';',
                'CONTENT_TYPE'          => 'application/x-www-form-urlencoded; charset=UTF-8',
                'HTTP_REFERER'          => 'http://localhost:8000/cart/',
                'HTTP_X-Requested-With' => 'XMLHttpRequest',
                'HTTP_USER_AGENT'       => 'MySuperBrowser/1.0',
            ]
        );
        //file_put_contents('/var/www/symfony-demo/error.html',$client->getResponse());
        $this->assertNull($client->getCookieJar()->get('cart'));
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }
}
