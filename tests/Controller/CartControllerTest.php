<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Controller;

use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class CartControllerTest extends WebTestCase
{
    public function testAddProductToCart()
    {
        $client = static::createClient();

        $crawler = $client->request(
            'POST',
            '/cart/add/',
            ['count' => 1, 'id' => 4653],
            [],
            [
                'CONTENT_TYPE'          => 'application/x-www-form-urlencoded; charset=UTF-8',
                'HTTP_REFERER'          => 'http://localhost:8000/catalog/',
                'HTTP_X-Requested-With' => 'XMLHttpRequest',
                'HTTP_USER_AGENT'       => 'MySuperBrowser/1.0',
            ]
        );
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }
}
