<!-- HEADER-->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
	<link rel="stylesheet" href="css/main.css">
	<title>Stomarket</title>
	<meta name="description" content="">
	<meta property="og:title" content="">
	<meta property="og:site_name" content="">
	<meta property="og:url" content="">
	<meta property="og:description" content="">
	<meta property="og:type" content="business.business">
	<meta property="og:image" content="">

	<link rel="apple-touch-icon" sizes="180x180" href="/img/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/img/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/img/favicons/favicon-16x16.png">
	<link rel="manifest" href="/img/favicons/manifest.json">
	<link rel="mask-icon" href="/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="/img/favicons/favicon.ico">
	<meta name="msapplication-config" content="/img/favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
</head>

<body class="body">
	<header class="header js-header">
		<div class="header__line1">
			<div class="innertube">
				<a href="/" class="header__logo">
					<img src="img/svg/logo.svg" alt="">
				</a>
				<nav class="header__nav">
					<a href="#" class="header__nav-el">Прайс-лист</a>
					<a href="#" class="header__nav-el">Франшиза</a>
					<a href="#" class="header__nav-el">Отзывы</a>
					<a href="#" class="header__nav-el">Помощь</a>
					<a href="#" class="header__nav-el">Блог</a>
					<a href="#" class="header__nav-el">О компании</a>
					<a href="#" class="header__nav-el">Контакты</a>
				</nav>
				<div class="header__contact contact">
					<div class="contact__b">
						<div class="contact__title">Cанкт-Петербург</div>
						<div class="contact__num popup-down__hover arr-d-after arr-d-after--large">8 812 426-19-39
							<div class="popup-down__block">
								<a href="#" class="popup-down__link js-change-city" data-id="2">Москва</a>
								<a href="#" class="popup-down__link js-change-city" data-id="3">Краснодар</a>
								<a href="#" class="popup-down__link js-change-city" data-id="4">Димитровград</a>
							</div>
						</div>
					</div>
					<div class="contact__b">
						<div class="contact__title">Бесплатно по России</div>
						<a href="tel:88007008343" class="contact__num">8 800 700-83-43</a>
					</div>
				</div>
			</div>
		</div>
		<div class="header__line2">
			<div class="innertube">
				<div class="header__catalog h-catalog">
					<div class="h-catalog__title js-h-catalog__title">
						<span class="h-catalog__title-text arr-d-after">Каталог товаров</span>
						<nav class="h-catalog__list js-custom-scroll">
							<div class="h-catalog__list-wrapper h-catalog__list-wrapper--l1 js-h-catalog__list-wrapper" data-level="0">
								<ul class="h-catalog__scroll js-scroll">
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="1"><a href="#">Производители</a></li>
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="2"><a href="#">Распродажа</a></li>
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="3"><a href="#">Наборы</a></li>
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="4"><a href="#">Производители</a></li>
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="5"><a href="#">Распродажа</a></li>
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="6"><a href="#">Наборы</a></li>
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="7"><a href="#">Производители</a></li>
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="1"><a href="#">Производители</a></li>
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="2"><a href="#">Распродажа</a></li>
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="3"><a href="#">Наборы</a></li>
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="4"><a href="#">Производители</a></li>
									<li class="h-catalog__el h-catalog__el--link" data-menu-target="4"><a href="#">Производители</a></li>
									<li class="h-catalog__el h-catalog__el--link" data-menu-target="5"><a href="#">Распродажа</a></li>
									<li class="h-catalog__el h-catalog__el--link" data-menu-target="6"><a href="#">Наборы</a></li>
									<li class="h-catalog__el h-catalog__el--link" data-menu-target="7"><a href="#">Производители</a></li>
								</ul>
							</div>
							
							
							<div class="h-catalog__list-wrapper h-catalog__list-wrapper--l2 js-h-catalog__list-wrapper" data-level="1" data-menu-block="1">
								<ul class="h-catalog__scroll js-scroll">
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="8"><a href="#">Производители</a></li>
									<li class="h-catalog__el h-catalog__el--link" ><a href="#">Распродажа</a></li>
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="8"><a href="#">Наборы</a></li>
									<li class="h-catalog__el h-catalog__el--link" ><a href="#">Производители</a></li>
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="8"><a href="#">Распродажа</a></li>
								</ul>
							</div>
							<div class="h-catalog__list-wrapper h-catalog__list-wrapper--l2 js-h-catalog__list-wrapper" data-level="1" data-menu-block="2">
								<ul class="h-catalog__scroll js-scroll">
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Производители</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Распродажа</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Наборы</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Производители</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Распродажа</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Наборы</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Производители</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Распродажа</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Наборы</a></li>
								</ul>
							</div>
							<div class="h-catalog__list-wrapper h-catalog__list-wrapper--l2 js-h-catalog__list-wrapper" data-level="1" data-menu-block="3">
								<ul class="h-catalog__scroll js-scroll">
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Производители</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Распродажа</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Наборы</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Производители</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Распродажа</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Наборы</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Производители</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Распродажа</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Наборы</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Производители</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Распродажа</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Наборы</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Производители</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Распродажа</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Наборы</a></li>
								</ul>
							</div>
							<div class="h-catalog__list-wrapper h-catalog__list-wrapper--l2 js-h-catalog__list-wrapper" data-level="1" data-menu-block="4">
								<ul class="h-catalog__scroll js-scroll">

									<li class="h-catalog__el h-catalog__el--link"><a href="#">Производители</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Распродажа</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Наборы</a></li>
								</ul>
							</div>
							<div class="h-catalog__list-wrapper h-catalog__list-wrapper--l2 js-h-catalog__list-wrapper" data-level="1" data-menu-block="5">
								<ul class="h-catalog__scroll js-scroll">

									<li class="h-catalog__el h-catalog__el--link"><a href="#">Рентгенография</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Распродажа</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Рентгенография</a></li>
								</ul>
							</div>
							<div class="h-catalog__list-wrapper h-catalog__list-wrapper--l2 js-h-catalog__list-wrapper" data-level="1" data-menu-block="6">
								<ul class="h-catalog__scroll js-scroll">
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Jртопедический</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Обработка ортопедический конструкций и изделий</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Обработка ортопедический конструкций и изделий</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Jртопедический</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Обработка ортопедический конструкций и изделий</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Обработка ортопедический конструкций и изделий</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Jртопедический</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Обработка ортопедический конструкций и изделий</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Обработка ортопедический конструкций и изделий</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Jртопедический</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Обработка ортопедический конструкций и изделий</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Обработка ортопедический конструкций и изделий</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Jртопедический</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Обработка ортопедический конструкций и изделий</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Обработка ортопедический конструкций и изделий</a></li>
								</ul>
							</div>
							<div class="h-catalog__list-wrapper h-catalog__list-wrapper--l2 js-h-catalog__list-wrapper" data-level="1" data-menu-block="7">
								<ul class="h-catalog__scroll js-scroll">
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Зуботехническая лаборатория</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Зуботехническая </a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Зуботехническая лаборатория</a></li>
								</ul>
							</div>
							
							
							<div class="h-catalog__list-wrapper h-catalog__list-wrapper--l3 js-h-catalog__list-wrapper" data-level="2" data-menu-block="8">
								<ul class="h-catalog__scroll js-scroll">
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="10"><a href="#">Производители</a></li>
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="11"><a href="#">Распродажа</a></li>
									<li class="h-catalog__el h-catalog__el--link js-h-catalog__el" ><a href="#">Наборы</a></li>
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="11"><a href="#">Производители</a></li>
								</ul>
							</div>
							<div class="h-catalog__list-wrapper h-catalog__list-wrapper--l3 js-h-catalog__list-wrapper" data-level="2" data-menu-block="9">
								<ul class="h-catalog__scroll js-scroll">
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="10"><a href="#">Зуботехническая лаборатория</a></li>
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="11"><a href="#">Зуботехническая </a></li>
									<li class="h-catalog__el js-h-catalog__el" data-menu-target="10"><a href="#">Зуботехническая лаборатория</a></li>
								</ul>
							</div>


							<div class="h-catalog__list-wrapper h-catalog__list-wrapper--l4 js-h-catalog__list-wrapper" data-level="3" data-menu-block="10">
								<ul class="h-catalog__scroll js-scroll">
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Производители</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Распродажа</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">2232</a></li>
								</ul>
							</div>
							<div class="h-catalog__list-wrapper h-catalog__list-wrapper--l4 js-h-catalog__list-wrapper" data-level="3" data-menu-block="11">
								<ul class="h-catalog__scroll js-scroll">
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Наборы</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">Распро22222дажа</a></li>
									<li class="h-catalog__el h-catalog__el--link"><a href="#">1111111111111111</a></li>
								</ul>
							</div>
						</nav>
					</div>
				</div>
				<form action="/catalog/search/" type="get" class="header__search h-search js-h-search">
					<input type="text" name="query" class="h-search__input js-h-search__input" placeholder="Введите название товара для поиска по каталогу" autocomplete="off">
					<div class="h-search__btn js-h-search__btn" value=""></div>
					<div class="h-search__results js-h-search__results">
						<div class="h-search__results-scroll">
							<div class="h-search__results-b js-h-search__results-b  js__results-brands"></div>
							<div class="h-search__results-b js-h-search__results-b  js__results-catalog"></div>
							<div class="h-search__results-b js-h-search__results-b  h-search__results-b--tags js__results-tags"></div>
							<div class="h-search__results-products js-h-search__results-b   js__results-products"></div>
						</div>
					</div>
				</form>
				<div class="basket-menu">
					<a href="/basket.html" class="basket-menu__link icon-basket js-basket-menu">
						<span class="basket-menu__count js-basket-menu__count"></span>
					</a>
					<div href="" class="basket-menu__link icon-logout js-login-open"></div>
					<div class="login js-login-block">
						<div class="login__block js-slide-block active" data-slide-name="login-menu">
							<a href="#login-login" class="js-slide-link login__btn">Войти на сайт</a>
							<a href="#login-registration" class="js-slide-link login__btn">Регистрация</a>
						</div>
						<div class="login__block js-slide-block " data-slide-name="login-login">
							<form action="/login/" class="js-form" data-error-msg="Произошла ошибка">
								<div class="form__b form__text-input-b">
									<input type="email" class="form__text-input js-input js-required " name="login_email" id="login_email">
									<label class="form__text-input-label" for="login_email">E-mail</label>
								</div>
								<div class="form__b form__text-input-b form__text-input-b--mb">
									<input type="password" class="form__text-input js-input js-required" name="login_pass" id="login_pass">
									<label class="form__text-input-label" for="login_email">Пароль</label>
								</div>
								<button type="submit" class="login__btn">Войти</button>
							</form>
							<a href="#login-menu" class="login__back-link js-slide-link">Назад</a>
						</div>
						<div class="login__block js-slide-block " data-slide-name="login-registration">
							<form action="/registration/" class="js-form" data-error-msg="Произошла ошибка">
								<div class="form__b form__text-input-b">
									<input type="email" class="form__text-input js-input js-required" name="login_email" id="reg_email">
									<label class="form__text-input-label" for="reg_email">E-mail</label>
								</div>
								<div class="form__b form__text-input-b">
									<input type="password" class="form__text-input js-input js-required js-double-pass" name="reg_pass" id="reg_pass">
									<label class="form__text-input-label" for="reg_pass">Пароль</label>
								</div>
								<div class="form__b form__text-input-b">
									<input type="password" class="form__text-input js-input js-required js-double-pass" name="reg_pass_check" id="reg_pass_check">
									<label class="form__text-input-label" for="reg_pass_check">Пароль</label>
								</div>
								<button type="submit" class="login__btn">Отправить</button>
								<div class="form__success js-form__success">
									<div class="content">
										<p class="h3">Регистрация</p>
										<p>Вы успешно зарегистрированы. Подтвердите почту, для активации учетной записи.</p>
									</div>
								</div>
							</form>
							<a href="#login-menu" class="login__back-link js-slide-link">Назад</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!-- END HEADER -->

	<main class="main">