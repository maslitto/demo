</main>
<footer class="footer">
	<div class="innertube">
		<div class="footer-contacts">
			<div class="footer__col">
				<a href="tel:88007008343" class="footer-contacts__main-phone icon-phone">8 800 700-83-43</a>
				<a href="mailto:sale@stommarket.ru" class="footer-contacts__main-email icon-mail">sale@stommarket.ru</a>
			</div>
			<div class="footer__col icon-marker">
				<div class="footer__title">Санкт-Петербург</div>
				<div class="footer-contacts__local-text">Республиканская, 22</div>
				<a href="tel:+78124261939" class="footer-contacts__local-text">+7 812 426-19-39</a>
			</div>
			<div class="footer__col">
				<div class="footer__title">Москва</div>
				<div class="footer-contacts__local-text">Колодезный пер., д. 2А</div>
				<a href="tel:+74951205919" class="footer-contacts__local-text">+7 495 120-59-19</a>
			</div>
			<div class="footer__col">
				<div class="footer__title">Краснодар</div>
				<div class="footer-contacts__local-text">ул. Уральская, д. 83а</div>
				<a href="tel:+78612025356" class="footer-contacts__local-text">+7 861 202-53-56</a>
			</div>
			<div class="footer__col">
				<div class="footer__title">Димитровград</div>
				<div class="footer-contacts__local-text">ул. Королева, д. 7</div>
				<a href="tel:+78422505118" class="footer-contacts__local-text">+7 842 250-51-18</a>
			</div>
		</div>
		<div class="footer-menu">
			<div class="footer__col footer__col--logo">
				<div class="footer-menu__logo">
					<img src="img/svg/logo.svg" alt="">
				</div>
				<div class="footer-menu__copy">© Stommarket.ru, товары для стоматологии, 2017</div>
				<a class="footer-menu__link">Обратная связь</a>
				<a class="footer-menu__link">Контакты</a>
				<a class="footer-menu__link">Договор публичной оферты</a>
			</div>
			<div class="footer__col">
				<div class="footer__title">Stommarket</div>
				<a class="footer-menu__link">Прайс-лист</a>
				<a class="footer-menu__link">Франшиза</a>
				<a class="footer-menu__link">Отзывы</a>
				<a class="footer-menu__link">Помощь</a>
				<a class="footer-menu__link">Блог</a>
				<a class="footer-menu__link">О компании</a>
			</div>
			<div class="footer__col">
				<div class="footer__title">Каталог</div>
				<a class="footer-menu__link">Анестезия</a>
				<a class="footer-menu__link">Дезинфекция</a>
				<a class="footer-menu__link">Ирригаторы</a>
				<a class="footer-menu__link">Ортопедия</a>
				<a class="footer-menu__link">Профилактика</a>
				<a class="footer-menu__link">Терапия</a>
			</div>
			<div class="footer__col">
				<div class="footer__title">Помощь</div>
				<a class="footer-menu__link">Как сделать заказ?</a>
				<a class="footer-menu__link">Как оплатить?</a>
				<a class="footer-menu__link">Доставка</a>
				<a class="footer-menu__link">Возврат товара</a>
				<a class="footer-menu__link">Гарантии</a>
				<a class="footer-menu__link">Обратная связь</a>
			</div>
			<div class="footer__col">
				<div class="footer__title">Соц. сети</div>
				<div class="footer-menu__icons">
					<a class="footer-menu__social icon-fb"></a>
					<a class="footer-menu__social icon-vk"></a>
				</div>
				<div class="footer__title">Оплата</div>
				<div class="footer-menu__icons">
					<a class="footer-menu__payment icon-yad"></a>
					<a class="footer-menu__payment icon-visa"></a>
					<a class="footer-menu__payment icon-mk"></a>
				</div>
			</div>
		</div>
	</div>
</footer>
@@include('../templates/popups.tpl')
<div class="veil-b js-body-veil"></div>
<script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
<script>
	window.jQuery || document.write('<script src="js/vendor/jquery.min.js">\x3C/script>')
</script>
<script src="./js/main.js"></script>
<!-- Gallery section -->
<!-- END Gallery section -->
</body>

</html>
