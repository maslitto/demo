"use strict";

//= ../bower_components/slick-carousel/slick/slick.js
//= ../bower_components/nouislider/distribute/nouislider.js
//= ../bower_components/magnific-popup/dist/jquery.magnific-popup.js
//= ../bower_components/history.js/scripts/uncompressed/history.adapter.jquery.js
//= ../bower_components/history.js/scripts/uncompressed/history.js
//= ../bower_components/fancybox/source/jquery.fancybox.pack.js

//= ../node_modules/simplebar/dist/simplebar.js
//= ../node_modules/select2/dist/js/select2.js
//= ../node_modules/pickmeup/js/pickmeup.js
//= ../node_modules/cropper/dist/cropper.js
// ../node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js
//= ../node_modules/vue/dist/vue.min.js
//= ../node_modules/noty/lib/noty.js


//= vendor/masonry.pkgd.min.js
//= vendor/jquery-ui.min.js
//= vendor/sodselect/selectordie.js
//= vendor/inputmask/inputmask.js
//= vendor/sticky-kit/sticky-kit.min.js


//= modules/helpers.js
//= modules/panel.js
//= modules/hoverMenu.js
//= modules/popup.js
//= modules/sliders.js
//= modules/catalogFilter.js
//= modules/sodSelect.js
//= modules/masonry.js
//= modules/forms.js
//= modules/tabs.js
//= modules/sticky.js
//= modules/maps.js
//= modules/toggle.js
//= modules/bodyVeil.js
//= modules/login.js
//= modules/basket.js
//= modules/carts.js
//= modules/reviews.js
//= modules/lk.js
//= modules/search.js
//= modules/catalog.js
//= modules/headers.js
//= modules/googleAnalytics.js
//= modules/registration.js
//= modules/citySwitch.js
//= modules/sendsay.js
//= modules/educationCatalogFilter.js
//= modules/cdek.js
//= modules/cropper.js
//= modules/product.vue.js
//= modules/convead.js

var app = (function () {
	return {
		init: function () {

		    productVue.init();

		    //Вспомогательное
			helpers.init();

			//Сетка блоков на главной
			masonry.init();

			//Товары и страница товара
			cart.init();

			//Инициализация попапов
			popup.init();

			//Инициализация слайдеров
			sliders.init();

			//catalog filter
			catalogFilter.init();

			//Кастомизация выпадающих списков
			sodSelect.init();

			//Затемнение на ховер
			//в каталоге
			hoverMenu.init();

			//Скрипты формы
			forms.init();

			// Плавающие элементы
			sticky.init();

			//табы
			tabs.init();

			//корзина
			basket.init();

			//Google maps
			maps.init();

			//Темная подложка
			bodyVeil.init();

			//Форма логина и регистрации
			login.init();

			// Прайс лист
			toggle.init();

			//Подгрузка отзывов
			reviews.init();

			// Личный кабинет
			lk.init();

			// Поиск
			search.init();

			// Каталог
			catalog.init();

			// Нижняя панель
			panel.init();

			// Шапка сайта
			headers.init();

			// Google analytics
			googleAnalytics.init();

			// Регистрация
			registration.init();

			//Переключение городов
			citySwitch.init();

			//sendsay
			//sendsay.init();

			//каталог обучаек
            educationCatalogFilter.init();
            
			// СДЭК
			//cdek.init();

			cropper.init();

			conveadApp.init();
		}
	}
})();

$(document).on('ready', app.init);






