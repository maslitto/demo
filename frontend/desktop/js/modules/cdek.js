var cdek = (function () {

    return {
        init: function(){
            cdek.autocomplete();
            cdek.showProductInfo();
            cdek.calculator();
            cdek.mapWidget();
        },
        autocomplete: function() {
            $(".js-cdek-autocomplete").autocomplete({
                source: function(request,response) {
                    $.ajax({
                        url: "https://api.cdek.ru/city/getListByTerm/jsonp.php?callback=?",
                        dataType: "jsonp",
                        data: {
                            q: function () { return $(".js-cdek-autocomplete").val() },
                            name_startsWith: function () { return $(".js-cdek-autocomplete").val() }
                        },
                        success: function(data) {
                            response($.map(data.geonames, function(item) {
                                return {
                                    label: item.name,
                                    value: item.name,
                                    id: item.id
                                }
                            }));
                        }
                    });
                },
                minLength: 1,
                select: function(event,ui) {
                    //console.log("Yep!");
                    $('#receiverCityId').val(ui.item.id);
                }
            });

        },
        showProductInfo: function() {
            var $container = $('.js-cdek-delivery');

            if(!$container.length) {
                console.log('fail');
                return;
            }

            $.ajax({
                type: 'POST',
                url: '/cdek/product/',
                data: {},
                dataType: "json",
                success: function(resp) {
                    cdek.renderCdekDeliveryInfo($container, resp);
                }
            });
        },
        calculator: function() {
            $('.js-cdek-calculator').on('form_success', function(event, resp) {
                var $container = $(this),
                    $result = $container.find('.js-cdek-calculator__result');

                $result.show();
                cdek.renderCdekDeliveryInfo($container, resp);

            })
        },
        renderCdekDeliveryInfo: function($container, resp) {
            var $deliveryDate = $container.find('.js-cdek-delivery-date'),
                $deliveryPrice = $container.find('.js-cdek-delivery-price'),
                $pickupDate = $container.find('.js-cdek-pickup-date'),
                $pickupPrice = $container.find('.js-cdek-pickup-price'),
                deliveryDate = helpers.formatDate(new Date(resp.deliveryDate)),
                pickupDate = helpers.formatDate(new Date(resp.pickupDate));

            if(resp.deliveryPrice) {
                $deliveryPrice.text(' от ' + resp.deliveryPrice + ' ₽');
            }
            else {
                $deliveryPrice.text('x');
            }

            if(resp.deliveryDate) {
                $deliveryDate.text('' + deliveryDate);
            }
            else {
                $deliveryDate.text('x');
            }

            if(resp.pickupPrice) {
                $pickupPrice.text(' от ' + resp.pickupPrice + ' ₽');
            }
            else {
                $pickupPrice.text('x');
            }

            if(resp.pickupDate) {
                $pickupDate.text('' + pickupDate);
            }
            else {
                $pickupDate.text('x');
            }

        },
        mapWidget: function() {
            var $openWidgetLink = $('.js-cdek-open-widget'),
                widjet = new ISDEKWidjet({
                showWarns: true,
                showErrors: true,
                showLogs: true,
                hideMessages: false,
                path: 'https://www.cdek.ru/website/edostavka/template/scripts/',
                servicepath: '/cdek/settings/',
                templatepath: 'https://www.cdek.ru/website/edostavka/template/scripts/template.php',
                choose: true,
                popup: true,
                country: 'Россия',
                defaultCity: $openWidgetLink.data('city'),
                cityFrom: 'Санкт-Петербург',
                link: false,
                hidedress: true,
                hidecash: true,
                hidedelt: false,
                goods: [{
                    length: 10,
                    width: 10,
                    height: 10,
                    weight: 1
                }],
                onChoose: onChoose
            });

            $openWidgetLink.on('click', function(event) {
                event.preventDefault();


                widjet.open();
            });

            function onChoose(wat) {
                var pickupInfo = "Клиент указал пункт самовывоза СДЭК: " + wat.cityName + ', ' + wat.PVZ.Address;
                basket.setOrderDescription(pickupInfo);

                $('.js-cdek-current-pickup').text('(' + wat.cityName + ', ' + wat.PVZ.Address + ')');
                $('.js-cdek-open-widget').text('Поменять');
            }
        }
    }
})();