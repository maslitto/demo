var bodyVeil = (function () {
	var $body = $('body'),
		$html = $('html'),
		$document = $(document),
		$veil = $('.js-body-veil');
	return {
		init: function(){
			$veil.click(function (){
				bodyVeil.close();
			});
			$(document).keyup(function(e) {
				if (e.keyCode === 27) bodyVeil.close();   // esc
			});
		},
		show: function(){
			$body.addClass('veil');
			$html.addClass('veil');
		},
		close: function(){
			$body.removeClass('veil');
			$html.removeClass('veil');
			$document.trigger('veilClose');
		}
	}
})();