var panel = (function () {

	return {
		init: function () {
			panel.closePanel();
			panel.addSpaceForPanel();
			panel.subscribePanelSuccess();
		},
		addSpaceForPanel: function() {
			var $panel = $('.js-panel');

			if($panel.length) {
				$('body').append('<span class="js-panel-space panel__space"></span>');
			}
		},
		removeSpaceForPanel: function() {
			var $spaceForPanel = $('.js-panel-space');

			if($spaceForPanel.length) {
				$spaceForPanel.remove();
			}
		},
		closePanel: function() {
			var $closeLink = $('.js-close-panel');

			$closeLink.on('click', function() {	
				var $panel = $(this).closest('.js-panel');

				if($panel.length) {
					var panelNamePostfix;

					if($panel.attr('data-name')) {
						panelNamePostfix = $panel.data('name');
					}
					else {
						panelNamePostfix = '';
					}

					$panel.remove();
					panel.removeSpaceForPanel();
					$.cookie('hide-js-panel' + panelNamePostfix, 1, {expires: 365, path: "/"});
				}
			});
		},
		subscribePanelSuccess: function() {
			var $subscribeForm = $('.js-subscribe-form'),
				$panelContent  = $('.js-panel-content'),
				successText    = $('.js-subscribe-success').text();

			$subscribeForm.on('form_success', function(event, resp) {
				console.log(successText);
				$panelContent.html('<div class="subscribe-panel__success">' + successText + '</div>');
			});
		}
	}
})();