//TODO empty basket
var basket = (function (){
	var $basket = $('.js-basket');
	var $product = $('.js-product');
	return {
		init: function() {
			if (!$basket.length) {
				return;
			}
			basket.basketInit();
			basket.productCountChange();
			basket.productDelete();

			basket.useBonuses();
			basket.openDeliveryAddress();
			basket.closeCashPayment();

			basket.orderStart();
			basket.orderSuccess();
			basket.applyPromocode();
		},
		// Корзина
		basketInit: function() {
			$.cart.on('render', function() {
				var $body = $('body');
				var finalPriceValue,
					isBonusEnable,
					$wrapper       = $('.js-basket-w'),
					$orderInfoBox     = $('.js-order-info'),
					$productsBox     = $('.js-order-products-box'),
					$orderForm     = $('.js-order-form'),
					$finalPrice = $body.find('.js-order-final-summ'),
					$fullPrice = $body.find('.js-order-summ'),

					$discountBox = $orderInfoBox.find('.js-order-discount-b'),
					$discount = $discountBox.find('.js-order-discount'),

					$deliveryLocal = $orderForm.find('.js-order-del-local').find('.js-price-val'),
					$deliveryRus = $orderForm.find('.js-order-del-rus').find('.js-price-val'),
					$deliveryBox = $('.js-delivery-box'),
					$deliveryInput = $('.js-delivery-input'),

					$bonusBox = $body.find('.js-order-bonuses-b'),
					$bonus = $body.find('.js-order-bonuses'),

					$cashelessPaymentBox   = $orderForm.find('.js-cashless-payment'),
					$bonusInput = $('.js-use-bonuses'),
					$cashelessPaymentInput = $cashelessPaymentBox.find('.js-input');

				if($.cart.isShowBonus === true) {
					$bonusBox.show();
				} else {
					$bonusBox.hide();
					$bonusInput.prop("checked", false);
				}

				isBonusEnable = $bonusInput.prop("checked");

				// Обновление блока с информацией о заказе
				$fullPrice.html($.cart.fullSum.formatNum(0,' '));

				if(!isBonusEnable) {
					finalPriceValue = $.cart.sum;
				} else {
					finalPriceValue = $.cart.sum - $.cart.bonus;
				}

				$finalPrice.html(finalPriceValue.formatNum(0,' '));	
				
				$discount.html($.cart.discount.formatNum(0,' '));
				$bonus.html($.cart.bonus.formatNum(0,' ') + helpers.decOfNum(+$.cart.bonus, [' бонус', ' бонуса', ' бонусов']));



				if  (typeof $.cart.local === 'number') {
					$deliveryLocal.html(helpers.decoratedPrice($.cart.local.formatNum(0,' ')));
				} else {
					$deliveryLocal.html($.cart.local);
				}
				if  (typeof $.cart.rus === 'number') {
					$deliveryRus.html(helpers.decoratedPrice($.cart.rus.formatNum(0,' ')));
				} else {
					$deliveryRus.html($.cart.rus);
				}

				console.log($.cart.isAllowCashlessPayment);

				if($.cart.isAllowCashlessPayment) {
					$cashelessPaymentBox.removeClass('hidden');
				}
				else {
					$cashelessPaymentBox.addClass('hidden');
					$cashelessPaymentInput.prop('checked',false);
				}
				if($.cart.isAllowDelivery) {
					$deliveryBox.removeClass('hidden');
				}
				else {
                    $deliveryBox.addClass('hidden');
                    $deliveryInput.prop('checked',false);
				}

				if($.cart.discount === 0) {
					$discountBox.addClass('hidden');
				} else {
					$discountBox.removeClass('hidden');
				}

				if($.cart.bonus === 0) {
					$bonusBox.addClass('hidden');
					$bonusBox.prop('checked',false)
				} else {
					$bonusBox.removeClass('hidden');
				}

				/*if($.cart.sum === 0 && $.cart.complete === 0) {
					basket.renderEmptyBasket();
				}*/

			});

			(function($product) {

			})();
		},
		renderEmptyBasket: function() {
			var $wrapper       = $('.js-basket-w'),
				$boxEmpty     = $('.js-basket-empty');
			$wrapper.hide();
			$boxEmpty.show();

		},
		// Изменение количества товара в корзине
		productCountChange: function() {
			if (!$product.length) {
				return;
			}
			$product.on('change', '.js-form__number', function() {
				var $input = $(this),
					$product = $input.closest('.js-product');
				$.cart.update({
					id: $product.data('id'),
					count: Number($input.val())
				});
			});
		},
		// Удаление товара из корзины
		productDelete: function() {
			if (!$product.length) {
				return;
			}
			$product.on('click', '.js-del', function(e) {
				e.preventDefault();
				var $el = $(this),
					$product = $el.closest('.js-product'),
					id = $product.data('id');
				$.cart.del(id);
				$product.fadeOut(300, function() {
					$product.remove();
				});

			});
		},
		fastOrder: function() {
			// btn.on('click', function() {
			//
			// 	$.ajax({
			// 		url: '/order/fast-order/',
			// 		method: 'post',
			// 		dataType: 'json',
			// 		data: {
			// 			pid: btn.data('id'),
			// 			count: btn.data('count'),
			// 			phone: phone.val()
			// 		},
			// 		success: function(resp){
			// 			//$.fireTarget('FAST_ORDER');
			// 			foNumber.html('№' + resp.id);
			//
			// 			$(foContainer).css({display: 'none'});
			// 			$(foSuccess).fadeIn(200);
			// 		}
			// 	});
			// });
		},
		openDeliveryAddress: function () {
			var $openBtn = $('.js-open-delivery'),
				$closeBtn = $('.js-close-delivery'),
				$block = $('.js-delivery-address'),
				$input = $block.find('.js-input');
			$openBtn.change(function() {
				if ($(this).prop("checked")) {
					$block.show('hidden');
					$input.addClass('js-required');
				}
			});
			$closeBtn.change(function() {
				if ($(this).prop("checked")) {
					$block.hide('hidden');
					$input.removeClass('js-required');
				}
			});
		},
		closeCashPayment: function() {
			var $openBtn = $('.js-open-cash-payment'),
				$closeBtn = $('.js-close-cash-payment'),

				$cashPaymentBlock = $('.js-cash-payment'),
				$cashPaymentInput = $cashPaymentBlock.find('.js-input'),

				$billPaymentWrapper = $('.js-bill-payment'),
				$billPaymentInput   = $billPaymentWrapper.find('.js-input');
			$openBtn.change(function() {
				if ($(this).prop("checked")) {
					$cashPaymentBlock.show('hidden');
					$cashPaymentInput.addClass('js-required');
				}
			});
			$closeBtn.change(function() {
				if ($(this).prop("checked")) {
					if($cashPaymentInput.prop('checked')) {
						$billPaymentInput.prop('checked', true);
					}
					
					$cashPaymentBlock.hide('hidden');
					$cashPaymentInput.removeClass('js-required');
					$cashPaymentInput.prop('checked',false);
				}
			});
		},
		useBonuses: function () {
			var $checkbox = $('.js-use-bonuses'),
				$finalPriceBox = $('.js-order-final-summ');

			$checkbox.change(function() {
				if ($(this).prop("checked")) {
					$checkbox.prop("checked", true);
					$finalPriceBox.text($.cart.price - $.cart.bonus);

				} else {
					$checkbox.prop("checked", false);
					$finalPriceBox.text($.cart.price);
				}
			});
		},
		orderStart: function() {
			$('.js-order-form').on('form_start', function(event) {
				$(this).addClass('order-form--processing');
			});
		},
		orderSuccess: function () {
			$('.js-order-form').on('form_success', function(event, resp) {
				var $form = $(this),
					successBox = $form.find('.js-form__success'),
					orderIdBox = successBox.find('.js-order-id'),
					paymentBtn = successBox.find('.js-payment-btn');

				if(resp.orderId) {
				    orderIdBox.text(resp.orderId);
                    googleAnalytics.orderPurchase(resp.orderId, resp.summ);
				}

				$.cart.clear();
				basket.renderEmptyBasket();

				/*if(resp.redirect) {
				    paymentBtn.css({display: 'inline-block'}).attr('href', resp.redirect);
				}*/
                if(resp.redirect) {
                    paymentBtn.attr('href',resp.redirect);
                	paymentBtn.show();
                    setTimeout(function() {
                        $(location).attr("href", resp.redirect);
                    }, 2000);
                }
				$(this).removeClass('order-form--processing');
				//basket.postProcessOrder(resp.orderId);
			})
		},
		postProcessOrder: function(orderNumber) {
			$.ajax({
			  type: "POST",
			  url: '/cart/post-process-order/',
			  data: {"orderNumber": orderNumber},
			});
		},
		applyPromocode: function () {
			$('.js-promocode-button').click(function (e) {
				e.preventDefault();
				var promocode = $('.js-promocode-input').val();
				$.cart.update({promocode: promocode});
            })
        }
	}
})();

