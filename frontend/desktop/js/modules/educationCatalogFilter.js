var educationCatalogFilter = (function () {
    var 
        $form = $('.js-education-filter'),
        $dropDown = $('.js-education-filter-dropdown'),
        $dropDownCurrent = $('.js-education-filter-dropdown-current'),
        doIt,
        $catalogResult = $('.js-education-catalog__result'),
        $catalogBlock = $('.js-education-catalog'),
        $catalogBody = $('.js-education-catalog__body'),
        $catalogPages = $('.js-education-filter-page'),
        $filteredCount = $('.js-education-filter-count'),
        $calendar = $('.js-education-filter-calendar'),
        $dateInput = $('.js-education-date-input'),
        $catalogFooter = $('.js-education-catalog__footer'),
        $catalogSearch = $('.js-education-catalog__search-input')
    ;
    return {
        init: function () {
            if (!$form.length) {
                return;
            }
            educationCatalogFilter.dropDown();
            educationCatalogFilter.tags();
            educationCatalogFilter.checkboxes();
            educationCatalogFilter.filterChange();
            educationCatalogFilter.reset();
            educationCatalogFilter.addMoreRequest();
            educationCatalogFilter.pageRequest();
            educationCatalogFilter.submitFilter();
            educationCatalogFilter.toggleFilter();
            educationCatalogFilter.initCalendar();
            educationCatalogFilter.search();

        },
        search: function () {
            $catalogSearch.keyup(function (e) {
                var query = $(this).val();
                if(query.length > 1){
                    yaCounter49316128.reachGoal('ed_search');
                    $.ajax({
                        type: 'GET',
                        url: '/education/search/',
                        data: {query:query},
                        dataType: 'json'

                    }).success(function( response ) {
                        $('.js-education-search__results').html(response.html);
                    });
                } else {
                    $('.js-education-search__results').html('');
                }

            })
        },
        tags: function () {
            $('.js-tag').click(function (e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).toggleClass('tag--active');
                var checkbox =$('#id-checkbox-education-direction-' + $(this).data('id'));
                checkbox.prop("checked", !checkbox.prop("checked"));
                $form.trigger('change');
            })
        },
        checkboxes: function () {
            $('.js-education-direction-checkbox').change(function (e) {
                $('#education-direction-tag-'+$(this).val()).toggleClass('tag--active');
            })
        },
        submitFilter: function() {
            $form.submit(function(e) {
                e.preventDefault();
            });
        },
        initCalendar: function() {
            $.date = function(dateObject) {
                var d = new Date(dateObject);
                var day = d.getDate();
                var month = d.getMonth() + 1;
                var year = d.getFullYear();
                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }
                var date = year + "-" + month + "-" + day;

                return date;
            };
            var now = new Date();
            var educationDates = $calendar.data('education-dates');
            pickmeup.defaults.locales['ru'] = {
                days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
            };
            var calendar = document.getElementById("education-calendar");
            pickmeup(calendar,{
                flat : true,
                min : now,
                format: 'Y-m-d',
                locale: 'ru',
                prev: '<i class="icon-arrow-prev"></i>',
                next: '<i class="icon-arrow-next"></i>',
                render : function (date) {
                    var strDate = $.date(date);
                    var position = $.inArray(strDate, educationDates);
                    if (position >= 0) {
                        console.log(strDate);
                        return {class_name : 'has-education'};
                    } else{
                        return {};
                    }
                }
            });
            calendar.addEventListener('pickmeup-change', function (e) {
                //console.log(e.detail.formatted_date); // New date according to current format
                //console.log(e.detail.date);           // New date as Date object
                $dateInput.val(e.detail.formatted_date);
                $form.trigger('change');
            })

        },

        filterChange: function() {
            $form.on('change', function(){
                clearTimeout(doIt);
                doIt = setTimeout(function(){
                    educationCatalogFilter.requestItems(educationCatalogFilter.collectData(), 'replace');
                }, 800);
            });
        },
        dropDown: function() {
            var $dropDownVariants = $dropDown.find('.js-education-filter-dropdown-variant');
            $dropDownVariants.click(function(e){
                e.preventDefault();
                var $val = $(this).data('val'),
                    $block = $(this).closest('.js-education-filter-dropdown');
                $block.find('.js-education-filter-dropdown-current').data('val', $val).text($(this).text());
                $dropDownVariants.show();
                $(this).hide();
                $form.trigger('change');
            });
        },
        collectData: function (page) {
            var data = $form.serializeArray();

            // Страница
            if (page) {
                data.push({
                    name: 'page',
                    value: page
                });
            }

            return data;
        },
        requestItems: function(data, action) {
            $.ajax({
                type: 'GET',
                url: '/education/',
                data: data,
                dataType: 'json'

            }).success(function( resp ) {
                if (resp.html) {
                    educationCatalogFilter.updateList(resp.html, action);

                    const urlParams = new URLSearchParams(window.location.search);
                    const query = urlParams.get('query');
                    console.log(query);
                    var pageUrl = window.location.pathname+'?' + 'page=' + resp.page;
                    if(query.length>0){
                        pageUrl = pageUrl + '&query='+query;
                    }
                    window.history.pushState('', '', pageUrl);
                }
                if (resp.count) {
                    $filteredCount.text(resp.count)
                }
                if (resp.page) {
                    $catalogPages
                        .removeClass('active')
                        .filter('[data-page="' + resp.page + '"]')
                        .addClass('active');
                    $('.js-education-filter-add').attr('data-next', Number(resp.page) + 1);
                }
            });
        },
        updateList: function(data, action) {
            var $items = $(data);
            var old_scroll = $(window).scrollTop();
            $('.js-education-catalog__footer').remove();

            if (action === 'add') {
                $items.appendTo($catalogResult).hide();
                $items.fadeIn(300);
                $(window).scrollTop(old_scroll);
            } else if (action === 'replace') {
                console.log('replace-flag');
                $('html').animate({scrollTop: $catalogBody.offset().top - 143 }, 400);
                $catalogResult.fadeOut(300, function() {
                    $catalogResult.html(data).fadeIn(300)
                });

            }
            $(window).trigger('update-impressions');
        },
        reset: function() {
            $('.js-education-filter-reset').click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                $form.trigger('reset');
                $form.trigger('change');
            });
        },
        addMoreRequest: function() {
            $catalogBlock.on('click', '.js-education-filter-add', function(e) {
                var page = $(this).data('next');
                e.preventDefault();
                e.stopPropagation();
                educationCatalogFilter.requestItems(educationCatalogFilter.collectData(page), 'add');
            });
        },
        pageRequest: function() {
            $catalogBlock.on('click', '.js-education-filter-page', function(e) {
                var page = $(this).data('page');
                e.preventDefault();
                e.stopPropagation();
                $catalogPages.removeClass('active');
                $(this).addClass('active');
                educationCatalogFilter.requestItems(educationCatalogFilter.collectData(page), 'replace');
            });
        },
        toggleFilter: function() {
            $('.js-education-filter-toggle').each(function() {
                var $toggleFilter  = $(this),
                    $filterContent = $toggleFilter.find('.js-education-filter-content'),
                    $toggleHandler = $toggleFilter.find('.js-education-filter-toggle-handler');

                $toggleHandler.click(function() {
                    $filterContent.animate({height: 'toggle'});
                    if($toggleFilter.hasClass('open')) {
                        $toggleFilter.removeClass('open');
                    }
                    else {
                        $toggleFilter.addClass('open');
                    }
                })
            });
        }
    }
})();