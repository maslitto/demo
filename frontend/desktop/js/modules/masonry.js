var masonry = (function () {
	var $grid = $('.js-grid');
	var options = {
		itemSelector: '.card',
	};
	return {
		init: function () {
			if($grid.length) {
				$grid.masonry(options);
			}
		}
	}
})();