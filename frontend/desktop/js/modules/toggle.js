var toggle = (function () {
	return {
		init: function() {
			toggle.expandBlock();
		},
		expandBlock: function() {
  			var $links = $(".js-toggle-link");


			if ($links.length) {

				$links.on('click', function(event) {
					var $current = $(this),
						type	 = $current.data('type'),
						$elem,
						$arrow;

					if($(event.target).hasClass('js-toggle-exception')) {
						return;
					}

					if('arrow' === type) {
						$current = $current.parent();
					}

					$elem  = $current.next('.js-toggle-body');
					$arrow = $current.find('.js-toggle-arr');

					$current.removeClass('active')
					$elem.slideToggle(400);

					if($arrow.length) {
						if($arrow.hasClass('arr-d-after')) {
							$arrow.removeClass('arr-d-after').addClass('arr-up-after');
						}
						else if($arrow.hasClass('arr-up-after')) {
							$arrow.removeClass('arr-up-after').addClass('arr-d-after');
						}
					}
				});
			}
		},
	}
})();