var sliders = (function () {
    return {
        init: function () {
            //Slick Sliders
            sliders.simpleSlider();
            sliders.mainPageSlider();
            sliders.cardImgSlider();
            //Block slider
            sliders.blockSlides();

        },
        simpleSlider: function() {
            var $slider = $('.js-simple-slider');
            if(!$slider.length){
                return;
            }
            /*$slider.on('init', function(event, slick){
                if(null != slick.$dots) {
                    $items = slick.$dots.find('li');
                    $items.addClass('slider-main__dots');
                    $items.find('button').remove();
                }
                sliders.setNavColor($sliderWrap, navColor);
            });*/

            $slider.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 10000,
                dots: false,
                infinite: true
            });

            /*$slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                var $nextSlideWrap = $slider.find('[data-slick-index="' + nextSlide + '"]'),
                    $nextSlide     = $nextSlideWrap.find('.slider-main__slide'),
                    bgColor        = $nextSlide.data('color'),
                    bgImage        = $nextSlide.data('img'),
                    bgStyles       = $nextSlide.data('bgstyles');

                sliders.setSliderBackground($sliderWrap, bgColor, bgImage, bgStyles);
            });

            $slider.on('afterChange', function(event, slick, currentSlide) {
                var $currentSlideWrap = $slider.find('[data-slick-index="' + currentSlide + '"]'),
                    $currentSlide = $currentSlideWrap.find('.slider-main__slide'),
                    navColor = $currentSlide.data('navcolor') ? $currentSlide.data('navcolor') : 'ffffff';

                sliders.setNavColor($sliderWrap, navColor);
            });*/


        },
        mainPageSlider: function() {
            var $sliderWrap = $('.js-slider-main-wrap'),
                $slider 	= $sliderWrap.find('.js-slider-main');
            if(!$slider.length){
                return;
            }

            $slider.on('init', function(event, slick){
                var $currentSlideWrap = $slider.find('[data-slick-index="0"]'),
                    $currentSlide = $currentSlideWrap.find('.slider-main__slide'),
                    navColor = $currentSlide.data('navcolor') ? $currentSlide.data('navcolor') : 'ffffff',
                    bgStyles = $currentSlide.data('bgstyles'),
                    $items;

                if(null != slick.$dots) {
                    $items = slick.$dots.find('li');

                    $items.addClass('slider-main__dots');
                    $items.find('button').remove();
                }
                sliders.setNavColor($sliderWrap, navColor);
                $sliderWrap.attr("style", $slider.attr("style") + "; " + bgStyles);
            });

            $slider.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 10000,
                dots: true,
                infinite: true,
            })
                .delay(500)
                .queue(function (next) {
                    $(this).css('opacity', '1','transition','0.2s');
                    next();
                });

            $slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                var $nextSlideWrap = $slider.find('[data-slick-index="' + nextSlide + '"]'),
                    $nextSlide     = $nextSlideWrap.find('.slider-main__slide'),
                    bgColor        = $nextSlide.data('color'),
                    bgImage        = $nextSlide.data('img'),
                    bgStyles       = $nextSlide.data('bgstyles');

                sliders.setSliderBackground($sliderWrap, bgColor, bgImage, bgStyles);
            });

            $slider.on('afterChange', function(event, slick, currentSlide) {
                var $currentSlideWrap = $slider.find('[data-slick-index="' + currentSlide + '"]'),
                    $currentSlide = $currentSlideWrap.find('.slider-main__slide'),
                    navColor = $currentSlide.data('navcolor') ? $currentSlide.data('navcolor') : 'ffffff';

                sliders.setNavColor($sliderWrap, navColor);
            });


        },
        setNavColor: function($slider, color) {
            var $arrows = $slider.find('.slick-arrow'),
                $dots = $slider.find('.slider-main__dots'),
                $activeDot = $slider.find('.slider-main__dots.slick-active');

            $arrows.css('background-color', "#" + color);
            $dots.css('border', "2px solid #" + color);
            $dots.css('background', "none");
            $activeDot.css('background', "#" + color);

        },
        setSliderBackground: function($slider, color, image, styles) {
            $slider.removeAttr('style');

            if(!image && !styles) {
                $slider.css({backgroundColor: '#' + color});
            }
            else if(!styles) {
                $slider.css({backgroundImage: 'url("'+ image +'")'});
            }
            else {
                $slider.attr("style", $slider.attr("style") + "; " + styles);
            }
        },
        cardImgSlider: function() {
            var $slider = $('.js-product__slider');
            var $slides;
            if(!$slider.length){
                return;
            }
            $slides = $slider.find('.product__slider-image');
            $slider.slick({
                dots: true,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                fade: true,
                customPaging: function ($slider, i) {
                    var source = $slides.eq(i).data('thumb');
                    return '<img src="' + source + '">';
                }
            });
        },
        blockSlides: function() {
            var $slideNextLink = $('.js-slide-link');
            if (!$slideNextLink.length) {
                return;
            }
            $slideNextLink.click(function(e){
                var target = $(this).attr('href'),
                    $slide = $(this).closest('.js-slide-block');
                e.preventDefault();
                e.stopPropagation();
                $slide.removeClass('active');
                if(target !== '#') {
                    target = target.replace('#','');
                    $('.js-slide-block[data-slide-name=' + target + ']').addClass('active');
                }
                // else {
                // 	$slide.next('.js-slide-block').addClass('active');
                // }
            });
        }
    }
})();