var cropper = (function () {
	var $container = $('.js-cropper'),

    $img = $container.find('.js-cropper-canvas'),
    $url = $img.attr('src'),

    $photoWrapper = $container.find('.js-photo-wrapper'),

    $photoForm = $container.find('.js-form'),
    $photoSrc = $photoForm.find('.js-photo-src'),
    $photoData = $photoForm.find('.js-photo-data'),
    $upload = $('.js-cropper-upload'),
    $photoInput = $container.find('.js-cropper-input'),
    $button = $('.js-cropper-button'),
    $buttons = $('.js-cropper-buttons')
	;
    var $support = {
            fileList: !!$('<input type="file">').prop('files'),
            blobURLs: !!window.URL && URL.createObjectURL,
            formData: !!window.FormData
    };
	return {
        init: function () {
            cropper.active = true;
            $support.datauri = $support.fileList && $support.blobURLs;

            cropper.addListener();

        },

        addListener: function () {
            $upload.click(function (e) {
                e.preventDefault();
                $photoInput.trigger('click');
                $buttons.show();
            });
            $photoInput.change(function (e) {
                $('.js-cropper-file-label').text($(this).val());
                $('.js-cropper-file-name-block').show();
                cropper.change();
            });
            $button.click(function (e) {
                e.preventDefault();
                cropper.zoom(e);
            });

        },

        change: function () {
            var files;
            var file;
            if ($support.datauri) {
                files = $photoInput.prop('files');

                if (files.length > 0) {
                    file = files[0];
                    console.log(file);
                    if (this.isImageFile(file)) {
                        if ($url) {
                            URL.revokeObjectURL($url); // Revoke the old one
                        }

                        $url = URL.createObjectURL(file);
                        console.log($url);
                        this.startCropper();
                    }
                }
            } else {
                file = $photoInput.val();

                if (this.isImageFile(file)) {
                    this.syncUpload();
                }
            }
        },

        submit: function () {
            if (!$photoSrc.val() && !$photoInput.val()) {
                return false;
            }

            if (support.formData) {
                ajaxUpload();
                return false;
            }
        },

        zoom: function (e) {
            var data;

            data = $(e.target).data();

            if (data.method) {
                $img.cropper(data.method, data.option);
            }
            return false;
        },

        isImageFile: function (file) {
            if (file.type) {
                return /^image\/\w+$/.test(file.type);
            } else {
                return /\.(jpg|jpeg|png|gif)$/.test(file);
            }
        },

        startCropper: function () {

            /*if (cropper.active) {
                $img.cropper('replace', $url);

            } else {*/
                $img = $('<img src="' + $url + '">');
                $photoWrapper.empty().html($img);
                $img.cropper({
                    aspectRatio: 1/1,
                    minCropBoxWidth: 250,
                    minCropBoxHeight: 250,
                    maxCropBoxHeight: 1000,
                    crop: function (e) {
                        var json = [
                            '{"x":' + e.detail.x,
                            '"y":' + e.detail.y,
                            '"height":' + e.detail.height,
                            '"width":' + e.detail.width,
                            '"rotate":' + e.detail.rotate + '}'
                        ].join();

                        $photoData.val(json);
                    }
                });
                console.log($photoData.val());
                cropper.active = true;
            //}


        },

        stopCropper: function () {
            if (cropper.active) {
                $img.cropper('destroy');
                $img.remove();
                cropper.active = false;
            }
        }

    };

})();
