var citySwitch = (function (){
    var $citySwitch = $('.js-city-switch'),
        isOpen = false,
        $step2 = $citySwitch.find('.js-city-switch-step2'),
        $openHandler = $('.js-city-switch-open'),
        $openHandlerStep2 = $('.js-city-switch-open-step2'),
        $closeHandler = $('.js-city-switch-close');
    return {
        init: function() {
            citySwitch.containerHandle();
            citySwitch.switchItemsHandle();
            citySwitch.resetCachedItemsCheck();
            citySwitch.weAskedCity();
        },
        open: function() {
            isOpen = true;
            $citySwitch.addClass('active');
        },
        close: function() {
            isOpen = false;
            $citySwitch.removeClass('active');
        },
        isOpen: function() {
            return isOpen;
        },
        containerHandle: function() {
            $step2.hide();
            if ($citySwitch.length &&
                $citySwitch.hasClass('enabled')) {
                citySwitch.open();
                bodyVeil.show(true);
            }

            $(document).on('click', function(e) {

                if (!$citySwitch.is(e.target) &&
                    $citySwitch.has(e.target).length === 0 &&
                    !$openHandler.is(e.target) &&
                    $openHandler.has(e.target).length === 0) {

                    citySwitch.close();
                }
            });

            $closeHandler.on('click', function() {
                citySwitch.setCookie('switch-city','yes', 12);
                citySwitch.close();
                bodyVeil.close();

            });

            $openHandler.on('click', function() {
                citySwitch.open();
                //$step2.show();
                //citySwitch.openSecondStep();
                citySwitch.open();
                bodyVeil.show(true);
            });
            $openHandlerStep2.on('click', function() {
                $step2.show();
                bodyVeil.show(true);
            });
        },
        openSecondStep: function() {
            //$step1.hide();
            $step2.show();
        },
        switchItemsHandle: function() {
            var $cityLink      = $('.js-switch-city-handler.first-step'),
                $finalCityLink = $('.js-switch-city-handler.final');

            $cityLink.click(function(){
                var cityId = $(this).data('id');
                if(0 == $.cart.count) {
                    citySwitch.proceed(cityId);
                }
                else {
                    $finalCityLink.data('id', cityId);

                    $.magnificPopup.open({
                        items: {
                            src: $(this).data('notification')
                        },
                        type: 'inline',
                        callbacks: {
                            close: function() {
                                bodyVeil.close();
                                citySwitch.resetCachedItemsCheck();
                            }
                        }
                    });
                }
            });

            $finalCityLink.click(function() {
                var cityId = $(this).data('id');
                citySwitch.proceed(cityId);
            });
        },
        proceed: function(cityId) {
            $.ajax({
                type: 'POST',
                url: '/version/set/',
                data: {
                    store: cityId
                },
                dataType: "json",
                success: function(resp) {
                    if(!resp.hasOwnProperty('error') &&
                        resp.hasOwnProperty('action')) {

                        var pathname = window.location.pathname,
                            redirectUrl = resp.domain + pathname;

                        switch(resp.action) {
                            case 'reload':
                                if(resp.hasOwnProperty('domain')) {
                                    window.location.href = redirectUrl;
                                }
                                else {
                                    location.reload();
                                }
                                break;
                            case 'redirect':
                                if(resp.hasOwnProperty('domain')) {

                                    if(resp.hasOwnProperty('keyParam')) {
                                        redirectUrl = redirectUrl + '?' + resp.keyParam;
                                    }
                                    window.location.href = redirectUrl;
                                }
                                break;
                        }
                    }
                    else {
                        console.log('version error: ' + resp.error);
                    }
                }
            });
        },
        resetCachedItemsCheck: function() {
            var $switchItems = $step2.find('input[name="city"]');

            $switchItems.each(function() {
                if($(this).hasClass('checked')) {
                    $(this).prop('checked', true);
                }
                else {
                    $(this).prop('checked', false);
                }
            });
        },
        setCookie: function (name, value, hours) {
            var expires = "";
            if (hours) {
                var date = new Date();
                date.setTime(date.getTime() + (hours * 60 * 60 * 1000));
                expires = "; expires=" + date.toUTCString();
            }
            document.cookie = name + "=" + (value || "")  + expires + "; path=.stommarket.ru";
        },
        getCookie: function (name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
            }
            return null;
        },
        weAskedCity: function () {
            if($.cookie('weAskedCity') === null){
                $.cookie("weAskedCity", 1, {expires : 1, path: '/' });
            } else {
                //sleep
            }

        }
    }
})();
