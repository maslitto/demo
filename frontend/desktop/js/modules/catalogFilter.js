var catalogFilter = (function () {
    var priceSlider =  document.getElementById('js-price-slider'),
        $priceInput = $('.js-price-input'),
        $form = $('.js-filter'),
        $dropDown = $('.js-filter-dropdown'),
        $dropDownCurrent = $('.js-filter-dropdown-current'),
        doIt,
        //$catalogList = $('.js-catalog__cards'),
        $catalogResult = $('.js-catalog__result'),
        $catalogBlock = $('.js-catalog'),
        $catalogBody = $('.js-catalog__body'),
        $catalogPages = $('.js-filter-page'),
        $filteredCount = $('.js-filter-count'),
        $changeCityCurrent = $('.js-change-city-current'),
        $changeCity = $('.js-change-city');
    return {
        init: function () {
            if (!$form.length) {
                return;
            }
            catalogFilter.rangeSlider();
            catalogFilter.cityChange();
            catalogFilter.dropDown();
            catalogFilter.filterChange();
            catalogFilter.reset();
            catalogFilter.addMoreRequest();
            catalogFilter.pageRequest();
            catalogFilter.submitFilter();
            catalogFilter.toggleFilter();

        },
        submitFilter: function() {
            $form.submit(function(e) {
                e.preventDefault();
            });
        },
        filterChange: function() {
            $form.on('change', function(){
                clearTimeout(doIt);
                doIt = setTimeout(function(){
                    catalogFilter.requestItems(catalogFilter.collectData(), 'replace');
                }, 800);
            });
        },
        rangeSlider: function () {
            if(!priceSlider) {
                return;
            }

            var maxValue =  +priceSlider.getAttribute('data-max-price'),
                minValue =  +priceSlider.getAttribute('data-min-price'),
                startValue =  +priceSlider.getAttribute('data-start-price');

            noUiSlider.create(priceSlider, {
                start: startValue,
                step: 1,
                range: {
                    'min': minValue,
                    'max': maxValue
                }
            });
            priceSlider.noUiSlider.on('update', function(values){
                $priceInput.val((+values[0]).formatNum(0, ' '))
            });
            priceSlider.noUiSlider.on('change', function(){
                $form.trigger('change');
            });
            $priceInput.on('click', function(){
                $(this).val('');
            });
            $priceInput.on('change', function(){
                var value = $(this).val(),
                    valueNumber = +(value.replace(/\D+/g, '')),
                    valueToSet = 0;
                if (valueNumber > maxValue) {
                    valueToSet = maxValue;
                } else if (valueNumber < minValue) {
                    valueToSet = minValue;
                } else {
                    valueToSet = valueNumber;
                }
                priceSlider.noUiSlider.set(valueToSet);
                $(this).val(valueToSet.formatNum(0, ' '));
            });
        },
        cityChange: function() {
            $changeCity.on('click', function(e){
                e.preventDefault();
                var $id = $(this).data('id');
                $changeCityCurrent.data('id', $id).text($(this).text());
                //$(this).hide();
                $form.trigger('change');
            });
        },
        dropDown: function() {
            var $dropDownVariants = $dropDown.find('.js-filter-dropdown-variant');
            $dropDownVariants.click(function(e){
                e.preventDefault();
                var $val = $(this).data('val'),
                    $block = $(this).closest('.js-filter-dropdown');
                $block.find('.js-filter-dropdown-current').data('val', $val).text($(this).text());
                $dropDownVariants.show();
                $(this).hide();
                $form.trigger('change');
            });
        },
        collectData: function (page) {
            var data = $form.serializeArray();

            // Страница
            if (page) {
                data.push({
                    name: 'page',
                    value: page
                });
            }
            // Сортирвовка и город
            $dropDownCurrent.each(function(){
                data.push({
                    name: $(this).data('name'),
                    value: $(this).data('val')
                });
            });
            // Сортирвовка и город
            $changeCityCurrent.each(function(){
                data.push({
                    name: 'versionId',
                    value: $(this).data('id')
                });
            });

            if(priceSlider) {
                // Цена
                var priceVal = '0;' + (+priceSlider.noUiSlider.get()).toFixed(0);
                data.push({
                    name: 'minMax',
                    value: priceVal
                });
            }

            return data;
        },
        requestItems: function(data, action) {
            $.ajax({
                type: 'GET',
                url: '/catalog/',
                data: data,
                dataType: 'json'

            }).success(function( resp ) {
                if (resp.html) {
                    catalogFilter.updateList(resp.html, action);

                    var pageUrl = window.location.href.split('?')[0] + '?' + 'page=' + resp.page;
                    window.history.pushState('', '', pageUrl);
                }
                if (resp.count) {
                    $filteredCount.text(resp.count)
                }
                if (resp.page) {
                    $catalogPages
                        .removeClass('active')
                        .filter('[data-page="' + resp.page + '"]')
                        .addClass('active');
                    $('.js-filter-add').attr('data-next', Number(resp.page) + 1);
                }
            });
        },
        updateList: function(data, action) {
            var $items = $(data);
            var old_scroll = $(window).scrollTop();
            $('.js-catalog__footer').remove();

            if (action === 'add') {
                $items.appendTo($catalogResult).hide();
                $items.fadeIn(300);
                $(window).scrollTop(old_scroll);
            } else if (action === 'replace') {
                $('html').animate({scrollTop: $catalogBody.offset().top - 143 }, 400);
                $catalogResult.fadeOut(300, function() {
                    $catalogResult.html($items).fadeIn(300)
                });

            }
            $(window).trigger('update-impressions');
        },
        reset: function() {
            $('.js-filter-reset').click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                $form.trigger('reset');
                $form.trigger('change');
                priceSlider.noUiSlider.set(priceSlider.dataset.maxPrice);
            });
        },
        addMoreRequest: function() {
            $catalogBlock.on('click', '.js-filter-add', function(e) {
                var page = $(this).data('next');
                e.preventDefault();
                e.stopPropagation();
                catalogFilter.requestItems(catalogFilter.collectData(page), 'add');
            });
        },
        pageRequest: function() {
            $catalogBlock.on('click', '.js-filter-page', function(e) {
                var page = $(this).data('page');
                e.preventDefault();
                e.stopPropagation();
                $catalogPages.removeClass('active');
                $(this).addClass('active');
                catalogFilter.requestItems(catalogFilter.collectData(page), 'replace');
            });
        },
        toggleFilter: function() {
            $('.js-filter-toggle').each(function() {
                var $toggleFilter  = $(this),
                    $filterContent = $toggleFilter.find('.js-filter-content'),
                    $toggleHandler = $toggleFilter.find('.js-filter-toggle-handler');

                $toggleHandler.click(function() {
                    $filterContent.animate({height: 'toggle'});
                    if($toggleFilter.hasClass('open')) {
                        $toggleFilter.removeClass('open');
                    }
                    else {
                        $toggleFilter.addClass('open');
                    }
                })
            });
        }
    }
})();