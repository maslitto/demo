var search = (function () {
	var $block 		   = $('.js-h-search__results'),
		$blockSections = $('.js-h-search__results-b'),
		$brands 	   = $('.js__results-brands'),
		$catalog 	   = $('.js__results-catalog'),
		$tags 		   = $('.js__results-tags'),
		$products 	   = $('.js__results-products'),
		$searchInput   = $('.js-h-search__input'),
		$headerLine    = $('.js-header__line2'),
		$searchBtn     = $('.js-h-search__btn');
	return {
		init: function () {
			search.handleSwitchInput();
			search.renderSearchBlock();
		},
		renderSearchBlock: function() {
			$searchInput.on('input', function() {
				var val = $(this).val();
				if (val.length < 2) {
					$block.hide();
					return;
				} else {
					$block.fadeIn('fast');
				}
				$.ajax({
					url: '/search/',
					type: 'GET',
					dataType: 'json',
					data: {
						query: val
					},
					success: function(data) {

						var yandexMetrikaId = data.yandexMetrikaId,
							tags = data.tags,//JSON.parse(data.tags.replace(/&quot;/g,'"')),
							products = data.products,
							brands = JSON.parse(data.brands.replace(/&quot;/g,'"')),
							catalogs = JSON.parse(data.catalogs.replace(/&quot;/g,'"'));
						search.clearAll();
						/*START TAGS*/
						if(tags.length > 0){
                            $.each(tags, function( index, item ) {
                                $tags.show();
                                if (item.active) {
                                	$tags.append('<a href="/catalog/'+item.url+'" class="tag tag--small">#'+item.name+'</a>');
                                }
							})
						}
						/*END OF TAGS*/

						/*START BRANDS*/
                        if(brands.length > 0){
                            $.each(brands, function( index, item ) {
                                var $element = $('<a></a>');
                                $brands.show();
                                $element
                                    .text(item.name)
                                    .attr('href','/catalog/brands/' + item.url)
                                    .data('name', item.name)
                                    .addClass('h-search__item-link')
                                    .addClass('h-search__item-switch')
                                    .addClass('js__item-switch');
                                if (item.hide) {
                                    $element.hide();
                                    $element.addClass('additional');
                                }
                                $brands.append($element);
                            })
                        }
                        /*END OF BRANDS*/

                        /*START CATALOGS*/
                        if(catalogs.length > 0){
                            $.each(catalogs, function( index, item ) {
								var $element = $('<a></a>');
								$catalog.show();
								$element
									.text(item.name)
									.attr('href','/catalog/' + item.urlPath)
									.data('name', item.name)
									.addClass('h-search__item-link')
									.addClass('h-search__item-switch')
									.addClass('js__item-switch');
								if (item.hide) {
									$element.hide();
									$element.addClass('additional');
								}
								$catalog.append($element);
                            })
                        }
                        /*END OF CATALOGS*/

                        /*START PRODUCTS*/
                        if(products.length > 0){
                            $.each(products, function( index, item ) {
                                var $element = $('<a></a>');
                                $products.show();
                                var $info = $('<div class="h-search__item-info"></div>');

                                $info.append('<span class="h-search__item-price">' + item.price + ' ₽ </span>');

                                if(Number(item.count) > 0) {
                                    $info.append('<span class="h-search__item-availability h-search__item-availability--available">В наличии</span>');
                                }
                                else {
                                    $info.append('<span class="h-search__item-availability h-search__item-availability--not-available">Нет в наличии</span>');
                                }

                                var $cartBtn = $('<a href="#" class="h-search__cart card__btn icon-basket js-cart-add js-ga-add-to-cart" data-id="'+ item.id +'" data-count="1" onclick="yaCounter' + yandexMetrikaId + '.reachGoal(\'KORZINAMINI\');"></a>')

                                $element
                                    .attr('href',item.url)
                                    .addClass('h-search__item')
                                    .addClass('h-search__item-switch')
                                    .addClass('js__item-switch')
                                    .addClass('js-ga-impression')
                                    .addClass('js-ga-proceed')
                                    .addClass('js-ga-search-item')
                                    .addClass('js-ga-product-card')
                                    .data('name', item.name)
                                    //.data('ga-props', jQuery.parseJSON(item.jsonAnalyticsData))
                                    .append('<div class="h-search__item-img"><img src="' + item.image + '" alt=""></div>')
                                    .append('<div class="h-search__item-name">' + item.name + '</div>')
                                    .append($info)

                                if('' != item.price) {
                                    $element.append($cartBtn);
                                }

                                if (item.hide) {
                                    $element.hide();
                                }
                                $products.append($element);
                            })
                        }
						/*END OF PRODUCTS*/

						$(window).trigger('update-impressions');
					}
				})
			});
			$(document).on('click', function handler(e) {
				if (!$(e.target).closest('.js-h-search').length) {
					$block.hide();
				}

				if (!$(e.target).closest('.js-header__line2').length) {
					$('.js-header__line2').removeClass('header__line2--active');
					$('.js-h-search').removeClass('h-search--active').removeClass('active');
					$searchInput.attr("placeholder", "Начните поиск здесь");
				}
			});

			$block.on('click', '.h-search__show-all span', function(){
				var $block = $(this).closest('.js-h-search__results-b');
				//$blockSections.hide();
				$block
					.show()
					.children()
					.show();


				$(this)
					.closest('.h-search__show-all')
					.hide();

				$block.find('.h-search__hide-all').css('display', 'block');
			});

			$block.on('click', '.h-search__hide-all span', function(){
				$block.find('.additional').hide();
				
				$(this)
					.closest('.h-search__hide-all')
					.hide();
				$block.find('.h-search__show-all').show();

			});

			$headerLine.on('click', '.js-h-search', function() {
				var $searchBlock = $(this),
					$parent      = $searchBlock.closest('.js-header__line2'),
					$searchInput = $parent.find('input.js-h-search__input');

				$parent.addClass('header__line2--active');
				$searchBlock.addClass('h-search--active').addClass('active');
				$searchInput.focus();
				$searchInput.attr("placeholder", "");
			});

			$headerLine.on('click', '.js-h-search__btn, .js-h-search__dub-btn', function() {
				var $searchBox = $(this).closest('.js-h-search');

				if($searchBox.hasClass('active')) {
					$searchBox.submit();
				}
			});
		},
		clearAll: function() {
			$brands.empty().hide();
			$catalog.empty().hide();
			$products.empty().hide();
			$tags.empty().hide();
		},
		handleSwitchInput: function() {
			$(document).on('keydown',function(e) {
		        var code = (e.keyCode ? e.keyCode : e.which),
		        	$prevItem,
		        	$nextItem;
		        switch (code){
		          //up
		          case 38:
		            //console.log("Up pressed");
		            $nextItem = search.getPrevSwitchItem();
		            search.renderSwitchItem($nextItem);
		            $searchInput.val($nextItem.data('name'));
		            break;
		          //down
		          case 40:
		            //console.log("Down pressed");
		            $prevItem = search.getNextSwitchItem();
		            search.renderSwitchItem($prevItem);
		            $searchInput.val($prevItem.data('name'));
		            break;
		        }
		    });

		    $(document).on('mousemove', '.js__item-switch',  function(e) {
		    	var $item = $(this);
		    	search.renderSwitchItem($item);
		    });

		},
		renderSwitchItem: function($item) {
			if($block.length && $block.is(':visible') && $item.length) {
				search.clearSelectSwtichItems();
				$item.addClass('active');

				var $container      = $block.find('.h-search__results-scroll'),
					containerScroll = $container.scrollTop(),
					itemPosition    = $item.position().top + containerScroll;

				if (!($item.position().top > 0)) {
					$container.scrollTop(itemPosition);
				}
				else if(!($item.position().top + $item.height() < $block.height())) {
					$container.scrollTop(
						itemPosition + $item.outerHeight() - $container.outerHeight()
					);
				}
				
			}
			else {
				console.log('search block closed');
			}

		},
		getCurrentSwitchItem: function() {
			return $block.find('.js__item-switch.active');
		},
		getPrevSwitchItem: function() {
			var $currentItem = search.getCurrentSwitchItem(),
				$prevItem    = $currentItem.prev('.js__item-switch:visible'),
				$prevBlock   = $currentItem.closest('.js__block-switch:visible').prevAll('.js__block-switch:visible:first');

			if(!$prevItem.length) {
				if($prevBlock.length) {
					//console.log('exists');
					$prevItem = $prevBlock.find('.js__item-switch:visible:last');
				}
				else {
					//console.log('not exists');
					$prevItem = $block.find('.js__item-switch:visible:last');
				}
			}

			return $prevItem;
		},
		getNextSwitchItem: function() {
			var $currentItem = search.getCurrentSwitchItem(),
				$nextItem    = $currentItem.next('.js__item-switch:visible'),
				$nextBlock   = $currentItem.closest('.js__block-switch:visible').nextAll('.js__block-switch:visible:first');

			if(!$nextItem.length) {
				if($nextBlock.length) {
					//console.log('exists');
					$nextItem = $nextBlock.find('.js__item-switch:first:visible');
				}
				else {
					//console.log('not exists');
					$nextItem = $block.find('.js__item-switch:first:visible');
				}
			}

			return $nextItem;
		},
		clearSelectSwtichItems: function() {
			$block.find('.js__item-switch').removeClass('active');
		},
	}
})();