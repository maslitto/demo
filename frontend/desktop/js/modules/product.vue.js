var productVue = (function () {
    return {
        init: function () {
            productVue.product();
            productVue.showBlock();
            productVue.stopSovetnik();
        },
        product: function () {
            return new Vue({
                el: '#product-vue',
                data:  typeof vueInject === 'undefined' ? {} : vueInject,
                delimiters: ['${', '}'],
                /*beforeMount: function() {
                    //console.log(this.$el.attributes['data-vue-inject'].value);
                    this.data = JSON.parse(this.$el.attributes['data-vue-inject'].value);
                },*/
                computed: {
                    remain: function () {
                        var remain = this.product.count - this.currentCount;
                        if(remain < 0) {remain = 0;}
                        return remain;
                    }
                },
                methods: {
                    formatPrice: function(value) {
                        return value;
                        //return number_format(value, 0, '.', '');
                    },
                    setCurrentCount: function(operation) {
                        if(operation == 'plus'){
                            this.currentCount += 1;
                        } else if(operation == 'minus'){
                            this.currentCount -= 1;;
                        }

                        if(this.currentCount < 1){
                            this.currentCount = 1;
                        }
                        if((this.currentCount > this.product.count) && (this.product.count > 0)){
                            this.currentCount = this.product.count;
                        }
                        if(this.product.count == 0){
                            this.currentCount = 1;
                        }
                        return this.currentCount;
                    },
                    addToCart: function(productId, count){
                        this.inCart = true;
                        var data = {
                            id:    productId,
                            count: count
                        };
                        console.log(data);
                        var ret = $.cart.add(data);
                        //yaCounter23244307.reachGoal('KORZINA');
                        return false;
                    }
                }
            })
        },
        showBlock: function () {
            $('.js-product--detail').show();
        },
        stopSovetnik: function () {

                        var max_wait_time = 0;
                        const interval_ya =  setInterval(function() {

                                var detect_ya_sov = [
                                  $('[title="Пocмoтpeть"]'),
                                  $('[title="Пocмотрeть"]'),
                                  $('[title="Пocмoтpеть"]'),
                                  $('[title="Посмотpeть"]'),
                                  $('[title="Поcмотреть"]'),
                                  $('[title="Пoсмoтpеть"]'),
                                  $('[title="Поcмoтpеть"]'),
                                  $('[title="Посмoтpeть"]'),
                                  $('[title="Пocмoтрeть"]'),
                                  $('[title="Пocмотpeть"]'),
                                  $('[title="Поcмотрeть"]'),
                                  $('[title="Пoсмотpeть"]'),
                                  $('[title="Поcмoтpeть"]'),
                                  $('[title="Поcмoтpeть"]'),
                                  $('[title="Посмoтрeть"]'),
                                  $('[title="Пoсмoтpeть"]'),
                                  $('[title="Поcмотpеть"]'),
                                  $('[title="Пocмотpеть"]'),
                                  $('[title="Поcмотpeть"]'),
                                  $('[title="Поcмoтреть"]'),
                                  $('[title="Пoсмотрeть"]'),
                                  $('[title="Пocмoтреть"]'),
                                  $('[title="Пocмотреть"]'),
                                  $('[title="Пoсмoтреть"]'),
                                  $('[title="Посмoтpеть"]'),
                                  $('[title="Поcмoтрeть"]'),
                                  $('[title="Пoсмoтрeть"]'),
                                  $('[title="Пoсмотpеть"]'),
                                  $('[title="Посмотрeть"]')
                                ];

                                var bool_detect_ya_sov = 0;
                                for (var i = 0; i < detect_ya_sov.length; i++) {
                                  if(detect_ya_sov[i].length == 1){
                                   bool_detect_ya_sov = 1;
                                   }
                                  }

                                if (bool_detect_ya_sov == 1) {

                                  $('[title="Пocмoтpeть"]').parent().parent().parent().remove();
                                  $('[title="Пocмотрeть"]').parent().parent().parent().remove();
                                  $('[title="Пocмoтpеть"]').parent().parent().parent().remove();
                                  $('[title="Посмотpeть"]').parent().parent().parent().remove();
                                  $('[title="Поcмотреть"]').parent().parent().parent().remove();
                                  $('[title="Пoсмoтpеть"]').parent().parent().parent().remove();
                                  $('[title="Поcмoтpеть"]').parent().parent().parent().remove();//7
                                  $('[title="Посмoтpeть"]').parent().parent().parent().remove();
                                  $('[title="Пocмoтрeть"]').parent().parent().parent().remove();//9
                                  $('[title="Пocмотpeть"]').parent().parent().parent().remove();
                                  $('[title="Поcмотрeть"]').parent().parent().parent().remove();//11
                                  $('[title="Пoсмотpeть"]').parent().parent().parent().remove();
                                  $('[title="Поcмoтpeть"]').parent().parent().parent().remove();//13
                                  $('[title="Поcмoтpeть"]').parent().parent().parent().remove();
                                  $('[title="Посмoтрeть"]').parent().parent().parent().remove();//15
                                  $('[title="Пoсмoтpeть"]').parent().parent().parent().remove();
                                  $('[title="Поcмотpеть"]').parent().parent().parent().remove();//17
                                  $('[title="Пocмотpеть"]').parent().parent().parent().remove();
                                  $('[title="Поcмотpeть"]').parent().parent().parent().remove();//19
                                  $('[title="Поcмoтреть"]').parent().parent().parent().remove();
                                  $('[title="Пoсмотрeть"]').parent().parent().parent().remove();//21
                                  $('[title="Пocмoтреть"]').parent().parent().parent().remove();
                                  $('[title="Пocмотреть"]').parent().parent().parent().remove();//23
                                  $('[title="Пoсмoтреть"]').parent().parent().parent().remove();
                                  $('[title="Посмoтpеть"]').parent().parent().parent().remove();//25
                                  $('[title="Поcмoтрeть"]').parent().parent().parent().remove();
                                  $('[title="Пoсмoтрeть"]').parent().parent().parent().remove();
                                  $('[title="Пoсмотpеть"]').parent().parent().parent().remove();//28
                                  $('[title="Посмотрeть"]').parent().parent().parent().remove();
                                  clearInterval(interval_ya);
                                  console.log('ya-killed');
                                } else if (max_wait_time == 7) {
                                  clearInterval(interval_ya);
                                  console.log('ya-stop');
                                } else {
                                  max_wait_time++;
                                }

                        }, 1000);
        }
    }
})();
