var catalog = (function () {
    var $changeView = $('.js-change-view');
    return {
        init: function(){
            if($('.js-catalog').hasClass('list-view')==false){
                catalog.toggleCardName();
            }
            catalog.toggleCatalogList();
            catalog.changeView();
        },
        toggleCardName: function() {
            $('.js-catalog__body, .js-main__catalog').each(function() {
                var $box = $(this);
                $box.on('mouseenter', '.js-catalog__item', function() {
                    var $cardItem = $(this),
                        $cardName = $cardItem.find('.js-card__name'),
                        longName  = $cardName.data('full-name');

                    $cardName.addClass('card__name--full');
                    $cardName.text(longName);

                    //console.log('on');
                });

                $box.on('mouseleave', '.js-catalog__item', function() {
                    var $cardItem     = $(this),
                        $cardName     = $cardItem.find('.js-card__name'),
                        maxNameLenght = 50,
                        shortName     = $cardName.text().substring(0, maxNameLenght) + '...';

                    if($cardName.text().length > maxNameLenght) {
                        $cardName.text(shortName);
                    }
                    //console.log('off');
                    $cardName.removeClass('card__name--full');
                });



            });
        },
        toggleCatalogList: function() {
            $('.js-main-catalog').each(function() {
                var $self = $(this),
                    $column = $self.find('.js-main-catalog__column'),
                    $listItem = $column.children('li'),
                    maxCategoryItems = 8,
                    totalItemsCount = $listItem.length,
                    requiredType = 'catalog',
                    coumnsCount = $self.data('columns-count'),
                    listType = $self.data('type'),
                    itemsInColumnMaxIndex = (maxCategoryItems / coumnsCount)  - 1;

                if(requiredType === listType &&
                   totalItemsCount > maxCategoryItems) {
                    
                    $listItem.each(function() {
                        var $selfListItem = $(this);

                        if($selfListItem.index() > itemsInColumnMaxIndex) {
                            $selfListItem.hide();
                        }
                    });

                    $self.append('<span class="main-catalog__link js-main-catalog__show-all">Показать все</span>');

                    $self.on('click', '.js-main-catalog__show-all', function() {
                        var $showAllSwitcher = $(this);

                        $listItem.each(function() {
                            var $selfListItem = $(this);

                            if($selfListItem.index() > itemsInColumnMaxIndex) {
                                $selfListItem.show();
                            }
                        });

                        $showAllSwitcher.hide();
                        $self.append('<span class="main-catalog__link js-main-catalog__hide_all">Скрыть</span>');
                    });

                    $self.on('click', '.js-main-catalog__hide_all', function() {
                        var $hideSwitcher = $(this);

                        $listItem.each(function() {
                            var $selfListItem = $(this);

                            if($selfListItem.index() > itemsInColumnMaxIndex) {
                                $selfListItem.hide();
                            }
                        });

                        $hideSwitcher.hide();
                        $self.append('<span class="main-catalog__link js-main-catalog__show-all">Показать все</span>');
                    });

                }
            });
        },
        changeView: function() {
            $changeView.click(function (e) {
                e.preventDefault();
                $changeView.toggleClass('active');
                $('.js-catalog').toggleClass('list-view');

            });
        }
    }
})();