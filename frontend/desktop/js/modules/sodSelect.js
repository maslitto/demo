var sodSelect = (function () {
	var $select = $('.js-select');
	return {
		init: function () {
			if($select.length) {
				$select.selectOrDie();
			}
		}
	}
})();