var reviews = (function () {
	var page = 2,
		$reviewsBlock = $('.js-reviews__list');
	return {
		init: function () {
			$('.js-reviews__show-more').click(function(e){
				e.preventDefault();
				var scrollPos = $(window).scrollTop(),
					box = $(this);
				$.ajax({
					type: 'GET',
					url: '/reviews/',
					dataType: 'json',
					data: {
						page: page
					}
				}).success(function( result ) {
					if(result.html) {
						var $items = $(result.html);
						$reviewsBlock.append(result.html);
						$items.hide().fadeIn(300);
						$(window).scrollTop(scrollPos);
						page++;
					}

					if(result.isEnd) {
						console.log('end');
						box.hide();
					}
				}).error(function(jqXHR, textStatus, errorThrown){
					console.log(errorThrown);
				});
			});
		}
	}
})();