var lk = (function () {
	var $order = $('.js-orders'),
		$form = $('.js-lk-company-info'),
		$inn = $('.js-company-inn'),
		$name = $('.js-company-name'),
		$kpp = $('.js-company-kpp'),
		$ogrn = $('.js-company-ogrn'),
		$factAddress = $('.js-company-fact-address'),
		$legalAddress = $('.js-company-legal-address'),
		$okved = $('.js-company-okved'),
        $educationForm = $('.js-education-form'),
        $formContent = $educationForm.find('.js-education-form-content'),
        $successBox = $educationForm.find('.js-form__success'),
        $educationIdBox = $successBox.find('.js-education-id'),
        $loading = $('.js-loading'),
        $educationTitle = $('.js-education-title')
	;
	return {
		init: function () {
			lk.order();
			lk.education();
			lk.referal();
			lk.changeInn();
			lk.initSelect2();
			lk.cloneCart();
		},
		order: function () {
			if (!$order.length) {
				return;
			}
			$('.js-order-num').click(function(e){
				e.preventDefault();
				e.stopPropagation();
				$(this).toggleClass('active').closest('.js-order-info').next('.js-order-detail').slideToggle(300)
			});

		},

        initCalendars: function () {
            var dateStart = document.getElementById("ed_date_start");
            var dateEnd = document.getElementById("ed_date_end");
            pickmeup.defaults.locales['ru'] = {
                days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
            };
            pickmeup(dateStart, {
                format: 'Y-m-d',
                locale: 'ru',
                position       : 'bottom',
                hide_on_select : true
            });
            pickmeup(dateEnd, {
                format: 'Y-m-d',
                locale: 'ru',
                position       : 'bottom',
                hide_on_select : true
            });

        },

        editEducation: function () {
            $('.js-education-edit').click(function (e) {
                e.preventDefault();
                var id = $(this).data('id');
                $successBox.hide();
                $formContent.show();
                $loading.show();
                $.ajax({
                    type: 'GET',
                    url: '/education/view/'+id,
                    dataType: "json",
                    success: function(response) {
                        var education = response.education;
                        $educationTitle.text('Редактирование карточки обучения №' + education.id);
                        $.each(education, function(index, value) {
                            if(index == 'program'){
                                editorinstance.setData(value);
                            } else{
                                var $element = $educationForm.find('[name="'+index+'"]');
                                $element.val(value);
                                $element.trigger('change');
                            }

                        });
                        $loading.hide();
                    },
                    error: function() {
                        $loading.hide();
                    }
                });
            });
        },

        createEducation: function () {
            $('.js-create-new-education').click(function (e) {
                e.preventDefault();
                var $form = $('.js-education-form'),
                    success = $form.find('.js-form__success'),
                    inputs = $form.find('.js-input')
                ;
                editorinstance.setData('');
                $form[0].reset();
                $('.js-education-hidden-id').val('');
                $.each(inputs, function( index, element ) {
                    $(element).trigger('change');
                    $(element).removeClass('error');
                });
                $('.js-education-form-content').show();
                success.removeClass('active');
            });
        },

		education: function () {
		    lk.initCalendars();
		    lk.createEducation();
		    lk.editEducation();

			$('.js-education-num').click(function(e){
				e.preventDefault();
				e.stopPropagation();
				$(this).toggleClass('active').closest('.js-education-info').next('.js-education-detail').slideToggle(300)
			});
			$('.js-date-format').change(function () {
				if(Number($(this).val()) === 1){
                    $('.js-date-block').removeClass('form__b--two-col');
                    $('.js-date-end-block').hide();
				} else {
                    $('.js-date-block').addClass('form__b--two-col');
                    $('.js-date-end-block').show();
				}
            });
            $('.js-is-free').change(function () {
                console.log(Number($(this).val()));
                if(Number($(this).val()) === 1){
                    $('.js-price-block').hide();
                } else {
                    $('.js-price-block').show();
                }
            });

            $educationForm.on('form_success', function(event, resp) {
                $formContent.hide();
                $successBox.show();
                if(resp.educationId) {
                    $educationIdBox.text(resp.educationId);
                }
                $('html, body').animate({
                    scrollTop: $educationForm.offset().top
                }, 1000);
            });
		},

		referal: function () {
			$('.js-copy-to-clipboard').click(function(e){
				var $target = $($(this).data('target'));
				console.log($target);
				e.preventDefault();
				helpers.toClipboard($target);
			});
		},

        changeInn: function () {
            $inn.click(function(e){
                var inn = $(this).val();
                $.ajax({
                    type: 'POST',
                    url: '/personal/get-info-by-inn/',
                    data: {inn : inn},
                    dataType: "json",
                    success: function(resp) {
                        console.log(resp);
						$kpp.val(resp.data.kpp);
						$ogrn.val(resp.data.ogrn);
						$okved.val(resp.data.okved);
						$name.val(resp.value);
						$legalAddress.val(resp.data.address.value);
                    },
                    error: function() {
                        //forms.showErrorMessage($form, [$form.data('error-msg')]);
                        //$form.trigger('form_error', resp);
                    }
                });
            });
        },

		initSelect2: function () {
            $('.js-select2').select2({
                language: "ru",
                minimumResultsForSearch: -1
                //placeholder: 'Буду проводить'
            });
        },

        cloneCart: function () {
            $('.js-clone-cart').click(function (e) {
                e.preventDefault();
                var id = $(this).data('id');
                var $cart = $.cart;
                $.ajax({
                    type: 'GET',
                    url: '/personal/copy-order-to-cart/'+id+'/',
                    dataType: "json",
                    success: function(response) {
                        $('.js-basket-menu__count').text(response.count);
                        $.magnificPopup.open({
                            items: {
                                src: '#clone-cart',
                                type: 'inline'
                            }
                        })
                    },
                    error: function() {

                    }
                });
            })
        }
	}
})();
