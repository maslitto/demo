var sendsay = (function () {
    return {
        init: function () {
            sendsay.cartListener();
        },
        cartListener: function () {
            $.cart.on('render', function() {
                $.get('/cart/json/', function(response) {
                    sendsay.updateSendsayCart(response.cart, response.email);
                });
            });
        },
        updateSendsayCart: function (cart, email) {

            if(typeof sndsyApi == 'undefined') {
                return;
            }
            //console.log(email);
            //var cart = $.cart.cart !== undefined ? JSON.stringify($.cart.cart) : '{}';
            //console.log($.cart.cart);

            if(typeof email != 'undefined') {
                sndsyApi.basketUpdate( cart,
                    { email: email }
                );
            } else {
                sndsyApi.basketUpdate(cart);
            }
            //console.log(sndsyApi);
            //console.log(cart);
        }
    }
})();