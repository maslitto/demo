var hoverMenu = (function () {
	var $target = $('.js-h-catalog__title'),
		$document = $(document),
		$menuItem = $('.js-h-catalog__el'),
		// $menuLink = $('.h-catalog__el h-catalog__el--link'),
		$menuBlock = $('.js-h-catalog__list-wrapper'),
		isOpen = false,
		$currBlock,
		$lastBlock,
		scrollBlocks = [],
		$window = $(window),
		winHeight = $window.height(),
		openedBlocks = [];
	return {
		init: function () {
			if(!$target.length){
				return;
			}

			/*.mouseenter(function(){
				if (!isOpen) {
					hoverMenu.menuOpen();
					bodyVeil.show();
				}
			})*/
			$target
				.click(function(){
					if (isOpen) {
						hoverMenu.menuClose();
						bodyVeil.close();
					} else {
						hoverMenu.menuOpen();
						bodyVeil.show();
					}
				});
			$document.on('veilClose', function (){
				hoverMenu.menuClose();
			});
			// Наведение на элемент списка
			hoverMenu.menuItemHover();
			hoverMenu.menuItemScroll();
		},
		menuClose: function () {
			isOpen = false;
			$target.removeClass('active');
			openedBlocks.filter(function(elem){
				elem.$block.hide();
				return false;
			});
			$menuItem.removeClass('active');
		},
		menuOpen: function () {
			isOpen = true;
			$target.addClass('active');
		},
		menuItemHover: function () {
			var timer;
			var lastLevel = 2;
			$menuItem.mouseenter(function(e) {
				var $el = $(e.currentTarget);
				e.preventDefault();
				timer = setTimeout(function(){
					var id = $el.data('menu-target'),
						currLvl;
					$currBlock = $menuBlock.filter('[data-menu-block="'+ id +'"]');
					currLvl = $el.closest('.js-h-catalog__list-wrapper').data('level');
					if ($currBlock.is($lastBlock)) {
						return;
					}
					// Ищем блок для открытия
					$lastBlock = $currBlock = $menuBlock.filter('[data-menu-block="'+ id +'"]');
					// Меньший уровень меню
					if (currLvl <= lastLevel) {
						//Скрываем ненужные блоки и очищаем массив
						openedBlocks = openedBlocks.filter(function($element, i, arr) {
							var bool = i < currLvl;
							if (!bool) {
								if ($element.$block.length) {
									$element.$block.hide();
								}
								$element.$link.removeClass('active');
							}
							return bool;
						});
						// Добавляем блок в массив открытых блоков
					}
					// Сохраняем уровень вложенности
					lastLevel = currLvl;
					// Добавляем блок в массив открытых блоков
					openedBlocks.push({
						$block: $currBlock.show(),
						$link: $el.addClass('active')
					});
					// $menuBlock.hide();
				}, 100);
			}).mouseleave(function() {
				clearTimeout(timer);
			});
		},
		menuItemScroll: function () {
			$('.js-scroll').each(function(){
				scrollBlocks.push(new SimpleBar($(this)[0], { autoHide: false }));
			});
			// Пересчет блоков меню при изменении размера экрана
			$(document).on('windowResize', function() {
				var newWinHeight = $window.height();
				if (newWinHeight === winHeight) return;
				winHeight = newWinHeight;
				$.each(scrollBlocks, function(index, el) {
					el.recalculate();
				});
			});
		},
		isOpen: function() {
			return isOpen;
		},
	}
})();