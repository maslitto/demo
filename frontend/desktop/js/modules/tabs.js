var tabs = (function () {
	return {
		init: function () {
			tabs.tabsSwitchAction();
			tabs.gotoTab();

		},
		tabsSwitchAction: function() {
			$('.js-tabs').each(function() {
				var $tabsWrap = $(this),
					$tabLinks = $tabsWrap.find('.js-tabs__link');

				$tabLinks.click(function(){
					var $currentTabLink   = $(this);
					tabs.changeTab($currentTabLink);
				});
			});
		},
		changeTab: function($tabLinkToSwitch) {
			var currentUrl   = $tabLinkToSwitch.data('url'),
				currentTitle = $tabLinkToSwitch.data('title'),
				$tabsWrap    = $tabLinkToSwitch.closest('.js-tabs'),
				$tabLinks    = $tabsWrap.find('.js-tabs__link'),
				$tabs 	     = $tabsWrap.find('.js-tabs__tab');

			$tabLinks.removeClass('active');
			$tabLinkToSwitch.addClass('active');
			$tabs.removeClass('active');
			$tabs.eq($tabLinkToSwitch.index()).addClass('active');
			History.replaceState({}, currentTitle, currentUrl);
		},
		gotoTab: function() {
			$(document).on('click', '.js-tab-switch', function() {
			    var $switchHandler = $(this),
			    	tabWrapName    = $switchHandler.data('wrap'),
			    	tabName 	   = $switchHandler.data('tab'),
			    	$tabsWrap 	   = $('.js-tabs[data-name="' + tabWrapName + '"]');

			    if($tabsWrap.length) {
			    	var $tabLinkToSwitch = $tabsWrap.find('.js-tabs__link[data-name="' + tabName + '"]');
			    	tabs.changeTab($tabLinkToSwitch);

			    	$('html, body').animate({
			    	    scrollTop: $($tabsWrap).offset().top - 160
			    	}, 'normal');
			    }

			        
			});
		}
	}
})();