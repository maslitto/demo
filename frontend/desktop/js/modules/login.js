var login = (function () {
	var $link = $('.js-login-open'),
		$block = $('.js-login-block');
	return {
		init: function () {
			$link.click(function(e) {
				e.preventDefault();
				e.stopPropagation();
				if (!$block.hasClass('active')) {
					$block.addClass('active');
					bodyVeil.show();
 				} else {
					$block.removeClass('active');
					bodyVeil.close();
				}
			});
			$(document).on('veilClose', function() {
				$block.removeClass('active');
			});

			login.loginSuccess();
		},
		loginSuccess: function() {
			$('.js-login-form').on('form_success', function() {
				window.location.replace("/personal/");
			});
		},
	}
})();