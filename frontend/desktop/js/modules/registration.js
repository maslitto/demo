var registration = (function () {
    return {
        init: function () {
            registration.registrationSuccess();
        },
        registrationSuccess: function() {
            $('.js-registration-form').on('form_success', function() {
                window.location.replace("/user/");
            });
        },
    }
})();