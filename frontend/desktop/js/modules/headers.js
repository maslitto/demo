var headers = (function () {
	var $headerWrap    = $('.js-header'),
		$topHeader     = $headerWrap.find('.js-header__line1'),
		$bottomHeader  = $headerWrap.find('.js-header__line2'),
		$headerContact = $topHeader.find('.js__header-contact'),
		$submenu       = $bottomHeader.find('.js__header-submenu'),
		$submenuIcon   = $submenu.find('.js__icon-hamburger'),
		isOpen 		   = false;

	return {
		init: function() {
			headers.stickyHeader();
			headers.submenu();
		},
        stickyHeader: function() {
            $bottomHeader.stick_in_parent({
                parent: $('body'),
                inner_scrolling: false,
            })
                .on("sticky_kit:stick", function(e) {
                    $headerContact.addClass("stuck");
                    $headerContact.toggleClass("arr-d-after arr-d-after--nopadding arr-d-after--large");

                })
                .on("sticky_kit:unstick", function(e) {
                    $headerContact.removeClass("stuck");
                    $headerContact.toggleClass("arr-d-after arr-d-after--nopadding arr-d-after--large");
                    headers.closeSubmenu();
                    if(!hoverMenu.isOpen() && !citySwitch.isOpen()) {
                        bodyVeil.close();
                    }
                });
        },
		submenu: function() {
            $submenu.click(function() {
            	if(isOpen) {
            		headers.closeSubmenu();
    				bodyVeil.close();
            	}
            	else {
            		headers.openSubmenu();
    				bodyVeil.show();
            	}
            });

            $(document).on('veilClose', function (){
            	headers.closeSubmenu();
            });
		},
		openSubmenu: function() {
			$submenu.add($submenuIcon).addClass('open');
    		isOpen = true;

		},
		closeSubmenu: function() {
			$submenu.add($submenuIcon).removeClass('open');
    		isOpen = false;

		},
	}
})();