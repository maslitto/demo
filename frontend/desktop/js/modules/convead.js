var conveadApp = (function () {
	return {
		init: function () {
			var version = parseInt($('.js-version').attr('content'));
			if(version === 1 || version === 2){
				conveadApp.initSettings();
				conveadApp.initProductView();
				conveadApp.initCartListener();
			}
		},
		initSettings: function () {
			var userEmail = null;
			var visitorInfo;
			var email = $('.js-user-email').attr('content');

			if(email.length){
				visitorInfo = {email: userEmail};
			} else {
				visitorInfo = {};
			}
			window.ConveadSettings = {
				visitor_info: visitorInfo,
				onload: function() {
					console.log('Convead onload');
				},
				onready: function() {
					console.log("Convead initialized");
				},

				app_key: "893d1beb27b4547911f6608c49535f0e"
			};
			(function(w,d,c){w[c]=w[c]||function(){(w[c].q=w[c].q||[]).push(arguments)};var ts = (+new Date()/86400000|0)*86400;var s = d.createElement('script');s.type = 'text/javascript';s.async = true;s.charset = 'utf-8';s.src = 'https://tracker.convead.io/widgets/'+ts+'/widget-893d1beb27b4547911f6608c49535f0e.js';var x = d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);})(window,document,'convead');
			/*$.ajax({
				type: 'GET',
				url: '/cart/convead/',
				dataType: 'json',
				success: function(response) {
					if(response.email){
						userEmail = response.email;
						if(userEmail){
							visitorInfo = {email: userEmail};
						} else {
							visitorInfo = {};
						}
					}
					window.ConveadSettings = {
						visitor_info: visitorInfo,
						onload: function() {
							console.log('Convead onload');
						},
						onready: function() {
							console.log("Convead initialized");
						},

						app_key: "893d1beb27b4547911f6608c49535f0e"
					};
					(function(w,d,c){w[c]=w[c]||function(){(w[c].q=w[c].q||[]).push(arguments)};var ts = (+new Date()/86400000|0)*86400;var s = d.createElement('script');s.type = 'text/javascript';s.async = true;s.charset = 'utf-8';s.src = 'https://tracker.convead.io/widgets/'+ts+'/widget-893d1beb27b4547911f6608c49535f0e.js';var x = d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);})(window,document,'convead');
					conveadApp.initProductView();
				}
			});*/
		},

		initProductView: function () {
			var $product = $('.js-product--detail');

			if($product.length){
				var productProps = $product.data('ga-props');
				convead('event', 'view_product', {
					product_id: productProps.id,
					product_name: productProps.name,
					product_url: productProps.url
				});
			}
		},

		initCartListener: function () {
			$.cart.on('render', function() {
				$.get('/cart/convead-json/', function(response) {
					convead('event', 'update_cart', {
						items: response.cart
					});
					console.log('convead update_cart');
				});
			});
			$('.js-order-form').on('form_success', function(event, resp) {
				convead('event', 'purchase',
					{order_id: resp.orderId, revenue: resp.summ, items: resp.items},
					{first_name: resp.name, last_name: '', email: resp.email, phone: resp.phone}
				);
				fbq('track', 'Purchase');
				console.log('convead purchase');
			});
		},
	}
})();
