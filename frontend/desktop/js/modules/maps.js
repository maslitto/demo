/**
 * Created by pc on 25.06.2017.
 */
var maps = (function (){
	var loaded = 'loaded';
	var $maps = $('.js-map');
	var gMapsLoaded = false;
	return {
		init: function (){
				$(window).bind('gMapsLoaded', maps.createMap);
				maps.loadGoogleMaps();
		},
		createMap: function() {
			$maps.each(function(){
				var $map = $(this),
				    offsetLat = $map.data('offset-lat'),
					offsetLon = $map.data('offset-lon');
				var mapLatLng = {lat: $map.data('lat'), lng: $map.data('lon')};
				var mapCenter = mapLatLng;
				if (typeof offsetLat !== typeof undefined && offsetLat !== false && typeof offsetLon !== typeof undefined && offsetLon !== false) {
					mapCenter = {
						lat: offsetLat,
						lng: offsetLon
					}
				}
				console.log(33,$map.data('lat'), $map.data('lon'));

				var mapOptions = {
					zoom:      15,
					center: mapCenter,
					disableDefaultUI: true,
					zoomControl: true
				};
				var map = new google.maps.Map(document.getElementById($map.attr('id')), mapOptions);
				var icon = {
					url: '/img/map-marker-blue.png', // url
					scaledSize: new google.maps.Size(40, 58), // scaled size
					origin: new google.maps.Point(0, 0), // origin
					anchor: new google.maps.Point(20, 58) // anchor
				};
				new google.maps.Marker({
					position: mapLatLng,
					map: map,
					icon: icon
				});
			});
		},
		loadGoogleMaps: function (){
			if ( gMapsLoaded ) return window.gMapsCallback();
			var script_tag = document.createElement('script');
			script_tag.setAttribute("type", "text/javascript");
			script_tag.setAttribute("src", "https://maps.google.com/maps/api/js?key=AIzaSyBUCw8mTba39kqqs4OZbwdjaLpsVQTKCeg&sensor=false&language=ru&callback=maps.gMapsCallback");
			$('body').append(script_tag);
		},
		gMapsCallback:  function (){
			gMapsLoaded = true;
			$(window).trigger('gMapsLoaded');
		}
	}
})();