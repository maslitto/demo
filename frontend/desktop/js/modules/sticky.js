var sticky = (function () {
	return {
		init: function () {
			sticky.catalogFilter();
			sticky.basketForm();
			sticky.educationForm();
		},
		catalogFilter: function() {
			var $filter = $(".js-filter"),
				$header = $('.js-header__line2');

			$filter.stick_in_parent({
				offset_top: $header.height() + 10,
				inner_scrolling: false
			});
		},
		basketForm: function() {
			var $basketFormBlock = $(".js-order-info"),
				$header = $('.js-header__line2');

			$basketFormBlock.stick_in_parent({
				offset_top: $header.height() + 10,
				inner_scrolling: false
			});
		},
        educationForm: function() {
            var $educationFormBlock = $(".js-education-buy"),
                $header = $('.js-header__line2'),
				$infoEducationInfo = $('.js-info-education-info'),
				$buyEducationInfo = $('.js-buy-education-info')
			;


            $educationFormBlock.stick_in_parent({
                offset_top: $header.height() + 10,
                inner_scrolling: false,
                wrapperClassName: 'education-sticky'
            });

            $educationFormBlock.on('sticky_kit:stick', function() {
				$buyEducationInfo.html($infoEducationInfo.html());
			});
            $educationFormBlock.on('sticky_kit:unstick', function() {
				$buyEducationInfo.html('');
			});
        }
	}
})();