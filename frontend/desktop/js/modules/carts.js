var cart = (function (){
	var $basketBtn = $('.js-basket-menu');
	var $basketBtnCount = $('.js-basket-menu__count');
	return {
		init: function() {
			$.cart = new cart.cart();
			$.cart.init();
			cart.productPage();
			cart.addItemBtn();
			cart.openDeliveryAddress();
			cart.oneClickBuySuccess();
			cart.initViewedProducts();
			cart.productsTypes();
			cart.initBonusProduct();
		},
		cart: function () {
			this.price 					= 0;
			this.count 					= 0;
			this.sum   					= 0;
			this.fullSum				= 0;
			this.isAllowCashlessPayment	= false;
			this.isAllowDelivery		= false;
			this.discount   			= 0;
			this.bonus	    			= 0;
			this.complete   			= 0;
			this.isShowBonus      		= 0;
			this.init = function() {
                console.log('init started');
                this.trigger('render');
			};
			this.clear = function() {
				this.complete = 1;
				this.trigger('update');
			};
			this.add = function(data, options) {
                var exists = false;
				console.log('add clicked');
                $.ajax({
                    url: '/cart/add/',
                    type: "post",
                    data: data,
                    success: function(response) {
                        $basketBtnCount.text(response.count);
                        cart.sum = response.sum;
                        cart.count = response.count;
                        cart.discount = response.discount;
                        cart.bonus = response.bonus;
                        cart.fullSum = response.fullSum;
                        cart.isAllowCashlessPayment = response.isAllowCashlessPayment;
                        cart.isAllowDelivery = response.isAllowDelivery;
                        $.cart.trigger('render');
                    },
                    error: function(jqXHR, exception) {
                        console.log('cart add product fuck up');
                    }
                });

                
				return exists;
			};
            this.update = function(data) {
                var exists = false;
                var cart = this;
                $.ajax({
                    url: '/cart/update/',
                    type: "post",
                    data: data,
                    success: function(response) {
                        $basketBtnCount.text(response.count);
                        cart.sum = response.sum;
                        cart.count = response.count;
                        cart.discount = response.discount;
                        cart.bonus = response.bonus;
                        cart.fullSum = response.fullSum;
                        cart.isAllowCashlessPayment = response.isAllowCashlessPayment;
                        cart.isAllowDelivery = response.isAllowDelivery;
                        $.cart.trigger('render');
                        if(response.promocode === true){
                            $('.js-promocode-ok').html('Промокод применен');
						}
                        $('.js-promocode-error').html('');

                    },
                    error: function(jqXHR, exception) {
                        console.log('cart update product fuck up');
                        if (jqXHR.status === 422) {
                            $('.js-promocode-error').html('Неверный промокод');
                        }
                    }
                });
                
                return exists;
            };
			this.del = function(id) {
                var cart = this;
                $.ajax({
                    url: '/cart/delete/',
                    type: "post",
                    data: {id : id},
                    success: function(response) {
                        $basketBtnCount.text(response.count);
                        cart.sum = response.sum;
                        cart.count = response.count;
                        cart.discount = response.discount;
                        cart.bonus = response.bonus;
                        cart.fullSum = response.fullSum;
                        cart.isAllowCashlessPayment = response.isAllowCashlessPayment;
                        cart.isAllowDelivery = response.isAllowDelivery;
                        $.cart.trigger('render');
                    },
                    error: function(jqXHR, exception) {
                        console.log('cart delete product fuck up');
                    }
                });
                
			};
			this.on = function(event, fn) {
				$(this).on(event, fn);
			};
			this.trigger = function(event) {
				$(this).trigger(event);
			};
		},
		initBonusProduct:function () {
			$('.js-selectbox__menu-item').click(function (e) {
				e.preventDefault();
				var id = $(this).data('id');
				$('.js-selectbox__current').unbind('mouseenter mouseleave');
				$('.js-selectbox__current').text($(this).html());
				$.ajax({
					url:'/cart/product-json/',
					data: {id: id},
					method: 'get',
					contentType: 'json',
					dataType: 'json',
					success: function (response) {
						var product = JSON.parse(response);
						$('.basket-bonus .js-form__number').attr('max',product.countSpb);
						$('.basket-bonus .js-form__number').attr('data-id',product.id);
						$('.basket-bonus .js-form__number').val(1);
					}
				})
			});

			$('.js-basket-bonus-add').click(function (e) {

				var id = $('.basket-bonus .js-form__number').attr('data-id'),
					count = $('.basket-bonus .js-form__number').val();
				$.ajax({
					url:'/cart/add/',
					data: {id: id,count:count,bonus:1},
					method: 'post',
					success: function (response) {
						$('.basket-bonus .js-form__number').val(1);
						$.cart.trigger('render');
						location.reload();
					}
				});

				e.preventDefault();

			})
			$('.js-basket-bonus__close').click(function (e) {
				$('.js-basket-bonus').hide("fast", function(){ $(this).remove(); })
			})
		},
		// Страница товара
		productPage: function() {
			if (!$('.js-product--detail').length) {
				return;
			}
			var count,
				newPrice,
				newDiscountPrice,
				$productBlock = $('.js-product'),
				$inputValue = $productBlock.find('.js-change-sum-number'),
				$priceBlock = $productBlock.find('.js-summ'),
				$discountPriceBlock = $productBlock.find('.js-discount-summ'),
				$bonusesBlock = $productBlock.find('.js-bonuses'),
				$ocbCount = $productBlock.find('.js-ocb__count'),
				$deliveryExpress = $productBlock.find('.js-delivery-express'),
				$deliveryCommon = $productBlock.find('.js-delivery-common'),
				$productAddBtn = $productBlock.find('.js-cart-add'),
				price = parseFloat($productBlock.data('price')),
				discountPrice = parseFloat($productBlock.data('discount-price'));
			/*$inputValue.on('change', function() {
				var bonuses;
				count  = +$(this).val();
				newPrice = count * price;
				newDiscountPrice = count * discountPrice;
				bonuses = Math.round(newPrice*0.01);
				$priceBlock.text(newPrice.formatNum(0,' '));
				$discountPriceBlock.text(newDiscountPrice.formatNum(0,' '));
				$bonusesBlock.html(bonuses.formatNum(0,' ') + helpers.decOfNum(+bonuses, [' бонус', ' бонуса', ' бонусов']));
				$productAddBtn.data('count', count);
				$ocbCount.val(count);

				if(newPrice > 100000 ||
				   newDiscountPrice > 100000) {
					$discountPriceBlock.addClass('summary__total-discount-price--extend');
				}
				else {
					$discountPriceBlock.removeClass('summary__total-discount-price--extend');
				}

				$.ajax({
					type: 'POST',
					url: '/delivery/get-price/',
					dataType: 'json',
					data: {
						price: price,
						count: count
					},
					success: function(result) {
						if(typeof result.error !== 'undefined') {
							return;
						}
						//$deliveryExpress.html();
						$deliveryCommon.html(result.local);
					}
				});
			});*/
		},
		// Купить в один клик
		oneClickBuySuccess: function() {
			var $form = $('.js-ocb__form'),
				productInput = $('.js-product').find('.js-form__number'),
				productCost  = parseInt(productInput.data('cost')),
				productCount = parseInt(productInput.val()),
				productSum   = productCost * productCount;

			$form.on('form_success', function(event, resp){
				if (resp && resp.hasOwnProperty('id')) {
					$form.find('.js-form__success .content').text('Спасибо! Ваш заказ №' + resp.id +  ' принят');
				}
			});
		},
		// Кнопка добавить товар
		addItemBtn: function() {
			$('body').on('click', '.js-cart-add', function(e) {
				console.log('addItemBtn');
				var $addBtn = $(this),
					$card = $addBtn.closest('.js-catalog__item'),
					exist;

				if(!$addBtn.hasClass('in-cart') || !$addBtn.hasClass('js-product-page-btn')) {
					e.preventDefault();
					e.stopPropagation();

					exist = $.cart.add({
						id:    $addBtn.data('id'),
						count: $addBtn.data('count')
					});
					cart.addToCartAnimation($card);
					$addBtn.addClass('in-cart').attr('href', '/cart/');
					if($addBtn.hasClass('long')) {
						$addBtn.html('Оформить заказ');
					}
				}
			});
		},
		addToCartAnimation: function(box) {
		    if (box && box.hasClass('js-catalog__item')) {
	            var card = box.clone().appendTo('body');
	            var cardOffset = box.offset();

	            var cart = $('.js-basket-menu:visible');
	            var cartOffset = cart.offset();

	            card.css({
	                position: 'absolute',
	                top: cardOffset.top - 20,
	                left: cardOffset.left,
	                width: box.innerWidth(),
	                minWidth: 0,
	                zIndex: 11000
	            });

	            card.animate({
	                top: cartOffset.top,
	                left: cartOffset.left - 40,
	                opacity: 0.6,
	                width: 20,
	                height: 20,
	            }, 500, function () {
	                card.remove();
	            });

	            return false;
	        }
		},
		// Козина
		moveToCartAnim: function(orCard){
			console.log('moveToCartAnim');
			var $fakeCard = orCard.clone().appendTo('body'),
				$orCardOffset = orCard.offset(),
				basketBtnOffset = $basketBtn.offset();
			$fakeCard.css({
				position: 'absolute',
				top: $orCardOffset.top - 40,
				left: $orCardOffset.left,
				zIndex: 11000
			}).addClass('card--moving');
			$fakeCard.animate({
				top: basketBtnOffset.top,
				left: basketBtnOffset.left
			}, 0);
			setTimeout(function(){
				$fakeCard.remove();
			}, 500);
		},
		// Добавить адрес
		openDeliveryAddress: function () {
			var $openBtn = $('.js-open-delivery'),
				$closeBtn = $('.js-close-delivery'),
				$block = $('.js-delivery-address'),
				$input = $block.find('.js-input');
			$openBtn.change(function() {
				if ($(this).prop("checked")) {
					$block.show('hidden');
					$input.addClass('js-required');
				}
			});
			$closeBtn.change(function() {
				if ($(this).prop("checked")) {
					$block.hide('hidden');
					$input.removeClass('js-required');
				}
			});
		},
		// Товары
		Products: function() {
			this.products = [];
			this.cookie = 'viewed-products';
			this.countLimit = 4;

			this.init = function() {
			    var jsonProducts = $.cookie(this.cookie);
			    this.products = jsonProducts ? $.parseJSON(jsonProducts) : [];
			    this.trigger('update');
			};

			this.clear = function() {
			    this.products = [];
			    this.trigger('update');
			};

			this.add = function(data) {
			    var exists = false;

			    data.date = $.now();

			    for (var i in this.products) {
			        if(this.products[i].id == data.id) {
			            data.count = this.products[i].count + 1;
			            this.products.splice(i, 1, data);
			            exists = true;
			        }
			    }

			    if(!exists) {
			        data.count = 1;
			        this.products.push(data);
			    }

			    this.save();
			    this.trigger('update');
			};

			this.clearOverLimit = function() {
				if(this.products.length < this.countLimit) {
					return;
				}

				var productIndex = this.products.length - 1;

				while(this.products.length > this.countLimit) {
					console.log(this.products[productIndex].id);
					this.del(this.products[productIndex].id);
					
					productIndex--;
				}
			}

			this.del = function(id) {
			    for (var i in this.products) {
			        if(this.products[i].id == id) {
			            this.products.splice(i, 1);
			        }
			    }
			    this.save();
			    this.trigger('update');
			};

			this.getProducts = function() {
			    return this.products;
			};

			this.save = function() {
			    var productsJson = null;

			    if(this.products.length) {
			        productsJson = JSON.stringify(this.products);
			    }

			    this.products.sort(function(a, b){
			        return (a.date < b.date);
			    });

			    this.products = this.products.splice(0, 9);
			    $.cookie(this.cookie, productsJson, {expires: 365, path: "/"});
			};

			this.on = function(event, fn) {
			    $(this).on(event, fn);
			};

			this.trigger = function(event) {
			    $(this).trigger(event);
			};
		},
		initViewedProducts: function() {
			var productBox = $('.js-product');

			if(!productBox.length) {
				return;
			}

			var viewedProducts = new cart.Products();

			viewedProducts.cookie = 'viewed-products';
			viewedProducts.init();

			viewedProducts.add({
			    id: productBox.data('id')
			});
			viewedProducts.clearOverLimit();
		},
		productsTypes: function() {
			var $productBox = $('.js-product'),
				productTypeSwitcher = $productBox.find('.js-product-type');

			productTypeSwitcher.on('change', function() {
			    var productTypeOption = $('option[value="' + $(this).val() + '"]', productTypeSwitcher);
			    location.href = productTypeOption.data('url');
			});			
		}
	}
})();

