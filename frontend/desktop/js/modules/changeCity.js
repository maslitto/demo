var changeCity = (function () {
	var $cityLink 	   = $('.js-change-city.first-step'),
		$finalCityLink = $('.js-change-city.final');
	var options = {
		expires: 31536000
	};
	return {
		init: function () {
			
			$cityLink.click(function(){
				var cityId = $(this).data('id');
				if(0 == $.cart.count) {
					changeCity.proceed(cityId);
				}
				else {
					$finalCityLink.data('id', cityId);

					$.magnificPopup.open({
					  items: {
						src: $(this).attr('href')
					  },
					  type: 'inline'
					});
				}
			});

			$finalCityLink.click(function() {
				var cityId = $(this).data('id');
				changeCity.proceed(cityId);
			});
		},
		proceed: function(cityId) {
			$.ajax({
				type: 'POST',
				url: '/set-version/',
				data: {
					store: cityId
				},
				dataType: "json",
				success: function(resp) {
					if(!resp.hasOwnProperty('error') &&
					   resp.hasOwnProperty('action')) {

						var pathname = window.location.pathname,
							redirectUrl = resp.domain + pathname;

					   	switch(resp.action) {
					   		case 'reload':
					   			if(resp.hasOwnProperty('domain')) {
					   				window.location.href = redirectUrl;
					   			}
					   			else {
					   				location.reload();
					   			}
					   			break;
					   		case 'redirect':
					   			if(resp.hasOwnProperty('domain')) {

					   				if(resp.hasOwnProperty('keyParam')) {
					   					redirectUrl = redirectUrl + '?' + resp.keyParam;
					   				}
					   				window.location.href = redirectUrl;
					   			}
					   			break;
					   	}
							
					}
					else {
						console.log('version error: ' + resp.error);
					}
				}
			});
		}

    }
})();