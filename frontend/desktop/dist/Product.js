'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    name: "product",
    props: ['data'],
    delimiters: ['${', '}'],
    computed: {
        remain: function remain() {
            var remain = this.product.count - this.currentCount;
            if (remain < 0) {
                remain = 0;
            }
            return remain;
        }
    },
    methods: {
        formatPrice: function formatPrice(value) {
            return value;
            //return number_format(value, 0, '.', '');
        },
        setCurrentCount: function setCurrentCount(operation) {
            if (operation == 'plus') {
                this.currentCount += 1;
            } else if (operation == 'minus') {
                this.currentCount -= 1;;
            }

            if (this.currentCount < 1) {
                this.currentCount = 1;
            }
            if (this.currentCount > this.product.count && this.product.count > 0) {
                this.currentCount = this.product.count;
            }
            if (this.product.count == 0) {
                this.currentCount = 1;
            }
            return this.currentCount;
        },
        addToCart: function addToCart(productId, count) {
            this.inCart = true;
            var data = {
                id: productId,
                count: count
            };
            console.log(data);
            var ret = $.cart.add(data);
            //yaCounter23244307.reachGoal('KORZINA');
            return false;
        }
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div class=\"summary__b summary__b--1\">\n    <!--PRICE ROW-->\n    <div class=\"summary__row clearfix\">\n        <div class=\"summary__total-discount-price js-discount-summ\" v-if=\"product.oldPrice\">\n            ${product.oldPrice * currentCount}\n        </div>\n        <div class=\"summary__total-price js-summ\" v-if=\"product.price\">\n            ${product.price * currentCount}\n        </div>\n    </div>\n    <!--COUNT INPUT AND COUNT ROW-->\n    <div class=\"summary__row clearfix\">\n        <div class=\"not-added\" v-if=\"!inCart\">\n            <div class=\"summary__value\">\n                <label class=\"form__number-input-b form__number-input-b--small form__b--one\">\n                    <input class=\"form__number-input js-form__number js-change-sum-number js-ga-count\" name=\"item-count\" type=\"number\" step=\"1\" min=\"1\" max=\"product.count ? product.count : 1\" v-model=\"currentCount\" @change=\"setCurrentCount()\" data-bonus-cost=\"<?= $fullBonus ?>\" :data-cost=\"product.price\" :data-id=\"product.id\">\n                    <span class=\"form__number-input-minus js-form__number-minus\" @click=\"setCurrentCount('minus')\">−</span>\n                    <span v-if=\"remain > 0\" class=\"form__number-input-plus js-form__number-plus\" @click=\"setCurrentCount('plus')\">+</span>\n                    <span v-else=\"\" class=\"form__number-input-plus js-form__number-plus maxed\" @click.prevent=\"setCurrentCount(NULL)\">+</span>\n                </label>\n            </div>\n            <div class=\"availability\">\n                <span class=\"summary__text--green count\" v-if=\"product.count>0\"> В наличии <span class=\"remain\">${product.count}</span> шт.</span>\n                <span class=\"summary__text--red\" v-else=\"\">Под заказ<span class=\"remain\"></span></span>\n            </div>\n        </div>\n        <div class=\"added\" v-else=\"\">\n            <svg class=\"checkmark\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 52 52\">\n                <circle class=\"checkmark__circle\" cx=\"26\" cy=\"26\" r=\"25\" fill=\"none\"></circle>\n                <path class=\"checkmark__check\" fill=\"none\" d=\"M14.1 27.2l7.1 7.2 16.7-16.8\"></path>\n            </svg>\n\n            <span class=\"text\">${currentCount} шт. в корзине. Осталось ${remain} шт.</span>\n        </div>\n    </div>\n\n\n    <!--ADD TO BASKET / GOT TO CART-->\n    <div class=\"summary__row clearfix\">\n        <a v-if=\"product.price &amp;&amp; !inCart\" class=\"ga-event-add-product btn long btn--yellow summary__add-to-basket js-product-page-btn js-ga-add-to-cart\" @click.prevent=\"addToCart(product.id,currentCount)\"> Добавить в корзину </a>\n        <a v-if=\"inCart\" href=\"/cart/\" class=\"btn long btn--yellow summary__add-to-basket js-product-page-btn\">Перейти в корзину</a>\n    </div>\n    <!--ONE CLICK BUY ROW-->\n    <div class=\"summary__row ocb\" v-if=\"!inCart\">\n        <form action=\"/order/add-ocb-order/\" method=\"POST\" class=\"ocb__form js-form js-ocb__form\">\n            <input type=\"hidden\" name=\"pid\" :value=\"product.id\">\n            <input type=\"hidden\" class=\"js-ocb__count\" name=\"count\" :value=\"currentCount\">\n            <input type=\"tel\" placeholder=\"+7 (921) 123-45-67\" class=\"ocb__input js-input js-required js-phone\" name=\"phone\">\n            <button type=\"submit\" class=\"ocb__btn js-one-click-buy\" onclick=\"\">Купить в 1 клик</button>\n            <div class=\"form__success js-form__success ocb__success\">\n                <div class=\"content\">Спасибо! Ваш заказ принят</div>\n            </div>\n        </form>\n    </div>\n    <!--BUY 1 MORE-->\n    <div class=\"summary__row\" v-else=\"\">\n        <a class=\"ga-event-add-product btn btn--lblue summary__add-to-basket  js-ga-add-to-cart\" v-if=\"remain > 0\" data-count=\"1\" data-id=\"product.id\" @click=\"currentCount = currentCount+1;addToCart(product.id,1);\">\n            <span class=\"plus\">+1 шт.</span>ДОБАВИТЬ ЕЩЁ\n        </a>\n    </div>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  if (!module.hot.data) {
    hotAPI.createRecord("_v-0231b4fd", module.exports)
  } else {
    hotAPI.update("_v-0231b4fd", module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}