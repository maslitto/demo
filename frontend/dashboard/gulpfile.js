'use strict';

function swallowError( error ){
	var date = new Date();
	console.error('[' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + '] Error: ' + error.message);
	this.emit('end');
}

var gulp = require('gulp'),
	watch = require('gulp-watch'),
	prefixer = require('gulp-autoprefixer'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-sass'),
	rigger = require('gulp-rigger'),
	fileinclude = require('gulp-file-include'),
	cssnano = require('gulp-cssnano'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	rimraf = require('rimraf'),
	browserSync = require("browser-sync"),
	plumber = require('gulp-plumber'),
	replace = require('gulp-replace'),
	htmlmin = require('gulp-htmlmin'),
	iconfont = require('gulp-iconfont'),
	iconfontCss = require('gulp-iconfont-css'),
    vueify = require('gulp-vueify'),
	runTimestamp = Math.round(Date.now() / 1000),
	reload = browserSync.reload;


var path = {
	build: {
		html:      '../../public/',
		index_dir: '../../public/',
		js:        '../../public/dashboard/js/',
		css:       '../../public/dashboard/css/',
		img:       '../../public/dashboard/img/',
		fonts:     '../../public/dashboard/fonts/',
		icon_font: '../../public/dashboard/fonts/svg_icons/'
	},
	src:   {
		html:      ['*.html', 'html/**/*.html'],
		js:        'js/*.js',
		jsVendor:  'js/vendor/*.js',
		style:     'scss/*.scss',
		img:       'img/**/*.*',
		fonts:     'fonts/**/*.*',
		index_dir: 'index_dir/**/*.*',
		svg:       'img/svg/*.svg',
		vue:       'components/**/*.vue',
		svg_font_source:       'img/svg-font/*.svg'
	},
	watch: {
		html:      ['*.html', 'html/**/*.html', 'html/**/*.tpl'],
		js:        'js/**/*.js',
		style:     'scss/**/*.scss',
		img:       'img/**/*.*',
		fonts:     'fonts/**/*.*',
		index_dir: 'index_dir/**/*.*',
		svg:       'img/svg/*.svg'
	},
	clean: './build'
};

gulp.task('clean', function ( cb ){
	rimraf(path.clean, cb);
});


gulp.task('js:build', function (){
	gulp.src(path.src.js)
		.pipe(plumber())
		.pipe(rigger())
		//.pipe(uglify())
		.pipe(gulp.dest(path.build.js))
		.pipe(reload({stream: true}));

	/*gulp.src(path.src.jsVendor)
		.pipe(plumber())
		//.pipe(uglify())
		.pipe(gulp.dest(path.build.js + 'vendor/'))
		.pipe(reload({stream: true}));
	*/
});

gulp.task('style:build', function (){
	gulp.src(path.src.style)
		.pipe(sass({
			sourceMap:       false,
			errLogToConsole: true,
			//outputStyle: 'expanded',
			outputStyle: 'compressed',
			includePaths:    require('node-bourbon').includePaths
		}).on('error', sass.logError))
		.pipe(prefixer())
		//.pipe(cssnano())
		.pipe(gulp.dest(path.build.css))
		.pipe(reload({stream: true}));
});

gulp.task('fonts:build', function (){
	gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts))
});


gulp.task('build', [
	'js:build',
	'style:build',
	'fonts:build'
]);

gulp.task('watch', function (){
	watch([path.watch.style], function ( event, cb ){
		gulp.start('style:build');
	});
	watch([path.watch.js], function ( event, cb ){
		gulp.start('js:build');
	});
});

gulp.task('default', ['build', 'watch']);
