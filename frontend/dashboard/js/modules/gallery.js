var gallery = (function () {
    var $gallery = $('.js-gallery'),
        $removeButton = $('.js-remove-image');
	return {
		init: function() {
            gallery.initDropZone();
            gallery.removeImage();
		},
        initDropZone: function() {
			console.log('gallery init');
			var $dropzone = $('.js-dropzone'),
            	url = $dropzone.data('url'),
                productId = $dropzone.data('product-id');
			$dropzone.dropzone({
				url: url,
                acceptedFiles: "image/*",
                //acceptedFiles: '.png.,.jpg,.jpeg,.gif',
                dictDefaultMessage: 'Перетащите в это окно файлы для загрузки',
                init: function() {
                    this.on('sending', function(file, xhr, formData){
                        formData.append('product_id', productId);
                    });
                },
                success: function (file, response) {
                    $gallery.append('<div class="col-sm-1">\n' +
                        '<div class="gallery__image">\n' +
                        '<a href="#" class="js-remove-image remove" data-id="'+response.id+'"><i class="fa fa-close"></i></a>\n' +
                        '<img src="'+response.image+'" alt="" class="img-responsive thumbnail">\n' +
                        '</div>\n' +
                        '</div>');
                }
            });

		},
        removeImage: function () {
            $(document).on('click','.js-remove-image',function(e){
                e.preventDefault();
                var id = $(this).data('id'),
                    url = $(this).data('url'),
                    $image = $(this).closest('.js-gallery-image')
                 ;
                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        id: id
                    },
                    success: function (response) {
                        $image.remove();
                    }
                });
            })
        }
	}
})();