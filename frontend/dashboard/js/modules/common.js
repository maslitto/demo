var common = (function () {
	return {
		init: function() {
            common.deleteButton();
            common.changeCity();
            common.initCKEditor();
            common.phoneMask();
		},

		deleteButton: function() {
            $('.js-delete-btn').click(function (e) {
                e.preventDefault();
                var currentElement = $(this);
                swal({
                        title: "Точно удалить?",
                        text: "Удаленная запись не подлежит восстановлению!",
                        type: "warning",
                        showCancelButton: true,
                        cancelButtonText: "Отмена",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "ОК",
                        closeOnConfirm: false

                    }, function () {
                        window.location.href = currentElement.attr('href');
                    }
                );
            });
		},

		changeCity: function () {
            $('.js-city-select-form').change(function () {
                this.submit();
            })
        },
        initCKEditor: function () {
            var editors = $('.js-ckeditor');
            $.each( editors, function( key, value ) {
                ClassicEditor
                    .create( value )
                    .catch( error => {
                        console.error( error );
                    } );
            });

        },
        /**
         * Добавляет маску на инпуты с типом phone
         */
        phoneMask: function () {
            var $phoneInput = $(".js-phone");
            if ($phoneInput.length) {
                $phoneInput.mask("+7 (999) 999-99-99");
            }
        },
	}
})();