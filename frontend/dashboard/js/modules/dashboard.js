var dashboard = (function () {
    var $metrikaBlock = $('.js-metrika-chart'),
        $ordersChart = $('.js-orders-chart'),
        $ordersChartAllVersions = $('.js-orders-all-versions-chart'),
        $sparkLines = $(".js-sparkline"),
        metrika = $('.js-metrika').data('metrika'),
        lastPeriodOrders = $ordersChart.data('period'),
        lastPeriodOrdersAllVersions = $ordersChartAllVersions.data('period');

	return {
		init: function() {
            //if(!$metrikaBlock.length){return;}
            if(!$ordersChart.length){return;}
            if(!$ordersChartAllVersions.length){return;}
            dashboard.initCharts();
            dashboard.initSparkLines();
            dashboard.promocodeAnalitics();
		},
        initCharts: function() {

            Morris.Bar({
                element: 'orders-chart',
                data: lastPeriodOrders,
                xkey: 'date',
                ykeys: ['count'],
                labels: ['Количество заказов'],
                hideHover: 'auto',
                resize: true,
                parseTime : false,
                barColors: ['#1ab394']
            });
            Morris.Bar({
                element: 'orders-chart-all-versions',
                data: lastPeriodOrdersAllVersions,
                xkey: 'date',
                ykeys: ['count'],
                labels: ['Количество заказов'],
                hideHover: 'auto',
                resize: true,
                parseTime : false,
                barColors: ['#1144aa']
            });

            /*Morris.Line({
                element: 'metrika',
                data: metrika,
                xkey: 'date',
                ykeys: ['count'],
                labels: ['Уникальных посетителей'],
                hideHover: 'auto',
                resize: true,
                parseTime : false,
                barColors: ['#1ab394']
            });*/
		},
		initSparkLines: function () {
            $.each( $sparkLines, function( index, $sparkline ){
                //console.log($sparkline);
                var percentsJSON = $(this).attr('data-percents'),
                    percents = JSON.parse(percentsJSON);
                console.log(percents);
                $(this).sparkline(percents, {
                    type: 'pie',
                    height: '140',
                    sliceColors: ['#1ab394', '#F5F5F5']
                });
            });
            /*var percentsJSON = $('.js-sparkline').attr('data-percents'),
                percents = JSON.parse(percentsJSON);
            $('.js-sparkline').sparkline(percents, {
                type: 'pie',
                height: '140',
                sliceColors: ['#1ab394', '#F5F5F5']
            });*/

        },
    promocodeAnalitics: function () {
      var data = JSON.parse($('#js-pie_data').attr('data-pie_data'));

        var options = {
      series: {
          pie: {
              show: true,
              radius: 1,
              label: {
                  show: true,
                  radius: 0.8,
                  threshold: 0.1,
                  formatter: function (label, series) {
                      return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">' + label + '<br/>' + series.data[0][1] + '</div>';
                      },
                      background: {
                          opacity: 0.3,
                          color: '#000'
                      }
              }
          }
      },
      legend:{
          container:$("#legend-container"),
      },
      grid: {
          // hoverable: true
      },
      tooltip: true,
      tooltipOpts: {
          cssClass: "flotTip",
          content: "%p.0%, %s",
          shifts: {
              x: 20,
              y: 0
          },
          defaultTheme: false
      }
  };

      $.plot($("#flot-pie-chart"), data, options);
    }
	}
})();
