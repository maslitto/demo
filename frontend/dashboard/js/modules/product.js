var product = (function () {

	return {
		init: function() {
            product.initPropsArea();
            product.initTagsArea();
		},
        initPropsArea: function() {
            $('.js-add-input-area1').addInputArea({
                area_var: '.js-add-input-area-row1',
                btn_add: '.js-area-add1',
                btn_del: '.js-area-remove1'
            });
		},
        initTagsArea: function () {
            $('.js-add-input-area2').addInputArea({
                area_var: '.js-add-input-area-row2',
                btn_add: '.js-area-add2',
                btn_del: '.js-area-remove2'
            });
        }
	}
})();