"use strict";

//= vendor/bootstrap.js
// vendor/plugins/codemirror/codemirror.js
//= vendor/plugins/metisMenu/jquery.metisMenu.js

// vendor/plugins/morris/raphael-2.1.0.min.js
//= ../node_modules/eve-raphael/eve.js
//= ../node_modules/raphael/raphael.js
//= ../node_modules/morris.js/morris.js
//= ../node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js
//= ../node_modules/dropzone/dist/dropzone.js
// ../node_modules/ace-code-editor/lib/ace/ace.js
//= ../node_modules/jquery.add-input-area/dist/jquery.add-input-area.min.js

//= vendor/flot/jquery.flot.min.js
//= vendor/flot/jquery.flot.pie.min.js
//= vendor/flot/jquery.flot.tooltip.min.js

//= vendor/plugins/morris/morris.js
//= vendor/plugins/pace/pace.min.js
//= vendor/plugins/slimscroll/jquery.slimscroll.js
//= vendor/plugins/sweetalert/sweetalert.min.js
//= vendor/plugins/sparkline/jquery.sparkline.min.js
//= vendor/inspinia.js
//= vendor/inputmask/inputmask.js

//= modules/gallery.js
//= modules/common.js
//= modules/dashboard.js
//= modules/product.js

var app = (function () {
	return {
		init: function () {
		    common.init();
			gallery.init();
			dashboard.init();
			product.init();

		}
	}
})();
app.init();

//$(document).on('ready', app.init);
