<?php
header('Content-Type: application/json');

$rand = rand(1,10);
$html = '';
$catalogs = [];
for($i = 0; $i <= $rand; $i++){
    $catalogs[] = [
        'name' => uniqid(),
        'url' => uniqid(),
        'id' => rand(1,32),
        'has_children' => (bool)random_int(0, 1)
    ];
}

$return = [
    'parent_id' => rand(0,1),
    'catalogs' => $catalogs
];
echo json_encode($return);