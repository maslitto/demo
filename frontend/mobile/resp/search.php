<?php
header('Content-Type: application/json');

//all
//echo '{"errors": {"all": ["custom error here"]}}';

//login field
$rand = rand(1,10);
$html = '[
{"label": "Производители", "type": "title"},
{"label": "КМИЗ", "url": "/catalog/brands/КМИЗ/", "type": "brand", "hide": false},
{"label": "КМИЗ", "url": "/catalog/brands/КМИЗ/", "type": "brand", "hide": false},
{"label": "КМИЗ", "url": "/catalog/brands/КМИЗ/", "type": "brand", "hide": true},
{"label": "Показать все", "type": "show-all", "target": "brand"},

{"label": "Каталог", "type": "title"},
{"label": "Дезинфекция", "url": "/catalog/dezinfektsiya/", "type": "category", "hide": false},
{"label": "Дезинфекция", "url": "/catalog/dezinfektsiya/", "type": "category", "hide": false},
{"label": "Ортопедия", "url": "/catalog/ortopediya/", "type": "category", "hide": true},
{"label": "Показать все", "type": "show-all", "target": "brand"},

{"label": "Теги", "type": "title"},
{"label": "Мягкий", "url": "/catalog/treyneryi/myagkiy/", "type": "ctag", "hide": false},
{"label": "Без каркаса", "url": "/catalog/treyneryi/bez-karkasa/", "type": "ctag", "hide": false},
{"label": "Показать все", "type": "show-all", "target": "ctag"},

{"label": "Товары", "type": "title"},
  {
    "count": "128",
    "id": "2182",
    "img": "/img/content/img1.jpg",
    "label": "Эндометазон N, НАБОР (Septodont)",
    "price": 2479,
    "type": "product",
    "url": "/catalog/endometazon-n-14-g/"
  },
  {
    "count": "128",
    "id": "2182",
    "img": "/img/content/img2.jpg",
    "label": "Эндометазон N, НАБОР (Septodont)",
    "price": 2479,
    "type": "product",
    "url": "/catalog/endometazon-n-14-g/"
  }
]';
$html = json_decode($html);
$return = [
    'html' => $html,
];
echo $return;