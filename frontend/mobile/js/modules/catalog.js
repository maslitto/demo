var catalog = (function () {
    var
        $chooseLink = $('.js-choose-link'),
        $chooseWindow = $('.js-choose-window'),
        $chooseWindowClose = $('.js-choose-window-close'),

        $filtersLink = $('.js-filters-link'),
        $filtersWindow = $('.js-filters-window'),
        $filtersWindowClose = $('.js-filters-window-close'),
        $submitFiltersLink = $('.js-submit-filters-link'),

        $form = $('.js-filter'),
        priceSlider =  document.getElementById('js-price-slider'),
        $priceInput = $('.js-price-input'),

        $catalogResult = $('.js-catalog__result'),
        $catalogBlock = $('.js-catalog'),
        $catalogBody = $('.js-catalog__body'),
        $filteredCount = $('.js-filter-count'),
        $catalogPage = $('.js-catalog-page')
    ;
    return {
        init: function(){
            catalog.chooseCategory();
            catalog.pageScroll();
            catalog.initFilters();
            
            catalog.rangeSlider();
            catalog.reset();
            catalog.addMoreRequest();
            catalog.submitFilter();
        },

        chooseCategory: function() {
            $chooseWindow.hide();
            $chooseLink.click(function (e) {
                e.preventDefault();
                $chooseWindow.show();
                $filtersLink.hide();
            });
            $chooseWindowClose.click(function (e) {
                e.preventDefault();
                $chooseWindow.hide();
                $filtersLink.show();
            });
        },

        pageScroll: function() {
            $(document).scroll(function() {
                var scrollTop = $(document).scrollTop();
                if(scrollTop > 5){
                    $filtersLink.html('<i class="icon-plus"></i>');
                    $filtersLink.addClass('catalog__filters-link--scrolled');
                } else{
                    $filtersLink.html('Фильтры');
                    $filtersLink.removeClass('catalog__filters-link--scrolled');
                }
            })
        },

        initFilters: function () {
            $catalogPage.val(1);
            $filtersLink.click(function (e) {
                e.preventDefault();
                $filtersLink.hide();
                $filtersWindow.show();
                $catalogResult.hide();
                $submitFiltersLink.show();
            });
            $filtersWindowClose.click(function (e) {
                e.preventDefault();
                $filtersWindow.hide();
                $submitFiltersLink.hide();
                $catalogResult.show();
                $filtersLink.show();
            })
        },

        submitFilter: function() {
            $submitFiltersLink.click(function(e) {
                e.preventDefault();
                //$form.submit();
                catalog.requestItems(catalog.collectData(), 'replace');
                $filtersWindow.hide();
                $catalogResult.show();
                $filtersLink.show();
            });
        },

        rangeSlider: function () {
            if(!priceSlider) {
                return;
            }

            var maxValue =  +priceSlider.getAttribute('data-max-price'),
                minValue =  +priceSlider.getAttribute('data-min-price'),
                startValue =  +priceSlider.getAttribute('data-start-price');

            noUiSlider.create(priceSlider, {
                start: startValue,
                step: 1,
                range: {
                    'min': minValue,
                    'max': maxValue
                }
            });
            priceSlider.noUiSlider.on('update', function(values){
                $priceInput.val((+values[0]).formatNum(0, ' '))
            });
            priceSlider.noUiSlider.on('change', function(){
                $form.trigger('change');
            });
            $priceInput.on('click', function(){
                $(this).val('');
            });
            $priceInput.on('change', function(){
                var value = $(this).val(),
                    valueNumber = +(value.replace(/\D+/g, '')),
                    valueToSet = 0;
                if (valueNumber > maxValue) {
                    valueToSet = maxValue;
                } else if (valueNumber < minValue) {
                    valueToSet = minValue;
                } else {
                    valueToSet = valueNumber;
                }
                priceSlider.noUiSlider.set(valueToSet);
                $(this).val(valueToSet.formatNum(0, ' '));
            });
        },

        collectData: function (page) {
            var data = $form.serializeArray();

            // Страница
            /*if (page) {
                data.push({
                    name: 'page',
                    value: page
                });
            }*/

            if(priceSlider) {
                // Цена
                var priceVal = '0;' + (+priceSlider.noUiSlider.get()).toFixed(0);
                data.push({
                    name: 'minMax',
                    value: priceVal
                });
            }

            return data;
        },

        requestItems: function(data, action) {
            $.ajax({
                type: 'GET',
                url: '/catalog/',
                data: data,
                dataType: 'json'

            }).success(function( resp ) {
                if (resp.html) {
                    catalog.updateList(resp.html, action);

                    var pageUrl = '?' + 'page=' + resp.page;
                    //window.history.pushState('', '', pageUrl);
                }
                if (resp.count) {
                    $filteredCount.text(resp.count)
                }
                if (resp.page) {
                    $catalogPage.val(resp.page);
                }
            });
        },

        updateList: function(data, action) {
            var $items = $(data);
            var old_scroll = $(window).scrollTop();
            $('.js-catalog__footer').remove();

            if (action === 'add') {
                $items.appendTo($catalogResult).hide();
                $items.fadeIn(300);
                $(window).scrollTop(old_scroll);

            } else if (action === 'replace') {
                $('html').animate({scrollTop: $catalogBody.offset().top - 143 }, 400);
                $catalogResult.fadeOut(300, function() {
                    $catalogResult.html($items).fadeIn(300);
                });

            }
            $(window).trigger('update-impressions');
        },

        reset: function() {
            $('.js-filter-reset').click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                $form.trigger('reset');
                priceSlider.noUiSlider.set(priceSlider.dataset.maxPrice);
            });
        },

        addMoreRequest: function() {
            $catalogBlock.on('click', '.js-filter-add', function(e) {
                var page = Number($catalogPage.val());
                $catalogPage.val(page + 1);
                e.preventDefault();
                e.stopPropagation();
                catalog.requestItems(catalog.collectData(page), 'add');
            });
        }


    }
})();