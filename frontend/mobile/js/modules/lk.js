var lk = (function () {
	var $order = $('.js-orders'),
		$form = $('.js-lk-company-info'),
		$inn = $('.js-company-inn'),
		$name = $('.js-company-name'),
		$kpp = $('.js-company-kpp'),
		$ogrn = $('.js-company-ogrn'),
		$factAddress = $('.js-company-fact-address'),
		$legalAddress = $('.js-company-legal-address'),
		$okved = $('.js-company-okved')
	;
	return {
		init: function () {
			lk.order();
			//lk.education();
			lk.referal();
			lk.changeInn();
			lk.changeFile();
			lk.initSelect2();
		},
		order: function () {
			if (!$order.length) {
				return;
			}
			$('.js-order-num').click(function(e){
				e.preventDefault();
				e.stopPropagation();
				$(this).toggleClass('active').closest('.js-order-info').next('.js-order-detail').slideToggle(300)
			});

		},

		education: function () {
			if (!$order.length) {
				return;
			}
			$('.js-education-num').click(function(e){
				e.preventDefault();
				e.stopPropagation();
				$(this).toggleClass('active').closest('.js-education-info').next('.js-education-detail').slideToggle(300)
			});

		},
		referal: function () {
			$('.js-copy-to-clipboard').click(function(e){
				var $target = $($(this).data('target'));
				console.log($target);
				e.preventDefault();
				helpers.toClipboard($target);
			});
		},
        changeInn: function () {
            $inn.click(function(e){
                var inn = $(this).val();
                $.ajax({
                    type: 'POST',
                    url: '/personal/get-info-by-inn/',
                    data: {inn : inn},
                    dataType: "json",
                    success: function(resp) {
                        console.log(resp);
						$kpp.val(resp.data.kpp);
						$ogrn.val(resp.data.ogrn);
						$okved.val(resp.data.okved);
						$name.val(resp.value);
						$legalAddress.val(resp.data.address.value);
                    },
                    error: function() {
                        //forms.showErrorMessage($form, [$form.data('error-msg')]);
                        //$form.trigger('form_error', resp);
                    }
                });
            });
        },
		changeFile: function () {
            $('input[type="file"]').change(function() {
                // find the label for the currrent file input
                $('label[for="' + this.id + '"]').text('Прикрепить фотографию лектора ' + $(this).val());
            });
        },
		initSelect2: function () {
            $('.js-select2').select2({
                language: "ru",
                minimumResultsForSearch: -1,
                placeholder: 'Буду проводить'
            });
        }
	}
})();