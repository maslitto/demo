var tabs = (function () {
    var $tabs = $('.js-tabs');
	return {
		init: function () {
			tabs.tabsSwitchAction();
			tabs.gotoTab();
            $('.js-tabs-nav').slick({
                slidesToShow: 3,
                centerMode: true,
                dots: false,
                arrows: false,
                variableWidth: true
            });

		},
		initTabsIndexes: function(){

		},
		tabsSwitchAction: function() {
			$tabs.each(function() {
				var $tabsWrap = $(this),
					$tabLinks = $tabsWrap.find('.js-tabs__link');
				$tabLinks.each(function (index, value) {
                    $(value).attr('data-index',index);
                    console.log($(value).data('index'));
                });
				$tabLinks.click(function(){
					var $currentTabLink   = $(this);
					tabs.changeTab($currentTabLink);
				});
			});

		},
		changeTab: function($tabLinkToSwitch) {
			var currentUrl   = $tabLinkToSwitch.data('url'),
				currentTitle = $tabLinkToSwitch.data('title'),
				$tabsWrap    = $tabLinkToSwitch.closest('.js-tabs'),
				$tabLinks    = $tabsWrap.find('.js-tabs__link'),
				$tabs 	     = $tabsWrap.find('.js-tabs__tab'),
				$index       = $tabLinkToSwitch.closest('.slick-active').data('slick-index')
			;
			$tabLinks.removeClass('active');
			$tabLinkToSwitch.addClass('active');
			$tabs.removeClass('active');
			$tabs.eq($tabLinkToSwitch.data('index')).addClass('active');
			if($tabs.data('name') == 'product-tabs'){
                History.replaceState({}, currentTitle, currentUrl);
			}
		},
		gotoTab: function() {
			$(document).on('click', '.js-tab-switch', function() {
			    var $switchHandler = $(this),
			    	tabWrapName    = $switchHandler.data('wrap'),
			    	tabName 	   = $switchHandler.data('tab'),
			    	$tabsWrap 	   = $('.js-tabs[data-name="' + tabWrapName + '"]');

			    if($tabsWrap.length) {
			    	var $tabLinkToSwitch = $tabsWrap.find('.js-tabs__link[data-name="' + tabName + '"]');
			    	tabs.changeTab($tabLinkToSwitch);

			    	$('html, body').animate({
			    	    scrollTop: $($tabsWrap).offset().top - 160
			    	}, 'normal');
			    }

			        
			});
		}
	}
})();