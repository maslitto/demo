var productVue = (function () {
    return {
        init: function () {
            productVue.product();
            productVue.showBlock();
        },
        product: function () {
            return new Vue({
                el: '#product-vue',
                data:  typeof vueInject === 'undefined' ? {} : vueInject,
                delimiters: ['${', '}'],
                /*beforeMount: function() {
                    //console.log(this.$el.attributes['data-vue-inject'].value);
                    this.data = JSON.parse(this.$el.attributes['data-vue-inject'].value);
                },*/
                computed: {
                    remain: function () {
                        var remain = this.product.count - this.currentCount;
                        if(remain < 0) {remain = 0;}
                        return remain;
                    }
                },
                methods: {
                    formatPrice: function(value) {
                        return value;
                        //return number_format(value, 0, '.', '');
                    },
                    setCurrentCount: function(operation) {
                        if(operation == 'plus'){
                            this.currentCount += 1;
                        } else if(operation == 'minus'){
                            this.currentCount -= 1;;
                        }

                        if(this.currentCount < 1){
                            this.currentCount = 1;
                        }
                        if((this.currentCount > this.product.count) && (this.product.count > 0)){
                            this.currentCount = this.product.count;
                        }
                        if(this.product.count == 0){
                            this.currentCount = 1;
                        }
                        return this.currentCount;
                    },
                    addToCart: function(productId, count){
                        this.inCart = true;
                        var data = {
                            id:    productId,
                            count: count
                        };
                        console.log(data);
                        var ret = $.cart.add(data);
                        //yaCounter23244307.reachGoal('KORZINA');
                        return false;
                    }
                }
            })
        },
        showBlock: function () {
            $('.js-product--detail').show();
        }
    }
})();