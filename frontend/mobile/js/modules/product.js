var product = (function () {
    var
        $ocbDummy = $('.js-ocb-dummy'),
        $ocb = $('.js-ocb__form')
    ;
    return {
        init: function(){
            product.initOcb();

        },

        initOcb: function() {
            $ocbDummy.click(function (e) {
                e.preventDefault();
                $(this).hide();
                $ocb.show();
            });
        }

    }
})();