var search = (function () {
    var $searchInit = $('.js-search-init'),
        $searchWindow = $('.js-search-window'),
        $search = $('.js-search'),
        $searchBody = $('.js-search-body'),
        $close = $('.js-search-window-close'),
        $brands 	   = $('.js__results-brands'),
        $catalog 	   = $('.js__results-catalog'),
        $tags 		   = $('.js__results-tags'),
        $products 	   = $('.js__results-products')
    ;
    return {
        init: function() {
            $searchInit.click(function (e) {
                $searchWindow.show();
                $search.focus();
            });
            $close.click(function (e) {
                e.preventDefault();
                $searchWindow.hide();
            });
            $search.keyup(function (e) {
                var query = $search.val();
                if(query.length > 1){
                    $searchWindow.addClass('inited');
                    search.search(query);
                } else{
                    search.clearAll();
                    $searchWindow.removeClass('inited');
                }

            })
        },
        search: function (query) {

            $.ajax({
                url: '/search/',
                type: 'GET',
                dataType: 'json',
                data: {
                    query: query
                },
                success: function(data) {

                    var yandexMetrikaId = data.yandexMetrikaId,
                        tags = data.tags,//JSON.parse(data.tags.replace(/&quot;/g,'"')),
                        products = data.products,
                        brands = JSON.parse(data.brands.replace(/&quot;/g,'"')),
                        catalogs = JSON.parse(data.catalogs.replace(/&quot;/g,'"'));
                    search.clearAll();
                    /*START TAGS*
                    if(tags.length > 0){
                        $.each(tags, function( index, item ) {
                            $tags.show();
                            if (item.active) {
                                $tags.append('<a href="/catalog/'+item.url+'" class="tag tag--small">#'+item.name+'</a>');
                            }
                        })
                    }
                    /*END OF TAGS*/

                    /*START BRANDS
                    if(brands.length > 0){
                        $.each(brands, function( index, item ) {
                            var $element = $('<a></a>');
                            $brands.show();
                            $element
                                .text(item.name)
                                .attr('href','/catalog/brands/' + item.url)
                                .data('name', item.name)
                                .addClass('h-search__item-link')
                                .addClass('h-search__item-switch')
                                .addClass('js__item-switch');
                            if (item.hide) {
                                $element.hide();
                                $element.addClass('additional');
                            }
                            $brands.append($element);
                        })
                    }
                    /*END OF BRANDS*/

                    /*START CATALOGS*/
                    if(catalogs.length > 0){
                        $.each(catalogs, function( index, item ) {
                            var $element = $('<a></a>');
                            $catalog.show();
                            $element
                                .text(item.name)
                                .attr('href','/catalog/' + item.urlPath)
                                .data('name', item.name)
                                .addClass('search-window__item-link search-window__item-switch js__item-switch');
                            if (item.hide) {
                                $element.hide();
                                $element.addClass('additional');
                            }
                            $catalog.append($element);
                        })
                    }
                    /*END OF CATALOGS*/

                    /*START PRODUCTS*/
                    if(products.length > 0){
                        $.each(products, function( index, item ) {
                            var $element = $('<a></a>');
                            $products.show();
                            var $info = $('<div class="search-window__item-info"></div>');

                            $info.append('<div class="search-window__item-name">' + item.name + '</div>');
                            $info.append('<span class="search-window__item-price">' + item.price + ' </span>');

                            if(Number(item.count) > 0) {
                                $info.append('<span class="search-window__item-availability search-window__item-availability--available">В наличии</span>');
                            }
                            else {
                                $info.append('<span class="search-window__item-availability search-window__item-availability--not-available">Нет в наличии</span>');
                            }


                            $element
                                .attr('href', item.url)
                                .addClass('search-window__item')
                                .addClass('search-window__item-switch')
                                .addClass('js__item-switch')
                                //.addClass('js-ga-impression')
                                //.addClass('js-ga-proceed')
                                //.addClass('js-ga-search-item')
                                //.addClass('js-ga-product-card')
                                .data('name', item.label)
                                .append('<div class="search-window__item-img"><img src="' + item.image + '" alt=""></div>')

                                .append($info);

                            if (item.hide) {
                                $element.hide();
                            }
                            $products.append($element);
                        })
                    }
                    /*END OF PRODUCTS*/

                    $(window).trigger('update-impressions');
                }
            })
        },
        clearAll: function() {
            //$brands.empty().hide();
            $catalog.empty().hide();
            $products.empty().hide();
            //$tags.empty().hide();
        }
    }
})();