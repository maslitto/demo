var googleAnalytics = (function (){
    return {
        init: function() {
            try {
                if(!googleAnalytics.haveDataLayer()) {
                    throw 'dataLayer was not initialized';
                }

                googleAnalytics.productImpressions();
                googleAnalytics.productItem();
                googleAnalytics.cart();
                googleAnalytics.productPage();
            }
            catch(err) {
                console.log(err);
            }
        },
        haveDataLayer: function() {
            if (window.dataLayer !== undefined) {
                return true;
            }
            else {
                return false;
            }
        },
        productItem: function() {

            //add product card to cart
            $('body').on('click', '.js-ga-add-to-cart', function() {
                var $addHandler  = $(this),
                    $wrapperCard = $addHandler.closest('.js-ga-product-card'),
                    $countInput  = $wrapperCard.find('.js-ga-count'),
                    productData  = $wrapperCard.data('ga-props');

                if($countInput.length) {
                    productData.quantity = $countInput.val();
                }
                else {
                    productData.quantity = 1;
                }


                if(!$addHandler.hasClass('js-ga-in-cart')) {
                    dataLayer.push({
                        'event': 'addToCart',
                        'ecommerce': {
                            'currencyCode': 'RUB',
                            'add': {
                                'products': [productData]
                            }
                        }
                    });
                }
                else {
                    $addHandler.addClass('js-ga-in-cart');
                }
            });

            //proceed to product page from card
            $('body').on('click', '.js-ga-proceed', function() {
                var $linkItem    = $(this),
                    $wrapperCard = $linkItem.closest('.js-ga-product-card'),
                    productData = $wrapperCard.data('ga-props');

                dataLayer.push({
                    'event': 'productClick',
                    'ecommerce': {
                        'click': {
                            'actionField': {'list': googleAnalytics.getListName()},
                            'products': [productData]
                        }
                    },
                    'eventCallback': function() {
                        //document.location = $linkItem.attr('href');
                    }
                });
            });
        },
        productImpressions: function() {
            $(window).on('update-impressions', function() {
                if($('.js-ga-impression').length) {
                    var productsData = googleAnalytics.getJsonProductsData('js-ga-impression', 'impression-active');

                    dataLayer.push({
                        'ecommerce': {
                            'currencyCode': 'RUB',
                            'impressions': [productsData]
                        }
                    });
                }
            });

            $(window).trigger('update-impressions');
        },

        cart: function() {
            var lastChangedItemQty;

            $(document).on('click', '.js-ga-cart-item__remove', function() {
                var $removeHandler = $(this),
                    $wrapperCard = $removeHandler.closest('.js-ga-cart-item'),
                    productData = $wrapperCard.data('ga-props'),
                    $countInput = $wrapperCard.find('.js-ga-cart-item__count'),
                    quantity = $countInput.val();

                productData.quantity = quantity;

                dataLayer.push({
                    'event': 'removeFromCart',
                    'ecommerce': {
                        'remove': {
                            'products': [productData]
                        }
                    }
                });
            });


            $(document).on('focus', '.js-ga-cart-item__count', function() {
                lastChangedItemQty = $(this).val();
            });

            $(document).on('change', '.js-ga-cart-item__count', function() {
                var $countInput = $(this),
                    $wrapperCard = $countInput.closest('.js-ga-cart-item'),
                    productData = $wrapperCard.data('ga-props'),
                    currentQty = $countInput.val(),
                    changedQty = Math.abs(currentQty - lastChangedItemQty),
                    eventName;

                if(currentQty > lastChangedItemQty)  {
                    eventName = 'addToCart';
                }
                else {
                    eventName = 'removeFromCart';
                }

                productData.quantity = changedQty;

                dataLayer.push({
                    'event': eventName,
                    'ecommerce': {
                        'currencyCode': 'RUB',
                        'add': {
                            'products': [productData]
                        }
                    }
                });

            });
        },
        orderPurchase: function(orderId, orderSumm) {
            dataLayer.push({
                'event': 'ee-purchase',
                'ecommerce': {
                    'currencyCode': 'RUB',
                    'purchase': {
                        'actionField': {
                            'id': orderId,
                            'revenue': String(orderSumm.toFixed(2)),
                            'affiliation': 'desktop version',
                        },
                        'products': googleAnalytics.getJsonProductsData('js-ga-cart-item')
                    }
                },
            });
        },
        getJsonProductsData: function(productsClass, activeClass) {
            var $productsItems = $('.' + productsClass),
                jsonContainer = [];

            if(undefined !== activeClass) {
                $productsItems = $productsItems.not('.' + activeClass);
            }

            $productsItems.each(function() {
                var $productItem = $(this),
                    props = $productItem.data('ga-props');


                if($productItem.hasClass('js-ga-search-item')) {
                    props.list = 'Товары из всплывающей строки поиска';
                }
                else {
                    props.list = googleAnalytics.getListName();
                }

                jsonContainer.push(props);

                if(undefined !== activeClass) {
                    $productItem.addClass(activeClass);
                }
            });

            return jsonContainer;
        },
        productPage: function() {

            if($('.js-ga-product-view').length) {
                dataLayer.push({
                    'ecommerce': {
                        'detail': {
                            'products': googleAnalytics.getJsonProductsData('js-ga-product-view')
                        }
                    }
                });
            }
        },
        getListName: function() {
            var $listItem = $('.js-ga-list-name').last(),
                listName;

            if($listItem.length &&
                (typeof $listItem.data('name') !== 'undefined') &&
                '' != $listItem.data('name')
            ) {

                listName = $listItem.data('name');
            }
            else {
                listName = 'unnamed list';
            }

            return listName;
        }
    }
})();