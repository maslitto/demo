var toggle = (function () {
	return {
		init: function() {
			toggle.expandBlock();
		},
		expandBlock: function() {
  			var $links = $(".js-toggle-link");


			if ($links.length) {

				$links.on('click', function(event) {
					event.preventDefault();
					var $current = $(this),
						type	 = $current.data('type'),
						$elem,
						$arrow;

					if($(event.target).hasClass('js-toggle-exception')) {
						return;
					}

					if('arrow' === type) {
						$current = $current.parent();
					}

					$elem  = $current.next('.js-toggle-body');
					$arrow = $current.find('.js-toggle-arr');

					$current.removeClass('active')
					$elem.slideToggle(400);

					if($arrow.length) {
						if($arrow.hasClass('icon-arr-d')) {
							$arrow.removeClass('icon-arr-d').addClass('icon-arr-up');
						}
						else if($arrow.hasClass('icon-arr-up')) {
							$arrow.removeClass('icon-arr-up').addClass('icon-arr-d');
						}
					}
				});
			}
		},
	}
})();