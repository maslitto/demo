var menu = (function () {
    var $header = $('.js-header'),
        $menuIcon = $('.js-menu_icon'),
        $menu = $('.js-side_menu'),
        $menuTitle = $('.js-menu__title'),
        $menuBack = $('.js-menu-back'),
        $menuCatalogBack = $('.js-menu-catalog-back'),
        $html = $('.js-html_wrap'),
        $nextLevelLink = $('.js-next-level-link'),
        $catalogMenu = $('.js-catalog-menu'),
        isOpen = false,
        fullWidthClass = 'menu--full-width'
    ;
    return {
        init: function() {
            menu.openCloseHandle();
            menu.layerSwitch();
            menu.catalog();
        },
        openCloseHandle: function() {
            var $menuOpen = $header.find('.js-menu_open'),
                $menuClose = $header.find('.js-menu_close');

            $menuOpen.click(function() {
                menu.open();
            });

            $menuClose.click(function(e) {
                e.preventDefault();
                menu.close();
            });


        },
        open: function() {
            $menuIcon
                .add($menu)
                .addClass('open');

            $html
                .add($header)
                .addClass('open-menu');

            isOpen = true;

        },
        close: function() {
            $menuIcon
                .add($menu)
                .removeClass('open');

            $html
                .add($header)
                .removeClass('open-menu');

            isOpen = false;
        },
        layerSwitch: function() {
            $('.js-menu_layer_link').on('click', function(event) {
                event.preventDefault();

                var target = $(this).attr('href');

                if($(target).length) {

                    if($(target).data('fw')==true){
                        $menu.addClass(fullWidthClass);
                    } else {
                        $menu.removeClass(fullWidthClass);
                    }
                    $('.js-menu_layer').removeClass('active');
                    $(target).addClass('active');

                    if('#menu-main' !== target) {
                        $('.js-menu-back').show();
                        $menuTitle.text($(this).text());
                        $menuTitle.show();
                    }
                    else {
                        $('.js-menu-back').hide();
                        $menuTitle.text('');
                        $menuTitle.show();
                    }
                }
            });
        },
        catalog: function () {
            $(document).on('click', '.js-next-level-link', function(e) {
                e.preventDefault();
                //$menuTitle.text($(this).text());
                var catalogId = $(this).data('id');
                $.ajax({
                    type: 'GET',
                    url: '/catalog/get-menu',
                    data: {catalogId : catalogId},
                    dataType: "json",
                    success: function(response) {
                        $catalogMenu.empty();
                        var catalogs = response.catalogs,
                            catalogId = response.catalogId,
                            level = response.level,
                            parentId = response.parentId
                        ;
                        if(level > 0){
                            $menuBack.hide();
                            $menuCatalogBack.show();
                            $menuCatalogBack.data('id',parentId);
                        } else{
                            //если level = 0
                            $menuBack.show();
                            $menuCatalogBack.hide();
                        }
                        $catalogMenu.append(catalogs);

                    },
                    error: function(response) {

                    }
                });
            });

        }
    }
})();