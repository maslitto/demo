var accountEvents = (function () {
    return {
        init: function() {
            accountEvents.loginSuccess();
            accountEvents.registrationSuccess();
        },
        loginSuccess: function() {
            $('.js-login_form').on('form_success', function() {
                window.location.replace("/personal/");
            });
        },
        registrationSuccess: function() {
            $('.js-registration_form').on('form_success', function() {
                window.location.replace("/personal/");
            });
        }
    }
})();