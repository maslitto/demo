
var sliders = (function () {
    var
        $indexSlider = $('.js-index-slider'),
        $productSlider = $('.js-product-slider'),
        $additionalProductsSlider = $('.js-additional-products-slider')
    ;
    return {
        init: function(){
            sliders.indexSlider();
            sliders.productSlider();
            sliders.additionalProductsSlider();
        },
        indexSlider: function () {
            $indexSlider.slick({
                slidesToShow:1,
                dots: true
            });
        },
        productSlider: function() {
            $productSlider.slick({
                dots : true,
                dotsClass : 'product__slider-dots'
            });

        },
        additionalProductsSlider: function() {
            $additionalProductsSlider.slick({
                dots : false,
                prevArrow : '<button type="button" class="icon-arrow-prev icon"></button>',
                nextArrow : '<button type="button" class="icon-arrow-next icon"></button>'
            });

        }

    }
})();