var popup = (function () {

	return {
		init: function () {
			popup.simplePopup();
			popup.iframePopup();
			popup.ajaxPopup();
			popup.galleryPopup();
		},
		simplePopup: function() {
			var $popupLink = $('.js-open-popup');

			if (!$popupLink.length) {
				return;
			}
			$popupLink.magnificPopup({
				// Delay in milliseconds before popup is removed
				removalDelay: 300,
				// Class that is added to popup wrapper and background
				// make it unique to apply your CSS animations just to this exact popup
				mainClass: 'mfp-fade',
				overflowY: 'scroll',
				fixedContentPos: 'true',
				callbacks: {
					open: function() {
						$('.js-header').toggleClass('popup-open');
						$('.js-info-panel').toggleClass('popup-open');
					},
					close: function() {
						$('.js-header').toggleClass('popup-open');
						$('.js-info-panel').toggleClass('popup-open');
					}
				}
			});
		},
		iframePopup: function() {
			var $popupLink = $('.js-open-popup-iframe');

			$popupLink.magnificPopup({
				type: 'iframe',

			});
		},
		ajaxPopup: function() {
			var $popupLink = $('.js-open-popup-ajax');

			/*$popupLink.on('click', function(e) {
				e.preventDefault();
				console.log('zzz');
			});*/

			$popupLink.magnificPopup({
				type: 'ajax',
			});
		},
		galleryPopup: function() {
			$('.js-fancybox').fancybox({
                helpers: {
                    overlay: {
                        locked: false
                    }
                },
                wrapCSS : 'fancy-basket',
                beforeLoad: function() {
                },
                beforeClose: function() {
                }
            });
		}
	}
})();