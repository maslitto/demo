"use strict";

//= ../bower_components/slick-carousel/slick/slick.js
//= ../bower_components/nouislider/distribute/nouislider.js
//= ../bower_components/magnific-popup/dist/jquery.magnific-popup.js
// ../bower_components/history.js/scripts/uncompressed/history.adapter.jquery.js
// ../bower_components/history.js/scripts/uncompressed/history.js
//= ../bower_components/fancybox/source/jquery.fancybox.pack.js

//= ../node_modules/simplebar/dist/simplebar.js
//= ../node_modules/select2/dist/js/select2.js
//= ../node_modules/pickmeup/js/pickmeup.js

//= ../node_modules/vue/dist/vue.min.js
// ../node_modules/vue/dist/vue.common.js
// ../node_modules/vue/dist/vue.esm.js
// ../node_modules/vue/dist/vue.esm.browser.js
// ../node_modules/vue/dist/vue.runtime.common.js
//= ../node_modules/pickmeup/js/pickmeup.js


// vendor/masonry.pkgd.min.js
// vendor/sodselect/selectordie.js
//= vendor/inputmask/inputmask.js
// vendor/sticky-kit/sticky-kit.min.js

//= modules/helpers.js
//= modules/citySwitch.js
//= modules/sliders.js
//= modules/search.js
//= modules/cart.js
//= modules/menu.js
//= modules/forms.js
//= modules/accountEvents.js
//= modules/catalog.js
//= modules/toggle.js
//= modules/tabs.js
//= modules/product.js
//= modules/product.vue.js
//= modules/basket.js
//= modules/popup.js
//= modules/lk.js
//= modules/maps.js
//= modules/login.js
//= modules/educationCatalogFilter.js
//= modules/googleAnalytics.js
//= modules/convead.js
// ../dist/product.js

var app = (function () {
	return {
		init: function () {
            //Страница каталога
            product.init();

            //Страница каталога
            productVue.init();

		    //личный кабинет
            lk.init();

			// Вспомогательное
			helpers.init();

			// Переключение городов
			citySwitch.init();

			// Слайдеры
			sliders.init();

			// Поиск
			search.init();

			// Корзина
			cart.init();

			// Шапка сайта
			menu.init();

			// Формы
			forms.init();

			// Вход, регистрация, восстановление пароля
            accountEvents.init();

            //Страница каталога
            catalog.init();

            //Страница каталога
            toggle.init();

            //tabs
            tabs.init();

            //basket
            basket.init();

            //popups
            popup.init();

            //maps
            maps.init();

            //education
            educationCatalogFilter.init();

            //education
            googleAnalytics.init();

            conveadApp.init();
		}
	}
})();

$(document).on('ready', app.init);






