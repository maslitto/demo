<div class="popups">
	<!-- Форма добавления отзыва -->
	<div id="add__review" class="js-popup popup-block mfp-hide" >
		<form action="/review/add-review/" novalidate class="js-form" data-error-msg="Произошла ошибка">
		<div class="form__title">Новый отзыв</div>
		<p class="form__sub-title">Ирригатор Donfeel OR-820D compact</p>
			<div class="form__group">
				<div class="form__b form__text-input-b">
					<input type="text" class="form__text-input js-input js-required" name="customer_name" id="rec_name">
					<label class="form__text-input-label" for="rec_name">Как вас зовут?</label>
					<!--<span class="form__input-error">Обязательное поле</span>-->
				</div>
				<div class="form__b form__text-input-b">
					<textarea class="form__text-input form__text-input--textarea js-input js-required" name="customer_name" id="rec_text"></textarea>
					<label class="form__text-input-label" for="rec_name">Комментарий</label>
					<!--<span class="form__input-error">Обязательное поле</span>-->
				</div>
			</div>
			<div class="form__group">
				<div class="form__rating">
					<input class="form__rating-input" type="radio" id="rating-5" name="rec_rating" value="5" />
					<label class="form__rating-label" for="rating-5"></label>
					<input class="form__rating-input" type="radio" id="rating-4" name="rec_rating" value="4" />
					<label class="form__rating-label" for="rating-4"></label>
					<input class="form__rating-input" type="radio" id="rating-3" name="rec_rating" value="3" />
					<label class="form__rating-label" for="rating-3"></label>
					<input class="form__rating-input" type="radio" id="rating-2" name="rec_rating" value="2" />
					<label class="form__rating-label" for="rating-2"></label>
					<input class="form__rating-input" type="radio" id="rating-1" name="rec_rating" value="1" />
					<label class="form__rating-label" for="rating-1"></label>
				</div>
			</div>
			<button type="submit" class="btn btn--dark btn--full-w">Отправить отзыв</button>
			<div class="form__success js-form__success">
				<div class="content">
					<p class="h1">Сообщение отправлено</p>
					<p class="h2">Отзыв появится после модерации</p>
				</div>
			</div>
		</form>
	</div>
	<!-- Доставка в день заказа -->
	<div id="on_the_day_delivery" class="js-popup mfp-hide popup-block">
		<div class="popup-block__title">Доставка в день заказа</div>
		<ul>
			<li>Срочная доставка осуществляется по рабочим дням (с пн-пт).</li>
			<li>Заказ необходимо оформить до 15:00 по Московскому времени.</li>
			<li>Вес груза не должен превышать 10 кг и быть больше 0.1 м3 (обычная
				коробка 10×100 см, так как ее доставляет «пеший» курьер).
			</li>
			<li>Если сумма вашего заказа превышает 7 000 ₽, то срочная доставка
				в день заказа — бесплатна, если сумма счета меньше 7 000 ₽, то сумма
				срочной доставки составляет 300 рублей.
			</li>
			<li>Срочная доставка осуществляется только в пределах КАД.</li>
		</ul>
	</div>
	<!-- Доставка на следующий день -->
	<div id="next_day_delivery" class="js-popup mfp-hide popup-block">
		<div class="popup-block__title">Доставка в Санкт-Петербурге</div>

		<p><strong>Доставка по&nbsp;Санкт-Петербургу (в&nbsp;пределах КАД) возможна при сумме заказа свыше 2&nbsp;000&nbsp;₽</strong></p>
		<p>Если сумма вашего заказа от 2000&nbsp;до 3000 рублей — сумма доставка 300 р.</p>
		<p>Заказы свыше 3000 рублей мы доставляем бесплатно.</p>

		<div class="popup-block__title">Доставка по Москве</div>
		<p>Доставка осуществляется курьером транспортной компании.</p>
		<p>Стоимость доставки в пределах МКАД при заказе на сумму:</p>
		<p>До 3 т.р. - стоимость доставки 350 рублей.</p>
		<p>От 3 до 7 т.р. - стоимость доставки составляет 250 рублей.</p>
		<p>От 7 т.р. - доставка производится бесплатно.</p>

		<div class="popup-block__title">Доставка по Димитровграду</div>
		<p>Доставка осуществляется курьером.</p>
		<p>До 3 т.р. - стоимость доставки 350 рублей.</p>
		<p>От 3 до 7 т.р. - стоимость доставки составляет 250 рублей.</p>
		<p>От 7 т.р. - доставка производится бесплатно.</p>

		<div class="popup-block__title">Доставка в регионы</div>
		<p>Осуществляется по 100% предоплате и любой транспортной компанией на ваш выбор. При сумме заказа менее 30 000 р. оплата осуществляется за счет покупателя.</p>

		<div class="popup-block__title">Бесплатная доставка по РФ</div>
		<p>Оплату доставки заказа до вашего города берет на себя компания «Стоммаркет» при следующих условия:</p>
		<ul>
			<li>Сумма заказа в&nbsp;интернет-магазине должна превышать 30 000 р.</li>
			<li>Вес груза должен быть не более 20 кг;</li>
			<li>Объем груза не&nbsp;превышает 0,1 м3;</li>
			<li>Не использован бонус интернет-магазина.</li>
		</ul>
		<p><em>Бесплатная доставка осуществляется транспортной компанией «Деловые линии».</em></p>
	</div>
	<!-- Форма оформления заказа -->
	<div id="order-form" class="js-popup mfp-hide popup-block">
		<form action="/order/add-order/" class="form js-form js-order-form" novalidate data-error-msg="Произошла ошибка">
			<div class="form__title">Оформление заказа</div>
			<div class="form__group">
				<div class="form__b form__text-input-b">
					<input type="text" class="form__text-input js-input js-required" name="customer_name" id="customer_name">
					<label class="form__text-input-label" for="customer_name">Имя<span>*</span></label>
				</div>
				<div class="form__b form__text-input-b">
					<input type="num" placeholder="+7 (___) ___-__-__" class="form__text-input js-input js-required js-phone" name="customer_phone" id="customer_phone">
					<label class="form__text-input-label" for="customer_phone">Телефон<span>*</span></label>
				</div>
				<div class="form__b form__text-input-b">
					<input type="email" class="form__text-input js-input js-validate" name="customer_email" id="customer_email">
					<label class="form__text-input-label" for="customer_email">Почта</label>
				</div>
			</div>
			<div class="form__group">
				<div class="form__sub-title">Доставка</div>
				<div class="form__b form__radio-input-b">
					<input class="form__radio-input js-input js-close-delivery" type="radio" name="customer_delivery" value="self" id="customer_delivery_self" checked>
					<label class="form__radio-input-label" for="customer_delivery_self">Самовывоз — бесплатно</label>
					<input class="form__radio-input js-input js-open-delivery" type="radio" name="customer_delivery" value="city" id="customer_delivery_city">
					<label class="form__radio-input-label js-order-del-spb" for="customer_delivery_city">Доставка по СПБ — <span class="js-price-val">300 ₽</span></label>
					<input class="form__radio-input js-input js-open-delivery" type="radio" name="customer_delivery" value="country" id="customer_delivery_country">
					<label class="form__radio-input-label js-order-del-rus" for="customer_delivery_country">Доставка по РФ — <span class="js-price-val">300 ₽</span></label>
				</div>
				<div class="form__b form__text-input-b hidden js-delivery-address">
					<textarea class="form__text-input form__text-input--textarea js-input" name="customer_address" id="customer_address"></textarea>
					<label class="form__text-input-label" for="customer_address">Адрес доставки</label>
				</div>
			</div>
			<div class="form__group">
				<div class="form__b form__radio-input-b">
					<div class="form__sub-title">Способ оплаты</div>
					<input class="form__radio-input js-input" type="radio" name="customer_pay_type" value="cash" id="customer_pay_type_cash" checked>
					<label class="form__radio-input-label" for="customer_pay_type_cash">Наличными</label>
					<input class="form__radio-input js-input" type="radio" name="customer_pay_type" value="card" id="customer_pay_type_card">
					<label class="form__radio-input-label" for="customer_pay_type_card">Банковской картой</label>
					<input class="form__radio-input js-input" type="radio" name="customer_pay_type" value="bill" id="customer_pay_type_bill">
					<label class="form__radio-input-label" for="customer_pay_type_bill">По счету</label>
				</div>
			</div>
			<div class="form__group">
				<div class="form__checkbox-input-b js-order-bonuses-b">
					<input type="checkbox" class="form__checkbox-input js-use-bonuses" name="use_bonuses" id="form_use_bonuses">
					<label class="form__checkbox-input-label" for="form_use_bonuses">использовать <span class="js-order-bonuses">30 бонусов</span>
						<span class="tip">
							<span class="tip__block">
								использовать 76 бонусови спользовать 76 бонусов использовать 76 бонусов
							</span>
						</span>
					</label>
				</div>
			</div>
			<div class="form__footer">
				<div class="text__24">Всего к оплате:
					<span class="text__34"><span class="js-order-final-summ">23 280</span> ₽</span>
				</div>
				<p class="text__14">Нажатием кнопки «Оформить заказ» я даю свое согласие на обработку персональных данных в соответствии с указанным
					<a href="#" class="text__link">здесь</a> условиями.</p>
				<button class="btn btn--dark btn--full-w" type="submit">Отправить заказ</button>
			</div>
			<div class="form__success js-form__success">
				<div class="content">
					<p class="h1">Заказ успешно оформлен</p>
					<p class="h2">Мы свяжемся с вами в ближайшее время</p>
				</div>
			</div>
		</form>
	</div>
</div>
