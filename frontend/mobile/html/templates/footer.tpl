<footer class="footer">
    <div class="innertube">
        <!-- Logo start-->
        <div class="footer__logo">
            <img src="./mobile/img/svg/logo.svg">
        </div>
        <!--Logo end-->

        <!-- Contacts start-->
        <div class="footer__contacts">
            <div class="footer__contacts--main">
                <a href="tel:88007008343" class="phone icon-phone">8 800 700-83-43</a>
                <a href="mailto:sale@stommarket.ru" class="email icon-mail">sale@stommarket.ru</a>
            </div>
            <div class="footer__contacts--current icon-marker">
                <p class="city">Санкт-Петербург</p>
                <p class="address">Республиканская, 22</p>
                <a class="current-phone" href="tel:+78124261939">+7 812 426-19-39</a>
            </div>
        </div>
        <!-- Contacts end-->

        <hr class="footer__divider">
        <!--info start-->
        <div class="footer__info">
            <div class="footer__info--social">
                <p>Соц. сети</p>
                <a href="https://vk.com/stommarketru" class="social-link"><i class="icon-vk"></i></a>
                <a href="https://www.facebook.com/stommarket/" class="social-link"><i class="icon-facebook"></i></a>
            </div>
            <div class="footer__info--pay">
                <p>Оплата</p>
                <i class="icon-yandex icon"></i>
                <i class="icon-visa icon"></i>
                <i class="icon-mastercard icon"></i>
            </div>
        </div>
        <!--info start-->
    </div>
</footer>
<div class="veil-b js-body-veil"></div>
<script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
<script src="./mobile/js/main.js"></script>
</body>
</html>