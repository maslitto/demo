<!DOCTYPE html>
<html lang="ru" class="js-html_wrap">
<head>
    <base href="/">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="./mobile/css/main.css">
    <title>Stommarket</title>
    <!-- <link rel="icon" type="image/png" sizes="32x32" href="/mobile/img/favicons/favicon-32x32.png"> -->
    <link rel="icon" type="image/png" sizes="16x16" href="/mobile/img/favicons/favicon-16x16.ico">
    <link rel="apple-touch-icon" sizes="180x180" href="/mobile/img/favicons/apple-touch-icon.png">
    <link rel="manifest" href="./mobile/img/favicons/site.webmanifest">
    <link rel="mask-icon" href="./mobile/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/mobile/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
</head>

<body itemscope itemtype="http://schema.org/WebPage" class="body js-page_wrap">
    <div class="search-window js-search-window" style="display: none">
        <div class="search-window__header">
            <input type="text" class="js-search search-window__search">
            <a href="#" class="js-search-window-close search-window__close"><i class="icon-cancel2"></i></a>
        </div>
        <div class="search-window__body">
            <div class="search-window__catalog js__results-catalog"></div>
            <div class="search-window__products js__results-products"></div>
        </div>
    </div>
    <header class="header js-header">
        <div class="innertube">
            <a href="/" class="header__logo">
                <img src="./mobile/img/logo.svg">
            </a>
            <form class="header__search">
                <input type="search" placeholder="Поиск" class="js-search-init">
            </form>

            <div class="header__icon-menu icon-menu">
                <a href="/cart/" class="icon-menu__item icon-basket">
                    <span class="icon-menu__basket-count active js-basket-menu__count">10</span>
                </a>
                <a href="" onclick="return false;" class="icon-menu__item js-menu_open">
                    <span class="icon-hamburger js-menu_icon">
                        <span class="icon-hamburger__line"></span>
                        <span class="icon-hamburger__line"></span>
                        <span class="icon-hamburger__line"></span>
                    </span>
                </a>
            </div>

        </div>

        <menu class="menu js-side_menu">
            <div class="menu__content-wrap">
                <div class="menu__top-nav">
                    <a href="#menu-main" class="menu__back js-menu_layer_link js-menu-back" style="display:none"><i class="icon-arrow-back"></i></a>
                    <a href="#" class="menu__back js-menu-catalog-back js-next-level-link" data-id="" style="display:none"><i class="icon-arrow-back"></i></a>
                    <span class="menu__title js-menu__title" style="display:none"></span>
                    <a href="#" class="menu__close js-menu_close"><i class="icon-cancel"></i></a>
                </div>
                <!--Main menu-->
                <div class="menu__layer menu__layer--main js-menu_layer active" id="menu-main">
                    <div class="menu__block t-center">
                        <span class="menu__auth-icon"></span>
                        <a href="#menu-login" class="menu__auth-btn menu__auth-btn--small js-menu_layer_link">Войти</a>
                    </div>
                    <div class="menu__delimiter"></div>
                    <div class="menu__block">
                        <a href="#menu-catalog" class="menu__item-link menu__item-link--catalog js-menu_layer_link">Каталог</a>
                    </div>
                    <div class="menu__delimiter"></div>
                    <div class="menu__block">
                        <a href="#menu-city-switch" class="menu__item-link menu__item-link--location js-menu_layer_link">Санкт-Петербург</a>
                        <a href="" class="menu__item-link menu__item-link--phone">+7 812 426-19-39</a>
                    </div>
                    <div class="menu__delimiter"></div>
                    <div class="menu__block">
                        <a href="" class="menu__item-link menu__item-link--payment">Оплата</a>
                        <a href="" class="menu__item-link menu__item-link--delivery">Доставка</a>
                        <a href="" class="menu__item-link menu__item-link--blog">Блог</a>
                        <a href="" class="menu__item-link menu__item-link--about">О компании</a>
                        <a href="" class="menu__item-link menu__item-link--contacts">Контакты</a>
                    </div>
                    <div class="menu__block">
                        <a class="menu__social icon-vk" href="" target="_blank"></a>
                        <a class="menu__social icon-facebook" href="" target="_blank"></a>
                    </div>

                </div>
                <!--Login menu-->
                <div class="menu__layer menu__layer--login js-menu_layer" id="menu-login">
                    <div class="menu__block">
                        <span class="menu__auth-icon"></span>
                        <form action="/resp/ok.php" class="form js-form js-login_form" data-error-msg="Произошла ошибка">
                            <div class="form__b form__text-input-b">
                                <input type="email" class="form__text-input js-input js-required" name="login" id="login_email">
                                <label class="form__text-input-label" for="login">E-mail<span class="form__star-sign">*</span></label>
                            </div>
                            <div class="form__b form__text-input-b">
                                <input type="password" class="form__text-input js-input js-required" name="password" id="login_pass">
                                <label class="form__text-input-label" for="login">Пароль<span class="form__star-sign">*</span></label>
                            </div>
                            <button type="submit" class="menu__auth-btn">Войти</button>
                        </form>
                    </div>
                    <div class="menu__block">
                        <div>
                            <a href="#menu-remember" class="menu__auth-link link link--grey js-menu_layer_link">Забыли пароль?</a>
                        </div>
                        <div>
                            <a href="#menu-registration" class="menu__auth-link link link--grey js-menu_layer_link">Регистрация</a>
                        </div>
                    </div>
                </div>
                <!--Register menu-->
                <div class="menu__layer menu__layer--register js-menu_layer" id="menu-registration">
                    <div class="menu__block">
                        <span class="menu__auth-icon"></span>

                        <form action="/resp/ok.php" class="form js-form js-registration_form" data-error-msg="Произошла ошибка">
                            <div class="form__b form__text-input-b">
                                <input type="email" class="form__text-input js-input js-required" name="email" id="reg_email">
                                <label class="form__text-input-label" for="email">E-mail<span class="form__star-sign">*</span></label>
                            </div>
                            <div class="form__b form__text-input-b">
                                <input type="password" class="form__text-input js-input js-required" name="password" id="reg_pass">
                                <label class="form__text-input-label" for="password">Пароль<span class="form__star-sign">*</span></label>
                            </div>
                            <div class="form__b form__text-input-b">
                                <input type="password" class="form__text-input js-input js-required" name="password_repeat" id="reg_pass_check">
                                <label class="form__text-input-label" for="password_repeat">Повтор пароля<span class="form__star-sign">*</span></label>
                            </div>
                            <button type="submit" class="menu__auth-btn">Отправить</button>
                            <div class="form__success js-form_success">
                                <div class="content">
                                    <p class="h3">Регистрация</p>
                                    <p>Вы успешно зарегистрированы. Подтвердите почту, для активации учетной записи.</p>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="menu__block">
                        <div>
                            <a href="#menu-login" class="menu__auth-link link link--grey js-menu_layer_link">Войти</a>
                        </div>
                        <div>
                            <a href="#menu-remember" class="menu__auth-link link link--grey js-menu_layer_link">Забыли пароль?</a>
                        </div>
                    </div>
                </div>
                <!--Forgot password menu-->
                <div class="menu__layer menu__layer--remember js-menu_layer" id="menu-remember">
                    <div class="menu__block">
                        <span class="menu__auth-icon"></span>
                        <form action="/resp/remember-error.php" class="form js-form js-remember_form" data-error-msg="Произошла ошибка">
                            <div class="form__b form__text-input-b">
                                <input type="email" class="form__text-input js-input js-required " name="login" id="remember_email">
                                <label class="form__text-input-label" for="remember_email">E-mail<span class="form__star-sign">*</span></label>
                            </div>
                            <button type="submit" class="menu__auth-btn">Продолжить</button>
                            <div class="form__success js-form_success">
                                <div class="content">
                                    <p class="h3">Восстановление пароля</p>
                                    <p>На вашу почту было отправлено письмо с ссылкой для восстановления пароля.</p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="menu__block">
                        <div>
                            <a href="#menu-login" class="menu__auth-link link link--grey js-menu_layer_link">Войти</a>
                        </div>
                        <div>
                            <a href="#menu-registration" class="menu__auth-link link link--grey js-menu_layer_link">Регистрация</a>
                        </div>
                    </div>
                </div>
                <!--City menu-->
                <div class="menu__layer menu__layer--city-switch js-menu_layer" data-fw="true" id="menu-city-switch">
                    <div class="menu__block city-switch js_city-switch">

                        <div class="city-switch__item js-switch-city-handler" data-id="1">
                            <input type="radio" id="town1" name="town">
                            <label for="town1">
                                <span>Санкт-Петербург</span>
                                <i>8 812 426-19-39</i>
                            </label>
                        </div>

                        <div class="city-switch__item js-switch-city-handler" data-id="2">
                            <input type="radio" id="town2" name="town">
                            <label for="town2">
                                <span>Москва</span>
                                <i>8 495 120-59-19</i>
                            </label>
                        </div>

                        <div class="city-switch__item js-switch-city-handler" data-id="3">
                            <input type="radio" id="town3" name="town">
                            <label for="town3">
                                <span>Киров</span>
                                <i>8 833 266-04-39</i>
                            </label>
                        </div>

                        <div class="city-switch__item js-switch-city-handler" data-id="4">
                            <input type="radio" id="town4" name="town">
                            <label for="town4">
                                <span>Ярославль</span>
                                <i>8 485 260-90-68</i>
                            </label>
                        </div>

                    </div>
                </div>
                <!--Catalog menu-->
                <div class="menu__layer menu__layer--catalog js-menu_layer" data-fw="true" id="menu-catalog">
                    <div class="catalog-nav__inner">
                        <ul class="catalog-nav__list js-catalog-menu">
                            <li class="catalog-nav__item ">
                                <a href="#" class="icon-arrow-next js-next-level-link" data-id="1">Производители</a>
                            </li>
                            <li class="catalog-nav__item ">
                                <a href="#">Производители</a>
                            </li>
                            <li class="catalog-nav__item ">
                                <a href="#" class="icon-arrow-next js-next-level-link"  data-id="2">Производители</a>
                            </li>
                            <li class="catalog-nav__item ">
                                <a href="#" class="icon-arrow-next js-next-level-link"  data-id="1">Производители</a>
                            </li>
                            <li class="catalog-nav__item ">
                                <a href="#" class="icon-arrow-next js-next-level-link"  data-id="1">Производители</a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </menu>
    </header>