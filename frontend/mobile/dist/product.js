;(function(){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    name: "product.vue",
    el: '#product-vue',
    props: {
        inject: {
            type: Object,
            required: true
        }
    },

    delimiters: ['${', '}'],

    computed: {
        remain: function remain() {
            var remain = this.product.count - this.currentCount;
            if (remain < 0) {
                remain = 0;
            }
            return remain;
        }
    },
    methods: {
        formatPrice: function formatPrice(value) {
            return value;
        },
        setCurrentCount: function setCurrentCount(operation) {
            if (operation == 'plus') {
                this.currentCount += 1;
            } else if (operation == 'minus') {
                this.currentCount -= 1;;
            }

            if (this.currentCount < 1) {
                this.currentCount = 1;
            }
            if (this.currentCount > this.product.count && this.product.count > 0) {
                this.currentCount = this.product.count;
            }
            if (this.product.count == 0) {
                this.currentCount = 1;
            }
            return this.currentCount;
        },
        addToCart: function addToCart(productId, count) {
            this.inCart = true;
            var data = {
                id: productId,
                count: count
            };
            console.log(data);
            var ret = $.cart.add(data);

            return false;
        }
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
if (__vue__options__.functional) {console.error("[vueify] functional components are not supported and should be defined in plain js files using render functions.")}
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"product__summary summary js-product js-product--detail"},[_c('div',{staticClass:"summary__b summary__b--1"},[_c('div',{staticClass:"summary__row clearfix"},[(_vm.product.oldPrice)?_c('div',{staticClass:"summary__total-discount-price js-discount-summ"},[_vm._v("\n                ${product.oldPrice * currentCount}\n            ")]):_vm._e(),_vm._v(" "),(_vm.product.price)?_c('div',{staticClass:"summary__total-price js-summ"},[_vm._v("\n                ${product.price * currentCount}\n            ")]):_vm._e()]),_vm._v(" "),_c('div',{staticClass:"summary__row clearfix"},[(!_vm.inCart)?_c('div',{staticClass:"not-added"},[_c('div',{staticClass:"summary__value"},[_c('label',{staticClass:"form__number-input-b form__number-input-b--small form__b--one"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.currentCount),expression:"currentCount"}],staticClass:"form__number-input js-form__number js-change-sum-number js-ga-count",attrs:{"name":"item-count","type":"number","step":"1","min":"1","max":"product.count ? product.count : 1","data-bonus-cost":"<?= $fullBonus ?>","data-cost":_vm.product.price,"data-id":_vm.product.id},domProps:{"value":(_vm.currentCount)},on:{"change":function($event){_vm.setCurrentCount()},"input":function($event){if($event.target.composing){ return; }_vm.currentCount=$event.target.value}}}),_vm._v(" "),_c('span',{staticClass:"form__number-input-minus js-form__number-minus",on:{"click":function($event){_vm.setCurrentCount('minus')}}},[_vm._v("−")]),_vm._v(" "),(_vm.remain > 0)?_c('span',{staticClass:"form__number-input-plus js-form__number-plus",on:{"click":function($event){_vm.setCurrentCount('plus')}}},[_vm._v("+")]):_c('span',{staticClass:"form__number-input-plus js-form__number-plus maxed",on:{"click":function($event){$event.preventDefault();_vm.setCurrentCount(_vm.NULL)}}},[_vm._v("+")])])]),_vm._v(" "),_c('div',{staticClass:"availability"},[(_vm.product.count>0)?_c('span',{staticClass:"summary__text--green count"},[_vm._v(" В наличии "),_c('span',{staticClass:"remain"},[_vm._v("${product.count}")]),_vm._v(" шт.")]):_c('span',{staticClass:"summary__text--red"},[_vm._v("Под заказ"),_c('span',{staticClass:"remain"})])])]):_c('div',{staticClass:"added"},[_c('svg',{staticClass:"checkmark",attrs:{"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 52 52"}},[_c('circle',{staticClass:"checkmark__circle",attrs:{"cx":"26","cy":"26","r":"25","fill":"none"}}),_vm._v(" "),_c('path',{staticClass:"checkmark__check",attrs:{"fill":"none","d":"M14.1 27.2l7.1 7.2 16.7-16.8"}})]),_vm._v(" "),_c('span',{staticClass:"text"},[_vm._v("${currentCount} шт. в корзине. Осталось ${remain} шт.")])])]),_vm._v(" "),_c('div',{staticClass:"summary__row clearfix"},[(_vm.product.price && !_vm.inCart)?_c('a',{staticClass:"ga-event-add-product btn long btn--yellow summary__add-to-basket js-product-page-btn js-ga-add-to-cart",on:{"click":function($event){$event.preventDefault();_vm.addToCart(_vm.product.id,_vm.currentCount)}}},[_vm._v(" Добавить в корзину ")]):_vm._e(),_vm._v(" "),(_vm.inCart)?_c('a',{staticClass:"btn long btn--yellow summary__add-to-basket js-product-page-btn",attrs:{"href":"/cart/"}},[_vm._v("Перейти в корзину")]):_vm._e()]),_vm._v(" "),(!_vm.inCart)?_c('div',{staticClass:"summary__row ocb"},[_c('form',{staticClass:"ocb__form js-form js-ocb__form",attrs:{"action":"/order/add-ocb-order/","method":"POST"}},[_c('input',{attrs:{"type":"hidden","name":"pid"},domProps:{"value":_vm.product.id}}),_vm._v(" "),_c('input',{staticClass:"js-ocb__count",attrs:{"type":"hidden","name":"count"},domProps:{"value":_vm.currentCount}}),_vm._v(" "),_c('input',{staticClass:"ocb__input js-input js-required js-phone",attrs:{"type":"tel","placeholder":"+7 (921) 123-45-67","name":"phone"}}),_vm._v(" "),_c('button',{staticClass:"ocb__btn js-one-click-buy",attrs:{"type":"submit","onclick":""}},[_vm._v("Купить в 1 клик")]),_vm._v(" "),_vm._m(0)])]):_c('div',{staticClass:"summary__row"},[(_vm.remain > 0)?_c('a',{staticClass:"ga-event-add-product btn btn--lblue summary__add-to-basket  js-ga-add-to-cart",attrs:{"data-count":"1","data-id":"product.id"},on:{"click":function($event){_vm.currentCount = _vm.currentCount+1;_vm.addToCart(_vm.product.id,1);}}},[_c('span',{staticClass:"plus"},[_vm._v("+1 шт.")]),_vm._v("ДОБАВИТЬ ЕЩЁ\n            ")]):_vm._e()])]),_vm._v(" "),_vm._m(1),_vm._v(" "),_vm._m(2),_vm._v(" "),_vm._m(3)])}
__vue__options__.staticRenderFns = [function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"form__success js-form__success ocb__success"},[_c('div',{staticClass:"content"},[_vm._v("Спасибо! Ваш заказ принят")])])},function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"summary__b summary__b--link js-open-popup",attrs:{"href":"#store__availability"}},[_c('p',{staticClass:"summary__text summary__text--icon icon-stock"},[_vm._v("Проверить наличие по складам")])])},function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"summary__b summary__b--link js-open-popup-ajax",attrs:{"href":"/page/delivery/"}},[_c('p',{staticClass:"summary__text summary__text--icon icon-delivery"},[_vm._v("Доставка "),_c('span',{staticClass:"js-delivery-common"})])])},function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"summary__b summary__b--link js-open-popup-ajax",attrs:{"href":"/page/pickup/"}},[_c('p',{staticClass:"summary__text summary__text--icon icon-house"},[_vm._v("Самовывоз — бесплатно")])])}]
__vue__options__._scopeId = "data-v-02d89b23"
if (module.hot) {(function () {  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-02d89b23", __vue__options__)
  } else {
    hotAPI.reload("data-v-02d89b23", __vue__options__)
  }
})()}