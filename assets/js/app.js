// loads the Bootstrap jQuery plugins
/*import 'bootstrap-sass/assets/javascripts/bootstrap/collapse.js';
import 'bootstrap-sass/assets/javascripts/bootstrap/dropdown.js';
import 'bootstrap-sass/assets/javascripts/bootstrap/modal.js';
import 'bootstrap-sass/assets/javascripts/bootstrap/transition.js';

// loads the code syntax highlighting library
import './highlight.js';
*/
import Vue from 'vue';

import Example from './components/Example'

/**
 * Create a fresh Vue Application instance
 */
new Vue({
    el: '#app',
    components: {Example}
});