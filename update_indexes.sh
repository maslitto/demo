#!/bin/bash
start_time=$(date +%s)
curl -XDELETE 'http://localhost:9200/products?pretty'
curl -XDELETE 'http://localhost:9200/tags?pretty'
curl -XDELETE 'http://localhost:9200/brands?pretty'
curl -XDELETE 'http://localhost:9200/catalogs?pretty'

echo indexes deleted!
bin/console fos:elastica:create
bin/console fos:elastica:populate
end_time=$(date +%s)
